<?php

define('WP_MEMORY_LIMIT', '64M');
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sustaine_wp1');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'OgDZti8Uy6y5qpMvSdlpeLYrG4EpmM9CoiVwv4vZJ6fk1cfmEofgvB6fxEoV1wBQ');
define('SECURE_AUTH_KEY',  'yNJEcH5pFtK9ARpBbzerrot876QyRE2tXLoXzHZa2wRr6f3OnxIHDujhZMYfq24b');
define('LOGGED_IN_KEY',    'E15CvI4hbDjpA92ZkS81qgS6yAkFdFWc9McKgUryb254Is6DEjp7pzRAFFRvtYXo');
define('NONCE_KEY',        'FVKVj2EOwKGlSBamHwyopkq7GXgg4vvvHwjnUfHdjdMpKJzayHhvtR8GjMc9YR6u');
define('AUTH_SALT',        '6czA5H8WAcd2jDwAGi6MAEUBQ1vF1ndc4vFEG03meiHn2AuLhz8EMefAI3Pp0yFa');
define('SECURE_AUTH_SALT', '5O1ywSzECCe5EvBzrnf1urLcWpBPvoymviPRKWKPWWzPr5URW1h2MiCJ3C6GoBF3');
define('LOGGED_IN_SALT',   '5l21Qlp2EcKTDRdlALhBswbHEMtw7mkGUg5SiC7BftDC9uftrFLpJ6SpDqyTrImi');
define('NONCE_SALT',       'g9YweGcVLFFMRl9xOPxMIEe701NnetB3agJJ0lAJNqvhwrWoNZf8wjAazWdAqUWX');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );
define( 'WP_DEBUG_DISPLAY', false );
define( 'WP_DEBUG_LOG', true );


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
