<?php

//======================================================================
// Meta boxes for Pages
//======================================================================
 
add_action( 'add_meta_boxes', 'luv_page_meta_box' );

/**
 * Add Sidebar settings meta box to Post editor
 */
function luv_page_meta_box() {

	global $wp_registered_sidebars;
	
	// Add empty field
	$sidebars[0] = NULL;
	
	foreach($wp_registered_sidebars as $sidebar) {
		$sidebars[$sidebar['id']] = $sidebar['name'];
	}
	
	//======================================================================
	// Sidebars
	//======================================================================
	$meta_box = array(
		'id' => 'luv-page-sidebars-meta-box',
		'title' =>  esc_html__('Sidebar Settings', 'fevr'),
		'desc' => esc_html__('Settings related to the sidebar can be found here.', 'fevr'),
		'post_type' => 'page',
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array( 
				'name' => esc_html__('Sidebar', 'fevr'),
				'desc' => esc_html__('Please select which sidebar you want to be displayed. Custom sidebars can be created under the related menu in the "Theme Options".', 'fevr'),
				'id' => 'page-sidebar',
				'type' => 'select',
				'options' => $sidebars,
			),
			array( 
				'name' => esc_html__('Sidebar Position', 'fevr'),
				'desc' => esc_html__('On which side should the sidebar appear on?', 'fevr'),
				'id' => 'page-sidebar-position',
				'type' => 'select',
				'options' => array(
					'left-sidebar' => esc_html__('Left', 'fevr'),
					'right-sidebar' => esc_html__('Right', 'fevr'),
				),
				'default' => 'right-sidebar',
			),
		)
	);
	
	//======================================================================
	// Promo
	//======================================================================
	$meta_box = array(
		'id' => 'luv-page-promo-meta-box',
		'title' =>  esc_html__('Promo Page Settings', 'fevr'),
		'desc' => esc_html__('Settings related to the promo page can be found here.', 'fevr'),
		'post_type' => 'page',
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array( 
				'name' => esc_html__('Background Image', 'fevr'),
				'desc' => esc_html__('The image set here will be displayed as background.', 'fevr'),
				'id' => 'page-promo-bg-img',
				'type' => 'file_img',
			),
			array( 
				'name' => esc_html__('Screen Content', 'fevr'),
				'desc' => esc_html__('The image set here will be displayed as the content of the screen on the background.', 'fevr'),
				'id' => 'page-promo-content-img',
				'type' => 'file_img',
			),
		)
	);
	
	add_meta_box(
		$meta_box['id'],
		$meta_box['title'],
		'luv_create_meta_box_callback',
		$meta_box['post_type'],
		$meta_box['context'],
		$meta_box['priority'],
		$meta_box
	);
}
		
?>