<?php
	//Set is woocommerce as true
	add_filter('is_woocommerce', '__return_true');
	get_header();

	// Classes for .container
	$fevr_container_classes = array();
	// Full width content
	if(fevr_check_luvoption('woocommerce-collections-full-width', 1)) {
		$fevr_container_classes[] = 'container-fluid';
	}
	
	// Classes for .l-grid-row
	$fevr_grid_classes = array();
	// Sidebar
	if(fevr_check_luvoption('woocommerce-collections-sidebar-position', 'no-sidebar', '!=')) {
		$fevr_grid_classes[] = 'has-sidebar';
	}
	
	// Sidebar position
	if(fevr_check_luvoption('woocommerce-collections-sidebar-position', 'left-sidebar')) {
		$fevr_grid_classes[] = fevr_get_luvoption('woocommerce-collections-sidebar-position', 'left-sidebar');
	}
	
	// Classes for .collections-container
	$fevr_collections_container_classes = array();
	// Columns
	if(fevr_check_luvoption('woocommerce-collections-columns', 'one-column','!=')) {
		$fevr_collections_container_classes[] = fevr_get_luvoption('woocommerce-collections-columns').' masonry masonry-meta-overlay';
	}
	
	// No padding
	if(fevr_check_luvoption('woocommerce-product-gutter', 1)) {
		$fevr_collections_container_classes[] = 'no-padding';
	}
?>

<div id="content-wrapper" class="wrapper-padding">
	<?php
		// When custom page selected for reviews we display the custom header
		if(fevr_check_luvoption('woocommerce-collections-page', '', '!=')) {
			fevr_header();
			$collections_page_content = get_post(fevr_get_luvoption('woocommerce-collections-page'))->post_content;
		}
	?>
	
	<div class="container <?php echo esc_attr(implode(' ', $fevr_container_classes)); ?>">
		<div class="l-grid-row <?php echo esc_attr(implode(' ', $fevr_grid_classes)); ?>">
			<main id="main-content">
				<?php
					if((fevr_check_luvoption('woocommerce-collections-page','','!=') && empty($collections_page_content)) || fevr_check_luvoption('woocommerce-collections-page', '')):
				?>
					<div class="collections-container item-grid-container <?php echo esc_attr(implode(' ', $fevr_collections_container_classes)); ?>">
					<?php
						if ( have_posts() ) :
							while ( have_posts() ) : the_post();
							$fevr_meta_fields = get_post_meta( get_the_ID(), 'fevr_meta', true);
							
							// Article classes
							$fevr_article_classes = array();
							
							// Hover effect style
							if(fevr_check_luvoption('woocommerce-collections-masonry-hover-style', '', '!=')) {
								$fevr_article_classes[] = fevr_get_luvoption('woocommerce-collections-masonry-hover-style');
							}
							
							// Animation
								if(fevr_check_luvoption('woocommerce-collections-animation', '', '!=')) {
								$fevr_article_classes[] = 'c-has-animation ' . fevr_get_luvoption('woocommerce-collections-animation');
							}
					?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(esc_attr(implode(' ', $fevr_article_classes))); ?>>
							<?php get_template_part( 'luvthemes/post-templates/collections' ); ?>
						</article>
					<?php
							endwhile;
						
						else:
							esc_html_e('No posts were found', 'fevr');
						endif;
					?>
					</div>
					<?php get_template_part( 'luvthemes/luv-templates/pagination' ); ?>
				<?php else: ?>
				<?php
					// Add VC custom css
					$vc_css = get_post_meta(fevr_get_luvoption('woocommerce-collections-page', 0), '_wpb_post_custom_css', true);
					$vc_css .= get_post_meta(fevr_get_luvoption('woocommerce-collections-page', 0), '_wpb_shortcodes_custom_css', true);
					fevr_late_add_header_style($vc_css);
					// We should use the luv shortcode here
					echo apply_filters('the_content', $collections_page_content);
				?>
				<?php endif; ?>
			</main>
			
			<?php 
				if(fevr_check_luvoption('woocommerce-collections-sidebar-position', 'no-sidebar','!=')) {
					get_sidebar();
				}
			?>
		</div>
	</div>
</div>


<?php get_footer(); ?>