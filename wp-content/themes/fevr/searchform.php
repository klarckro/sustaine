<form action="<?php echo esc_attr(trailingslashit(site_url())); ?>" method="get">
	<input type="text" name="s" class="search" value="<?php the_search_query(); ?>" placeholder="Start typing..">
	<input type="submit" class="search-submit btn btn-full btn-global" value="<?php esc_html_e( 'SEARCH', 'fevr' ) ?>">
	<button type="submit" class="search-submit search-submit-icon"><i class="fa fa-search"></i></button>
</form>