<?php
	get_header();
	
	// Classes for .container
	$fevr_container_classes = array();
	// Full width content
	if(fevr_check_luvoption('bbpress-full-width', 1)) {
		$fevr_container_classes[] = 'container-fluid';
	}
	
	// Classes for .l-grid-row
	$fevr_grid_classes = array();
	// Sidebar
	if(fevr_check_luvoption('bbpress-sidebar-position', 'no-sidebar', '!=') && (!bbp_is_single_topic() || (bbp_is_single_topic() && fevr_check_luvoption('bbpress-sidebar-single', 1, '!=')))) {
		$fevr_grid_classes[] = 'has-sidebar';
	}
	
	// Sidebar position
	if(fevr_check_luvoption('bbpress-sidebar-position', 'left-sidebar') && (!bbp_is_single_topic() || (bbp_is_single_topic() && fevr_check_luvoption('bbpress-sidebar-single', 1, '!=')))) {
		$fevr_grid_classes[] = fevr_get_luvoption('bbpress-sidebar-position');
	}
?>

<div id="content-wrapper" class="wrapper-padding">
	<?php
		// When custom page selected for portfolio we display the custom header
		if(fevr_check_luvoption('bbpress-page', '', '!=')):
			fevr_header();
		else:
	?>
			<div class="container">
				<div id="page-header-default">
					<h1 class="page-header-title">
					<?php the_title(); ?>
					</h1>
					<?php get_template_part('luvthemes/luv-templates/header-pagination'); ?>
				</div>
			</div>
	<?php endif; ?>
	
	<div class="container <?php echo esc_attr(implode(' ', $fevr_container_classes)); ?>">
		<div class="l-grid-row <?php echo esc_attr(implode(' ', $fevr_grid_classes)); ?>">
			<main id="main-content">
				<?php
					if ( have_posts() ) :
						while ( have_posts() ) : the_post();
				?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<?php the_content(); ?>
						</article>
				
				<?php
						endwhile;
					
					else:
						esc_html_e('No posts were found', 'fevr');
					endif;
				?>
			</main>
			
			<?php 
				if(fevr_check_luvoption('bbpress-sidebar-position', 'no-sidebar', '!=') && (!bbp_is_single_topic() || (bbp_is_single_topic() && fevr_check_luvoption('bbpress-sidebar-single', 1, '!=')))) {
					get_sidebar();
				}
			?>
		</div>
	</div>
</div>


<?php get_footer(); ?>