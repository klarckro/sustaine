<?php
	get_header();

	// Classes for .l-grid-row
	$fevr_grid_classes = array();
	
	// Sidebar
	if(fevr_check_luvoption('blog-sidebar-position','','!=') && fevr_check_luvoption('blog-sidebar-position', 'no-sidebar', '!=') && fevr_check_luvoption('blog-sidebar-single',1,'!=')) {
		$fevr_grid_classes[] = 'has-sidebar';
	}
	
	// Sidebar position
	if(fevr_check_luvoption('blog-sidebar-position', 'left-sidebar') && fevr_check_luvoption('blog-sidebar-single',1,'!=')) {
		$fevr_grid_classes[] = fevr_get_luvoption('blog-sidebar-single');
	}
?>
	<?php if ( have_posts() ) :
			while ( have_posts() ) : the_post();
	?>

				<div id="content-wrapper">
					<?php
						// If we have custom title/subtitle/content we display the custom header
						fevr_header();
						
						if(!empty($fevr_header_title) || !empty($fevr_header_subtitle) || !empty($fevr_header_content) || !empty($fevr_header_background) || !empty($fevr_header_background_color) || !empty($fevr_header_video_mp4) || !empty($fevr_header_video_ogv) || !empty($fevr_header_videoesc_html_embedded)) {
							$fevr_grid_classes[] = 'has-custom-page-header';
						} else {
							$fevr_grid_classes[] = 'has-default-page-header';
						}
						
						$fevr_show_thumbnail = (is_single() && fevr_check_luvoption('blog-featured-image', 1, '!=') && isset($fevr_meta_fields['blog-hide-featured-image']) && $fevr_meta_fields['blog-hide-featured-image'] != 'enabled');
					?>
					<div class="container">
						<div class="l-grid-row <?php echo esc_attr(implode(' ', $fevr_grid_classes)); ?>">
							<main id="main-content">
								<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
									<?php
										get_template_part( 'luvthemes/post-templates/post', get_post_format() );
										
										wp_link_pages();
										
										if (is_single() && has_tag() && fevr_check_luvoption('blog-tags-meta', 1)):
									?>
										<div class="post-tags">
											<?php the_tags('', '', ''); ?>
										</div>					
									<?php endif; ?>
								</article>
					<?php
							endwhile;
						
						else:
							esc_html_e('No posts were found', 'fevr');
						endif;
					?>
								
								<?php
									$fevr_like = fevr_check_luvoption('blog-likes-meta', 'hide-on-single', '!=') && fevr_check_luvoption('blog-likes-meta', 'hide-on-both', '!=');
									$fevr_share = fevr_check_luvoption('blog-social-meta',1,'!=');
								?>
								
								<?php if($fevr_like || $fevr_share): ?>
								<div id="single-share-box" class="full-width-section">
									<div class="container">
										<?php get_template_part('luvthemes/luv-templates/social'); ?>
									</div>
								</div>
								<?php endif; ?>
								
								<?php get_template_part('luvthemes/luv-templates/footer-pagination'); ?>
								
								<?php
									// Hide the bio section if the user selected it in the options panel
									if(fevr_check_luvoption('blog-author-bio', 1, '!=')):
								?>
								<div id="author-bio" class="full-width-section">
									<div class="container">
										<div id="author-bio-inner">
											<?php
												if (function_exists('get_avatar')):
													echo get_avatar( get_the_author_meta('email'), 100 );
											?>
													<h3 class="author-name"><?php the_author(); ?></h3>
													<p><?php the_author_meta('description'); ?></p>
													<a class="btn btn-global" href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>"><?php esc_html_e('More posts by', 'fevr'); ?> <?php echo get_the_author(); ?></a>
											<?php
												endif;
											?>
										</div>
									</div>
								</div>
								<?php endif; ?>
								
								<?php
									if ((comments_open() || get_comments_number()) && fevr_check_luvoption('blog-comments-feature', 1, '!=')) {
										comments_template();
									}
								?>
							</main>
				
							<?php 
								if(fevr_check_luvoption('blog-sidebar-position', 'no-sidebar', '!=') && fevr_check_luvoption('blog-sidebar-single', 1, '!=')) {
									get_sidebar();
								}
							?>
						</div>
					</div>
				</div>
<?php get_footer(); ?>