<?php
	get_header();
	
	// Classes for .container
	$fevr_container_classes = array();
	// Full width content
	if(fevr_check_luvoption('portfolio-full-width', 1)) {
		$fevr_container_classes[] = 'container-fluid';
	}
	
	// Classes for .l-grid-row
	$fevr_grid_classes = array();
	// Sidebar
	if(fevr_check_luvoption('portfolio-sidebar-position', 'no-sidebar','!=')) {
		$fevr_grid_classes[] = 'has-sidebar';
	}
	
	// Sidebar position
	if(fevr_check_luvoption('portfolio-sidebar-position', 'left-sidebar')) {
		$fevr_grid_classes[] = fevr_get_luvoption('portfolio-sidebar-position', 'left-sidebar');
	}
	
	// Classes for .portfolio-container and #content-wrapper
	$fevr_portfolio_container_classes = array();
	$fevr_content_wrapper_classes = array();
	// Columns
	if(fevr_check_luvoption('portfolio-columns', 'one-column', '!=')) {
		$fevr_portfolio_container_classes[] = fevr_get_luvoption('portfolio-columns').' masonry';
	}
	
	// Masonry layout
	if(fevr_check_luvoption('portfolio-masonry-layout', 'standard', '!=')) {
		$fevr_portfolio_container_classes[] = 'masonry-'.fevr_get_luvoption('portfolio-masonry-layout');
	}
	
	// Masonry rounded corners
	if(fevr_check_luvoption('portfolio-masonry-layout', 'meta-overlay') && fevr_check_luvoption('portfolio-masonry-rounded-corners',1)) {
		$fevr_portfolio_container_classes[] = 'masonry-rounded-corners';
	}
	
	// Masonry shadows
	if(fevr_check_luvoption('portfolio-masonry-layout', 'meta-overlay') && fevr_check_luvoption('portfolio-masonry-shadows',1)) {	
		$fevr_portfolio_container_classes[] = 'masonry-shadows';
	}
	
	// Gutter
	if(fevr_check_luvoption('portfolio-masonry-gutter', '', '!=') && fevr_check_luvoption('portfolio-columns','one-column', '!=') && fevr_check_luvoption('portfolio-masonry-layout','standard', '!=')) {
		$fevr_portfolio_container_classes[] = 'masonry-no-gap';
	}
	
	// Wrapper Class
	if(!(fevr_check_luvoption('portfolio-masonry-gutter', 1) && fevr_check_luvoption('portfolio-masonry-layout', 'meta-overlay'))) {
		$fevr_content_wrapper_classes[] = 'wrapper-padding';
	}
	
	// Data for .portfolio-container
	$fevr_portfolio_container_data = array();
	// Data crop images (data-crop-images="")
	if(fevr_check_luvoption('portfolio-masonry-crop-images', 1)) {
		$fevr_portfolio_container_data[] = 'data-crop-images="true"';
	} else {
		$fevr_portfolio_container_data[] = 'data-crop-images="false"';
	}
	
	// Background Check (data-bg-check="")
	if(fevr_check_luvoption('portfolio-masonry-auto-text-color', 1) && fevr_check_luvoption('portfolio-masonry-layout', 'meta-overlay') && fevr_check_luvoption('portfolio-masonry-hover-style', 'masonry-style-title-bottom','!=')) {
		$fevr_portfolio_container_data[] = 'data-bg-check="true"';
	}
	
	// Automatic Metro Layout
	if (fevr_check_luvoption('portfolio-automatic-metro-layout', 1)){
		global $wp_query;
		$fevr_masonry_size_overrides = array();
	
		switch (fevr_get_luvoption('portfolio-columns')){
			case 'auto-columns':
				$columns = 5;
				break;
			case 'four-columns':
				$columns = 4;
				break;
			case 'three-columns':
				$columns = 3;
				break;
			case 'two-columns':
				$columns = 2;
				break;
			case 'one-column':
				$columns = 1;
				break;
		}
		$remand = (int)$wp_query->post_count % $columns;
		$count	= (int)$wp_query->post_count;
	
		if ($columns == 5 && $wp_query->post_count >= $columns){
			if ($remand == 0){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_tall';
				$fevr_masonry_size_overrides[$wp_query->posts[1]->ID] = 'fevr_wide';
				$fevr_masonry_size_overrides[$wp_query->posts[2]->ID] = 'fevr_wide_tall';
			}
			else if ($remand == 1){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_wide_tall';
				$fevr_masonry_size_overrides[$wp_query->posts[5]->ID] = 'fevr_wide';
			}
			else if ($remand == 2){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_wide_tall';
				if ($count >= 8){
					$fevr_masonry_size_overrides[$wp_query->posts[3]->ID] = 'fevr_wide';
					$fevr_masonry_size_overrides[$wp_query->posts[5]->ID] = 'fevr_wide';
					$fevr_masonry_size_overrides[$wp_query->posts[4]->ID] = 'fevr_tall';
					$fevr_masonry_size_overrides[$wp_query->posts[$count-2]->ID] = 'fevr_wide';
					$fevr_masonry_size_overrides[$wp_query->posts[$count-5]->ID] = 'fevr_tall';
				}
			}
			else if ($remand == 3){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_tall';
				$fevr_masonry_size_overrides[$wp_query->posts[1]->ID] = 'fevr_wide';
				if ($count >= 8){
					$fevr_masonry_size_overrides[$wp_query->posts[$count-4]->ID] = 'fevr_tall';
					$fevr_masonry_size_overrides[$wp_query->posts[$count-5]->ID] = 'fevr_tall';
					$fevr_masonry_size_overrides[$wp_query->posts[$count-8]->ID] = 'fevr_wide_tall';
				}
			}
			else if ($remand == 4){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_wide_tall';
				$fevr_masonry_size_overrides[$wp_query->posts[5]->ID] = 'fevr_wide_tall';
			}
		}
	
		if ($columns == 4 && $wp_query->post_count >= $columns){
			if ($remand == 0){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_wide_tall';
				$fevr_masonry_size_overrides[$wp_query->posts[1]->ID] = 'fevr_wide';
				if ($count >= 8){
					$fevr_masonry_size_overrides[$wp_query->posts[$count-1]->ID] = 'fevr_wide';
					$fevr_masonry_size_overrides[$wp_query->posts[$count-3]->ID] = 'fevr_wide';
					$fevr_masonry_size_overrides[$wp_query->posts[$count-4]->ID] = 'fevr_tall';
					$fevr_masonry_size_overrides[$wp_query->posts[$count-5]->ID] = 'fevr_tall';
				}
			}
			else if ($remand == 1){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_wide_tall';
			}
			else if ($remand == 2){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_tall';
				$fevr_masonry_size_overrides[$wp_query->posts[1]->ID] = 'fevr_wide';
			}
			else if ($remand == 3){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_wide';
			}
		}
	
		if ($columns == 3 && $wp_query->post_count >= $columns){
			if ($remand == 0){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_wide_tall';
			}
			else if ($remand == 1){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_tall';
				$fevr_masonry_size_overrides[$wp_query->posts[1]->ID] = 'fevr_wide';
			}
			else if ($remand == 2){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_wide';
			}
		}
	
		if ($columns == 2 && $wp_query->post_count >= $columns){
			if ($remand == 0){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_wide';
				$fevr_masonry_size_overrides[$wp_query->posts[1]->ID] = 'fevr_tall';
			}
			else if ($remand == 1){
				$fevr_masonry_size_overrides[$wp_query->posts[1]->ID] = 'fevr_wide';
			}
		}
	}
?>
<div id="content-wrapper" class="<?php echo esc_attr(implode(' ', $fevr_content_wrapper_classes));?>">
	<?php
		// When custom page selected for portfolio we display the custom header
		if(fevr_check_luvoption('portfolio-page','','!=')) {
			fevr_header();
			$project = get_post(fevr_get_luvoption('portfolio-page'));
			$portfolio_page_content = $project->post_content;
		}
	?>
	
	<div class="container <?php echo esc_attr(implode(' ', $fevr_container_classes)); ?>">
		<div class="l-grid-row <?php echo esc_attr(implode(' ', $fevr_grid_classes)); ?>">
			<main id="main-content">
				<?php
					if((fevr_check_luvoption('portfolio-page','','!=') && empty($portfolio_page_content)) || fevr_check_luvoption('portfolio-page','')):
				?>
				
					<?php 
						// Enable Masonry Filter
						if(fevr_check_luvoption('portfolio-masonry-filter', 1)) {
							get_template_part( 'luvthemes/luv-templates/masonry-filter' );
						}
					?>
					
					<div class="portfolio-container item-grid-container <?php echo esc_attr(implode(' ', $fevr_portfolio_container_classes)); ?>" <?php echo implode(' ', $fevr_portfolio_container_data); ?>>
					
					<?php
						if ( have_posts() ) :
							while ( have_posts() ) : the_post();
							$fevr_meta_fields = get_post_meta( get_the_ID(), 'fevr_meta', true);
							
							// Automatic Metro Layout
							if (fevr_check_luvoption('portfolio-automatic-metro-layout',1)){
								if (isset($fevr_masonry_size_overrides[get_the_ID()])){
									$fevr_meta_fields['portfolio-masonry-size'] = $fevr_masonry_size_overrides[get_the_ID()];
								}
								else{
									$fevr_meta_fields['portfolio-masonry-size'] = 'fevr_normal';
								}
							}
							
							// Article classes
							$fevr_article_classes = array();
							
							// Set masonry size
							if (isset($fevr_meta_fields['portfolio-masonry-size']) && !empty($fevr_meta_fields['portfolio-masonry-size']) &&
								fevr_check_luvoption('portfolio-masonry-layout', 'meta-overlay'))
							{
								$fevr_article_classes[] = 'masonry-size-'.$fevr_meta_fields['portfolio-masonry-size'];
							}
							else {
								$fevr_article_classes[] = 'masonry-size-fevr_normal';
							}
							
							// When masonry is active without custom content we display the style for the hover effect. If we have custom content and the style is still masonry we add a helper class to disable the effects
							if (fevr_check_luvoption('portfolio-masonry-hover-style', '', '!=') &&
								fevr_check_luvoption('portfolio-masonry-layout', 'meta-overlay') &&
								(!isset($fevr_meta_fields['portfolio-masonry-show-content']) || $fevr_meta_fields['portfolio-masonry-show-content'] != 'enabled'))
							{
								$fevr_article_classes[] = fevr_get_luvoption('portfolio-masonry-hover-style');
							}
							else if(fevr_check_luvoption('portfolio-masonry-layout', 'meta-overlay') &&
								isset($fevr_meta_fields['portfolio-masonry-show-content']) && $fevr_meta_fields['portfolio-masonry-show-content'] == 'enabled')
							{
								$fevr_article_classes[] = 'masonry-custom-content';
							}
							
							// Animation
							if(fevr_check_luvoption('portfolio-animation', '', '!=')){
								$fevr_article_classes[] = 'c-has-animation ' . fevr_get_luvoption('portfolio-animation');
							}

					?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(esc_attr(implode(' ', $fevr_article_classes))); ?>>
							<?php get_template_part( 'luvthemes/post-templates/portfolio' ); ?>
						</article>
					<?php
							endwhile;
						
						else:
							esc_html_e('No posts were found', 'fevr');
						endif;
					?>
					</div>
					
					<?php get_template_part( 'luvthemes/luv-templates/pagination' ); ?>
				<?php else: ?>
				<?php
					// Add VC custom css
					$vc_css = get_post_meta(fevr_get_luvoption('portfolio-page', 0), '_wpb_post_custom_css', true);
					$vc_css .= get_post_meta(fevr_get_luvoption('portfolio-page', 0), '_wpb_shortcodes_custom_css', true);
					fevr_late_add_header_style($vc_css);
					// We should use the luv shortcode here
					echo apply_filters('the_content', $portfolio_page_content);
				?>
				<?php endif; ?>
			</main>
			
			<?php 
				if(fevr_check_luvoption('portfolio-sidebar-position', 'no-sidebar', '!=')) {
					get_sidebar();
				}
			?>
		</div>
	</div>
</div>


<?php get_footer(); ?>