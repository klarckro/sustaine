<?php
/**
 * The template for displaying comments
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>
<?php if (fevr_check_luvoption('facebook-comments', 1, '!=')):?>
<div id="comments" class="comments-area">

	<?php if ( have_comments() ) : ?>
		<h2 class="comments-title">
			<?php
				printf( _nx( '1 Comments', '%1$s Comments', get_comments_number(), 'comments title', 'fevr' ),
					number_format_i18n( get_comments_number() ), get_the_title() );
			?>
		</h2>

		<?php the_comments_navigation(); ?>
		
		<ul class="comment-list">
			<?php
				wp_list_comments( array(
					'style'       => 'ul',
					'short_ping'  => true,
					'avatar_size' => 64,
					'max_depth'	  => 10,
				) );
			?>
		</ul>
		
		<?php the_comments_navigation(); ?>

	<?php endif; ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'fevr' ); ?></p>
	<?php endif; ?>

	<?php
		$commenter = wp_get_current_commenter();
		$req = get_option( 'require_name_email' );
		$aria_req = ( $req ? " aria-required='true'" : '' );
		$required_text = 'wtfff';

		$fields =  array(

		  'author' =>
		    '<div class="l-grid-row"><div class="l-grid-4"><input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .'" size="30"' . $aria_req . ' placeholder="'.esc_html__('Name', 'fevr').''.( $req ? '*' : '' ).'"></div>',
		
		  'email' =>
		    '<div class="l-grid-4"><input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .'" size="30"' . $aria_req . ' placeholder="'.esc_html__('Email', 'fevr').''.( $req ? '*' : '' ).'"></div>',
		
		  'url' =>
		    '<div class="l-grid-4"><input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .'" size="30" placeholder="'.esc_html__('Website', 'fevr').'"></div></div>',
		);
		
		$args = array(
		  'id_form'           => 'commentform',
		  'id_submit'         => 'submit',
		  'class_submit'      => 'submit btn btn-global btn-full btn-l',
		  'name_submit'       => 'submit',
		  'title_reply'       => esc_html__( 'Leave a Reply', 'fevr' ),
		  'title_reply_to'    => esc_html__( 'Leave a Reply', 'fevr' ),
		  'cancel_reply_link' => esc_html__( 'Cancel Reply', 'fevr' ),
		  'label_submit'      => esc_html__( 'SUBMIT', 'fevr' ),
		  'format'            => 'html5',
		
		  'comment_field' =>  '<div class="l-grid-row"><div class="l-grid-12"><textarea id="comment" name="comment" rows="12" aria-required="true">' .
		    '</textarea></div></div>',
		
		  'must_log_in' => '<p class="must-log-in">' .
		    sprintf(
		      fevr_kses( 'You must be <a href="%s">logged in</a> to post a comment.', 'fevr' ),
		      esc_url(wp_login_url( apply_filters( 'the_permalink', get_permalink() ) ))
		    ) . '</p>',
		
		  'logged_in_as' => '<p class="logged-in-as">' .
		    sprintf(
		    fevr_kses( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', 'fevr' ),
		      admin_url( 'profile.php' ),
		      $user_identity,
		      esc_url(wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ))
		    ) . '</p>',
		
		  'comment_notes_before' => '',
		
		  'comment_notes_after' => '',
		
		  'fields' => apply_filters( 'comment_form_default_fields', $fields ),
		);
	?>
	
	<?php comment_form($args); ?>
</div>
<?php else:?>
<?php 
	do_shortcode('[luv_enqueue_facebook_sdk]');
?>
	<div class="fb-comments" data-href="<?php the_permalink();?>" data-width="100%" data-order-by="<?php echo esc_attr(fevr_get_luvoption('facebook-comments-order'))?>" data-numposts="<?php echo esc_attr(fevr_get_luvoption('facebook-comments-numposts'))?>"></div>
<?php endif;?>