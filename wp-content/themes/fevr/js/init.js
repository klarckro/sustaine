/* jshint devel:true */
/* global response, fevr, json, BackgroundCheck */
/* jshint -W020 */
/* jshint unused:false */
/* jshint latedef:false */
// Set initial window size
var fevr_initial_width	= jQuery(window).width();
var fevr_initial_height	= jQuery(window).height();

/*----------------------------------------------------------------------------------*/
/* Create fevrFront object
/*----------------------------------------------------------------------------------*/

var fevrFront = function(){
	"use strict";

	// Localization
	this.translate = function(text){
		return fevr.messages[text] || text;
	};

	this.lightbox = fevr.lightbox;
	
	// Display ajax messages
	this.ajax_message = function(container, message, type){
		type = type || 'success';
		jQuery(container).empty().text(message);
		jQuery(container).removeClass('is-hidden');
		jQuery(container).removeClass(function (index, css) {
		    return (css.match (/(^|\s)luv-message-\S+/g) || []).join(' ');
		});
		jQuery(container).addClass('luv-message-' + type);
	};
	
	this.reset_ajax_message_container = function(container){
		jQuery(container).empty();
		jQuery(container).addClass('is-hidden');
		jQuery(container).removeClass(function (index, css) {
		    return (css.match (/(^|\s)luv-message-\S+/g) || []).join(' ');
		});
	};
};

var fevr_front = new fevrFront();

/** 
 * Background Check
 */
function fevr_bg_check() {
	if(jQuery('.posts-container[data-bg-check="true"], .portfolio-container[data-bg-check="true"]').length > 0) {
		BackgroundCheck.init({
		  changeParent: true,
		  targets: '.post-inner:not(.background--dark):not(.background--light):not(.background--complex) .post-content',
		  images: '.post-featured-img .wp-post-image'
		});
	}
}

/** 
 * Initialize full-width section
 */
function fevr_full_width_section(){
	if(jQuery('body[data-layout="boxed"]').length === 0 && jQuery('body[data-header-position="left"]').length === 0 && jQuery('aside#sidebar').length === 0) {
		jQuery('.full-width-section').each(function(){
			var container_width = jQuery(this).closest('.container').width();
			var window_width = jQuery(window).width();
			
			jQuery(this).css({
				'margin-left' : -(window_width-container_width)/2,
				'margin-right' : -(window_width-container_width)/2,
				'visibility' : 'visible',
			});
		});
	} else {
		jQuery('.full-width-section').css({
			'visibility' : 'visible',
		});
	}
	
}

/**
 * Init slider, masonry after VC fullwidth resize
 */
function fevr_vc_fullwidth_late_init(){
	fevr_masonry_init();
	fevr_slider_init();
	fevr_gallery_init();
	fevr_carousel_init();
	fevr_init_slide_boxes();
	jQuery('[data-vc-full-width="true"]').addClass('vc-fullwidth-initialized');
}

/**
 * Init full height columns
 */ 
function fevr_full_height_columns_init(){
	// Reset padding
	jQuery('.full_height_column').each(function(){
		jQuery(this).children('.vc_column-inner').height(jQuery(window).height());
	});
}


/*----------------------------------------------------------------------------------*/
/* Function for Small Header
/*----------------------------------------------------------------------------------*/

function fevr_small_header(){
	var offset = jQuery(window).scrollTop();
	var windowWidth = jQuery(window).width();
	var windowHeight = jQuery(window).height();
	var scrollHeight = jQuery('#main-header').outerHeight();
	
	if(jQuery('#top-bar').length > 0) {
		scrollHeight += jQuery('#top-bar').height();
	}

	if(jQuery('body').attr('data-header-position') !== 'left' && jQuery('body').attr('data-sticky-header-type') === 'always') {
		if(offset >= scrollHeight && windowWidth > 768) {
				jQuery('body').addClass('small-header');
				if(jQuery('body').attr('data-transparent-header') === 'true') { 
					jQuery('header#main-header').removeClass('is-transparent');
				}
		} else {
			jQuery('body').removeClass('small-header');
			if(jQuery('body').attr('data-transparent-header') === 'true') {
				jQuery('header#main-header').addClass('is-transparent');
			}
		}
	} else if(jQuery('body').attr('data-header-position') !== 'left' && jQuery('body').attr('data-sticky-header-type') === 'on-scroll') {
		
		jQuery("header#main-header").headroom({
			classes : {
		        // when element is initialised
		        initial : "headroom",
		        // when scrolling up
		        pinned : "headroom-pinned",
		        // when scrolling down
		        unpinned : "headroom-unpinned",
		        // when above offset
		        top : "headroom-top",
		        // when below offset
		        notTop : "headroom-not-top"
		    },
		    onPin : function() {
		        jQuery('body').addClass('small-header');
		        if(jQuery('body').attr('data-transparent-header') === 'true') { 
					jQuery('header#main-header').removeClass('is-transparent');
				}
	        },
	        onTop : function() {
		        jQuery('body').removeClass('small-header');
		        if(jQuery('body').attr('data-transparent-header') === 'true') {
					jQuery('header#main-header').addClass('is-transparent');
				}
	        },
	        offset : windowHeight/2,
			"tolerance": 5,
		});
	}
}

/*----------------------------------------------------------------------------------*/
/* Header
/*----------------------------------------------------------------------------------*/

function fevr_header_init() {
	if(jQuery('#page-header-custom').length > 0) {
		var page_header_height = jQuery('#page-header-custom').outerHeight();
		var page_header_content_height = jQuery('#page-header-content').outerHeight();
		
		if (jQuery('#main-header.no-border').length == 0 && jQuery('body.small-header').length == 0){
			var page_header_inner_top = ((jQuery('#page-header-custom').outerHeight() - jQuery('#main-header').outerHeight()) - jQuery('#page-header-inner').outerHeight())/2 + jQuery('#main-header').outerHeight();
			jQuery('#page-header-inner').css('top', page_header_inner_top + 'px');
		}
		
		fevr_full_height_header();
		fevr_parallax_header();
		fevr_zoom_out_header();
	}
}

function fevr_parallax_header() {
	
	if(jQuery('[data-parallax-header="standard-parallax"]').length > 0) {
		
		//var header_height = jQuery('header#main-header').outerHeight();
		var page_header_height = jQuery('#page-header-custom').outerHeight();
		var page_header_content_height = jQuery('#page-header-inner').outerHeight();
		var scroll_top = jQuery(window).scrollTop();
		
		if(jQuery('#page-header-wrapper').offset().top-scroll_top <= 0) {
			
			var multiplier = 1-(1/(page_header_height+(page_header_content_height/2)))*scroll_top;
			
			var opacity = multiplier >= 0 ? multiplier : 0;
			
			jQuery('#page-header-inner').css({ 
				'opacity' : opacity,
			});
		
			jQuery('#page-header-custom').stop(true, true).transition({
				y: (scroll_top-jQuery('#page-header-wrapper').offset().top)*0.5
			},0);
			
		} else {
			jQuery('#page-header-custom').stop(true, true).transition({
				y: 0
			},0);
			
			jQuery('#page-header-inner').css({ 
				'opacity' : 1
			});
		}
	}
}

function fevr_zoom_out_header() {
	
	if(jQuery('[data-parallax-header="zoom-out-parallax"]').length > 0) {
		
		var page_header_height = jQuery('#page-header-custom').outerHeight();
		var scroll_top = jQuery(window).scrollTop();
		var zoom_multiplier = 0.4;
		// scrolltop/ratio calculation
		var calculation = (scroll_top-jQuery('#page-header-wrapper').offset().top)/page_header_height;
		var multiplier = calculation < 0 ? 0 : calculation;
		multiplier = 1-multiplier;
		if(multiplier <= 1) {
			var bg_position = (scroll_top-jQuery('#page-header-wrapper').offset().top) < 0 ? 0 : (scroll_top-jQuery('#page-header-wrapper').offset().top);
			jQuery('#page-header-custom').css({
				'background-size' : 100*(1+(multiplier*zoom_multiplier))+'% auto',
				'background-position' : 'center '+ bg_position+'px'
			});
/*
			jQuery('#page-header-inner').stop(true, true).transition({
				scale: 1*multiplier
			});
*/
		}
	}
	
}

function fevr_full_height_header() {
	// If the header is full height we should set it to the window's height
	if(jQuery('[data-full-height-header="true"]').length > 0) {
		jQuery('[data-full-height-header="true"]').each(function(){
			// The size is reduced with the height of the elements above
			jQuery(this).height(jQuery(window).height()-jQuery('#page-header-wrapper').offset().top);
		});
	}
}

/*----------------------------------------------------------------------------------*/
/* Slider
/*----------------------------------------------------------------------------------*/

function fevr_slider_init(parent) {
	jQuery('.luv-slider').each(function(){
		console.log(jQuery(this).parent().outerHeight());
		if (jQuery(this).parent().outerHeight() == 0){
			return;
		}
		var infinite = jQuery(this).attr('data-luv-slider-infinite') === 'true' ? true : false;
		var nav = jQuery(this).attr('data-luv-slider-nav') === 'true' ? true : false;
		var dots = jQuery(this).attr('data-luv-slider-dots') === 'true' ? true : false;
		var lazy_load = jQuery(this).attr('data-luv-slider-lazy-load') === 'true' ? true : false;
		var autoplay = jQuery(this).attr('data-luv-slider-autoplay') === 'true' ? true : false;
		var autoplay_timeout = jQuery(this).attr('data-luv-slider-autoplay-timeout');
		var autoplay_pause = jQuery(this).attr('data-luv-slider-autoplay-pause') === 'true' ? true : false;
		var slider = jQuery(this);
		var transition_type = jQuery(this).attr('data-luv-slider-transition-type') || null;
		
		slider.owlCarousel({
            items: 1,
            loop: infinite,
            nav: nav,
            dots: dots,
            lazyLoad: lazy_load,
            autoplay: autoplay,
            autoplayTimeout: autoplay_timeout,
            autoplayHoverPause: autoplay_pause,
            animateOut: transition_type,
            navText: [],
            autoplaySpeed: 700,
            smartSpeed: 700,
            baseClass: 'luv-slider',
            themeClass: 'luv-theme',
			itemClass: 'luv-slider-item',
			controlsClass: 'luv-slider-controls',
			navContainerClass: 'luv-slider-nav',
			dotClass: 'luv-slider-dot',
			dotsClass: 'luv-slider-dots',
			autoHeightClass: 'luv-slider-height',
			onInitialized: initialize_fevr_slider,
			onLazyloaded: fevr_relative_center,
		});
		
		
		slider.on('change.owl.carousel', function(event){
			jQuery(slider).find('.animated').removeClass('animated');
			setTimeout(function(){
				jQuery(slider).find('.active .luv-slider-animation').addClass('animated');
			},300);
			
			jQuery(slider).find('video').each(function(){
				jQuery(this).get(0).pause();
			});

			setTimeout(function(){
				if (jQuery(slider).find('.active video').length > 0){
					jQuery(slider).find('.active video').get(0).play();
				}
			},300);
			
		});
		
		slider.on('translated.owl.carousel', function(event) {
			var active_item = slider.find('.active').find('li');
			if(jQuery('#main-header').offset().top === slider.offset().top) {
				jQuery('#main-header').attr('data-header-skin', active_item.attr('data-header-skin'));
			}
			jQuery(slider).parent().attr('data-skin', active_item.attr('data-header-skin'));
		});
		
		function initialize_fevr_slider() {
			var active_item = slider.find('.active li');
			if(jQuery('#main-header').offset().top === slider.offset().top) {
				// Header skin
				jQuery('#main-header').attr('data-header-skin', active_item.attr('data-header-skin'));
			}
			jQuery(slider).parent().attr('data-skin', active_item.attr('data-header-skin'));
			
			fevr_relative_center();
		}
		
		function fevr_relative_center() {
			// Relative header
			if (jQuery('#main-header').offset().top === slider.offset().top && jQuery('#main-header.no-border').length == 0 && jQuery('body.small-header').length == 0) {
				setTimeout(function() {
					var header_slider_inner_top = ((jQuery(slider).outerHeight() - jQuery('#main-header').outerHeight()) - jQuery(slider).find('.luv-slider-inner').outerHeight())/2 + jQuery('#main-header').outerHeight();
					jQuery(slider).find('.luv-slider-inner').css('top', header_slider_inner_top + 'px');
					jQuery(slider).find('.luv-slider-inner').addClass('relative-center');
				}, 100);
			}
		}
		
		jQuery(window).resize(function(){
			// Resize on scroll fix
			if (fevr_initial_width != jQuery(window).width() || fevr_initial_height != jQuery(window).height()){
				fevr_relative_center();
			}
		});
		
		window.addEventListener('orientationchange', function() {
			fevr_relative_center();
		});
		
		fevr_parallax_headerSlider(jQuery(this));
		fevr_full_height_slider();
	});
}

function fevr_parallax_headerSlider(slider) {
	
	var header_height = jQuery('header#main-header').outerHeight();
		
	jQuery('[data-luv-slider-parallax="true"]').each(function(){
		var wrapper = jQuery(this).parent('.luv-slider-wrapper');
		var content = jQuery(this).find('.luv-slider-inner');
		
		var slider_height = jQuery(wrapper).outerHeight();
		var slider_content_height = jQuery(content).outerHeight();
		var scroll_top = jQuery(window).scrollTop();
		
		if(jQuery(wrapper).offset().top-scroll_top <= 0) {
			jQuery(content).css({ 
				'opacity' : 1-(1/(slider_height+(slider_content_height/2)))*scroll_top
			});
		
			jQuery(this).stop(true, true).transition({
				y: (scroll_top-jQuery(wrapper).offset().top)*0.5
			},0);
		} else {
			jQuery(this).stop(true, true).transition({
				y: 0
			},0);
			
			jQuery(content).css({ 
				'opacity' : 1
			});
		}
	});
}

// Gallery

function fevr_gallery_init(){
	jQuery('.post-gallery').each(function(){
		var gallery = jQuery(this);
		var autoplay = gallery.attr('data-gallery-autoplay') === "true" ? true : false;
		var navigation = gallery.attr('data-gallery-arrows') === "true" ? true : false;
		var nav_dots = gallery.attr('data-gallery-dots') === "true" ? true : false;

		gallery.owlCarousel({
            items: 1,
            loop: gallery.attr('data-gallery-loop') == 'false' ? false : true,
            nav: navigation,
            dots: nav_dots,
            autoplay: autoplay,
            autoplayTimeout: 3000,
            autoplaySpeed: 700,
            smartSpeed: 700,
            navText: [],
            baseClass: 'luv-slider',
            themeClass: 'luv-theme',
			itemClass: 'luv-slider-item',
			controlsClass: 'luv-slider-controls',
			navContainerClass: 'luv-slider-nav',
			dotClass: 'luv-slider-dot',
			dotsClass: 'luv-slider-dots',
			autoHeightClass: 'luv-slider-height',
		});
	});
}

function fevr_full_height_slider() {
	// If the header is full height we should set it to the window's height
	if(jQuery('[data-luv-slider-full-height="true"]').length > 0) {
		jQuery('[data-luv-slider-full-height="true"]').each(function(){
			// The size is reduced with the height of the elements above
			var wrapper = jQuery(this).parents('#main-content').length == 0 ? jQuery('.luv-slider-wrapper').offset().top : jQuery('#main-header[data-header-position]').outerHeight();
			jQuery(this).find('li, li > .container').height(jQuery(window).height()-wrapper);
		});
	}
}

/*----------------------------------------------------------------------------------*/
/* Force to Hide Elements on Large Devices
/*----------------------------------------------------------------------------------*/

function fevr_bigger_than_s() {
	if(window.innerWidth > 768) {
		jQuery('#mobile-nav').hide();
		
		//Hide left navigation
		jQuery('body').removeClass('mobile-nav-opened');
	}
}

/*----------------------------------------------------------------------------------*/
/* Force to Hide Elements on Small Devices
/*----------------------------------------------------------------------------------*/

function fevr_smaller_than_s() {
	
}

/*----------------------------------------------------------------------------------*/
/* Function for Off-Canvas Menu Scrolling
/*----------------------------------------------------------------------------------*/

function fevr_content_scrolling(){ 
	jQuery('#off-canvas-menu, #mobile-nav, #overlay-navigation, .luv-demo-customizer').mousewheel(function(event) {
		this.scrollTop -= (event.deltaY * 28);
		
		event.preventDefault();
	});
}

/*----------------------------------------------------------------------------------*/
/* Masonry Init
/*----------------------------------------------------------------------------------*/

function fevr_masonry_init() {
	
	if(jQuery('.posts-container.masonry:not(.luv-carousel), .portfolio-container.masonry:not(.luv-carousel), .photo-reviews-container.masonry:not(.luv-carousel), .collections-container.masonry:not(.luv-carousel), .gallery.masonry:not(.luv-carousel), .products.masonry:not(.luv-carousel)').length > 0) {
	
		var container = jQuery('.posts-container.masonry:not(.luv-carousel), .portfolio-container.masonry:not(.luv-carousel), .photo-reviews-container.masonry:not(.luv-carousel), .collections-container.masonry:not(.luv-carousel), .gallery.masonry:not(.luv-carousel), .products.masonry:not(.luv-carousel)');
		
		container.each(function(){	
			
			var masonry_gutter = jQuery(this).hasClass('masonry-no-gap') ? 0 : jQuery(window).width() > 1481 ? jQuery(this).width()*0.015 : jQuery(this).width()*0.02;
			
			var isotope_init = jQuery(this).isotope({
				itemSelector: 'article, figure, .product',
				transitionDuration: '0.5s',
				layoutMode: 'packery',
				
				packery: { 
					gutter: masonry_gutter, 
				}
			});
			
			var item_container = jQuery(this);
			
			function set_visibility() {
				setTimeout(function(){
					fevr_resize_masonry_images();
					isotope_init.isotope('layout');
				},100);
				
				jQuery(item_container).children('article, figure, .product').each(function(i){
					if (!jQuery(this).hasClass('c-has-animation')){
						jQuery(this).css('opacity','1');
					}
				});			
			}

			if (jQuery(item_container).find('img').length > 0){
				isotope_init.imagesLoaded().progress(set_visibility);
			}
			else {
				set_visibility();
			}
			
			fevr_bg_check();
			
			jQuery(this).prev().find('li').click(function(){
				jQuery(this).siblings('li').removeClass('active-term');
				jQuery(this).addClass('active-term');
				jQuery(this).parent().prev().text(jQuery(this).text());
				jQuery(isotope_init).find('article.hentry').stop().removeClass('c-animated').addClass('appended-item');
				isotope_init.isotope({filter: jQuery(this).attr('data-masonry-filter')});
				isotope_init.isotope('layout');
				setTimeout(fevr_lazy_load_images,500);
			});
			
			
		});
	
	}
}

/**
 *  Set the height of masonry images if the user uses wide/tall images 
 */
function fevr_resize_masonry_images(){
	if(jQuery('.posts-container.masonry, .portfolio-container.masonry, .photo-reviews-container.masonry, .collections-container.masonry, .gallery.masonry, .products.masonry').length > 0) {
		var container = jQuery('.posts-container.masonry, .portfolio-container.masonry, .photo-reviews-container.masonry, .collections-container, .gallery.masonry, .products.masonry');
		
		container.each(function(){
			// Recalculate gutter based on viewport
			var masonry_gutter = jQuery(this).hasClass('masonry-no-gap') ? 0 : jQuery(window).width() > 1481 ? jQuery(this).width()*0.015 : jQuery(this).width()*0.02;

			if((jQuery(this).hasClass('masonry-meta-overlay') && jQuery(this).attr('data-crop-images') === 'true') || jQuery(this).hasClass('products')) {
		
				var item_height = 0;
				
				jQuery(this).find('.masonry-size-fevr_normal:not(.masonry-custom-content)').each(function(){
					if(jQuery(this).find('img').length > 0) {
						item_height = jQuery(this).find('img').height() + parseInt(jQuery(this).find('.post-inner').css('padding-right'))*2;
						return false;
					}
				});	
				
				
				if (item_height !== 0){
					jQuery(this).find('.masonry-size-fevr_tall img, .masonry-size-fevr_tall, .masonry-size-fevr_wide_tall img, .masonry-size-fevr_wide_tall').css('height', item_height*2);
					jQuery(this).find('.masonry-size-fevr_wide img, .masonry-size-fevr_wide, .masonry-size-fevr_normal').css('height', item_height);
				}
			}
			
			// Run same height masonry
			fevr_same_height_masonry();
			
			// Reset gutter
			jQuery(this).isotope({
				packery: { 
					gutter: masonry_gutter, 
				}
			});
			
			// Play animations for elements which appended in viewport after resize 
			fevr_play_c_animation();
		});
	}
	
}

/**
 * Set masonry items to equal height
 */
function fevr_same_height_masonry(){
	jQuery('.masonry-equal-height article, .masonry-equal-height li').height('auto');

	if (jQuery('.masonry-equal-height').length > 0){
		if(jQuery(window).width() >= 460) {
			jQuery('.masonry-equal-height').each(function() {
				// Get height
				var equal_height = 0;
				jQuery(this).find('article, li').each(function(){				
					if (jQuery(this).outerHeight() > equal_height){
						equal_height = jQuery(this).outerHeight();
					}
				});
				
				// Set height
				jQuery(this).find('article, li').each(function(){				
					if (jQuery(this).outerHeight() < equal_height) {
						jQuery(this).height(equal_height);
					}
					
				});
				
			});
		} 
	}
}

/**
 * 'Under the rug' footer
 */ 
function fevr_footer_under_the_rug() {
	if(jQuery('body').is('[data-footer-under-the-rug]')) {
		var footer_height = jQuery('footer#footer').height();
		if(footer_height+120 <= jQuery(window).height()) {
			jQuery('body').attr('data-footer-under-the-rug', 'true');
			jQuery('#content-wrapper').css('margin-bottom', footer_height);
		} else {
			jQuery('body').attr('data-footer-under-the-rug', 'false');
			jQuery('#content-wrapper').css('margin-bottom', '0px');
		}
	}
}

/**
 * Display toast messages
 * @param string message
 */
function fevr_wc_toast(message){
	if (typeof message === 'undefined' || message == ''){
		return;
	}
	jQuery('#luv-toast-container').css('display','block');
	var toast = jQuery('<div>',{
		'class' : 'luv-toast-message',
		'text'	: message
	}).appendTo('#luv-toast-container').fadeIn(500).delay(2000).fadeOut(2000);
	setTimeout(function(){
		jQuery(toast).remove();
		if (jQuery('.luv-toast-message').length == 0){
			jQuery('#luv-toast-container').css('display','none');
		}
	},4501);
}

/**
 * Load async CSS
 */
function fevr_load_defered_css(){
	jQuery('head link[media="defer"]').each(function(){
		jQuery(this).attr('media','all');
	});
}

/**
 * Luv Lazy load
 */
function fevr_lazy_load_images(){
	jQuery('[data-luv-lazy-load]').each(function(){
		if (jQuery(this).offset().top - jQuery(window).height()/2 < (jQuery('html').scrollTop() > 0 ? jQuery('html').scrollTop() : jQuery('body').scrollTop()) + jQuery(window).height()){
			if (jQuery(this).attr('data-srcset') != ''){
				jQuery(this).attr('srcset', jQuery(this).attr('data-srcset'));
			}
			if (jQuery(this).attr('data-sizes') != ''){
				jQuery(this).attr('sizes', jQuery(this).attr('data-sizes'));
			}
			jQuery(this).attr('src', jQuery(this).attr('data-src'));
			jQuery(this).removeAttr('data-luv-lazy-load');
			
			// Reinit masonry images
			if (jQuery(this).parents('.masonry').length > 0){
				jQuery(this).on('load', function(){
					jQuery(this).css('height','unset');
					setTimeout(fevr_resize_masonry_images,100);
				});
			}
		}
	});
}

/**
 * Initialize mindight header
 */
function fevr_init_midnight(){
	if (jQuery('body[data-auto-header-skin][data-header-position="default"]').length > 0 && jQuery(window).width() >= 768){
		 jQuery('#main-header').midnight();
		 
		 jQuery(document).on('mouseover', '.main-header-inner', function(){
			 jQuery('#main-header').css('overflow','visible');
			 jQuery(this).closest('.midnightHeader').css('overflow','visible');
			 jQuery(this).closest('.midnightInner')[0].style.setProperty('overflow','visible', 'important');
		 });
		 
		 jQuery(document).on('mouseout', '.main-header-inner', function(){
			 jQuery('#main-header').css('overflow','hidden');
			 jQuery(this).closest('.midnightHeader').css('overflow','hidden');
			 jQuery(this).closest('.midnightInner')[0].style.setProperty('overflow','hidden', 'important');
		 });
	}
}

/**
 * Handle sticking out menu items 
 */
function fevr_menu_overflow(){
	jQuery('.sub-menu-left').removeClass('sub-menu-left');
	jQuery('[data-header-position="default"] ul.sub-menu').each(function(){
	  if (jQuery(this).offset().left + jQuery(this).width() > jQuery(window).width()){
	   jQuery(this).addClass('sub-menu-left');
	  }  
	});
}

jQuery(document).ready(function(){
	"use strict";
	
	/*----------------------------------------------------------------------------------*/
	/* Styled Select
	/*----------------------------------------------------------------------------------*/
	
// 	jQuery('body:not(.woocommerce-checkout) select:visible, body:not(.single-product)').niceSelect();
	
	/*----------------------------------------------------------------------------------*/
	/* Nice Scroll
	/*----------------------------------------------------------------------------------*/
		
	function fevr_nice_scroll_init() {
		jQuery("html").niceScroll({
			scrollspeed: 40,
			mousescrollstep: 40,
			cursorwidth: 7,
			cursorcolor: '#262424',
			cursorborderradius: 6,
			autohidemode: false,
			horizrailenabled: false,
			cursorborder: 0,
		});
	}

	if(jQuery('body').attr('data-nice-scroll') == 'true') {
		fevr_nice_scroll_init();
	}

	/*----------------------------------------------------------------------------------*/
	/* Back top top
	/*----------------------------------------------------------------------------------*/
	
	function fevr_back_to_top_visibility() {
		jQuery(window).scrollTop() > 300 && jQuery('#to-top').length === 1 ? jQuery('a#to-top').addClass('top-is-visible') : jQuery('a#to-top').removeClass('top-is-visible top-fade-out');
		if( jQuery(window).scrollTop() > 1200 ) { 
			jQuery('a#to-top').addClass('top-fade-out');
		}
	}
	
	fevr_back_to_top_visibility();
	
	jQuery(document).on('click', 'a#to-top', function(e){
		e.preventDefault();
		jQuery('body, html').animate({
			scrollTop: 0 ,
		 	}, 700
		);
	});
	
	/*----------------------------------------------------------------------------------*/
	/* Slider Click Scroll
	/*----------------------------------------------------------------------------------*/
	
	jQuery('.header-scroll').click(function(){
		jQuery('html, body').animate({
			scrollTop : jQuery(this).parent().height()+'px',
		}, 500, 'swing');
	});
	
	/*----------------------------------------------------------------------------------*/
	/* Full Width Section
	/*----------------------------------------------------------------------------------*/
	
	fevr_full_width_section();
	
	/*----------------------------------------------------------------------------------*/
	/* Parallax Scene
	/*----------------------------------------------------------------------------------*/
	
	if(jQuery('.parallax-scene').length > 0) {
		jQuery('.parallax-scene').each(function(){
			jQuery(this).parallax();
		});
	}
	
	/*----------------------------------------------------------------------------------*/
	/* FitVid.js
	/*----------------------------------------------------------------------------------*/
	
 	jQuery('body').fitVids({ ignore: '.luv-animated-svg'});

	/*----------------------------------------------------------------------------------*/
	/* Detect Mobile Devices
	/*----------------------------------------------------------------------------------*/
	
	if(navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/)) {
		jQuery('body').addClass('mobile-device');
	} else {
		jQuery('body').addClass('desktop-device');
	}
	
	/*----------------------------------------------------------------------------------*/
	/* Hide Search Bar
	/*----------------------------------------------------------------------------------*/
	
	jQuery('.search-bar-trigger').click(function(e){
		e.preventDefault();
		jQuery('#search-bar').addClass('is-visible');
		jQuery('#search-bar-overlay').addClass('is-visible');
	});
	
	jQuery('#search-bar-overlay').click(function(e){
		if (jQuery(e.target).closest('#search-bar').length == 0){
			jQuery('#search-bar').removeClass('is-visible');
			jQuery('#search-bar-overlay').removeClass('is-visible');
		}
	});
	
	/*----------------------------------------------------------------------------------*/
	/* Search Filter
	/*----------------------------------------------------------------------------------*/
	
	jQuery('#search-filter-container li input').click(function(){
		jQuery('#search-bar-inner input[type="hidden"]').remove();
		if (typeof jQuery(this).attr('data-search-in') !== typeof undefined && jQuery(this).attr('data-search-in') !== false) {
			jQuery('#search-bar-inner form').append('<input type="hidden" name="post_type" value="'+jQuery(this).attr('data-search-in')+'" />');
		}
	});
	
	/*----------------------------------------------------------------------------------*/
	/* Hide Top Bar
	/*----------------------------------------------------------------------------------*/
	
	jQuery('#top-bar-close').click(function(e){
		e.preventDefault();
		jQuery('#top-bar').css({
			'margin-top': -jQuery('#top-bar').outerHeight()+'px',
		});
		jQuery('#top-bar').one('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function(){
			jQuery(this).remove();
		});
		document.cookie = "fevr-top-bar-close=true";
	});
	
	/*----------------------------------------------------------------------------------*/
	/* Display Cart (Style 2)
	/*----------------------------------------------------------------------------------*/
	
	if(window.innerWidth > 768) {
		jQuery('.cart-style-2 a').click(function(e){
			e.preventDefault();
			jQuery(this).parent().toggleClass('nav-cart-opened');
			jQuery('#full-width-cart').toggleClass('nav-cart-opened');
		});
	}
	
	/*----------------------------------------------------------------------------------*/
	/* Display Mobile Navigation
	/*----------------------------------------------------------------------------------*/
	
	jQuery('.nav-icon, .mobile-nav-trigger, #mobile-nav-overlay').click(function(e){
		e.preventDefault();
		// When the navigation is on the left..
		jQuery('body').toggleClass('mobile-nav-opened');
		if( jQuery('body').attr('data-mobile-nav-position') === 'left' ) {
		} else {
			jQuery('#mobile-nav').stop(true,true).slideToggle();
		}
		
		// Restore the default mobile navigation view by removing the search slide
		setTimeout(function(){
			jQuery('#mobile-nav').removeClass('is-search-active');
			jQuery('#mobile-nav li').removeClass('submenu-opened');
			jQuery('#mobile-nav .sub-menu').removeAttr('style');
		}, 500);
	});

	jQuery('#mobile-nav-overlay').click(function(){
		jQuery('body').removeClass('mobile-nav-opened');
	});
	
	/*----------------------------------------------------------------------------------*/
	/* Display Mobile Navigation Search
	/*----------------------------------------------------------------------------------*/
	
	jQuery('.mobile-nav-search-trigger').click(function(e) {
		e.preventDefault();
		
		jQuery('#mobile-nav').toggleClass('is-search-active');
	});
	
	/*----------------------------------------------------------------------------------*/
	/* Display Child Items of Mobile Navigation
	/*----------------------------------------------------------------------------------*/

	jQuery('#mobile-nav .menu-indicator').click(function(e){
		e.preventDefault();
		jQuery(this).parent().parent().toggleClass('submenu-opened');
		jQuery(this).parent().next('.sub-menu').stop(true,true).slideToggle();
	});
	
	/*----------------------------------------------------------------------------------*/
	/* Display child navigation when header is on the left
	/*----------------------------------------------------------------------------------*/
	
	jQuery(document).on('click', '[data-header-position="left"] .nav-menu li.menu-item-has-children > a', function(e){
		e.preventDefault();
		jQuery(this).parent().addClass('sub-menu-opened');
		jQuery(this).closest('ul').addClass('sub-menu-opened');
		if (jQuery(this).siblings('ul').find('.prev-level').length === 0){
				var prev_level = jQuery(this).parent().clone();
				jQuery(prev_level).find('.ion-chevron-right').removeClass('ion-chevron-right').addClass('ion-chevron-left');
				jQuery(prev_level).find('.menu-indicator').show();
				jQuery(prev_level).removeAttr('class');
				jQuery(prev_level).addClass('prev-level');
				jQuery(this).siblings('ul').prepend(prev_level);
		}
		if (jQuery('#nav-primary > ul').hasClass('sub-menu-opened')){
			jQuery('#nav-primary').addClass('hide-icons');
		}
	});
	
	jQuery(document).on('click', '[data-header-position="left"] .nav-menu li.prev-level > a', function(e){
		e.preventDefault();
		var that = jQuery(this).closest('li.sub-menu-opened');
		var timeout = jQuery(this).closest('ul.sub-menu-opened').css('transition-duration').replace(/([^\d.]*)/g,'')*1000;
		jQuery(this).closest('ul.sub-menu-opened').removeClass('sub-menu-opened');
		if (!jQuery('#nav-primary > ul').hasClass('sub-menu-opened')){
			jQuery('#nav-primary').removeClass('hide-icons');
		}
		
		setTimeout(function(){
			jQuery(that).removeClass('sub-menu-opened');
		}, timeout);
	});
	
	/*----------------------------------------------------------------------------------*/
	/* Init Masonry
	/*----------------------------------------------------------------------------------*/
	if(jQuery('[data-vc-full-width="true"]').length == 0) {
		fevr_masonry_init();
	}
	
	/*----------------------------------------------------------------------------------*/
	/* Nav Menu Widget
	/*----------------------------------------------------------------------------------*/
	
	jQuery('.widget_nav_menu .menu-item-has-children > a').click(function(e){
		e.preventDefault();
		//jQuery(this).closest('.widget_nav_menu').find('.sub-menu').slideUp();
		if(!jQuery(this).next('.sub-menu').is(":visible")) {
			jQuery(this).next('.sub-menu').slideDown();
		}
	});
	
	/*----------------------------------------------------------------------------------*/
	/* Overlay Nav Menu
	/*----------------------------------------------------------------------------------*/
	
	if(jQuery('#overlay-nav-container').length > 0) {
		jQuery('#overlay-nav-container').dlmenu({
			animationClasses : { classin : 'o-nav-animate-in', classout : 'o-nav-animate-out' }
		});
	}
	
	/*----------------------------------------------------------------------------------*/
	/* Display Slide Out Widget Area
	/*----------------------------------------------------------------------------------*/

	jQuery('.off-canvas-menu-trigger > a, a.off-canvas-menu-trigger').click(function(e){
		e.preventDefault();
		jQuery('body').toggleClass('off-canvas-menu-opened');
	});
	
	jQuery('#off-canvas-menu-overlay').click(function(){
		jQuery('body').removeClass('off-canvas-menu-opened');
	});
	
	/*----------------------------------------------------------------------------------*/
	/* Display Overlay Navigation
	/*----------------------------------------------------------------------------------*/
	
	jQuery('.overlay-navigation-trigger > a').click(function(e){
		e.preventDefault();
		jQuery('body').addClass('overlay-navigation-opened');
	});
	
	jQuery('a.overlay-navigation-trigger, #overlay-navigation').click(function(e){
		if(jQuery(e.target).closest('#overlay-navigation-inner').length === 0) {
			e.preventDefault();
			jQuery('body').removeClass('overlay-navigation-opened');
		}	
	});
		
	
	/* Initalize Header / center content, parallax /
	/*----------------------------------------------------------------------------------*/
	fevr_header_init();

	/*----------------------------------------------------------------------------------*/
	/* Luv Like
	/*----------------------------------------------------------------------------------*/
	
	jQuery(document).on('click','.luv-like',function(e){
		e.preventDefault();
		var to_like = jQuery(this).attr('data-to-like');
		var counter = jQuery('.luv-like[data-to-like="' + to_like + '"]').find('.luv-like-count');
		
		if (jQuery(this).hasClass('luv-liked')){
			jQuery('.luv-like[data-to-like="' + to_like + '"]').find('i').removeClass('fa-heart').addClass('fa-heart-o');
			jQuery('.luv-like[data-to-like="' + to_like + '"]').removeClass('luv-liked luv-like-animation').addClass('luv-unlike-animation');
			jQuery(counter).text((parseInt(jQuery(counter).text()) > 0 ? parseInt(jQuery(counter).text()) - 1 : 0));
		}
		else{
			jQuery('.luv-like[data-to-like="' + to_like + '"]').find('i').removeClass('fa-heart-o').addClass('fa-heart');
			jQuery('.luv-like[data-to-like="' + to_like + '"]').addClass('luv-liked luv-like-animation').removeClass('luv-unlike-animation');
			jQuery(counter).text(parseInt(jQuery(counter).text()) + 1);
		}
		
		jQuery.post(fevr.ajax_url, {'action': 'fevr_like','to-like': to_like});
	});
	
	/**
	 * Load luv like count
	 */
	jQuery('[data-luv-like-post_id]').each(function(){
		var post_id = jQuery(this).attr('data-luv-like-post_id');
		var that = jQuery(this);

		jQuery(this).load(fevr.ajax_url, {'action': 'fevr_load_like_count', 'post_id' : post_id});
	});
	
	// Init lightbox for portfolio attachments
	if (fevr.lightbox['portfolio'] == true){
		jQuery('[rel="portfolio-attachment"]').each(
		function() {
			jQuery(this).iLightBox({
				skin: fevr.lightbox['skin']
			});
		});
	}
	
	// Init lightbox for photo review attachments
	if (fevr.lightbox['photoreview'] == true){
		jQuery('[rel="photoreview-attachment"]').each(
		function() {
			jQuery(this).iLightBox({
				skin: fevr.lightbox['skin']
			});
		});
	}
	
	// Init lightbox for photo review attachments
	if (fevr.lightbox['woocommerce'] == true){
		jQuery('html.no-touchevents [rel="woocommerce-attachment"]').each(
		function() {
			jQuery(this).iLightBox({
				skin: fevr.lightbox['skin']
			});
		});
	}
	
	if (fevr.lightbox['attachment'] == true){
		// Init lightbox for attachments
		jQuery('[rel^="attachment"]').each(
		function() {
			jQuery(this).iLightBox({
				skin: fevr.lightbox['skin']
			});
		});
	}
	
	if (fevr.lightbox['gallery'] == true){
		// Init lightbox for attachments
		jQuery('.gallery').each(
		function() {
			jQuery(this).find('.gallery-item a').iLightBox({
				skin: fevr.lightbox['skin']
			});
		});
	}
	
	// Init lightbox for vc_single_image
	jQuery('.luv-lightbox').each(
	function() {
		jQuery(this).iLightBox({
			skin: fevr.lightbox['skin']
		});
	});
	
	
	
	/*----------------------------------------------------------------------------------*/
	/* Infinite Scroll
	/*----------------------------------------------------------------------------------*/
	
	if(jQuery('.pagination-infinite-scroll').length > 0) {
		var container = jQuery('#main-content > .item-grid-container');
		
		jQuery(container).infinitescroll({
		  loading: {
		    msgText: "<div class='loader'><svg class='loader-circular' viewBox='25 25 50 50'><circle class='loader-path' cx='50' cy='50' r='20' fill='none' stroke-width='2' stroke-miterlimit='10'/></svg></div>",
		    speed: 'fast',
			selector: '.infinite-loader-container',
		  },
		  state: {
		    currPage: 1
		  },
		  binder: jQuery(window),
		  nextSelector: "div.pagination-infinite-scroll a.next",
		  navSelector: "div.pagination-infinite-scroll",
		  contentSelector: container,
		  extraScrollPx: 150,
		  itemSelector: "article.hentry",
		  animate: false,
		  dataType: 'html',
		  debug: false,
		  appendCallback: true,
		  bufferPx: 40,
		  errorCallback: function () { },
		  infid: 0,
		  path: function(page){
			  window.fevr_pagination_url = fevr['pagination_base'] + '/' + page + '/'; 
			  return window.fevr_pagination_url;
		  }
		}, function(items){
			
			var newItems = jQuery(items);
			
			if (container.hasClass('masonry')){
				newItems.imagesLoaded(function() {
					container.isotope( 'appended', newItems );
					fevr_resize_masonry_images();
					jQuery(items).each(function(){
						if (jQuery(this).hasClass('c-has-animation')){
							jQuery(this).css('opacity',0);
							jQuery(this).css('transition','unset');
						}
					});
				});
			}
			
			jQuery(items).each(function(){
				jQuery(this).addClass('appended-item');
				// Init fitvids
				jQuery(this).fitVids({ ignore: '.luv-animated-svg'});
				//Init gallery
				fevr_gallery_init(jQuery(this).find('.post-gallery'));
				if (jQuery(this).hasClass('c-has-animation')){
					jQuery(this).css('opacity',0);
					jQuery(this).css('transition','unset');
				}
			});
			
			fevr_bg_check();
			
			// Get luv like counts
			jQuery(items).find('[data-luv-like-post_id]').each(function(){
				var post_id = jQuery(this).attr('data-luv-like-post_id');
				var that = jQuery(this);

				jQuery(this).load(fevr.ajax_url, {'action': 'fevr_load_like_count', 'post_id' : post_id});
			});
			
			// Get luv inline css
			jQuery.get(window.fevr_pagination_url + '?infinite-inline-css=1', function(style){
				jQuery('head').append(style);
			});
			
			if (typeof jQuery.fn.mediaelementplayer === 'undefined'){
				jQuery.post(fevr.ajax_url, {'action' : 'fevr_load_infinite_assets'}, function(html){
					jQuery('head').append(html);
					jQuery(items).find('.wp-audio-shortcode, .wp-video-shortcode').mediaelementplayer();
				});
			}
			else{
				jQuery(items).find('.wp-audio-shortcode, .wp-video-shortcode').mediaelementplayer();
			}
			
		});
		
	}
	
	/*----------------------------------------------------------------------------------*/
	/* One page navigation
	/*----------------------------------------------------------------------------------*/
	
	// Select active menu on one page navigation
	function fevr_one_page_navigation(){
		var active_item;
		jQuery('.one-page-active').removeClass('one-page-active');
		jQuery('.one-page-section').each(function(){
			if (jQuery(this).offset().top < (jQuery('html').scrollTop() > 0 ? jQuery('html').scrollTop() : jQuery('body').scrollTop()) + jQuery(window).height()){
				active_item = jQuery(this);
				if (jQuery(this).offset().top > (jQuery('html').scrollTop() > 0 ? jQuery('html').scrollTop() : jQuery('body').scrollTop())){
					jQuery('.one-page-active').removeClass('one-page-active');
					jQuery('#nav-primary .menu-item a[href="#'+jQuery(this).attr('id')+'"]').parents('li').addClass('one-page-active');
					jQuery('.page-submenu .menu-item a[href="#'+jQuery(this).attr('id')+'"]').parents('li').addClass('one-page-active');
					jQuery('.one-page-slide-dot a[href="#'+jQuery(this).attr('id')+'"]').closest('li').addClass('one-page-active');
					return false;
				}
			}
		});
		if (jQuery('#nav-primary .one-page-active')){	
			jQuery('#nav-primary .menu-item a[href="#'+jQuery(active_item).attr('id')+'"]').parents('li').addClass('one-page-active');
			jQuery('.page-submenu .menu-item a[href="#'+jQuery(active_item).attr('id')+'"]').parents('li').addClass('one-page-active');
			jQuery('.one-page-slide-dot a[href="#'+jQuery(active_item).attr('id')+'"]').closest('li').addClass('one-page-active');
		}
	}
	 
	if (jQuery('.one-page-section').length > 0){
		// Bind scrolling if one page layout section is present
		jQuery(window).on('scroll', fevr_one_page_navigation);
		
		// Handle hash in URL
		if (jQuery('body[data-one-page-navigation]').length > 0 && document.location.hash != '' && jQuery(document.location.hash).length > 0){
	    	 jQuery('html, body').scrollTop(0);
		     jQuery(window).on('load', function(){ 
		         var correction	= (jQuery('[data-header-position="default"] #main-header').length > 0 ? jQuery('[data-header-position="default"] #main-header').offset().top + jQuery('[data-header-position="default"] #main-header').height() - jQuery(window).scrollTop()  : 0);
		    	 correction		= (jQuery('.page-submenu.page-submenu-onpage').length > 0 ? correction + jQuery('.page-submenu.page-submenu-onpage').height() : correction);
		    	 jQuery('html, body').animate({scrollTop : jQuery(document.location.hash + '.one-page-section').offset().top - correction + 'px'}, 1000);
		     });
		}
		
		// Handle full URL with hash
		jQuery('a[href*="#"]').each(function(){
	        if (jQuery(this).attr('href').replace(document.location.href.replace(document.location.hash, ''),'').match(/^#/)){
	            jQuery(this).attr('href',jQuery(this).attr('href').replace(document.location.href.replace(document.location.hash, ''),''));
	         }
	    });
		
		// Animate clicks
		jQuery(document).on('click','body[data-one-page-navigation] .one-page-slide-dot a[href^="#"], body[data-one-page-navigation] #nav-primary .menu-item a[href^="#"], body[data-one-page-navigation] .page-submenu .menu-item a[href^="#"], #mobile-nav .menu-item a[href^="#"], a.btn[href^="#"]',function(e){
			e.preventDefault();
			var correction	= (jQuery('[data-header-position="default"] #main-header').length > 0 ? jQuery('[data-header-position="default"] #main-header').height() : 0);
			correction		= (jQuery('.page-submenu.page-submenu-onpage').length > 0 ? correction + jQuery('.page-submenu.page-submenu-onpage').height()*2 : correction);
			var target		= jQuery(jQuery(this).attr('href') + '.one-page-section').offset().top - correction;
			jQuery('html, body').stop().animate({scrollTop : target + 'px'}, 500);
		});
	}	

	/*----------------------------------------------------------------------------------*/
	/* Play page header title animation if any
	/*----------------------------------------------------------------------------------*/
	
	//Play page header navigation (only if loader overlay isn't present)
	if (jQuery('#loader-overlay').length == 0){
		jQuery('.page-header-animation').addClass('animated');
	}
	//If loader overlay is present we will play the animatin after we removed it (@see Loading overlay)
	else{
		jQuery('.page-header-animation').css('opacity','0');
	}
	
	// Fire with delay
	setTimeout(function(){
		fevr_full_height_columns_init();
	}, 50);

	
	/*----------------------------------------------------------------------------------*/
	/* When page loaded
	/*----------------------------------------------------------------------------------*/
	
	jQuery(window).on('load',function(){
		
		//Lazy Load Images
		fevr_lazy_load_images();
		
		// Typewriter effect
		if (jQuery('.page-header-animation.typewriter .page-header-title').length > 0){
			jQuery('.page-header-animation.typewriter .page-header-title').typed({
				strings: [fevr.page_header_title], typeSpeed: 55 
			});
			setTimeout(function(){jQuery('.page-header-animation.typewriter h1').css('opacity','unset')},500);
		}



		// Handle sticking out menu items
		fevr_menu_overflow();

		
		// Loading overlay

		var overlay = jQuery('#loader-overlay');
		jQuery(overlay).find('svg').remove();
		jQuery(overlay).fadeOut(500);
		setTimeout(function(){
			jQuery(overlay).remove();
			//Play page header navigation
			jQuery('.page-header-animation').addClass('animated');
			jQuery('.page-header-animation').css('opacity','1');
		}, 500);
				
		// Portfolio Color Palette

		if(jQuery('#portfolio-color-palette').length > 0) {
			var portfolio_img = jQuery('.post-featured-img img');
			var colorThief = new ColorThief();
			var palette = colorThief.getPalette(portfolio_img[0], 6);
			
			jQuery(palette).each(function(){
				var color = jQuery(this);
				jQuery('#portfolio-color-palette').append(
					jQuery('<div>', {
						'style' : 'background-color: rgb(' +color[0]+', '+color[1]+', '+color[2]+')',
					})
				);
			});
		}
		
		// Load defered CSS
		fevr_load_defered_css();

		//Initalize Slider
		if(jQuery('.luv-slider').length > 0 && jQuery('[data-vc-full-width="true"]').length == 0) {
			fevr_slider_init();
		}
		
		// Gallery
		if(jQuery('.post-gallery').length > 0 && jQuery('[data-vc-full-width="true"]').length == 0) {
			fevr_gallery_init();
		}
		
		// Initialize Midnight Header
		fevr_init_midnight();
		
		// Chrome masonry, VC fullwidth bugfix
		setTimeout(function(){jQuery(window).trigger('resize')},1);

	});
	
	/*----------------------------------------------------------------------------------*/
	/* Async CSS loaded
	/*----------------------------------------------------------------------------------*/
	
	if (jQuery('#luv-merged-async-css').length > 0){
		jQuery('#luv-merged-async-css').load(function(){
			// Remove is-loading class
			jQuery('body').removeClass('is-loading');
	
			// Re-init header
			fevr_header_init();
		});
	}
	
	/*----------------------------------------------------------------------------------*/
	/* Fire Init functions
	/*----------------------------------------------------------------------------------*/
	
	// Call display functions
	
	fevr_small_header();
	fevr_content_scrolling();
	fevr_bigger_than_s();
	fevr_smaller_than_s();
	fevr_footer_under_the_rug();
	
	jQuery(window).on('scroll', function(){
		fevr_small_header();
		fevr_parallax_header();
		fevr_zoom_out_header();
		fevr_parallax_headerSlider();
		fevr_back_to_top_visibility();
		fevr_lazy_load_images();
	});
	
	jQuery(window).resize(function(){
		// Resize on scroll fix
		if (fevr_initial_width != jQuery(window).width() || fevr_initial_height != jQuery(window).height()){
			
			// Reset current sizes
			fevr_initial_width	= jQuery(window).width();
			fevr_initial_height	= jQuery(window).height();
			
			// Fire functions
			fevr_bigger_than_s();
			fevr_smaller_than_s();
			fevr_header_init();
			fevr_parallax_headerSlider();
			fevr_full_height_slider();
			fevr_resize_masonry_images();
			fevr_full_width_section();
			fevr_footer_under_the_rug();
			fevr_lazy_load_images();
			fevr_menu_overflow();
		}
	});
	
	window.addEventListener('orientationchange', function() {
		fevr_bigger_than_s();
		fevr_smaller_than_s();
		fevr_header_init();
		fevr_parallax_headerSlider();
		fevr_full_height_slider();
		fevr_resize_masonry_images();
		fevr_full_width_section();
		fevr_footer_under_the_rug();
		fevr_lazy_load_images();
		fevr_menu_overflow();
	});
});


// Touch handling on touch devices
jQuery(document).on('click', 'html.touchevents', function(){
	jQuery('.touch-hovered').removeClass('touch-hovered');
	
	if (jQuery('body[data-auto-header-skin]').length > 0){
		jQuery('.main-header-inner').trigger('mouseout');
	}
});

jQuery(document).on('click', 'html.touchevents body[data-header-position="default"] #nav-primary li.menu-item-has-children > a, html.touchevents .product-wrapper, html.touchevents .cart-contents, .masonry:not(.masonry-meta-overlay) .post-featured-img > a, html.touchevents .masonry-color-overlay-text a', function(e){
	if (jQuery(this).hasClass('touch-hovered') || (jQuery(e.target).closest('.nav-cart').length > 0 && jQuery(window).width() <= 769)){
		return true;
	}
	else {
		jQuery(this).addClass('touch-hovered');
		jQuery(this).trigger('mouseover');
		e.preventDefault();
		return false;
	}
});

