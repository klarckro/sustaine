jQuery(window).load(function(){
	jQuery('.luv-instawidget').each(function(){
		var id = jQuery(this).attr('id');
		var limit = jQuery(this).attr('data-limit');
		var user = jQuery(this).attr('data-user');
		var clientId = jQuery(this).attr('data-client-id');
		var accessToken = jQuery(this).attr('data-token');
		var image_size = parseInt(jQuery(this).width()/3);
		
		var options = {
			target: id,
			limit: limit,
	        clientId: clientId,
	        template: '<div class="luv-instalink"><a href="{{link}}" target="_blank"><img src="{{image}}" width="' + image_size + '" height="' + image_size + '"></a></div>',
	        accessToken: accessToken,
			get : 'user',
			userId : user,
			after: fevr_footer_under_the_rug
		}
		
		try{
			var feed = new Instafeed(options);
			feed.run();
		}
		catch(e){
			console.log(e.message);
		}
	
	});
});