jQuery(function() {
	var fevr_window_height = jQuery(window).height();
	var fevr_window_width = jQuery(window).width();
	var fevr_correction = jQuery('#header-wrapper').height() - 500;
	var fevr_page_header_custom = jQuery('#page-header-custom');
	var fevr_promo_container = jQuery('.promo-container');
	var fevr_promo_container_nc = jQuery('.promo-container:not(.content)');
	var fevr_ratio = (fevr_window_width / fevr_window_height >= 198 / 119);
	var fevr_promo_container_content = jQuery('.promo-container.content');
	var fevr_content_wrapper = jQuery('#content-wrapper');

	function promo_scroll() {
		var zoom = function() {
			fevr_page_header_custom.removeAttr('data-top');
			fevr_content_wrapper.css('margin-top', 5000);

			fevr_promo_container
					.css({
						'position' : 'fixed',
						'display' : 'block',
						'top' : 0,
						'left' : 0,
						'transform' : 'scale(' + 5 * ((1 - (jQuery(window).scrollTop() / fevr_window_height) / 3)) + ')',
					});

			if (navigator.userAgent.match(/webkit/i)) {
				fevr_promo_container_content.css({
					'transform' : 'scale(' + 5 * ((1 - (jQuery(window).scrollTop() / fevr_window_height) / 3)) / 4 + ')'
				});
			}
			fevr_promo_container_nc.css('background-position', '0 ' + fevr_correction + 'px');
		};
		var _zoom = function() {
			if (typeof fevr_page_header_custom.attr('data-top') == 'undefined') {
				fevr_page_header_custom.attr('data-top', fevr_page_header_custom.offset().top);
				fevr_content_wrapper.css('margin-top', fevr_page_header_custom.offset().top);
			}

			fevr_promo_container.css({
				top : fevr_page_header_custom.attr('data-top') - jQuery(window).scrollTop(),
				'transform' : 'scale(1)',
			});

			if (navigator.userAgent.match(/webkit/i)) {
				fevr_promo_container_content.css({
					'transform' : 'scale(0.25)',
				});
			}
		};

		if (5 * ((1 - (jQuery(window).scrollTop() / fevr_window_height) / 3)) > 1
				&& fevr_ratio) {
			zoom();
		} else if (fevr_ratio) {
			_zoom();
		} else {
			fevr_page_header_custom.removeAttr('data-top');
			fevr_content_wrapper.css('margin-top', 0);

			fevr_promo_container.css({
				'background-position' : 'center top',
				'position' : 'absolute',
				'top' : 0,
				'left' : 0,
				'transform' : 'scale(1)',
			});

			fevr_promo_container_content.css({
				'position' : 'absolute'
			});
		}
	}

	jQuery(document).on(
			'click',
			'#page-header-custom:not(.clicked)',
			function() {
				jQuery(this).addClass('clicked');
				jQuery('html, body').animate({'scrollTop' : jQuery('.l-grid-row').offset().top - parseInt(fevr_content_wrapper.css('margin-top')) - jQuery('#main-header').outerHeight()}, 2500);
			});

	jQuery(window).load(function() {
		jQuery(window).trigger('scroll');
	});

	jQuery(window)
			.on(
					'scroll',
					function() {
						if (jQuery('body[data-promo-sliding-content="true"]').length > 0) {
							fevr_promo_container_content[0].style.setProperty(
									'background-position', jQuery(window).scrollTop() + 'px 50%', 'important');
						}
						if (navigator.userAgent.match(/webkit/i)) {
							fevr_promo_container_content[0].style.setProperty('background-size', '500%', 'important');
						}
						promo_scroll();
					});

	jQuery(window).on('resize', function() {
		fevr_page_header_custom.removeAttr('data-top');
		promo_scroll();
	});
});