jQuery(function(){
	"use strict";

	jQuery('.luv-flickr-container').each(function(){
		var container	= jQuery(this);
		var username	= jQuery(this).attr('data-username');
		var api_key		= jQuery(this).attr('data-api-key');
		var items		= jQuery(this).attr('data-items');
		//Get user_id
		jQuery.getJSON('https://api.flickr.com/services/rest/?method=flickr.people.findByUsername&format=json&jsoncallback=?&api_key=' + api_key +'&username=' + username, function(person){
			jQuery.getJSON('https://api.flickr.com/services/rest/?method=flickr.people.getPublicPhotos&format=json&jsoncallback=?&api_key=' + api_key +'&user_id=' + person.user.id, function(channel){
				for (var i=0; i<items; i++){
					jQuery.getJSON('https://api.flickr.com/services/rest/?method=flickr.photos.getSizes&format=json&jsoncallback=?&api_key=' + api_key +'&photo_id=' + channel.photos.photo[i].id, function(photo){
						for(var j in photo.sizes.size){
							if (photo.sizes.size[j].label == 'Small'){
								jQuery(container).append(
									jQuery('<div>',{
										'class' : 'luv-flickr-image'
									}).append(
											jQuery('<a>',{
												'href'		: 'https://www.flickr.com/photos/' + username + '/' +  channel.photos.photo[i].id + '/',
												'target'	: '_blank'
											}).append(jQuery('<img>',{
												src : photo.sizes.size[j].source
											}))
									)
								);
							}
						};
					});
				}
			});
		});
	});
});