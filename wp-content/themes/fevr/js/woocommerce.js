/* jshint devel:true */
/* global response, fevr, fevr_front, json, BackgroundCheck, wc_add_to_cart_params */
/* jshint -W020 */
/* jshint unused:false */
/* jshint latedef:false */

/*----------------------------------------------------------------------------------*/
/* Photo Reviews Masonry
/*----------------------------------------------------------------------------------*/

jQuery(document).ready(function(){
	"use strict";

	/*----------------------------------------------------------------------------------*/
	/* Image Zoom
	/*----------------------------------------------------------------------------------*/
	
	if(!jQuery('body').hasClass('mobile-device') && typeof jQuery.fn.easyZoom !== 'undefined') {
		jQuery(".product-zoom").easyZoom({
			preventClicks: true,
			errorNotice: ' ',
			loadingNotice: ' ',
		});
	}
	
	/*----------------------------------------------------------------------------------*/
	/* Woocommerce Headless Checkout Popup
	/*----------------------------------------------------------------------------------*/

	
	jQuery(document).on('click','.luv-break-iframe, .nav-cart .cart-contents, .woocommerce-message .wc-forward',function(e){
		// Open a popup, because checkout in iframe isn't secure
		if (fevr_in_iframe()){
			e.preventDefault();

			window.open(jQuery(this).attr('href'), "_blank", "toolbar=0,scrollbars=1,locationbar=0,menubar=0,titlebar=0");
		}
		
	});
	
	/*----------------------------------------------------------------------------------*/
	/* Woocommerce Quick View
	/*----------------------------------------------------------------------------------*/
	jQuery(document).on('click','.luv-product-quick-view',function(e){
		e.preventDefault();
		
		var product_id = jQuery(this).attr('data-product-id');
		
		var button = jQuery(this);
		jQuery(button).attr('data-text', encodeURIComponent(jQuery(this).html()));
		jQuery(button).html('<i class="fa fa-spinner fa-spin"></i>');
		
		jQuery('.wc-quick-view-container .wc-quick-view-inner').load(fevr.ajax_url, {action: 'fevr_product_quick_view', 'product_id': product_id}, function(){
			if (typeof jQuery( '.variations_form' ).wc_variation_form === 'function'){
				jQuery( '.variations_form' ).wc_variation_form();
				jQuery( '.variations_form .variations select' ).change();
			}
			jQuery('.wc-quick-view-container').addClass('is-visible');
			jQuery(button).html(decodeURIComponent(jQuery(button).attr('data-text')));
		});
	});
	
	jQuery(document).on('click','.wc-quick-view-close',function(e){
		e.preventDefault();
		jQuery('.wc-quick-view-container .wc-quick-view-inner').empty();
		jQuery('.wc-quick-view-container').removeClass('is-visible');
	});
	
	/*----------------------------------------------------------------------------------*/
	/* Photo Reviews
	/*----------------------------------------------------------------------------------*/
	
	//Popup
	jQuery(document).on('click','.luv-photo-review',function(e){
		e.preventDefault();
		
		var product_id = jQuery(this).attr('data-product-id');
		jQuery('.photo-review-popup').addClass('is-visible').css({
			'top' : (jQuery(window).scrollTop()+100)+'px',
		});
		jQuery('.photo-review-popup .photo-review-container').load(fevr.ajax_url, {action: 'fevr_photo_review', 'product_id': product_id}, function(){});
	});
	
	jQuery(document).on('click','.photo-review-close',function(e){
		e.preventDefault();
		jQuery('.photo-review-popup .photo-review-container').empty();
		fevr_front.reset_ajax_message_container('.photo-review-message');
		jQuery('.photo-review-popup').removeClass('is-visible');
	});

	jQuery(document).on('change','#review-file-select',function(){
		jQuery(this).parent('form').submit();
	});
	
	//Upload Function 
	jQuery(document).on('submit','#photo-review-upload-form',function(e){
		e.preventDefault();
		
		var fileSelect = document.getElementById('review-file-select');
		var uploadButton = jQuery('#photo-review-upload');
			
		// Update button text.
		uploadButton.text('Uploading..');
		
		// Get the selected files from the input.
		var files = fileSelect.files;
		
		// Create a new FormData object.
		var formData = new FormData();
		
		// Loop through each of the selected files.
		for (var i = 0; i < files.length; i++) {
			var file = files[i];
			
			// Check the file type.
			if (!file.type.match('image.*')) {
				continue;
			}
			
			// Add the file to the request.
			formData.append('photo_review_photos[]', file, file.name);
			formData.append('action', 'fevr_photo_review_image_upload');
			formData.append('product_id', jQuery('.photo-review-product-id').val());
			formData.append('photo_review_id', jQuery('.photo-review-forms .photo-review-id').val());
			formData.append('wp_nonce', fevr.wp_nonce);
			
		}
		
		// Set up the request.
		var xhr = new XMLHttpRequest();
		
		// Open the connection.
		xhr.open('POST', fevr.ajax_url, true);
		
		// Set up a handler for when the request finishes.
		xhr.onload = function () {
			if (xhr.status === 200) {
				// File(s) uploaded.
				try{
					var response = JSON.parse(xhr.responseText);
					jQuery('.photo-review-id').val(response.review_id);
					if (response.error === false){
						for (var i in response.photos){
							jQuery('#photo-review-image-container').append(
								jQuery('<div>',{
									'id'		: 'photo-review-image-inner-' + response.photos[i].id,
									'class'		: 'photo-review-image-inner',
								}).append(
									jQuery('<img>',{
											'src'	: response.photos[i].src,
											'class'	: 'photo-review-preview-image'
									}),
									jQuery('<div>',{
										'html'	: '<i class="ion-close"></i>',
										'class'	: 'remove-photo-review-image',
										'data-id'	: response.photos[i].id
									})
								)
							);
						}
					}
					else{
						fevr_front.ajax_message('.photo-review-message', response.message, 'error');
					}
				}
				catch (e){
					fevr_front.ajax_message('.photo-review-message', fevr_front.translate('An error occurred!'), 'error');
				}
			} else {
				fevr_front.ajax_message('.photo-review-message', fevr_front.translate('An error occurred!'), 'error');
			}
			uploadButton.text('Upload');
		};
		
		// Send the Data.
		xhr.send(formData);
		//Reset the form
		jQuery('#photo-review-upload-form').find('input.to-empty, textarea').val('');
	});
	
	//Send the content and finalize review
	jQuery(document).on('submit', '#photo-review-content-form', function(e){
		e.preventDefault();
		
		// Validation
		if(jQuery('.photo-review-preview-image').length == 0){
			fevr_front.ajax_message('.photo-review-message', fevr_front.translate('Please upload an image'), 'error');
			return;
		}
		else if(jQuery('[name="photo_review_title"]').val() == ''){
			fevr_front.ajax_message('.photo-review-message', fevr_front.translate('The title can not be empty'), 'error');
			return;	
		}
		
		jQuery.post(fevr.ajax_url, jQuery(this).serialize(),function(){
			jQuery('.photo-review-container').empty();
			fevr_front.ajax_message('.photo-review-message', fevr_front.translate('Thank you for your review!'), 'success');
		});
	});
	
	//Remove photo review image
	jQuery(document).on('click', '.remove-photo-review-image', function(e){
		e.preventDefault();
		var container = jQuery(this).parent();
		jQuery.post(fevr.ajax_url, {'action' : 'fevr_photo_review_remove_image', 'photo_review_image_id' : jQuery(this).attr('data-id'), 'wp_nonce': fevr.wp_nonce},function(){
			container.remove();
		});
	});
	
	// Dragenter for file field
	jQuery(document).on('dragenter', '#review-file-select', function() {
	    jQuery(this).parent().addClass('active-field');
	});
	
	jQuery(document).on('dragleave dragend', '#review-file-select', function() {
	    jQuery(this).parent().removeClass('active-field');
	});
	
	
	/*----------------------------------------------------------------------------------*/
	/* WooCommerce Wishlist
	/*----------------------------------------------------------------------------------*/
	
	jQuery(document).on('click','.luv-wc-wishlist',function(e){
		e.preventDefault();
		var that 			= jQuery(this);
		var product_id		= jQuery(this).attr('data-product-id');
		var product_title	= jQuery(this).attr('data-product-title');
		var to_remove		= jQuery(this).closest(jQuery(this).attr('data-remove-closest'));
		jQuery.post(fevr.ajax_url, {'action': 'fevr_wc_wishlist','product': product_id}, function(response){
			if (response == 1){
				fevr_wc_toast(product_title + ' ' + fevr_front.translate('added to wishlist'));
				jQuery('.luv-wc-wishlist[data-product-id="' + product_id + '"]').addClass('on-my-wishlist');
			if (jQuery(that).hasClass('luv-wishlist-icon')){
					jQuery('.luv-wc-wishlist[data-product-id="' + product_id + '"]').empty().append(jQuery('<i>',{
						'class':'fa fa-heart'
					}));
				}
				else{
					jQuery('.luv-wc-wishlist[data-product-id="' + product_id + '"]').text(fevr_front.translate('Remove from wishlist'));
				}
			}
			else if (response == -1){
				fevr_wc_toast(product_title + ' ' + fevr_front.translate('removed from wishlist'));
				jQuery('.luv-wc-wishlist[data-product-id="' + product_id + '"]').removeClass('on-my-wishlist');
				if (jQuery(that).hasClass('luv-wishlist-icon')){
					jQuery('.luv-wc-wishlist[data-product-id="' + product_id + '"]').empty().append(jQuery('<i>',{
						'class':'fa fa-heart-o'
					}));
				}
				else{
					jQuery('.luv-wc-wishlist[data-product-id="' + product_id + '"]').text(fevr_front.translate('Add to wishlist'));
				}
			}
			
			// Remove container
			if (to_remove.length > 0){
				jQuery(to_remove).remove();
			}
		});
	});
	
	/*----------------------------------------------------------------------------------*/
	/* Quantity + / -
	/*----------------------------------------------------------------------------------*/

	jQuery(document).on('click', '.luv-wc-qty-plus', function(){
		jQuery(this).siblings('.qty').val(parseInt(jQuery(this).siblings('.qty').val())+1).trigger('change');
	});
	
	jQuery(document).on('click', '.luv-wc-qty-minus', function(){
		if(parseInt(jQuery(this).siblings('.qty').val()) > 0) {
			jQuery(this).siblings('.qty').val(parseInt(jQuery(this).siblings('.qty').val())-1).trigger('change');
		}
	});
	
	/*----------------------------------------------------------------------------------*/
	/* Cart
	/*----------------------------------------------------------------------------------*/
	
	// Remove item via ajax
	jQuery(document).on('click','.mini_cart_item a.remove', function(e){
		e.preventDefault();
		var that = jQuery(this);
		jQuery('body').addClass('luv-waiting');
		jQuery.get(jQuery(that).attr('href'), function(){
			jQuery('a[data-product_id="' + jQuery(that).attr('data-product_id') + '"]').removeClass('added');
			fevr_refresh_wc_cart();
			jQuery('body').removeClass('luv-waiting');
		});
	});
	
	// Add to cart toast
	jQuery('body').on('adding_to_cart', function(event, button){
		jQuery('.nav-cart').removeClass('is-hidden');

		var product_title = jQuery(button[0]).attr('data-product-title');
		fevr_wc_toast(product_title + ' ' + fevr_front.translate('added to cart'));
	});
	
	// Refresh cart via ajax
	function fevr_refresh_wc_cart(){
		jQuery('.nav-cart').removeClass('is-hidden');
		
		jQuery.post( wc_add_to_cart_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'get_refreshed_fragments' ), function( response ) {

			if ( ! response ) {
				return;
			}
			if ( response.error && response.product_url ) {
				window.location = response.product_url;
				return;
			}

			var fragments = response.fragments;
			var cart_hash = response.cart_hash;
			
			if ( fragments ) {
				jQuery.each( fragments, function( key, value ) {
					jQuery( key ).replaceWith( value );
				});
			}
			
			if (cart_hash == ''){
				jQuery('.nav-cart').addClass('is-hidden');
			}
		});
	}
	
	
	jQuery(window).on('load', function(){
		/*----------------------------------------------------------------------------------*/
		/* Single Product Gallery Slider
		/*----------------------------------------------------------------------------------*/
		
		if(jQuery('.product-gallery-carousel').length > 0) {
			var product_gallery = jQuery('.product-gallery-carousel');
			var autoplay = product_gallery.attr('data-gallery-autoplay') === "true" ? true : false;
			var navigation = product_gallery.attr('data-gallery-arrows') === "true" ? true : false;
			product_gallery.owlCarousel({
	            items: 1,
	            loop: false,
	            nav: navigation,
	            dots: false,
	            autoplay: autoplay,
	            autoplayTimeout: 3000,
	            autoplaySpeed: 700,
	            smartSpeed: 700,
	            navText: [],
	            baseClass: 'luv-carousel',
	            themeClass: 'luv-theme',
				itemClass: 'luv-carousel-item',
				controlsClass: 'luv-carousel-controls',
				navContainerClass: 'luv-carousel-nav',
				dotClass: 'luv-carousel-dot',
				dotsClass: 'luv-carousel-dots',
				autoHeightClass: 'luv-carousel-height',
			});
			
			jQuery(document).on('click', '.summary-container .thumbnails span', function(){
				var image_class = jQuery(this).attr('class');
				
				var image_index = jQuery('.summary-container .images ul li.'+image_class).parent().index();
				
				jQuery(product_gallery).trigger('to.owl.carousel', [image_index, 400, true]);
			});
		}
		
		/*----------------------------------------------------------------------------------*/
		/* Related Products
		/*----------------------------------------------------------------------------------*/

		if(jQuery('.related.products > ul.products').length > 0) {
			var related_product_gallery = jQuery('.related.products > ul.products');
			var related_product_container = related_product_gallery.closest('.products-container');
			var items;
			
			if(jQuery(related_product_container).hasClass('two-columns')) {
				items = 2;
			} else if(jQuery(related_product_container).hasClass('three-columns')) {
				items = 3;
			} else if(jQuery(related_product_container).hasClass('four-columns')) {
				items = 4;
			}
			
			if(jQuery(related_product_gallery).children('li').length > items) {
				related_product_gallery.owlCarousel({
		            responsive:{
				        0:{
				            items:1
				        },
				        461:{
				            items:2
				        },
				        769:{
				            items: items,
				        }
				    },
		            nav: false,
		            margin: 30,
		            dots: true,
		            autoplay: false,
		            navText: [],
		            baseClass: 'luv-carousel',
		            themeClass: 'luv-theme',
					itemClass: 'luv-carousel-item',
					controlsClass: 'luv-carousel-controls',
					navContainerClass: 'luv-carousel-nav',
					dotClass: 'luv-carousel-dot',
					dotsClass: 'luv-carousel-dots',
					autoHeightClass: 'luv-carousel-height',
				});
			}
		}
		
		/*----------------------------------------------------------------------------------*/
		/* Upsells Products
		/*----------------------------------------------------------------------------------*/

		if(jQuery('.upsells.products > ul.products').length > 0) {
			var upsells_product_gallery = jQuery('.upsells.products > ul.products');
			var upsells_product_container = upsells_product_gallery.closest('.products-container');
			var items;
			
			if(jQuery(upsells_product_container).hasClass('two-columns')) {
				items = 2;
			} else if(jQuery(upsells_product_container).hasClass('three-columns')) {
				items = 3;
			} else if(jQuery(upsells_product_container).hasClass('four-columns')) {
				items = 4;
			}
			
			if(jQuery(upsells_product_gallery).children('li').length > items) {
				upsells_product_gallery.owlCarousel({
		            responsive:{
				        0:{
				            items: 1
				        },
				        461:{
				            items: 2
				        },
				        769:{
				            items: items,
				        }
				    },
		            nav: false,
		            margin: 30,
		            dots: true,
		            autoplay: false,
		            navText: [],
		            baseClass: 'luv-carousel',
		            themeClass: 'luv-theme',
					itemClass: 'luv-carousel-item',
					controlsClass: 'luv-carousel-controls',
					navContainerClass: 'luv-carousel-nav',
					dotClass: 'luv-carousel-dot',
					dotsClass: 'luv-carousel-dots',
					autoHeightClass: 'luv-carousel-height',
				});
			}
		}
		
	});
});

/**
 * Check is the page in iframe
 * @returns {Boolean}
 */
function fevr_in_iframe () {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}