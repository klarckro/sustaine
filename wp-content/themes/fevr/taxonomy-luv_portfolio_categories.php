<?php
	get_header();
	
	// Classes for .container
	$fevr_container_classes = array();
	// Full width content
	if(fevr_check_luvoption('portfolio-full-width', 1)) {
		$fevr_container_classes[] = 'container-fluid';
	}
	
	// Classes for .l-grid-row
	$fevr_grid_classes = array();
	// Sidebar
	if(fevr_check_luvoption('portfolio-sidebar-position', 'no-sidebar', '!=')) {
		$fevr_grid_classes[] = 'has-sidebar';
	}
	
	// Sidebar position
	if(fevr_check_luvoption('portfolio-sidebar-position' == 'left-sidebar')) {
		$fevr_grid_classes[] = fevr_get_luvoption('portfolio-sidebar-position');
	}
	
	// Classes for .portfolio-container
	$fevr_portfolio_container_classes = array();
	// Columns
	if(fevr_check_luvoption('portfolio-columns', 'one-column')) {
		$fevr_portfolio_container_classes[] = fevr_get_luvoption('portfolio-columns').' masonry';
	}
	
	// Masonry layout
	if(fevr_check_luvoption('portfolio-masonry-layout', 'standard', '!=')) {
		$fevr_portfolio_container_classes[] = 'masonry-'.fevr_get_luvoption('portfolio-masonry-layout');
	}
	
	// Gutter
	if(fevr_check_luvoption('portfolio-masonry-gutter', '', '!=') && fevr_check_luvoption('portfolio-columns', 'one-column', '!=') && fevr_check_luvoption('portfolio-masonry-layout', 'standard','!=')) {
		$fevr_portfolio_container_classes[] = 'masonry-no-gap';
	}
	
	// Data for .portfolio-container
	$fevr_portfolio_container_data = array();
	// Data crop images (data-crop-images="")
	if(fevr_check_luvoption('portfolio-masonry-crop-images', 1)) {
		$fevr_portfolio_container_data[] = 'data-crop-images="true"';
	} else {
		$fevr_portfolio_container_data[] = 'data-crop-images="false"';
	}
	
	// Background Check (data-bg-check="")
	if(fevr_check_luvoption('portfolio-masonry-auto-text-color',1) && fevr_check_luvoption('portfolio-masonry-layout', 'meta-overlay') && fevr_check_luvoption('portfolio-masonry-hover-style', 'masonry-style-title-bottom', '!=')) {
		$fevr_portfolio_container_data[] = 'data-bg-check="true"';
	}
?>

<div id="content-wrapper">
	<?php
		// When custom page selected for portfolio we display the custom header
		if(fevr_check_luvoption('portfolio-page', '', '!=')) {
			fevr_header();
			$portfolio_page_content = get_post(fevr_get_luvoption('portfolio-page'))->post_content;
		}
	?>
	
	<div class="container <?php echo esc_attr(implode(' ', $fevr_container_classes)); ?>">
		<div class="l-grid-row <?php echo esc_attr(implode(' ', $fevr_grid_classes)); ?>">
			<main id="main-content">				
				<?php 
					// Enable Masonry Filter
					if(fevr_check_luvoption('portfolio-masonry-filter', 1)) {
						get_template_part( 'luvthemes/luv-templates/masonry-filter' );
					}
				?>
				
				<?php
					$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
				?>
				<h1 class="page-title"><?php echo esc_html($term->name); ?></h1>
				<div class="portfolio-container <?php echo esc_attr(implode(' ', $fevr_portfolio_container_classes)); ?>" <?php echo implode(' ', $fevr_portfolio_container_data); ?>>
				
				<?php
					if ( have_posts() ) :
						while ( have_posts() ) : the_post();
						$fevr_meta_fields = get_post_meta( get_the_ID(), 'fevr_meta', true);
						
						// Article classes
						$fevr_article_classes = array();
						
						// Set masonry size
						if (isset($fevr_meta_fields['portfolio-masonry-size']) && !empty($fevr_meta_fields['portfolio-masonry-size']) &&
							fevr_check_luvoption('portfolio-masonry-layout', 'meta-overlay'))
						{
							$fevr_article_classes[] = 'masonry-size-'.$fevr_meta_fields['portfolio-masonry-size'];
						}
						else {
							$fevr_article_classes[] = 'masonry-size-fevr_normal';
						}
						
						// When masonry is active without custom content we display the style for the hover effect. If we have custom content and the style is still masonry we add a helper class to disable the effects
						if (fevr_check_luvoption('portfolio-masonry-hover-style','','!=') &&
							fevr_check_luvoption('portfolio-masonry-layout', 'meta-overlay') &&
							fevr_check_luvoption('portfolio-masonry-show-content', 'enabled'))
						{
							$fevr_article_classes[] = fevr_get_luvoption('portfolio-masonry-hover-style');
						}
						else if(fevr_check_luvoption('portfolio-masonry-layout', 'meta-overlay') &&
							isset($fevr_meta_fields['portfolio-masonry-show-content']) && $fevr_meta_fields['portfolio-masonry-show-content'] == 'enabled')
						{
							$fevr_article_classes[] = 'masonry-custom-content';
						}

				?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(esc_attr(implode(' ', $fevr_article_classes))); ?>>
						<?php get_template_part( 'luvthemes/post-templates/portfolio' ); ?>
					</article>
				<?php
						endwhile;
					
					else:
						esc_html_e('No posts were found', 'fevr');
					endif;
				?>
				</div>
				
				<?php get_template_part( 'luvthemes/luv-templates/pagination' ); ?>
			</main>
			
			<?php 
				if(fevr_check_luvoption('portfolio-sidebar-position', 'no-sidebar', '!=')) {
					get_sidebar();
				}
			?>
		</div>
	</div>
</div>


<?php get_footer(); ?>