<?php
	
	get_header();
	
	// Classes for .container
	$fevr_container_classes = array();
	// Full width content
	if(fevr_check_luvoption('woocommerce-full-width', 1) && !is_single()) {
		$fevr_container_classes[] = 'container-fluid';
	}

	// Classes for .l-grid-row
	$fevr_grid_classes = array();
	// Sidebar
	if(fevr_check_luvoption('woocommerce-sidebar-position', 'no-sidebar', '!=') && (is_archive() || (is_single() && fevr_check_luvoption('woocommerce-sidebar-single', 1, '!=')))) {
		$fevr_grid_classes[] = 'has-sidebar';
	}
	
	// Sidebar position
	if(fevr_check_luvoption('woocommerce-sidebar-position', 'left-sidebar') && (is_archive() || (is_single() && fevr_check_luvoption('woocommerce-sidebar-single', 1, '!=' )))) {
		$fevr_grid_classes[] = fevr_get_luvoption('woocommerce-sidebar-position');
	}
	
	// Classes for .products-container
	$fevr_products_container_classes = array();
	// Columns
	if(fevr_check_luvoption('woocommerce-columns', '','!=')) {
		$fevr_products_container_classes[] = fevr_get_luvoption('woocommerce-columns');
	}
	
	// Product box style
	$fevr_products_container_classes[] = fevr_get_luvoption('woocommerce-product-box-style', 'wc-style-1');
	
	// No padding
	if(fevr_check_luvoption('woocommerce-product-gutter', 1)) {
		$fevr_products_container_classes[] = 'no-padding';
	}
?>

<div id="content-wrapper" class="wrapper-padding">
	<?php
		// If we have custom title/subtitle/content we display the custom header
		fevr_header();
	?>
	<div class="container <?php echo esc_attr(implode(' ', $fevr_container_classes)); ?>">
		<div class="l-grid-row <?php echo esc_attr(implode(' ', $fevr_grid_classes)); ?>">
			<main id="main-content">
				<div class="products-container item-grid-container <?php echo esc_attr(implode(' ', $fevr_products_container_classes)); ?>">
					<?php woocommerce_content(); ?>
				</div>
			</main>
			
			<?php 
				if(fevr_check_luvoption('woocommerce-sidebar-position', 'no-sidebar', '!=') && (is_archive() || (is_single() && fevr_check_luvoption('woocommerce-sidebar-single', 1, '!=')))) {
					get_sidebar();
				}
			?>
		</div>
	</div>
</div>

<?php get_footer(); ?>