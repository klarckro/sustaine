<?php
	get_header();
		
	// Classes for .container
	$fevr_container_classes = array();
	// Full width content
	if(fevr_check_luvoption('woocommerce-full-width', 1)) {
		$fevr_container_classes[] = 'container-fluid';
	}
	
	// Classes for .l-grid-row
	$fevr_grid_classes = array();
	// Sidebar
	if(fevr_check_luvoption('woocommerce-collections-sidebar-position', 'no-sidebar','!=') && fevr_check_luvoption('woocommerce-collections-sidebar-single',1,'!=')) {
		$fevr_grid_classes[] = 'has-sidebar';
	}
	
	// Sidebar position
	if(fevr_check_luvoption('woocommerce-collections-sidebar-position','left-sidebar') && fevr_check_luvoption('woocommerce-collections-sidebar-single', 1, '!=')) {
		$fevr_grid_classes[] = fevr_get_luvoption('woocommerce-collections-sidebar-position');
	}
	
	// Classes for .products-container
	$fevr_products_container_classes = array();
	// Columns
	if(fevr_check_luvoption('woocommerce-columns','','!=')) {
		$fevr_products_container_classes[] = fevr_get_luvoption('woocommerce-columns');
	}
	
	// Product box style
	if(fevr_check_luvoption('woocommerce-product-box-style', '', '!=')) {
		$fevr_products_container_classes[] = fevr_get_luvoption('woocommerce-product-box-style');
	} else {
		$fevr_products_container_classes[] = 'wc-style-1';
	}
?>

<div id="content-wrapper">
	<?php fevr_header(); ?>
	<div class="container <?php echo esc_attr(implode(' ', $fevr_container_classes)); ?>">
		<div class="l-grid-row  <?php echo esc_attr(implode(' ', $fevr_grid_classes)); ?>">
			<main id="main-content">
				<div class="products-container <?php echo esc_attr(implode(' ', $fevr_products_container_classes)); ?>">
					<?php get_template_part( 'luvthemes/post-templates/collections', 'single' ); ?>
				</div>
				
				<?php if(fevr_check_luvoption('woocommerce-collections-social-meta',1, '!=')): ?>
				<div id="single-share-box" class="full-width-section">
					<div class="container">
						<?php get_template_part('luvthemes/luv-templates/social'); ?>
					</div>
				</div>
				<?php endif; ?>
			</main>
			
			<?php 
				if(fevr_check_luvoption('woocommerce-collections-sidebar-position', 'no-sidebar', '!=') && fevr_check_luvoption('woocommerce-collections-sidebar-single', 1, '!=')) {
					get_sidebar();
				}
			?>
		</div>
	</div>
</div>

<?php get_footer(); ?>