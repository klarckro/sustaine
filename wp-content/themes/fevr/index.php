<?php
	get_header();

	// Classes for .container
	$fevr_container_classes = array();
	// Full width content
	if (fevr_check_luvoption('blog-full-width', 1)) {
		$fevr_container_classes[] = 'container-fluid';
	}
	
	// Classes for .l-grid-row
	$fevr_grid_classes = array();
	// Sidebar
	if(fevr_check_luvoption('blog-sidebar-position', 'no-sidebar', '!=')) {
		$fevr_grid_classes[] = 'has-sidebar';
	}
	
	// Sidebar position
	if(fevr_check_luvoption('blog-sidebar-position', 'left-sidebar')) {
		$fevr_grid_classes[] = fevr_get_luvoption('blog-sidebar-position');
	}
	
	// Classes for .posts-container and #content-wrapper
	$fevr_posts_container_classes = array();
	$fevr_content_wrapper_classes = array();
	// Columns
	if(fevr_check_luvoption('blog-layout-style', 'masonry') && fevr_check_luvoption('blog-columns', '','!=')) {
		$fevr_posts_container_classes[] = fevr_get_luvoption('blog-columns');
	}
	
	// Alternate layout
	if(fevr_check_luvoption('blog-layout-style', 'alternate') && fevr_check_luvoption('blog-alternate-same-column', 0)) {
		$fevr_posts_container_classes[] = 'varied-columns';
	}
	
	// Layout Style (alternate, standard, masonry)
	if(fevr_check_luvoption('blog-layout-style', '', '!=')) {
		$fevr_posts_container_classes[] = fevr_get_luvoption('blog-layout-style');
	}
	
	// Gutter when we use masonry layout
	if(fevr_check_luvoption('blog-masonry-gutter') && fevr_check_luvoption('blog-masonry-layout', 'meta-overlay') && fevr_check_luvoption('blog-layout-style','masonry')) {
		$fevr_posts_container_classes[] = 'masonry-no-gap';
	}
	
	// Wrapper Class
	if(!(fevr_check_luvoption('blog-masonry-gutter', 1) && fevr_check_luvoption('blog-masonry-layout', 'meta-overlay') && fevr_check_luvoption('blog-layout-style', 'masonry'))) {
		$fevr_content_wrapper_classes[] = 'wrapper-padding';
	}
	
	// Masonry layout style
	if(fevr_check_luvoption('blog-masonry-layout', '', '!=') && fevr_check_luvoption('blog-layout-style', 'masonry')) {
		$fevr_posts_container_classes[] = 'masonry-'.fevr_get_luvoption('blog-masonry-layout');
	}
	
	// Masonry rounded corners
	if(fevr_check_luvoption('blog-layout-style', 'masonry') && fevr_check_luvoption('blog-masonry-rounded-corners', 1)) {
		$fevr_posts_container_classes[] = 'masonry-rounded-corners';
	}
	
	// Masonry shadows
	if(fevr_check_luvoption('blog-layout-style', 'masonry') && fevr_check_luvoption('blog-masonry-shadows', 1)) {
		$fevr_posts_container_classes[] = 'masonry-shadows';
	}
	
	// Masonry equal height
	$masonry_equal_height = false;
	if(fevr_check_luvoption('blog-masonry-layout', 'standard') && fevr_check_luvoption('blog-layout-style', 'masonry') && fevr_check_luvoption('blog-masonry-equal-height', 1)) {
		$fevr_posts_container_classes[] = 'masonry-equal-height';
		$masonry_equal_height = true;
	}
	
	// Data for .posts-container
	$fevr_posts_container_data = array();
	// Data crop images (data-crop-images="")
	if(fevr_check_luvoption('blog-masonry-crop-images', 1)) {
		$fevr_posts_container_data[] = 'data-crop-images="true"';
	} else {
		$fevr_posts_container_data[] = 'data-crop-images="false"';
	}
	
	// Background Check (data-bg-check="")
	if(fevr_check_luvoption('blog-masonry-auto-text-color', 1) && fevr_check_luvoption('blog-layout-style', 'masonry') && fevr_check_luvoption('blog-masonry-layout', 'meta-overlay') && fevr_check_luvoption('blog-masonry-hover-style', 'masonry-style-title-bottom','!=')) {
		$fevr_posts_container_data[] = 'data-bg-check="true"';	
	}
	
	// Automatic Metro Layout
	if (fevr_check_luvoption('blog-automatic-metro-layout', 1)){
		global $wp_query;
		$fevr_masonry_size_overrides = array();
	
		switch (fevr_get_luvoption('blog-columns')){
			case 'auto-columns':
				$columns = 5;
				break;
			case 'four-columns':
				$columns = 4;
				break;
			case 'three-columns':
				$columns = 3;
				break;
			case 'two-columns':
				$columns = 2;
				break;
			case 'one-column':
				$columns = 1;
				break;
		}
		$remand = (int)$wp_query->post_count % $columns;
		$count	= (int)$wp_query->post_count;
	
		if ($columns == 5 && $wp_query->post_count >= $columns){
			if ($remand == 0){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_tall';
				$fevr_masonry_size_overrides[$wp_query->posts[1]->ID] = 'fevr_wide';
				$fevr_masonry_size_overrides[$wp_query->posts[2]->ID] = 'fevr_wide_tall';
			}
			else if ($remand == 1){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_wide_tall';
				$fevr_masonry_size_overrides[$wp_query->posts[5]->ID] = 'fevr_wide';
			}
			else if ($remand == 2){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_wide_tall';
				if ($count >= 8){
					$fevr_masonry_size_overrides[$wp_query->posts[3]->ID] = 'fevr_wide';
					$fevr_masonry_size_overrides[$wp_query->posts[5]->ID] = 'fevr_wide';
					$fevr_masonry_size_overrides[$wp_query->posts[4]->ID] = 'fevr_tall';
					$fevr_masonry_size_overrides[$wp_query->posts[$count-2]->ID] = 'fevr_wide';
					$fevr_masonry_size_overrides[$wp_query->posts[$count-5]->ID] = 'fevr_tall';
				}
			}
			else if ($remand == 3){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_tall';
				$fevr_masonry_size_overrides[$wp_query->posts[1]->ID] = 'fevr_wide';
				if ($count >= 8){
					$fevr_masonry_size_overrides[$wp_query->posts[$count-4]->ID] = 'fevr_tall';
					$fevr_masonry_size_overrides[$wp_query->posts[$count-5]->ID] = 'fevr_tall';
					$fevr_masonry_size_overrides[$wp_query->posts[$count-8]->ID] = 'fevr_wide_tall';
				}
			}
			else if ($remand == 4){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_wide_tall';
				$fevr_masonry_size_overrides[$wp_query->posts[5]->ID] = 'fevr_wide_tall';
			}
		}
	
		if ($columns == 4 && $wp_query->post_count >= $columns){
			if ($remand == 0){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_wide_tall';
				$fevr_masonry_size_overrides[$wp_query->posts[1]->ID] = 'fevr_wide';
				if ($count >= 8){
					$fevr_masonry_size_overrides[$wp_query->posts[$count-1]->ID] = 'fevr_wide';
					$fevr_masonry_size_overrides[$wp_query->posts[$count-3]->ID] = 'fevr_wide';
					$fevr_masonry_size_overrides[$wp_query->posts[$count-4]->ID] = 'fevr_tall';
					$fevr_masonry_size_overrides[$wp_query->posts[$count-5]->ID] = 'fevr_tall';
				}
			}
			else if ($remand == 1){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_wide_tall';
			}
			else if ($remand == 2){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_tall';
				$fevr_masonry_size_overrides[$wp_query->posts[1]->ID] = 'fevr_wide';
			}
			else if ($remand == 3){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_wide';
			}
		}
	
		if ($columns == 3 && $wp_query->post_count >= $columns){
			if ($remand == 0){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_wide_tall';
			}
			else if ($remand == 1){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_tall';
				$fevr_masonry_size_overrides[$wp_query->posts[1]->ID] = 'fevr_wide';
			}
			else if ($remand == 2){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_wide';
			}
		}
	
		if ($columns == 2 && $wp_query->post_count >= $columns){
			if ($remand == 0){
				$fevr_masonry_size_overrides[$wp_query->posts[0]->ID] = 'fevr_wide';
				$fevr_masonry_size_overrides[$wp_query->posts[1]->ID] = 'fevr_tall';
			}
			else if ($remand == 1){
				$fevr_masonry_size_overrides[$wp_query->posts[1]->ID] = 'fevr_wide';
			}
		}
	}
	
?>
<div id="content-wrapper" class="<?php echo esc_attr(implode(' ', $fevr_content_wrapper_classes));?>">
	<?php fevr_header(); ?>
	<div class="container <?php echo esc_attr(implode(' ', $fevr_container_classes)); ?>">
		<div class="l-grid-row  <?php echo esc_attr(implode(' ', $fevr_grid_classes)); ?>">
			<main id="main-content">
				
				<?php 
					// Enable Masonry Filter
					if(fevr_check_luvoption('blog-layout-style', 'masonry') && fevr_check_luvoption('blog-masonry-filter', 1)) {
						get_template_part( 'luvthemes/luv-templates/masonry-filter' );
					}
				?>
				
				<div class="posts-container item-grid-container <?php echo esc_attr(implode(' ', $fevr_posts_container_classes)); ?>" <?php echo esc_html(implode(' ', $fevr_posts_container_data)); ?>>
				<?php
					if ( have_posts() ) :
						while ( have_posts() ) : the_post();
							$fevr_meta_fields = get_post_meta( get_the_ID(), 'fevr_meta', true);
							
							// Automatic Metro Layout
							if (fevr_check_luvoption('blog-automatic-metro-layout', 1)){
								if (isset($fevr_masonry_size_overrides[get_the_ID()])){
									$fevr_meta_fields['post-masonry-size'] = $fevr_masonry_size_overrides[get_the_ID()];
								}
								else{
									$fevr_meta_fields['post-masonry-size'] = 'fevr_normal';
								}
							}
							
							// Article classes
							$fevr_article_classes = array();
							
							// Expired
							if(isset($fevr_meta_fields['post-expiration-date']) && !empty($fevr_meta_fields['post-expiration-date']) && strtotime($fevr_meta_fields['post-expiration-date']) < time()){
								$fevr_article_classes[] = 'expired-post';
								$fevr_expiration_message = isset($fevr_meta_fields['post-expiration-message']) ? $fevr_meta_fields['post-expiration-message'] : '';
							}
							
							// Set masonry size
							if (isset($fevr_meta_fields['post-masonry-size']) && !empty($fevr_meta_fields['post-masonry-size']) &&
								fevr_check_luvoption('blog-masonry-layout', 'meta-overlay') &&
								fevr_check_luvoption('blog-layout-style', 'masonry'))
							{
								$fevr_article_classes[] = 'masonry-size-'.$fevr_meta_fields['post-masonry-size'];
							} else if(fevr_check_luvoption('blog-masonry-layout', 'meta-overlay') &&
									fevr_check_luvoption('blog-layout-style', 'masonry')) {
								$fevr_article_classes[] = 'masonry-size-fevr_normal';
							}
							
							// Masonry Style for Standard Layout
							if (isset($fevr_meta_fields['post-masonry-style']) && !empty($fevr_meta_fields['post-masonry-style']) &&
								fevr_check_luvoption('blog-masonry-layout', 'standard') &&
								fevr_check_luvoption('blog-layout-style', 'masonry'))
							{
								$fevr_article_classes['background-image-style'] = 'masonry-style-'.$fevr_meta_fields['post-masonry-style'];
								
								if($fevr_meta_fields['post-masonry-style'] == 'background-image' || ($masonry_equal_height && in_array(get_post_format(), array('gallery', 'image')) && $fevr_meta_fields['post-masonry-style'] != 'featured') ) {
									if(has_post_thumbnail()) {
										$background_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'fevr_tall');
										$fevr_article_classes[] = fevr_enqueue_inline_css(array('style' => 'background-image: url('.$background_image[0].');'));
										$fevr_article_classes['background-image-style'] = 'masonry-style-background-image';
									}
								}
								
								if($fevr_meta_fields['post-masonry-style'] == 'featured') {
									$fevr_article_classes[] = 'masonry-size-fevr_wide';
								}
							}
							
							// When masonry is active without custom content we display the style for the hover effect. If we have custom content and the style is still masonry we add a helper class to disable the effects
							if (fevr_check_luvoption('blog-masonry-hover-style','','!=') &&
								fevr_check_luvoption('blog-masonry-layout', 'meta-overlay') &&
	 							fevr_check_luvoption('blog-layout-style', 'masonry') &&
								(!isset($fevr_meta_fields['post-masonry-show-content']) || $fevr_meta_fields['post-masonry-show-content'] != 'enabled'))
							{
								$fevr_article_classes[] = fevr_get_luvoption('blog-masonry-hover-style');
							} 
							else if (fevr_check_luvoption('blog-masonry-layout', 'meta-overlay') &&
								fevr_check_luvoption('blog-layout-style', 'masonry') &&
								isset($fevr_meta_fields['post-masonry-show-content']) && $fevr_meta_fields['post-masonry-show-content'] == 'enabled')
							{
								$fevr_article_classes[] = 'masonry-custom-content';
							}
							
							// Animation
							if(fevr_check_luvoption('blog-animation','','!=')){
								$fevr_article_classes[] = 'c-has-animation ' . fevr_get_luvoption('blog-animation');
							}
							
							// Hide thumbnail/video/audio if background image is in use
							$fevr_show_thumbnail = true;
	
							if(!is_single() && isset($fevr_meta_fields['post-masonry-style']) && $fevr_meta_fields['post-masonry-style'] == 'background-image' &&
						           fevr_check_luvoption('blog-masonry-layout', 'standard') &&
						           fevr_check_luvoption('blog-layout-style', 'masonry')) {
						        $fevr_show_thumbnail = false;
						    }
						    
						    if($masonry_equal_height && in_array(get_post_format(), array('gallery', 'image'))) {
							    $fevr_show_thumbnail = false;
						    }
							
							// Background image for featured masonry style
							$fevr_featured_class = '';
							if(!is_single() && isset($fevr_meta_fields['post-masonry-style']) && $fevr_meta_fields['post-masonry-style'] == 'featured' &&
						            fevr_check_luvoption('blog-masonry-layout', 'standard') &&
						            fevr_check_luvoption('blog-layout-style', 'masonry')) {
								
								$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'fevr_wide_tall' );
								$fevr_featured_class = fevr_enqueue_inline_css(array('style' => 'background-image: url('.$image[0].');'));
							}
				?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(esc_attr(implode(' ', $fevr_article_classes))); ?>>
							<?php 
								if((is_home() || is_archive()) && fevr_check_luvoption('blog-masonry-layout', 'meta-overlay') && fevr_check_luvoption('blog-layout-style', 'masonry')) {
									get_template_part( 'luvthemes/post-templates/post', 'masonry' );
								} elseif((is_home() || is_archive()) && fevr_check_luvoption('blog-layout-style', 'alternate')) {
									get_template_part( 'luvthemes/post-templates/post', 'alternate' );
								} else {
									get_template_part( 'luvthemes/post-templates/post', get_post_format() );
								}
							?>
						</article>
				<?php
						endwhile;
					
					else:
						esc_html_e('No posts were found', 'fevr');
					endif;
				?>
				</div>
				
				<?php get_template_part( 'luvthemes/luv-templates/pagination' ); ?>
			</main>
			
			<?php 
				if(fevr_check_luvoption('blog-sidebar-position', 'no-sidebar', '!=')) {
					get_sidebar();
				}
			?>
		</div>
	</div>
</div>

<?php get_footer(); ?>