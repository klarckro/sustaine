<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>	
	<link rel="pingback" href="<?php echo esc_url(get_bloginfo('pingback_url')); ?>">	
	<?php wp_head(); ?>
</head>
<body <?php do_action('fevr_body_attributes')?> <?php body_class(); ?>>
	<?php do_action('fevr_custom_after_body_html');?>
	<?php do_action('fevr_loader_animation');?>
	<div id="l-wrapper">
	<?php do_action('fevr_header_search');?>
	<?php do_action('fevr_top_bar');?>
		<div id="l-wrapper-inner">
			<?php 
				// Hide page header
				global $fevr_meta_fields;
			?>
			<?php if (!isset($fevr_meta_fields['page-header-hide']) || $fevr_meta_fields['page-header-hide'] != 'enabled'):?>
			<?php do_action('fevr_main_header_outer_start');?>
			<header id="main-header" class="<?php echo do_action('fevr_main_header_classes');?>" <?php do_action('fevr_main_header_attributes');?>>
				<div class="container">
					<div class="main-header-inner">
						<?php do_action('fevr_print_logo');?>
						<?php do_action('fevr_nav_primary');?>
					</div>
				</div>
			<?php do_action('fevr_wc_cart');?>
			</header>
			<?php do_action('fevr_main_header_outer_end');?>
			<?php endif;?>
			<div id="mobile-nav-overlay"></div>
			<?php do_action('fevr_mobile_nav');?>
			<?php do_action('fevr_overlay_nav');?>
			<?php do_action('fevr_off_canvas_nav');?>
			<?php do_action('fevr_header_custom_content');?>