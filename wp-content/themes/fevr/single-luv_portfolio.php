<?php get_header(); ?>	
	<?php if ( have_posts() ) :
			while ( have_posts() ) : the_post();
	?>

				<div id="content-wrapper">
					<?php
						// If we have custom title/subtitle/content we display the custom header
						fevr_header();
						
						if(!empty($fevr_header_title) || !empty($fevr_header_subtitle) || !empty($fevr_header_content) || !empty($fevr_header_background) || !empty($fevr_header_background_color) || !empty($fevr_header_video_mp4) || !empty($fevr_header_video_ogv) || !empty($fevr_header_video_embedded)) {
							$fevr_grid_classes[] = 'has-custom-page-header';
						} else {
							$fevr_grid_classes[] = 'has-default-page-header';
						}
					?>
					<div class="container">
						<div class="l-grid-row <?php echo esc_attr(implode(' ', $fevr_grid_classes)); ?>">
							<main id="main-content">
								<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
									<?php get_template_part( 'luvthemes/post-templates/portfolio', 'single' ); ?>
								</article>
					<?php
							endwhile;
						
						else:
							esc_html_e('No posts were found', 'fevr');
						endif;
					?>
								
								<?php
									$fevr_like	= fevr_check_luvoption('portfolio-likes-meta','hide-on-single','!=') && fevr_check_luvoption('portfolio-likes-meta','hide-on-both','!=');
									$fevr_share = fevr_check_luvoption('portfolio-social-meta', 1, '!=');
								?>
								
								<?php if(($fevr_like || $fevr_share) && fevr_check_luvoption('portfolio-social-position',0)): ?>
								<div id="single-share-box" class="full-width-section">
									<div class="container">
										<?php get_template_part('luvthemes/luv-templates/social'); ?>
									</div>
								</div>
								<?php endif; ?>
								
								<?php get_template_part('luvthemes/luv-templates/footer-pagination'); ?>
								
								<?php
									if ((comments_open() || get_comments_number()) && fevr_check_luvoption('portfolio-comments-feature', 1, '!=')) {
										comments_template();
									}
								?>
							</main>
						</div>
					</div>
				</div>
<?php get_footer(); ?>