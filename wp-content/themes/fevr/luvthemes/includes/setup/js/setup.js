(function() {
	jQuery(function() {		
		// Init WP Color Picker
		jQuery('.color-field').wpColorPicker();
		
		// Plugin activation
		jQuery(document).on('click', '.luv-setup-plugin-action', function(e){
			e.preventDefault();
			jQuery('body').append('<div id="loader-overlay"><div class="loader"><svg class="loader-circular" viewBox="25 25 50 50"><circle class="loader-path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div></div>');
			jQuery.get(jQuery(this).attr('href'), function(){
				jQuery('#luv-setup-plugin-table').load(fevr_setup.ajax_url, {'action' : 'fevr_setup', 'luvthemes-nonce' : fevr_setup.nonce, 'ajax-action' : 'install_plugins_page'}, function(){
					jQuery('#loader-overlay').remove();
				});
			});			
		});
		
		// Bulk actions
		jQuery(document).on('click', '.luv-setup-install-plugins #doaction, .luv-setup-install-plugins #doaction2, .luv-setup-install-plugins .luv-setup-next', function(e){
			if (jQuery('.plugin.column-plugin').length > 0){
				e.preventDefault();
				var ajax_reload = !jQuery(this).hasClass('luv-setup-next');
				jQuery('body').append('<div id="loader-overlay"><div class="loader"><svg class="loader-circular" viewBox="25 25 50 50"><circle class="loader-path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div></div>');
				jQuery.post(fevr_setup.tgm_endpoint, jQuery('#luvthemes-setup-form').serialize(), function(){
					if (ajax_reload){
						jQuery('#luv-setup-plugin-table').load(fevr_setup.ajax_url, {'action' : 'fevr_setup', 'luvthemes-nonce' : fevr_setup.nonce, 'ajax-action' : 'install_plugins_page'}, function(){
							jQuery('#loader-overlay').remove();
						});
					}
					else{
						jQuery('#luvthemes-setup-form').submit();
					}
				});
			}
		});
    });
	
	/*----------------------------------------------------------------------------------*/
	/* Media Uploader
	/*----------------------------------------------------------------------------------*/
	
	jQuery(document).on('click', '.luv-media-upload', function(){
		luv_media_upload(jQuery(this), false);
	});
	
	jQuery(document).on('click', '.luv-media-upload-reset', function(){
		jQuery(this).closest('.luv-media-upload-container').find('.luv-media-upload-url, .luv-media-upload-by-id').val('').trigger('change');
        jQuery(this).closest('.luv-media-upload-container').find('.luv-media-upload-preview').attr('src', '').addClass('luv-hidden');
        jQuery(this).addClass('luv-hidden');
        jQuery(this).closest('.luv-media-upload-container').find('.luv-media-upload, .luv-media-upload-by-id').text(__('Upload'));
        jQuery(this).closest('.luv-media-upload-container').find('.luv-repeat-field').addClass('luv-hidden');
	});
	
	/**
	 * Media uploader
	 * @param target
	 * @param byid use attachment id instead of URL. Default false
	 */
	function luv_media_upload(target, byid) {
		var byid = byid || false;
	    media_uploader = wp.media({
	        frame:    "post",
	        state:    "insert",
	        multiple: false
	    });

	    media_uploader.on("insert", function(){
	        var json = media_uploader.state().get("selection").first().toJSON();

	        var image_url = byid === true ? json.id : json.url;
	        target.closest('.luv-media-upload-container').find('.luv-media-upload-url').val(image_url).trigger('change');
	        target.closest('.luv-media-upload-container.media-image').find('.luv-media-upload-preview').attr('src', image_url).removeClass('luv-hidden');
	        target.next().removeClass('luv-hidden');
	        target.next().next().removeClass('luv-hidden');
	        target.text(__('Modify'));
	    });

	    media_uploader.open();
	}
	
	/**
	 * Localization
	 * @param string text
	 * @return string
	 */
	function __(text){
		if (typeof fevr_setup.i18n[text] !== 'undefined'){
			return fevr_setup.i18n[text];
		}
		else {
			return text;
		}
	}
})();