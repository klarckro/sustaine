<?php
if (! class_exists ( 'WP_List_Table' )) {
	require_once (ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class Fevr_Setup_Pages_List_Table extends WP_List_Table {
	
	/**
	 * Table content
	 * @var array
	 */
	public $table_data = array ();
	
	/**
	 * Construct for list table
	 * @param array $table_data
	 */
	public function __construct() {
		global $wpdb;
		
		parent::__construct ( array (
				'singular' => esc_html__ ( 'Pages', 'fevr' ),
				'plural' => esc_html__ ( 'Pages', 'fevr' ),
				'ajax' => false 
		) );
		
		$this->table_data = $wpdb->get_results('SELECT * FROM ' . $wpdb->posts .' WHERE `post_type` = "page" AND  post_status IN ("publish", "draft")', ARRAY_A);
		
	}
	
	/**
	 * Colunm name
	 * @param array $item
	 * @param string $column_name
	 * @return string
	 */
	public function column_default($item, $column_name) {
		switch ($column_name) {
			case 'post_title' :
				return '<a href="'.esc_url(admin_url('post.php?post='.$item['ID'].'&action=edit')) .'" target="_blank">' . (isset($item['post_title']) && !empty($item['post_title']) ? $item['post_title'] : esc_html__('No title', 'fevr')) . '</a>';
			case 'post_author' :
				$user_info = get_userdata($item['post_author']);
				return $user_info->display_name;
			case 'post_date' :
				return $item[$column_name];
		}
	}
	
	/**
	 * Get all columns and set names to show
	 * @return array
	 */
	public function get_columns() {
		$columns = array (
				'post_title' => esc_html__('Title', 'fevr' ),
				'post_author' => esc_html__ ( 'Author', 'fevr' ),
				'post_date' => esc_html__ ( 'Date', 'fevr' ) 
				);
		return $columns;
	}
	
	/**
	 * Get sortable columns
	 * @return array
	 */
	public function get_sortable_columns() {
		$sortable_columns = array (
				'post_title' => array (
						'post_title',
						false 
				),
				'post_author' => array (
						'post_author',
						false 
				),
				'post_date' => array (
						'post_date',
						true
				)
		);
		return $sortable_columns;
	}
	
	/**
	 * Get column type
	 * @return string
	 */
	public function get_column_type($column) {
		switch ($column){
			case 'post_title':
				return 'string';				
			case 'post_author':
				return 'string';			
			case 'post_date':
				return 'date';
			default:
				return 'string';
		}
	}
	
	/**
	 * Sorting function
	 * @param string $a
	 * @param string $b
	 * @return array
	 */
	public function usort_reorder( $a, $b ) {
		// If no sort, default to name
		$orderby = ( ! empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'post_date';
		// If no order, default to asc
		$order = ( ! empty($_GET['order'] ) ) ? $_GET['order'] : 'desc';
		// Determine column type and sort order 
		if ($this->get_column_type($orderby) == 'number'){
			// Number
			$result = ($a[$orderby] > $b[$orderby] ? 1 : ($a[$orderby] == $b[$orderby] ? 0 : -1 ));
		}	
		else if ($this->get_column_type($orderby) == 'date'){
			// Date
			$result = (strtotime($a[$orderby]) > strtotime($b[$orderby]) ? 1 : (strtotime($a[$orderby]) == strtotime($b[$orderby]) ? 0 : -1 ));
		}
		else{	
			// String
			$result = strcmp( $a[$orderby], $b[$orderby] );
		}
		// Send final sort direction to usort
		return ( $order === 'asc' ) ? $result : -$result;
	}

	/**
	 * Actions and rewrites for column "Title"
	 * @param array $item
	 */
	public function column_post_title($item) {
		return  '<a href="'.esc_url(admin_url('post.php?post='.$item['ID'].'&action=edit')).'" target="_blank">' . (isset($item['post_title']) && !empty($item['post_title']) ? $item['post_title'] : esc_html__('No title', 'fevr')) . '</a>'.
				'<div class="row-actions">'.
				'<span class="edit"><a href="'.esc_url(admin_url('post.php?post='.$item['ID'].'&action=edit')).'" target="_blank">' . esc_html__('Edit', 'fevr'). '</a></span> | '.
				'<span class="trash"><a href="'.esc_url(wp_nonce_url(admin_url('post.php?post='.$item['ID'].'&action=trash')), 'trash-post_' . $item['ID']).'" class="submitdelete">' . esc_html__('Trash', 'fevr'). '</a></span>'.
				'</div>';
	}
	
	/**
	 * Prepare items to show
	 */
	public function prepare_items() {
		global $wpdb;
		$columns = $this->get_columns();
		$hidden = array ();
		$sortable = $this->get_sortable_columns ();
		$this->_column_headers = array (
				$columns,
				$hidden,
				$sortable 
		);
			
		if (!empty($this->table_data)){
			usort ( $this->table_data, array (
					&$this,
					'usort_reorder' 
			) );
			
			$per_page = 10;
			$current_page = $this->get_pagenum();
			$total_items = count($this->table_data);
			
			$this->table_data = array_slice($this->table_data, (($current_page - 1) * $per_page), $per_page);
			
			$this->set_pagination_args ( array (
					'total_items' => $total_items,
					'per_page' => $per_page )
			);
		}
		$this->items = $this->table_data;
	}
}
?>