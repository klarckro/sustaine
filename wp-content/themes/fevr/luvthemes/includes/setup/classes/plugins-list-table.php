<?php
class Fevr_Setup_Plugins_List_Table extends TGMPA_List_Table{
	public function get_views(){
		return;
	}	

	public function extra_tablenav($which){
		return;
	}
	
	
	public function no_items() {
		echo  esc_html__('No plugins to install, update or activate.', 'fevr');
	}
	
	/**
	 * Get the actions which are relevant for a specific plugin row.
	 *
	 * @since 2.5.0
	 *
	 * @param array $item Array of item data.
	 * @return array Array with relevant action links.
	 */
	protected function get_row_actions( $item ) {
		$actions      = array();
		$action_links = array();
	
		// Display the 'Install' action link if the plugin is not yet available.
		if ( ! $this->tgmpa->is_plugin_installed( $item['slug'] ) ) {
			$actions['install'] = esc_html_x( 'Install %2$s', '%2$s = plugin name in screen reader markup', 'fevr' );
		} else {
			// Display the 'Update' action link if an update is available and WP complies with plugin minimum.
			if ( false !== $this->tgmpa->does_plugin_have_update( $item['slug'] ) && $this->tgmpa->can_plugin_update( $item['slug'] ) ) {
				$actions['update'] = esc_html_x( 'Update %2$s', '%2$s = plugin name in screen reader markup', 'fevr' );
			}
	
			// Display the 'Activate' action link, but only if the plugin meets the minimum version.
			if ( $this->tgmpa->can_plugin_activate( $item['slug'] ) ) {
				$actions['activate'] = esc_html_x( 'Activate %2$s', '%2$s = plugin name in screen reader markup', 'fevr' );
			}
		}
	
		// Create the actual links.
		foreach ( $actions as $action => $text ) {
			$nonce_url = wp_nonce_url(
					add_query_arg(
							array(
									'plugin'           => urlencode( $item['slug'] ),
									'tgmpa-' . $action => $action . '-plugin',
							),
							$this->tgmpa->get_tgmpa_url()
							),
					'tgmpa-' . $action,
					'tgmpa-nonce'
					);
	
			$action_links[ $action ] = sprintf(
					'<a href="%1$s" class="luv-setup-plugin-action">' . esc_html( $text ) . '</a>',
					esc_url( $nonce_url ),
					'<span class="screen-reader-text">' . esc_html( $item['sanitized_plugin'] ) . '</span>'
					);
		}
	
		$prefix = ( defined( 'WP_NETWORK_ADMIN' ) && WP_NETWORK_ADMIN ) ? 'network_admin_' : '';
		return apply_filters( "tgmpa_{$prefix}plugin_action_links", array_filter( $action_links ), $item['slug'], $item, $this->view_context );
	}
}
?>