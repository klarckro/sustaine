<?php

class Fevr_Setup {

	/**
	 * Array of steps
	 * @var array
	 */
	public $steps = array();
	
	/**
	 * Current step
	 * @var array
	 */
	public $current_step = array();

	/**
	 * Show steps in footer
	 * @var boolean
	 */
	public $show_steps = true;
	
	/**
	 * Localization array for JS
	 * @var array
	 */
	public $localize = array();
	
	/**
	 * Catch pseudo function calls and do nothing (we use init for early catch the page);
	 * @param string $function
	 * @param array $params
	 */
	public function __call($function, $params){
		// Do nothing
	}
	
	/**
	 * Create instance
	 */
	public function __construct() {
		// Return if page is not the Luvthemes Setup Wizard page
		if (!isset($_GET['page']) || $_GET['page'] != 'fevr_setup'){
			return false;
		}
		
		if (defined('DOING_AJAX')){
			add_filter( 'tgmpa_load', '__return_true');
		}
		
		
		// Set installer directory path
		if (!defined('FEVR_SETUP_DIR')){
			define ('FEVR_SETUP_DIR', trailingslashit(get_template_directory()) . 'luvthemes/includes/setup/');
		}
		
		// Set installer directory URI
		if (!defined('FEVR_SETUP_URI')){
			define('FEVR_SETUP_URI', trailingslashit(get_template_directory_uri()) . 'luvthemes/includes/setup/');
		}
		
		// Init steps
		$this->steps = array(
				array(
					'title'	=> (isset($_REQUEST['luvthemes-nonce']) ? esc_html__('Purchase key', 'fevr') : esc_html__('Welcome', 'fevr')),
					'id'	=> 'purchase-key',
				),
				array(
					'title'	=> esc_html__('Install Plugins', 'fevr'),
					'id'	=> 'install-plugins',
				),
				array(
					'title'	=> esc_html__('Basic settings', 'fevr'),
					'id'	=> 'basic-settings',
				),
				array(
					'title'	=> esc_html__('Create pages', 'fevr'),
					'id'	=> 'create-pages',
				),
				array(
					'title'	=> esc_html__('Finish', 'fevr'),
					'id'	=> 'finish',
				)
		);
			
		// Load list table classes
		include 'classes/plugins-list-table.php';
		include 'classes/pages-list-table.php';
		
		// Init
		add_action('admin_init', array($this, 'init'));
		
		// Ajax handlers
		add_action('wp_ajax_fevr_setup', array($this, 'ajax_handler'));
		
		// Change wp title
		add_action('wp_title', array($this, 'wp_title'));
	}
	
	/**
	 * Init setup wizard
	 */
	public function init(){		
		$fevr_purchase_key = fevr_purchase_key();
		
		if (!current_user_can('manage_options')){
			return;
		}
		
		// Localization
		$this->localize = array(
				'i18n' => array(
						'Upload' => esc_html__('Upload', 'fevr'),
						'Modify' => esc_html__('Modify', 'fevr'),
						'Please wait...' => esc_html__('Please wait...', 'fevr')
				),
				'ajax_url'		=> add_query_arg('page', 'fevr_setup', admin_url('admin-ajax.php')),
				'nonce'			=> wp_create_nonce('fevr-setup'),
				'tgm_endpoint'	=> add_query_arg('page',$GLOBALS['tgmpa']->menu,admin_url('admin.php'))
		);
		
		// Enqueue Setup Wizard CSS
		wp_enqueue_style('fevr-setup', trailingslashit(get_template_directory_uri()) . 'luvthemes/includes/setup/css/setup.css');
		
		//WP admin styles
		wp_enqueue_style( 'wp-admin' );
		
		// Add the color picker css file
		wp_enqueue_style( 'wp-color-picker' );
			
		// Include our custom jQuery file with WordPress Color Picker dependency
		wp_enqueue_script( 'fevr-setup', trailingslashit(get_template_directory_uri()) . 'luvthemes/includes/setup/js/setup.js', array( 'wp-color-picker', 'jquery-ui-sortable'), false, true );
		wp_localize_script('fevr-setup', 'fevr_setup', $this->localize);
		
		
		// Enqueue Luvthemes assets
		add_action('fevr_setup_enqueue_scripts','fevr_admin_scripts',0);
			
		// Set current step
		if (!empty($fevr_purchase_key)){
			$step = isset($_REQUEST['step']) ? (int)$_REQUEST['step'] : 1;
			unset($this->steps[0]);
		}
		else{
			$step = isset($_REQUEST['step']) ? (int)$_REQUEST['step'] : 0;
		}
		$this->current_step 			= $this->steps[$step];
		$this->current_step['index']	= $step;
		
		// Render step
		$this->render();
	}
	
	/**
	 * Render current step
	 */
	public function render(){	
		$template = 'start-wizard';
		
		if (defined('DOING_AJAX')){
			$GLOBALS['hook_suffix'] = 'fevr_setup';
			return;
		}
		
		// Verify nonce
		if (isset($_REQUEST['luvthemes-nonce']) && wp_verify_nonce($_REQUEST['luvthemes-nonce'], 'fevr-setup') && current_user_can('manage_options')){
			// Save settings
			$this->_save_settings();
			
			// Set template
			$template = $this->current_step['id'];
		}
		
		// Get header part
		$this->_get_template_part('admin-header');
		
		// Get Body
		if (!isset($_REQUEST['luvthemes-nonce']) || !wp_verify_nonce($_REQUEST['luvthemes-nonce'], 'fevr-setup') && current_user_can('manage_options')){
			$this->_get_template_part($template);
			$this->show_steps = false;
		}
		else{
			$this->_get_template_part($template);
		}
		
		// Get Footer
		$this->_get_template_part('admin-footer');
		
		// Exit
		die;
	}
	
	/**
	 * Print prev/next step links
	 */
	public function step_links() {
		$current 	= $this->current_step['index'];
		$prev		= isset($this->steps[$current-1]) ? '<a class="luv-btn luv-btn-gray luv-btn-lg" href="'. esc_url(wp_nonce_url(add_query_arg('step', ($current-1), menu_page_url('fevr_setup', false)), 'fevr-setup', 'luvthemes-nonce')) . '">'.esc_html__('Previous step', 'fevr').'</a>' : '';
		$finish		= '';
		if (isset($this->steps[$current+2])){
			$skip = '<a class="luv-btn luv-btn-gray luv-btn-lg luv-skip-step" href="'. esc_url(wp_nonce_url(add_query_arg('step', ($current+1), menu_page_url('fevr_setup', false)), 'fevr-setup', 'luvthemes-nonce')) . '">'.esc_html__('Skip this step', 'fevr').'</a>';
			$next = wp_nonce_field('fevr-setup', 'luvthemes-nonce').
					'<input type="hidden" name="step" value="'.($current + 1).'">'.
					'<input type="hidden" name="fevr-setup-action" value="'.esc_attr($this->current_step['id']).'">'.
					'<button class="luv-btn luv-btn-green luv-setup-next luv-btn-lg">'.esc_html__('Continue', 'fevr').'</button>';
		}
		else{
			$skip = $next = '';
			$finish = '<a class="luv-btn luv-btn-green luv-btn-finish luv-btn-lg" href="'. esc_url(wp_nonce_url(add_query_arg('step', ($current+1), menu_page_url('fevr_setup', false)), 'fevr-setup', 'luvthemes-nonce')) . '">'.esc_html__('Finish', 'fevr').'</a>';
		}
		echo '<div class="luv-setup-btn-wrapper">';
		echo fevr_kses($prev);
		echo fevr_kses($skip);
		echo fevr_kses($next);
		echo fevr_kses($finish);
		echo '</div>';
	}
	
	/**
	 * Implement TGM plugin activate view
	 */
	public function install_plugins_page() {
			// Store new instance of plugin table in object.
			$plugin_table = new Fevr_Setup_Plugins_List_Table;

			// Return early if processing a plugin installation action.
			if ( ( ( 'tgmpa-bulk-install' === $plugin_table->current_action() || 'tgmpa-bulk-update' === $plugin_table->current_action() ) && $plugin_table->process_bulk_actions() ) || $this->do_plugin_install() ) {
				return;
			}

			// Force refresh of available plugin information so we'll know about manual updates/deletes.
			wp_clean_plugins_cache( false );

			?>
			<div class="tgmpa wrap">
				<?php $plugin_table->prepare_items(); ?>

				<?php
				if ( ! empty( $this->message ) && is_string( $this->message ) ) {
					echo wp_kses_post( $this->message );
				}
				?>
				<?php $plugin_table->views(); ?>

				<input type="hidden" name="tgmpa-page" value="<?php echo esc_attr($GLOBALS['tgmpa']->menu);?>" />
				<input type="hidden" name="plugin_status" value="<?php echo esc_attr( $plugin_table->view_context ); ?>" />
				<?php $plugin_table->display(); ?>
			</div>
			<?php
	}
	
	/**
	 * Handle ajax requests
	 */
	public function ajax_handler(){
		if (!isset($_REQUEST['luvthemes-nonce']) || !wp_verify_nonce($_REQUEST['luvthemes-nonce'], 'fevr-setup') && current_user_can('manage_options')){
			wp_die(0);
		}
		
		if (isset($_REQUEST['ajax-action'])){
			switch ($_REQUEST['ajax-action']){
				case 'install_plugins_page':
					$this->install_plugins_page();
					break;
			}
		}
		wp_die();
	}
	
	/**
	 * Go back to the previous step
	 */
	private function _revert_step(){
		$index 							= $this->current_step['index']-1;
		$this->current_step 			= $this->steps[$index];
		$this->current_step['index']	= $index;
	}
	
	/**
	 * Save settings
	 */
	private function _save_settings(){
		if (current_user_can('manage_options') && isset($_POST['fevr-setup-action'])){
			switch ($_POST['fevr-setup-action']){
				case 'basic-settings':
					$fevr_options = get_option('fevr_options');
					foreach ($_POST['fevr_setup'] as $key => $value){
						$fevr_options[$key] = $value;
					}
					update_option('fevr_options', $fevr_options);
					break;
				case 'purchase-key':
					//Verify purchase key via Luv API
					$response = wp_remote_post ( FEVR_API_URL . 'validate/', array (
							'timeout' => 60,
							'sslverify' => false,
							'user-agent' => 'luv',
							'headers' => array (
									'X-ENVATO-PURCHASE-KEY' => trim ( $_POST['envato-purchase-key'] )
							),
							'body' => array (
									'site' => home_url ()
							)
					) );
					
					//Handle HTTP errors
					if (is_wp_error($response)) {
						fevr_print_admin_notice(array('class' => 'error', 'message' => esc_html__('Couldn\'t connect to server, please try again.', 'fevr')));
						// Go back;
						$this->_revert_step();
					}
					else{
						$response = json_decode($response['body'],true);
						//API error
						if ($response['error'] === true){
							// Go back;
							$this->_revert_step();
							fevr_print_admin_notice(array('class' => 'error', 'message' => esc_html__('API error: ', 'fevr') . $response['response']));
						}
						else if ($response === null){
							// Go back;
							$this->_revert_step();
							fevr_print_admin_notice(array('class' => 'error', 'message' => esc_html__('Unknown error, please try again.', 'fevr')));
						}
						//Success
						else{
							unset($this->steps[0]);
							global $fevr_purchase_key;
							$fevr_purchase_key = $_POST['envato-purchase-key'];
							if (defined('MULTISITE') && MULTISITE === true){
								if (is_super_admin()){
									update_site_option('fevr_purchase_key', $fevr_purchase_key);
								}
							}
							else{
								update_option('fevr_purchase_key', $fevr_purchase_key);
							}
						}
					}					
					break;
			}
		}
	}
	
	/**
	 * Includes the given template
	 * @param string $template
	 */
	private function _get_template_part($template) {
		if (strpos($template, '.') !== false){
			return false;
		}
		if (file_exists(trailingslashit(get_template_directory()) . 'luvthemes/includes/setup/templates/' . $template . '.php')){
			include trailingslashit(get_template_directory()) . 'luvthemes/includes/setup/templates/' . $template . '.php';
		}
	}
	
	public function wp_title(){
		return esc_html__( 'Luvthemes Setup Wizard - ', 'fevr' );
	}
	
}