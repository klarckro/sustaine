<?php 
global $fevr_setup;
$fevr_setup->show_steps = false;
?>
<h1><?php esc_html_e('Your website is ready!', 'fevr'); ?></h1>
<p><?php esc_html_e('What\'s next?', 'fevr'); ?></p>
<div class="luvthemes-setup-row">
	<div class="luvthemes-setup-col">
		<ul>
			<li><i class="fa fa-rocket"></i> <a href="<?php echo esc_url(menu_page_url('fevr-dashboard',false)); ?>"><?php echo sprintf(esc_html__('Start using %s', 'fevr'), FEVR_THEME_NAME); ?></a></li>
			<?php if (!defined('FEVR_WHITELABEL')):?>
			<li><i class="fa fa-support"></i> <a href="http://support.luvthemes.com/" target="_blank"><?php esc_html_e('Support', 'fevr'); ?></a></li>
			<li><i class="fa fa-vimeo"></i> <a href="https://vimeo.com/luvthemes" target="_blank"><?php esc_html_e('Video Tutorials', 'fevr'); ?></a></li>
			<li><i class="fa fa-leaf"></i> <a href="http://themeforest.net/user/luvthemes/follow" target="_blank"><?php esc_html_e('Follow us on Envato', 'fevr'); ?></a></li>
			<?php endif;?>
		</ul>
	</div>
	<?php if (!defined('FEVR_WHITELABEL')):?>
	<div class="luvthemes-setup-col">
		<ul>
			<li><i class="fa fa-star-o"></i> <a href="<?php echo esc_url(menu_page_url('fevr_feature_request',false)); ?>"><?php esc_html_e('Suggest a new feature', 'fevr'); ?></a></li>
		</ul>
	</div>
	<?php endif;?>
</div>