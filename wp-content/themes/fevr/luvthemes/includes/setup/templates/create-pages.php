<h1><?php esc_html_e('Pages', 'fevr'); ?></h1>
<p><?php esc_html_e('Here you can edit your existing pages or add new from pre-defined templates.', 'fevr'); ?></p>
<?php global $fevr_purchase_key?>
<?php if (empty($fevr_purchase_key)):?>
<div class="luv-admin-notice luv-admin-notice-warning">
	<?php esc_html_e('This option needs API connection. Please set your purchase key ', 'fevr')?>
	<a href="<?php echo esc_url(wp_nonce_url(menu_page_url('fevr_setup', false), 'fevr-setup', 'luvthemes-nonce'));?>"><?php esc_html_e('here', 'fevr')?></a>
</div>
<button class="luv-btn luv-btn-green" disabled><?php esc_html_e('Add New Page', 'fevr'); ?></button>
<?php else:?>
<a class="luv-btn luv-btn-green" href="<?php echo esc_url(menu_page_url('fevr_use_template', false));?>" target="_blank"><?php esc_html_e('Add New Page', 'fevr'); ?></a>
<?php endif;?>
<?php 
$wp_list_table = new Fevr_Setup_Pages_List_Table();
$wp_list_table->prepare_items();
$wp_list_table->display();
?>