<h1><?php esc_html_e('Plugins', 'fevr'); ?></h1>
<p><?php esc_html_e('Please select and install your plugins from the list. Visual Composer and Luvthemes Core are required.', 'fevr'); ?></p>
<?php global $fevr_setup;?>
<div id="luv-setup-plugin-table">
<?php $fevr_setup->install_plugins_page();?>
</div>