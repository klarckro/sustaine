<?php
	global $fevr_options;
	$header_logo		= (isset($fevr_options['header-logo']['url']) ? $fevr_options['header-logo']['url'] : '');
	$header_logo_dark	= (isset($fevr_options['header-logo-dark']['url']) ? $fevr_options['header-logo-dark']['url'] : '');
	$header_logo_light	= (isset($fevr_options['header-logo-light']['url']) ? $fevr_options['header-logo-light']['url'] : '');
?>
<h1><?php esc_html_e('Basic Settings', 'fevr'); ?></h1>

<div class="luvthemes-setup-section">
	<label><?php esc_html_e('Accent Color #1', 'fevr')?></label>
	<input type="text" class="color-field" name="fevr_setup[accent-color-1]" value="<?php echo esc_attr(fevr_get_luvoption('accent-color-1', '#f04649'));?>">
</div>
<div class="luvthemes-setup-section">
	<label><?php esc_html_e('Accent Color #2', 'fevr')?></label>
	<input type="text" class="color-field" name="fevr_setup[accent-color-2]" value="<?php echo esc_attr(fevr_get_luvoption('accent-color-2', '#1B1D1F'));?>">
</div>
<div class="luvthemes-setup-section">
	<label><?php esc_html_e('Logo (default)', 'fevr')?></label>
	<div class="luv-media-upload-container media-image">
		<input type="hidden" id="header-logo" name="fevr_setup[header-logo][url]" class="luv-media-upload-url" value="<?php echo esc_attr($header_logo);?>">
		<img src="<?php echo esc_url( $header_logo);?>" class="luv-media-upload-preview<?php echo (empty($header_logo) ? ' luv-hidden' : '')?>">
		<div class="luv-media-buttons">
			<span class="button media_upload_button luv-media-upload"><?php esc_html_e('Upload', 'fevr');?></span>
			<span class="button remove-image luv-media-upload-reset luv-hidden"><?php esc_html_e('Remove', 'fevr');?></span>
		</div>
	</div>
</div>
<div class="luvthemes-setup-section">
	<label><?php esc_html_e('Logo (dark)', 'fevr')?></label>
	<div class="luv-media-upload-container media-image">
		<input type="hidden" id="header-logo-dark" name="fevr_setup[header-logo-dark][url]" class="luv-media-upload-url" value="<?php echo esc_attr($header_logo_dark);?>">
		<img src="<?php echo esc_url( $header_logo_dark);?>" class="luv-media-upload-preview<?php echo (empty($header_logo_dark) ? ' luv-hidden' : '')?>">
		<div class="luv-media-buttons">
			<span class="button media_upload_button luv-media-upload"><?php esc_html_e('Upload', 'fevr');?></span>
			<span class="button remove-image luv-media-upload-reset luv-hidden"><?php esc_html_e('Remove', 'fevr');?></span>
		</div>
	</div>
</div>
<div class="luvthemes-setup-section">
	<label><?php esc_html_e('Logo (light)', 'fevr')?></label>
	<div class="luv-media-upload-container media-image">
		<input type="hidden" id="header-logo-light" name="fevr_setup[header-logo-light][url]" class="luv-media-upload-url" value="<?php echo esc_attr($header_logo_light);?>">
		<img src="<?php echo esc_url($header_logo_light);?>" class="luv-media-upload-preview<?php echo (empty($header_logo_light) ? ' luv-hidden' : '')?>">
		<div class="luv-media-buttons">
			<span class="button media_upload_button luv-media-upload"><?php esc_html_e('Upload', 'fevr');?></span>
			<span class="button remove-image luv-media-upload-reset luv-hidden"><?php esc_html_e('Remove', 'fevr');?></span>
		</div>
	</div>
</div>