<?php 
global $title, $hook_suffix, $current_screen, $wp_locale, $pagenow, $wp_version,
		$update_title, $total_update_count, $parent_file, $fevr_setup;

		$current = $fevr_setup->current_step['index'];
		
		$fevr_purchase_key = fevr_purchase_key();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
	<meta name="viewport" content="width=device-width" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<?php fevr_theme_slug_render_title();?>
	<?php do_action( 'fevr_setup_enqueue_scripts' ); ?>
	<?php do_action( 'admin_print_styles' ); ?>
	<?php do_action( 'admin_print_scripts' );?>
	<?php do_action( 'admin_head' ); ?>
</head>
<body class="wp-core-ui<?php echo esc_attr(' luv-setup-'.$fevr_setup->current_step['id']);?><?php echo(!empty($fevr_purchase_key) ? ' luv-setup-activated' : '')?>">
	<div class="luvthemes-setup-header">
		<?php if (!defined('FEVR_WHITELABEL')):?>
		<img width="300" src="<?php echo esc_url(FEVR_SETUP_URI . '/images/luvthemes_logo.png');?>">
		<?php endif;?>
	</div>
	<ul class="luvthemes-setup-steps">
		<?php foreach ($fevr_setup->steps as $key => $step):?>
		<li<?php echo ($key == $fevr_setup->current_step['index'] ? ' class="active"' : ($key > $fevr_setup->current_step['index'] ? ' class="disabled"' : ''))?>><?php echo fevr_kses($step['title']); ?></li>
		<?php endforeach;?>
	</ul>
	<div class="luvthemes-setup-wrapper">
		<form method="post" id="luvthemes-setup-form" action="<?php echo esc_url(wp_nonce_url(add_query_arg('step', ($current+1), menu_page_url('fevr_setup', false)), 'fevr-setup', 'luvthemes-nonce'));?>">