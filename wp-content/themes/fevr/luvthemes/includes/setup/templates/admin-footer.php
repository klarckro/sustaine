<?php
	global $fevr_setup;
	if ($fevr_setup->show_steps){
		$fevr_setup->step_links();
	}
?>
	</form>
</div>
<?php wp_print_media_templates();?>
<a class="back-to-dashboard" href="<?php echo esc_url(menu_page_url('fevr-dashboard',false)); ?>"><?php esc_html_e('Back to Dashboard', 'fevr')?></a>
<?php do_action( 'admin_print_footer_scripts' ); ?>
</body>
</html>