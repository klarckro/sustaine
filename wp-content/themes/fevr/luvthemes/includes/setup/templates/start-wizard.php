<h1><?php echo sprintf(esc_html__('Welcome to %s!', 'fevr'), FEVR_THEME_NAME); ?></h1>
<p><?php echo sprintf(esc_html__('Thank you for choosing %s! With the following few step will help you to configure the basic settings and add the main pages.', 'fevr'), FEVR_THEME_NAME); ?></p>
<p><?php echo sprintf(esc_html__('If you don\'t want to continue the wizard, you can skip and return to the WordPress dashboard. Come back anytime if you change your mind! (%s -> Setup Wizard)', 'fevr'), FEVR_THEME_NAME); ?></p>
<div class="luv-setup-btn-wrapper">
	<a href="<?php echo esc_url(menu_page_url('fevr-dashboard',false)); ?>" class="luv-btn luv-btn-gray luv-btn-md"><?php esc_html_e('Back to Dashboard', 'fevr')?></a>
	<a href="<?php echo esc_url(menu_page_url('fevr_installer', false)); ?>" class="luv-btn luv-btn-gray luv-btn-md"><?php esc_html_e('Install Demo Content', 'fevr')?></a>
	<a href="<?php echo esc_url(wp_nonce_url(menu_page_url('fevr_setup', false), 'fevr-setup', 'luvthemes-nonce')); ?>" class="luv-btn luv-btn-brand luv-btn-md btn-start-wizard"><?php esc_html_e('Start Wizard', 'fevr')?></a>
</div>
