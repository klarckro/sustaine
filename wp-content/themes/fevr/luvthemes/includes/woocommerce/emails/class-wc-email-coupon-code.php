<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WC_Email_Coupon_Code' ) ) :

/**
 * Customer New Account
 *
 * An email sent to the customer with a coupon code.
 *
 * @class       Fevr_WC_Email_Coupon_Code
 * @extends     WC_Email
 */
class Fevr_WC_Email_Coupon_Code extends WC_Email {
	
	public $coupon_code;

	/**
	 * Constructor
	 */
	public function __construct() {

		$this->id               = 'coupon_code';
		$this->title            = esc_html__( 'Coupon code', 'fevr' );
		$this->description      = esc_html__( 'Send generated coupon code to customer', 'fevr' );

		$this->template_html    = 'emails/coupon-code.php';
		$this->template_plain   = 'emails/plain/coupon-code.php';

		$this->subject          = esc_html__( 'You\'ve unlocked a new discount on {site_title}', 'fevr' );
		$this->heading          = esc_html__( 'You\'ve unlocked a new discount on {site_title}', 'fevr' );

		// Call parent constuctor
		parent::__construct();
	}

	/**
	 * Trigger.
	 */
	public function trigger($user_id, $coupon_code, $description, $expiry) {

		if ( $user_id ) {
			$this->object				= new WP_User( $user_id );
			$this->recipient			= stripslashes( $this->object->user_email );
			$this->coupon_code			= $coupon_code;
			$this->coupon_description	= $description;
			$this->coupon_expiry		= $expiry;
		}
		
		
		if ( ! $this->is_enabled() || ! $this->get_recipient() ) {
			return;
		}

		$this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
	}

	/**
	 * get_content_html function.
	 *
	 * @access public
	 * @return string
	 */
	public function get_content_html() {
		ob_start();
		wc_get_template( $this->template_html, array(
			'email_heading'      => $this->get_heading(),
			'coupon_code'        => $this->coupon_code,
			'coupon_description' => $this->coupon_description,
			'coupon_expiry'		 => $this->coupon_expiry,
			'blogname'           => $this->get_blogname(),
			'sent_to_admin'      => false,
			'plain_text'         => false
		) );
		return ob_get_clean();
	}

	/**
	 * get_content_plain function.
	 *
	 * @access public
	 * @return string
	 */
	public function get_content_plain() {
		ob_start();
		wc_get_template( $this->template_plain, array(
			'email_heading'      => $this->get_heading(),
			'coupon_code'        => $this->coupon_code,
			'coupon_description' => $this->coupon_description,
			'coupon_expiry'		 => $this->coupon_expiry,
			'blogname'           => $this->get_blogname(),
			'sent_to_admin'      => false,
			'plain_text'         => true
		) );
		return ob_get_clean();
	}
}

endif;

return new Fevr_WC_Email_Coupon_Code();
