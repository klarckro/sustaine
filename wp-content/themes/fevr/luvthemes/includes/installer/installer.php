<?php 

class Fevr_Installer{


	/**
	 * Settings array
	 * Example: array (
	 * 				'scheme'			=>	'example-scheme',
	 * 				'step'				=> 	'select-main-colors',
	 * 				'settings'			=>	array(...)
	 * 				'scheme-data'		=>	array(
	 * 											'name'					=> 'Sample scheme'
	 * 											'dependencies'			=>	array(...),
	 * 											'pages'					=>	array(...),
	 * 											'default-redux'			=>	array(
	 * 																			'colors'			=>	array(
	 * 																										'ocean' => array(...),
	 *	 																									'field' => array(...)
	 * 																									)
	 * 																			'settings'	=>	array(...)	
	 * 																		),
	 * 											'default-settings'		=>	array(...), 
	 * 										),
	 * 				'in-progress'		=>	true
	 * 			)
	 * @var array
	 */
	public $settings = array();
	
	/**
	 * Installer menu slug
	 * @var string
	 */
	public $installer_slug;
	
	/**
	 * API URL
	 * @var string
	 */
	public $api_url = '';
	
	/**
	 * API key (Envato purchase code)
	 * @var string
	 */
	public $api_key = '';
	
	/**
	 * Error message
	 * @var string
	 */
	public $error = '';
	
	/**
	 * WordPress upload dir
	 * @var array
	 * @see wp_upload_dir();
	 */
	public $wp_upload_dir;
	
	/**
	 * Contains the old and the current post ID connections
	 * @var unknown
	 */
	public $post_ids = array();
	
	/**
	 * Installer mode. 
	 * @var string installer|demo default is installer. 
	 */
	public $mode = 'installer';

	/**
	 * Skip plugin install for subsites on multisite. True if can install, false if can't
	 * @var boolean
	 */
	public $can_install;
	
	/**
	 * Is ajax request
	 * @var boolean
	 */
	public $is_ajax = false;
	
	//======================================================================
	//	MAIN FUNCTIONS
	//======================================================================
	
	/**
	 * Create installer object 
	 */
	public function __construct($installer_slug){
		// Prevent PHP timeout
		set_time_limit(3600);
		
		global $pagenow;
		
		$this->installer_slug = $installer_slug;
		
		//Force WP Filesystem to use direct method
		add_filter('filesystem_method', array(&$this, 'force_direct_filesystem_method'));
		
		add_action('init', array($this, 'set_installer_uri'));
		
		// Define IS_INSTALLER (true) if installer is active page
		if ($pagenow == 'admin.php' && isset($_GET['page']) && $_GET['page'] == $installer_slug && !defined('FEVR_IS_LUVINSTALLER')){
			define('FEVR_IS_LUVINSTALLER', true);
		}
				
		// Get the settings 
		$this->settings = get_option('fevr_installer',array());
			
		// Set current step
		$this->step = (isset($_REQUEST['step']) ? $_REQUEST['step'] : (isset($this->settings['current-step']) ? $this->settings['current-step'] : 'select_scheme'));
		
		// Ajax handler
		add_action('wp_ajax_fevr_installer', array(&$this, 'ajax_handler'));

		// WP upload array
		$this->wp_upload_dir = wp_upload_dir();
				
		// Check plugin install ability
		$this->can_install= (!defined('MULTISITE') || MULTISITE !== true || get_current_blog_id() == BLOG_ID_CURRENT_SITE);
		
		if (defined('FEVR_IS_LUVINSTALLER') && FEVR_IS_LUVINSTALLER){
			// Enqueue assets
			add_action('admin_enqueue_scripts', array(&$this, 'enqueue_assets'));
			
			// JS messages
			$this->messages = array(				
				'Unknown error' => esc_html__('Unknown error', 'fevr'),
				'Downloading demo content' => esc_html__('Downloading demo content', 'fevr'),
				'This action will delete your current posts/pages and override your current settings! Are you sure you would like to continue?' => esc_html__('This action will delete your current posts/pages and override your current settings! Are you sure you would like to continue?', 'fevr'), 
			);
		}
		
		// Disable WooCommerce installer on ajax requests
		if (defined('DOING_AJAX') && DOING_AJAX){
			add_filter('woocommerce_enable_setup_wizard', '__return_false');
		}
	}
	
	/**
	 * Set installer URI and nonce the URL
	 */
	public function set_installer_uri(){
		// Set installer URL
		if (!defined('FEVR_LUVINSTALLER_INSTALLER_URL')){
			define('FEVR_LUVINSTALLER_INSTALLER_URL',  esc_url(wp_nonce_url(admin_url('admin.php?page=' . $this->installer_slug),'luv-installer','luv-nonce')));
		}
	}
	
	/**
	 * Run the installer
	 */
	public function init(){
		// Init API
		$this->init_api();
		
		// Handle requests
		$this->router();
	}
		
	/**
	 * Enqueue assets
	 */
	public function enqueue_assets(){
		wp_enqueue_style('fevr--installer', trailingslashit(get_template_directory_uri()) . 'luvthemes/includes/installer/assets/installer.css');
		
		wp_enqueue_script('fevr--installer', trailingslashit(get_template_directory_uri()) . 'luvthemes/includes/installer/assets/installer.js');
		wp_localize_script('fevr--installer', 'fevr_installer', array('ajax_url' => admin_url('admin-ajax.php'), 'redux_headless_url'=> menu_page_url('theme-options', false),'nonce' => wp_create_nonce('fevr_installer'), 'i18n' => $this->messages));
	}
	
	/**
	 * Magic function to handle unknown step
	 * @param $function function name (step)
	 * @param $args arguments
	 */
	public function __call($function, $args){
		$this->error = esc_html__('Unknown step: ', 'fevr') . $function;
		$this->render('error');
	}
	
	/**
	 * Set API URL, and purchase code
	 */
	public function init_api(){				
		// Set API URL
		$this->api_url = FEVR_API_URL;
		
		// Set purchase key
		$this->api_key = fevr_purchase_key();
	}
	
	/**
	 * Call API
	 * @param string $function
	 * @param array $args
	 */
	public function api($function, $args = array()){
		set_time_limit(3600);
		$timeout = ini_get('max_execution_time');
		$response = wp_remote_post (
				$this->api_url . $function ,
				array(
						'timeout' => $timeout,
						'sslverify' => false,
						'user-agent' => 'luv',
						'headers' => array (
								'X-ENVATO-PURCHASE-KEY' => trim ($this->api_key)
						),
						'body' => array (
								'site' => trailingslashit(home_url()),
								'args' => $args
						)
				)
		);
		if (is_wp_error($response)){
			$this->error = esc_html__('Couldn\'t connect to API server: ', 'fevr') . $response->get_error_message();
		}
		else{
			$response = json_decode($response['body'],true);
			if ($response['error'] == true){
				$this->error = esc_html__('API error: ', 'fevr') . $response['response'];
			}
			else{
				return $response['response'];
			}
		}
	}
	
	/**
	 * Localize the data recursively (eg upload dir)
	 * @param array|string $data
	 * return array
	 */
	public function localize($data){
		if (is_array($data)){
			foreach ((array)$data as $key => $value){
				$data[$key] = $this->localize($value);
			}
		}
		else{
			$data = $this->_localize($data);
		}
		return $data;
	}
	
	/**
	 * Localize the settings json from the API
	 * @param string $json
	 * return array
	 */
	private function _localize($data){
		global $wpdb;
		$data = preg_replace('~^([0-9]+)/([0-9]+)~', substr($this->wp_upload_dir['subdir'],1), $data);
		$data = str_replace('{UPLOAD_DIR_FOLDER}', substr($this->wp_upload_dir['subdir'],1), $data);
		$data = str_replace('{ADMIN_EMAIL}', get_option('admin_email'), $data);
		$data = str_replace('{SITE_URL}', parse_url(site_url(), PHP_URL_HOST), $data);
		return str_replace('{UPLOAD_DIR_URL}', $this->wp_upload_dir['url'], $data);
	}
	
	/**
	 * Render the selected template
	 * @param string template
	 */
	public function render($template, $template_args = array()){		
		echo '<div class=" wrap luv-dashboard-wrap">';
		// Don't play with the $template name
		if (strpos($template, '.') !== false){
			$this->error = esc_html__('Unknown error', 'fevr');
			include_once trailingslashit(get_template_directory()). 'luvthemes/includes/installer/templates/error.tpl.php';
		}
		
		if (file_exists(trailingslashit(get_template_directory()). 'luvthemes/includes/installer/templates/'.$template.'.tpl.php')){
			include_once trailingslashit(get_template_directory()). 'luvthemes/includes/installer/templates/'.$template.'.tpl.php';
		}
		else{
			$this->error = esc_html__('Missing template: ', 'fevr') . $template;
			include_once trailingslashit(get_template_directory()). 'luvthemes/includes/installer/templates/error.tpl.php';
		}
		echo '</div>';
	}

	/**
	 * Router function
	 * Redirect to next step based on current settings and install scenario
	 */
	public function router(){
		switch ($this->step){
			case 'select_scheme':
			default:
				$this->select_scheme();
				break;
			case 'finish':
				$this->finish();
				break;
			case 'installer_exit':
				$this->installer_exit();
				break;
		}
		
	}
	
	/**
	 * Save current step
	 * save the settings to fevr_installer option
	 */
	public function save_current_step(){
		if (isset($this->settings['scheme']) && !empty($this->settings['scheme'])){
			// Set current step
			$this->settings['current-step'] = $this->step;
	
			update_option('fevr_installer', $this->settings);
		}
	}
	
	/**
	 * Reset the installer
	 * Overwrite fevr_installer to an empty array
	 */
	public function reset_settings(){
			update_option('fevr_installer', array());
	}
	
	
	/**
	 * Check dependences
	 * @param array $dependency
	 * return boolean
	 */
	public function check_dependency($dependency){
		$pass = true;
		foreach ((array)$dependency['checkings'] as $check){
			$condition = false;
			switch ($check['type']){
				case 'scheme_images':
					$condition = file_exists($this->wp_upload_dir['path'].'/' . $this->settings['scheme']);
					break;				
				case 'class_exists':
					$condition = class_exists($check['condition']);
					break;
				case 'function_exists':
					$condition = function_exists($check['condition']);
					break;
				case 'plugin_exists':
					$plugins = get_option('active_plugins');
					$condition = false;
					foreach ((array)$plugins as $plugin){
						if ($plugin == $check['condition']){
							$condition = true;
						}
					}
					break;
			}
			if (!$condition){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Check page is exists. Returns the current post ID if the post already exists
	 * @param array $post
	 * return int|boolean
	 */
	public function check_page($post){
		$args = array(
			'post_type' => $post['data']['post_type'],
			'meta_query' => array(
				array(
						'key'     	=> '_original_post_ID',
						'value'   	=> $post['meta']['_original_post_ID'],
						'compare'	=> 'EQUALS'
				)
			)
		);
		
		$query = new WP_Query($args);
		return $query->post_count > 0 ? $query->posts[0]->ID : false;
	}
	
	/**
	 * Download and install dependency (plugin, pictures, etc)
	 * @param string $dependency_id dependency id
	 * @return boolean 
	 */
	public function install_dependency($dependency_id){
		// Prevent PHP timeout
		set_time_limit(3600);
		$timeout = ini_get('max_execution_time');
		$dependency = $this->settings['dependencies'][$dependency_id];
		switch ($dependency['type']){
			case 'plugin':
				//Activate plugin if it is already installed
				if (file_exists(WP_PLUGIN_DIR . '/' . $dependency['install']['plugin-file'])){
					$activate = activate_plugin( $dependency['install']['plugin-file']);
					if (is_wp_error($activate)){
						throw new Exception(esc_html__('Plugin activation failed', 'fevr'));
					}
					else{
						return true;
					}
				}
				// Install plugin if it is necessary
				else if($this->can_install){
					WP_Filesystem();
					global $wp_filesystem;
					if ($dependency['install']['type'] == 'automatic'){
						// Source URL for (self)hosted plugins
						if (!isset($dependency['install']['source']) || $dependency['install']['source'] == 'hosted' || $dependency['install']['source'] == 'local'){
							$source_url = $dependency['install']['url'];
						}
						// Source URL for WordPress.org 
						else if ($dependency['install']['source'] == 'wordpress.org'){
							$wp_api = wp_remote_get(esc_url('https://api.wordpress.org/plugins/info/1.0/'.$dependency['install']['slug'].'.json'), array('sslverify' => false, 'timeout' => $timeout));
							// Download failed
							if (is_wp_error($wp_api)){
								throw new Exception($download->get_error_message());
							}
							else{
								$wp_api = json_decode($wp_api['body'], true);
								$source_url = $wp_api['download_link'];
							}
						}
						
						// Download required zip from source URL
						if ($dependency['install']['source'] != 'local'){
							$download = wp_remote_get(esc_url($source_url), array('sslverify' => false, 'timeout' => $timeout));
						}
						//Bypass downloading if we use local source
						else{
							$download['body'] = $wp_filesystem->get_contents(trailingslashit(get_template_directory()) . 'luvthemes/plugins/' . $dependency['install']['url']);
						}
						// Download failed
						if (is_wp_error($download)){
							throw new Exception($download->get_error_message());
						}
						else{
							// Save temporary zip file
							$temp_name = hash('crc32',time()) . '.zip';
							$filename = trailingslashit($this->wp_upload_dir['path']) . $temp_name;
							if ( !$wp_filesystem->put_contents( $filename, $download['body'], FS_CHMOD_FILE) ) {
							    throw new Exception(esc_html__('Installer can\'t save the downloaded plugin', 'fevr'));
							}
							else{
								// Install (unzip) downloaded plugin
								$unzip = unzip_file($filename, WP_PLUGIN_DIR);
								if (is_wp_error($unzip)){
									throw new Exception($unzip->get_error_message());
								} 
								else{
									unlink ($filename);
									$activate = activate_plugin( $dependency['install']['plugin-file']);
									if (is_wp_error($activate)){
										throw new Exception(esc_html__('Plugin activation failed', 'fevr'));
									}
									else{
										return true;
									}
									
								}
							}
						}
					}
				}
				break;
			case 'scheme-images':
				// Download required zip from source URL
				$filename = download_url(esc_url($this->api_url . '/demo-content/' . $this->settings['scheme'] . '/images.zip', 600));
				
				// Download failed
				if (is_wp_error($filename)){
					throw new Exception($filename->get_error_message());
				}
				else{
					$this->prevent_timeout();
					// Unzip downloaded images
					WP_Filesystem();
					global $wp_filesystem;
					$unzip = unzip_file($filename, $this->wp_upload_dir['path']);
					if (is_wp_error($unzip)){
						throw new Exception($unzip->get_error_message());
					}
					else{
						// Delete temporary zip
						unlink ($filename);
						return true;
					}
				}
				break;
		}
		
		return false;
	}
	
	/**
	 * Import posts
	 */
	public function import_posts(){
		global $wpdb;
		// Insert posts
		$wpdb->query('INSERT IGNORE INTO '. $wpdb->posts . $this->localize($this->settings['posts']));
		
		// Insert post meta
		$wpdb->query('INSERT IGNORE INTO '. $wpdb->postmeta . $this->settings['postmeta']);
		
		$__postmeta	= $wpdb->get_results('SELECT * FROM '.$wpdb->postmeta, ARRAY_A);
		
		foreach ($__postmeta as $__meta){
			$localized = maybe_serialize($this->localize(maybe_unserialize($__meta['meta_value'])));
			if ($localized !== $__meta['meta_value']){
				$wpdb->update($wpdb->postmeta, array('meta_value' => $localized), array('meta_id' => $__meta['meta_id']));
			}
		}
		
	}
	
	/**
	 * Import terms and taxonomies
	 */
	public function import_terms(){
		global $wpdb;
		foreach ((array)$this->settings['terms'] as $key => $term){
			$wpdb->insert($wpdb->terms, $term);
		}
		foreach ((array)$this->settings['taxonomy'] as $key => $taxonomy){
			$wpdb->insert($wpdb->term_taxonomy, $taxonomy);
		}
		foreach ((array)$this->settings['term_relationships'] as $key => $term_relationship){
			$wpdb->insert($wpdb->term_relationships, $term_relationship);
		}		
	}
	
	/**
	 * Import wp_options
	 */
	public function import_options(){
		foreach ((array)$this->settings['options'] as $option => $value){
			$option = ($option == 'theme_mods' ? 'theme_mods_' . get_option('stylesheet') : $option); 
			update_option($option, $value);
		}
	}
	
	/**
	 * Generate thumbnails for demo images
	 */
	public function generate_thumbnails(){
		global $wpdb;

		$files  = $wpdb->get_results('SELECT meta_value FROM '.$wpdb->postmeta .' WHERE meta_key = "_wp_attachment_metadata" ', ARRAY_A);

		foreach ((array)$files as $file){
			$meta	= maybe_unserialize($file['meta_value']);
			if (isset($meta['sizes'])){
				$_file = $this->wp_upload_dir['path'] . '/'  . basename($meta['file']);
				foreach ((array)$meta['sizes'] as $size){
					if (!file_exists($this->wp_upload_dir['path'] . '/' .$size['file'])){
						$image = wp_get_image_editor( $_file );
						if ( ! is_wp_error( $image ) ) {
							$image->resize( $size['width'], $size['height'], true );
							$image->save($this->wp_upload_dir['path'] . '/' .$size['file']);
						}
					}
				}
			}
		}
	}
	
	
	/**
	 * Handle ajax requests
	 */
	public function ajax_handler(){
		// Set is ajax true
		$this->is_ajax = true;
		
		// Init API
		$this->init_api();
		
		if ( ! isset( $_POST['fevr_installer_nonce'] ) || ! wp_verify_nonce( $_POST['fevr_installer_nonce'], 'fevr_installer' )  || !current_user_can('manage_options')) {
			wp_die();
		}

		try{
			if (empty($this->settings)){
				// Override scheme if necessary, or continue with previously selected
				$this->settings['scheme'] = isset($_REQUEST['scheme']) ? $_REQUEST['scheme'] : $this->settings['scheme'];
					
				// Get scheme details from API
				$this->settings = json_decode($this->api('get-demo-content', array('scheme' => $this->settings['scheme'])), true);
			}
			switch ($_POST['demo_step']){				
				case 0:
					// Prepare install
					$this->prepare_install();
					
					$dependencies = $this->settings['dependencies'];
					$dependency = array_shift($dependencies);

					wp_send_json(array('error' => false, 'message' => esc_html__('Installing dependencies', 'fevr'), 'step' => 1, 'dependencies' => array_keys($this->settings['dependencies'])));
				case 1:
					if (count($_POST['dependencies'] > 0)){
						$dependency = array_shift($_POST['dependencies']);
						$this->install_dependency($dependency);
					}
					if (is_array($_POST['dependencies']) && count($_POST['dependencies'] > 0) && isset($_POST['dependencies'][0]) && isset($this->settings['dependencies'][$_POST['dependencies'][0]]['name'])){
						$message	= esc_html__('Installing dependencies', 'fevr');
						$step 		= 1;
					}
					else{
						$message	= esc_html__('Importing posts', 'fevr');
						$step		= 2;
					}
					
					wp_send_json(array('error' => false, 'message' => $message, 'step' => $step, 'dependencies' => $_POST['dependencies']));
					break;
					
				case 2:
					// Import posts
					$this->import_posts();
					wp_send_json(array('error' => false, 'message' => esc_html__('Importing categories', 'fevr'), 'step' => 3));
					break;
				case 3:
					// Import terms
					$this->import_terms();
					wp_send_json(array('error' => false, 'message' => esc_html__('Applying settings', 'fevr'), 'step' => 4));
					break;
			
				case 4:
					// Import options
					$this->import_options();
			
					update_option('fevr_options', $this->localize($this->settings['redux']));
					
					// Finalize install
					$this->save_current_step();
					flush_rewrite_rules();
					
					//Delete temporary installer option
					delete_option('fevr_installer');
					
					wp_send_json(array('error' => false,  'message' => esc_html__('Generating thumbnails', 'fevr'), 'finish' => add_query_arg('step','finish', FEVR_LUVINSTALLER_INSTALLER_URL)));
					break;
			}
		}
		catch (Exception $e){
			$this->error = $e->getMessage();
			$this->finish();
		}
		
	}
	
	/**
	 * Prepare install
	 */
	public function prepare_install(){
		WP_Filesystem();
		global $wp_filesystem;
		
		// Truncate posts, postmeta, wp_terms, wp_term_relationships, wp_term_taxonomy
	
		foreach (array('posts','postmeta','terms','term_relationships','term_taxonomy') as $table){
			global $wpdb;
			$wpdb->query('TRUNCATE ' . $wpdb->{$table});
		}
	
		// Override scheme if necessary, or continue with previously selected
		$this->settings['scheme'] = isset($_REQUEST['scheme']) ? $_REQUEST['scheme'] : $this->settings['scheme'];
			
		// Get scheme details from API
		$this->settings = json_decode($this->api('get-demo-content', array('scheme' => $this->settings['scheme'])), true);
	
		// Ovierwrite dynamic.css
		try{
			$download = $this->api('get-dynamic-css', array('scheme' => $this->settings['scheme'], 'mode' => $this->mode));
			$wp_filesystem->put_contents(trailingslashit(get_template_directory()) . 'css/dynamic.css', $download);
		}
		catch (Exception $e){
			// Download failed
			throw new Exception($download->get_error_message());
		}
	
		// Apply redux settings
		update_option('fevr_options', $this->localize($this->settings['redux']));
	
		// Save current step
		$this->save_current_step();
	}
	
	/**
	 * Get all image sizes
	 * @return array
	 */
	public function get_image_sizes() {
		global $_wp_additional_image_sizes;
	
		$sizes = array();
	
		foreach ( get_intermediate_image_sizes() as $_size ) {
			if ( in_array( $_size, array('thumbnail', 'medium', 'medium_large', 'large') ) ) {
				$sizes[ $_size ]['width']  = get_option( "{$_size}_size_w" );
				$sizes[ $_size ]['height'] = get_option( "{$_size}_size_h" );
				$sizes[ $_size ]['crop']   = (bool) get_option( "{$_size}_crop" );
			} elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
				$sizes[ $_size ] = array(
					'width'  => $_wp_additional_image_sizes[ $_size ]['width'],
					'height' => $_wp_additional_image_sizes[ $_size ]['height'],
					'crop'   => $_wp_additional_image_sizes[ $_size ]['crop'],
				);
			}
		}
	
		return $sizes;
	}
	
	/**
	 * Force WP_Filesystem to use direct method
	 */
	public function force_direct_filesystem_method(){
		return 'direct';
	}
	
	/**
	 * Prevent timeout for reverse proxy
	 */
	public function prevent_timeout() {
		if ($this->is_ajax){
			echo ' ';
		}
	}
	
	//======================================================================
	//	STEPS
	//======================================================================
	
	/**
	 * Select scheme
	 */
	public function select_scheme(){
		// Get available scheme list from API
		$schemes = $this->api('get-schemes');
		
		// Render
		$this->render('select-scheme', array('schemes' => $schemes));
	}
	
	/**
	 * Show finish screen
	 */
	public function finish(){
		$this->generate_thumbnails();
		$this->render('finish');
	}
	
	/**
	 * Leave install and set selected scheme to empty
	 *
	 */
	public function installer_exit(){
		// Render
		$this->reset_settings();
		$this->select_scheme();
	}
	
}


?>