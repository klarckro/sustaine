(function(){
	
	var dependencies = [];
	
	jQuery(function(){
		// Start installer
		jQuery(document).on('click','.luv-installer-start', function(e){
			if (confirm(__('This action will delete your current posts/pages and override your current settings! Are you sure you would like to continue?'))){
				// Install demo content
				if (jQuery(this).hasClass('luv-installer-demo-content')){
					e.preventDefault();
					jQuery('body').append('<div id="loader-overlay"><p class="message-text">' + __('Downloading demo content') + '</p><div class="loader"><svg class="loader-circular" viewBox="25 25 50 50"><circle class="loader-path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div></div>');
					demo_install(jQuery(this).attr('data-scheme'), 0);
				}
				else{
					return true;
				}
			}
			else{
				e.preventDefault();
				return false;
			}
		});
	});
	
	function demo_install(scheme, step){
		jQuery.ajax({
		      type		: "POST",
		      'url'		: fevr_installer.ajax_url,
		      'data'	: {'action' : 'fevr_installer', 'fevr_installer_action' : 'demo-content', 'demo_step' : step, 'scheme': scheme, 'dependencies': dependencies,'fevr_installer_nonce' : fevr_installer.nonce},
		      'timeout' : 119000,
		      'success'	: function(response){
				try{
					if (response.error == true){
						jQuery('.luv-installer-error').removeClass('is-hidden');
						jQuery('.luv-installer-error > p').text(response.message);
					}
					else{
						if (typeof response.dependencies != 'undefined'){
							dependencies = response.dependencies;
						}
						jQuery('#loader-overlay p.message-text').text(response.message);
						if (typeof response.finish === 'undefined'){
							setTimeout(function(){
								demo_install(scheme, response.step)
							},1000);
						}
						else{
							jQuery('#loader-overlay p.message-text').text(response.message);
							document.location.href = response.finish;
						}
					}
				}
				catch (error){
					jQuery('.luv-installer-error').removeClass('is-hidden');
					jQuery('.luv-installer-error > p').text(error.message);
				}
		      }
			});
	}
	
	/**
	 * Localization
	 * @param string text
	 * @return string
	 */
	function __(text){
		if (typeof fevr_installer.i18n[text] !== 'undefined'){
			return fevr_installer.i18n[text];
		}
		else {
			return text;
		}
	}
})();