<?php 
	global $fevr_installer;?>
<h1><?php esc_html_e('Schemes', 'fevr')?></h1>
<div class="error luv-installer-error<?php echo (empty($fevr_installer->error) ? ' is-hidden' : '');?>">
 <p><?php echo fevr_kses($fevr_installer->error); ?></p>
</div>
<ul class="luv-schemes">
	<?php foreach((array)$template_args['schemes'] as $scheme) : ?>
	<li>
		<div class="scheme-container">
			<h3 class="scheme-name"><?php echo fevr_kses($scheme['name'])?></h3>
			<div class="luv-scheme-img"><img src="<?php echo esc_url(FEVR_API_URL . 'demo-content/'.$scheme['scheme'].'/thumbnail.png');?>"></div>
			<?php if(isset($scheme['excerpt']) && !empty($scheme['excerpt'])):?>
			<div class="scheme-excerpt"><?php echo fevr_kses($scheme['excerpt']);?></div>
			<?php endif;?>
		</div>
		<div class="scheme-nav">
				<?php if (version_compare(FEVR_THEME_VER, $scheme['version'], '>=')):?>
				<a class="luv-btn luv-btn-blue luv-btn-block luv-installer-start luv-installer-demo-content" href="<?php echo esc_url(FEVR_LUVINSTALLER_INSTALLER_URL . '&step=demo&scheme=' . $scheme['scheme']);?>" data-scheme="<?php echo esc_attr($scheme['scheme']);?>">
					<?php echo esc_html__('Install demo content', 'fevr'); ?>
				</a>
				<a class="luv-btn luv-btn-blue luv-btn-block luv-installer-start luv-installer-demo-content" href="<?php echo esc_url(FEVR_LUVINSTALLER_INSTALLER_URL . '&step=demo&scheme=' . $scheme['scheme']);?>-wo-images" data-scheme="<?php echo esc_attr($scheme['scheme']);?>-wo-images">
					<?php echo esc_html__('Install demo content without images', 'fevr'); ?>
				</a>
				<?php else:?>
				<a href="<?php echo esc_url(menu_page_url('fevr-dashboard',false));?>" class="luv-btn luv-btn-blue luv-btn-block"><?php echo esc_html__('Not compatible. Please update ', 'fevr') . FEVR_THEME_NAME;?></a>
				<?php endif;?>
				<a class="luv-btn luv-btn-green luv-btn-block" href="<?php echo esc_url(FEVR_DEMO_URL . $scheme['scheme']);?>" target="_blank">
					<?php echo esc_html__('Live Demo', 'fevr')?>
				</a>
		</div>
	</li>
	<?php endforeach; ?>
</ul>