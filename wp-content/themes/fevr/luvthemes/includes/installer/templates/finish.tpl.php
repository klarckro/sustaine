<?php 
	global $fevr_installer;	
?>
<h1><?php esc_html_e('Install Finished', 'fevr')?></h1>
<div class="error luv-installer-error<?php echo (empty($fevr_installer->error) ? ' is-hidden' : '');?>">
 <p><?php echo fevr_kses($fevr_installer->error); ?></p>
</div>
<?php
if (isset($fevr_installer->settings['finish_message']) && !empty($fevr_installer->settings['finish_message'])){
	echo fevr_kses($fevr_installer->settings['finish_message']);
}
?>
<a class="luv-btn-green luv-btn" href="<?php echo esc_url(trailingslashit(site_url()))?>"><?php esc_html_e('Visit site', 'fevr');?></a> <a class="luv-btn-blue luv-btn" href="<?php echo esc_url(menu_page_url('theme-options',false))?>"><?php esc_html_e('Theme settings', 'fevr');?></a>