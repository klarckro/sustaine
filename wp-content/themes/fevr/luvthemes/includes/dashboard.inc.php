<?php 
$fevr_purchase_key = fevr_purchase_key();

// Hide menus and functions on subsites. True if should show, false if should hide
$ms_restriction = (!defined('MULTISITE') || MULTISITE !== true || get_current_blog_id() == BLOG_ID_CURRENT_SITE);

// Show purge CDN if MaxCDN is set
$maxcdn = (fevr_check_luvoption('enable-cdn', 1) && fevr_check_luvoption('maxcdn-alias', '','!=') && fevr_check_luvoption('maxcdn-key', '','!=') && fevr_check_luvoption('maxcdn-secret', '','!='));

// Show clear cache if merge scripts/styles are enabled
$merge_assets = (fevr_check_luvoption('merge-styles', 1) || fevr_check_luvoption('merge-scripts', 1));

// Prepare tabs
$tabs = array(
		'dashboard' => esc_html__('Dashboard', 'fevr'),
		'activate' => empty($fevr_purchase_key) ? esc_html__('Activate', 'fevr') : null,
		'performance' => ($merge_assets || $maxcdn ? esc_html__('Performance', 'fevr') : ''),
		'fevr-mobile' => fevr_check_luvoption('enable-mobile-app', 1) && !empty($fevr_purchase_key) ? esc_html__('Fevr Mobile', 'fevr') : null,
		'support' => esc_html__('Support', 'fevr'),
);

// Remove support tab if whitelabel mode is active
if (defined('FEVR_WHITELABEL')){
	unset($tabs['support']);
}

// Select active tab
$active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'dashboard'; 

// Manually check updates
if (isset($_GET['check-luv-updates'])){
	fevr_update_checker();
}

// Run update
if (isset($_GET['luv-updater'])){
	include_once trailingslashit(get_template_directory()) . 'luvthemes/includes/updater.inc.php';

	$updater = new Fevr_Updater($fevr_purchase_key);
	try {
		if ($updater->update()){
			fevr_print_admin_notice(array('class' => 'success', 'message' =>esc_html__('Theme sucessfully updated', 'fevr')));
		}
	} catch (Exception $e) {
		fevr_print_admin_notice(array('class' => 'error', 'message' => $e->getMessage()));
	}
}

?>

<div class="wrap luv-dashboard-wrap">
	<?php if (empty($fevr_purchase_key)):?>
	<h1><?php echo sprintf(esc_html__('Welcome to %s!', 'fevr'), FEVR_THEME_NAME); ?></h1>
	<div class="luv-page-dashboard-desc">
		<img src="http://cdn.luvthem.es/images/luvthemes_heart.png">
		<?php echo sprintf(fevr_kses(__('<strong>Congratulations!</strong><br> %s is now installed. Please register your product to enable automatic theme updates and support. If you need help, please feel free to contact us at', 'fevr')), FEVR_THEME_NAME); ?> <a href="mailto:support@luvthemes.com">support@luvthemes.com</a>
	</div>
	<?php else:?>
	<h1><?php echo sprintf(esc_html__('%s Dashboard', 'fevr'), FEVR_THEME_NAME); ?></h1>
	<?php endif?>
	<h2 class="nav-tab-wrapper">
	<?php foreach($tabs as $key=>$tab): 
	  if (empty($tab)){
	  	continue;
	  } 
	 ?>
	 <a href="<?php echo esc_url(add_query_arg('tab', $key, menu_page_url('fevr-dashboard', false)));?>" class="nav-tab <?php echo ($key == $active_tab ? 'nav-tab-active' : '');?>"><?php echo esc_html($tab)?></a>
	 <?php endforeach;?>
	</h2>

	<?php
	//Activate
	if ($active_tab == 'activate' && empty($fevr_purchase_key) && $ms_restriction):?>
			<h4><?php esc_html_e('Add your Envato purchase key', 'fevr')?></h4>
			<form method="post">
				<input type="text" name="envato-purchase-key" value="<?php echo esc_attr($fevr_purchase_key);?>">
				<input type="hidden" name="luv-save-settings" value="purchase-key">
				<?php wp_nonce_field( 'fevr-save-settings', 'luv-nonce')?> 
				<button class="luv-btn luv-btn-green"><?php esc_html_e('Save', 'fevr')?></button>
			</form>
	<?php
	//Support 
	elseif ($active_tab == 'support'):?>
		<h4><?php esc_html_e('Do you have a question? Chat with us!', 'fevr')?></h4>
		<strong>support@luvthemes.com</strong>
	<?php 
	//Support 
	elseif ($active_tab == 'performance'):?>		
		<?php if($merge_assets):?>
			<h4><?php esc_html_e('Theme Cache', 'fevr')?></h4>
			<span><?php esc_html_e('With this option you can clear the merged scripts/styles cache.', 'fevr')?></span>
			<div class="luv-dashboard-section">
				<a class="luv-btn luv-btn-blue" href="<?php echo esc_url(wp_nonce_url(add_query_arg('clear-luv-cache', 1, site_url(str_replace(site_url(), '', 'http'.(isset($_SERVER['HTTPS']) ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']))), 'clear-luv-cache'))?>"><?php esc_html_e('Clear Theme Cache', 'fevr')?></a>
			</div>
		<?php endif;?>
		<?php if($maxcdn):?>
			<h4><?php esc_html_e('Purge CDN', 'fevr')?></h4>
			<form method="post">
				<div class="luv-dashboard-section">
					<button class="luv-btn luv-btn-red"><?php esc_html_e('Purge CDN', 'fevr')?></button>
				</div>
				<div class="luv-dashboard-section">
				<?php 				
					$maxcdn_api = new Fevr_MaxCDN(fevr_get_luvoption('maxcdn-alias'),fevr_get_luvoption('maxcdn-key'),fevr_get_luvoption('maxcdn-secret'));
					$response = json_decode($maxcdn_api->get('/zones.json'),true);
				?>
				<?php if ($response['code'] == '200'):?>
					<select name="zone-id">
					<?php foreach ($response['data']['zones'] as $zone):?>
						<option value="<?php echo esc_attr($zone['id'])?>"><?php echo esc_html($zone['name'])?> (<?php echo esc_attr($zone['id'])?>)</option>
					<?php endforeach;?>
					</select>
				<?php endif;?>
				<input type="hidden" name="purge-fevr-cdn" value="1">
				<?php wp_nonce_field('purge-fevr-cdn')?>
				</div>
			</form>
		<?php endif;?>		
	<?php 
	//Support 
	elseif ($active_tab == 'fevr-mobile'):?>
		<?php $fevr_mobile_data = fevr_get_fevr_mobile_data();?>		
		<?php if (!empty($fevr_mobile_data['link'])):?>
		<div class="luv-dashboard-row">
			<div class="luv-dashboard-col">
				<h4><?php esc_html_e('Download App', 'fevr')?></h4>
				<a href="<?php echo esc_url($fevr_mobile_data['link'])?>" target="_blank" class="luv-btn luv-btn-blue"><?php esc_html_e('Download Zip', 'fevr')?> (v<?php echo esc_html($fevr_mobile_data['app_version'])?>)</a>
				<a href="https://build.phonegap.com/" target="_blank" class="luv-btn luv-btn-green"><?php esc_html_e('Build with PhoneGap', 'fevr')?></a>
			</div>
			<div class="luv-dashboard-col">
				<h4><?php esc_html_e('Android Key', 'fevr')?></h4>
				<a href="<?php echo esc_url($fevr_mobile_data['keystore'])?>" target="_blank" class="luv-btn luv-btn-blue"><?php esc_html_e('Download Android Key File', 'fevr')?></a><br><br>
				<div class="luv-dashboard-info">
					<strong><?php esc_html_e('Android Key Alias:','fevr')?></strong> fevr<br>
					<strong><?php esc_html_e('Certificate Password:','fevr')?></strong> <?php echo $fevr_mobile_data['keystore_password']?><br>
					<strong><?php esc_html_e('Keystore Password:','fevr')?></strong> <?php echo $fevr_mobile_data['keystore_password']?><br>
				</div>
			</div>
		</div>
		<?php endif;?>
		<h4><?php echo sprintf(esc_html__('Generate %s Mobile App', 'fevr'), FEVR_THEME_NAME);?></h4>
		<form method="post">
			<div class="luv-meta-field-section">
				<label><?php esc_html_e('Author E-mail', 'fevr');?></label>
				<div class="luv-meta-field-container">
					<input type="text" name="author_email" value="<?php echo esc_attr(isset($fevr_mobile_data['author_email']) ? $fevr_mobile_data['author_email'] : get_option('admin_email'));?>">
				</div>
			</div>
			<div class="luv-meta-field-section">
				<label><?php esc_html_e('Author Web', 'fevr');?></label>
				<div class="luv-meta-field-container">		
					<input type="text" name="author_web" value="<?php echo esc_url(isset($fevr_mobile_data['author_web']) ? $fevr_mobile_data['author_web'] : trailingslashit(site_url()))?>">
				</div>
			</div>
			<div class="luv-meta-field-section">
				<label><?php esc_html_e('App Icon', 'fevr');?></label>
				<div class="luv-meta-field-container">
					<div class="luv-media-upload-container media-image">
						<input type="hidden" name="icon" class="luv-media-upload-url" value="<?php echo esc_url(isset($fevr_mobile_data['icon']) ? $fevr_mobile_data['icon'] : '')?>">
						<img src="<?php echo esc_url(isset($fevr_mobile_data['icon']) ? $fevr_mobile_data['icon'] : '')?>" class="luv-media-upload-preview <?php echo (isset($fevr_mobile_data['icon']) && !empty($fevr_mobile_data['icon']) ? '' : 'is-hidden') ?>">
						<div class="luv-media-buttons">
							<span class="button media_upload_button luv-media-upload"><?php echo (isset($fevr_mobile_data['icon']) && !empty($fevr_mobile_data['icon']) ? esc_html__('Modify', 'fevr') : esc_html__('Upload', 'fevr'));?></span>
							<span class="button remove-image luv-media-upload-reset <?php echo (isset($fevr_mobile_data['icon']) && !empty($fevr_mobile_data['icon']) ? '' : 'is-hidden') ?>">Remove</span>
						</div>
					</div>
				</div>
			</div>
			<div class="luv-meta-field-section">
				<label><?php esc_html_e('App Version', 'fevr');?></label>
				<div class="luv-meta-field-container">
					<input type="text" name="app_version" value="<?php echo esc_attr(isset($fevr_mobile_data['app_version']) ? $fevr_mobile_data['app_version'] : '1.0');?>">
				</div>
			</div>
			<div class="luv-meta-field-section">
				<label><?php esc_html_e('App Name', 'fevr');?></label>
				<div class="luv-meta-field-container">
					<input type="text" name="app_name" value="<?php echo esc_attr(isset($fevr_mobile_data['app_name']) ? $fevr_mobile_data['app_name'] : get_bloginfo('name'));?>">
				</div>
			</div>
			<div class="luv-meta-field-section">
				<label><?php esc_html_e('App Description', 'fevr');?></label>
				<div class="luv-meta-field-container">
					<input type="text" name="app_description" value="<?php echo esc_attr(isset($fevr_mobile_data['app_description']) ? $fevr_mobile_data['app_description'] : '');?>">
				</div>
			</div>
			<div class="luv-meta-field-section"> 
				<button class="luv-btn luv-btn-green luv-generate-mobile-app"><?php esc_html_e('Generate', 'fevr')?></button>
			</div>
			<input type="hidden" name="generate-fevr-mobile" value="generate">
			<?php wp_nonce_field( 'fevr-generate-mobile-app', 'luv-nonce')?>
		</form>
	<?php 
	//Dashboard
	else:
		$update_info = get_option('fevr_update',array());
		
		$installer_info = get_option('fevr_installer',array());
	?>
		
		<?php if (!empty($fevr_purchase_key)):?>
			<?php if (!empty($update_info) && !isset($update_info['dismiss']) && version_compare(FEVR_THEME_VER, $update_info['version'], '<')):?>
				<div class="luv-notification"><?php echo FEVR_THEME_NAME . ' ' . $update_info['version'] . esc_html__(' is available! Please update now.', 'fevr')?></div>
			<?php else:?>
				<?php if (isset($_GET['check-luv-updates'])):?>
				<div class="luv-notification"><?php echo sprintf(esc_html__('You are already using the latest version of %s', 'fevr'), FEVR_THEME_NAME);?></div>
				<?php endif;?>
			<?php endif;?>
			<div class="luv-dashboard-row">
			<?php if (!defined('FEVR_WHITELABEL')):?>
			<div class="luv-dashboard-col">
				<div class="luv-icon-wrapper"><i class="ion-ios-settings-strong"></i></div>
				<h4><?php esc_html_e('Demo Content', 'fevr')?></h4>	
				<a class="luv-btn luv-btn-blue" href="<?php echo esc_url(FEVR_LUVINSTALLER_INSTALLER_URL);?>"><?php echo (isset($installer_info['scheme']) && !empty($installer_info['scheme'])) ? esc_html__('Continue install', 'fevr') : esc_html__('Install demo content', 'fevr')?></a>
			</div>
			<?php endif;?>
			<?php if($ms_restriction):?>
			<div class="luv-dashboard-col">
				<div class="luv-icon-wrapper"><i class="ion-loop"></i></div>
				<h4><?php esc_html_e('Updates', 'fevr')?></h4>
				<?php if (!empty($update_info) && !isset($update_info['dismiss']) && version_compare(FEVR_THEME_VER, $update_info['version'], '<')):?>
					<a class="luv-btn luv-btn-green" href="<?php echo esc_url(add_query_arg( array('luv-updater' => '1', 'tab' => 'dashboard'), menu_page_url('fevr-dashboard', false)));?>"><?php esc_html_e('Update now', 'fevr')?></a>
				<?php else:?>
					<a class="luv-btn luv-btn-green" href="<?php echo  esc_url(add_query_arg( array('check-luv-updates' => '1', 'tab' => 'dashboard'), menu_page_url('fevr-dashboard', false)));?>"><?php esc_html_e('Check for updates', 'fevr')?></a>
				<?php endif;?>
				</div>
			</div>
			<?php endif;?>			
		<?php else:?>
			<a class="luv-btn luv-btn-blue" href="<?php echo  esc_url(add_query_arg( array('tab' => 'activate'), menu_page_url('fevr-dashboard', false)));?>"><?php esc_html_e('Please activate your copy', 'fevr')?></a>
		<?php endif;?>
	<?php endif;?>
</div>