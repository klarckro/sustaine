<?php
//======================================================================
// Megamenu
//======================================================================

if( ! class_exists( 'Fevr_Mega_Menu' ) ) :
	class Fevr_Mega_Menu extends Walker_Nav_Menu {
	
	public $mega_menu_parent;
	
	/**
	 * Starts the list before the elements are added.
	 *
	 * @see Walker_Nav_Menu::start_lvl()
	 *
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   Not used.
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ) {}
	
	/**
	 * Ends the list of after the elements are added.
	 *
	 * @see Walker_Nav_Menu::end_lvl()
	 *
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   Not used.
	 */
	public function end_lvl( &$output, $depth = 0, $args = array() ) {}
	
	/**
	 * Start the element output.
	 *
	 * @see Walker_Nav_Menu::start_el()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item   Menu item data object.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   Not used.
	 * @param int    $id     Not used.
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $_wp_nav_menu_max_depth;
		
		$_wp_nav_menu_max_depth = $depth > $_wp_nav_menu_max_depth ? $depth : $_wp_nav_menu_max_depth;
	
		ob_start();
		$item_id = esc_attr( $item->ID );
		$removed_args = array(
				'action',
				'customlink-tab',
				'edit-menu-item',
				'menu-item',
				'page-tab',
				'_wpnonce',
		);
		
		$original_title = '';
		if ( 'taxonomy' == $item->type ) {
			$original_title = get_term_field( 'name', $item->object_id, $item->object, 'raw' );
			if ( is_wp_error( $original_title ) )
				$original_title = false;
		} elseif ( 'post_type' == $item->type ) {
			$original_object = get_post( $item->object_id );
			$original_title = get_the_title( $original_object->ID );
		}
	
		$classes = array(
				'menu-item menu-item-depth-' . $depth,
				'menu-item-' . esc_attr( $item->object ),
				'menu-item-edit-' . ( ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? 'active' : 'inactive'),
		);
		
		$title = $item->title;
	
		if ( ! empty( $item->_invalid ) ) {
			$classes[] = 'menu-item-invalid';
			/* translators: %s: title of menu item which is invalid */
			$title = sprintf( esc_html__( '%s (Invalid)', 'fevr' ), $item->title );
		} elseif ( isset( $item->post_status ) && 'draft' == $item->post_status ) {
			$classes[] = 'pending';
			/* translators: %s: title of menu item in draft status */
			$title = sprintf( esc_html__('%s (Pending)', 'fevr'), $item->title );
		}
	
		$title = ( ! isset( $item->label ) || '' == $item->label ) ? $title : $item->label;
	
		$submenu_text = '';
		if ( 0 == $depth ){
			$submenu_text = 'style="display: none;"';
		}
		?>
			<li id="menu-item-<?php echo esc_attr($item_id); ?>" class="<?php echo esc_attr(implode(' ', $classes )); ?>">
				<dl class="menu-item-bar">
					<dt class="menu-item-handle">
						<span class="item-title"><span class="menu-item-title"><?php echo esc_html( $title ); ?></span> <span class="is-submenu" <?php echo esc_attr($submenu_text); ?>><?php esc_html_e( 'sub item', 'fevr' ); ?></span></span>
						<span class="item-controls">
							<span class="item-type"><?php echo esc_html( $item->type_label ); ?></span>
							<span class="item-order hide-if-js">
								<a href="<?php
									echo esc_url(
											wp_nonce_url(
												add_query_arg(
													array(
														'action' => 'move-up-menu-item',
														'menu-item' => $item_id,
													),
													remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) )
												),
												'move-menu_item'
											)
										);
								?>" class="item-move-up"><abbr title="<?php esc_attr_e('Move up', 'fevr'); ?>">&#8593;</abbr></a>
								|
								<a href="<?php
									echo esc_url(
										wp_nonce_url(
											add_query_arg(
												array(
													'action' => 'move-down-menu-item',
													'menu-item' => $item_id,
												),
												remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) )
											),
											'move-menu_item'
										)
									);
								?>" class="item-move-down"><abbr title="<?php esc_attr_e('Move down', 'fevr'); ?>">&#8595;</abbr></a>
							</span>
							<a class="item-edit" id="edit-<?php echo esc_attr($item_id); ?>" title="<?php esc_attr_e('Edit Menu Item', 'fevr'); ?>" href="<?php
								echo esc_url(( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? admin_url( 'nav-menus.php' ) : add_query_arg( 'edit-menu-item', $item_id, remove_query_arg( $removed_args, admin_url( 'nav-menus.php#menu-item-settings-' . $item_id ) ) ));
							?>"><?php esc_html_e( 'Edit Menu Item', 'fevr' ); ?></a>
						</span>
					</dt>
				</dl>
	
				<div class="menu-item-settings" id="menu-item-settings-<?php echo esc_attr($item_id); ?>">
					<?php if( 'custom' == $item->type ) : ?>
						<p class="field-url description description-wide">
							<label for="edit-menu-item-url-<?php echo esc_attr($item_id); ?>">
								<?php esc_html_e( 'URL', 'fevr' ); ?><br />
								<input type="text" id="edit-menu-item-url-<?php echo esc_attr($item_id); ?>" class="widefat code edit-menu-item-url" name="menu-item-url[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->url ); ?>" />
							</label>
						</p>
					<?php endif; ?>
					<p class="description description-thin">
						<label for="edit-menu-item-title-<?php echo esc_attr($item_id); ?>">
							<?php esc_html_e( 'Navigation Label', 'fevr' ); ?><br />
							<input type="text" id="edit-menu-item-title-<?php echo esc_attr($item_id); ?>" class="widefat edit-menu-item-title" name="menu-item-title[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->title ); ?>" />
						</label>
					</p>
					<p class="description description-thin">
						<label for="edit-menu-item-attr-title-<?php echo esc_attr($item_id) ?>">
							<?php esc_html_e( 'Title Attribute', 'fevr' ); ?><br />
							<input type="text" id="edit-menu-item-attr-title-<?php echo esc_attr($item_id); ?>" class="widefat edit-menu-item-attr-title" name="menu-item-attr-title[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->post_excerpt ); ?>" />
						</label>
					</p>
					<p class="field-link-target description">
						<label for="edit-menu-item-target-<?php echo esc_attr($item_id); ?>">
							<input type="checkbox" id="edit-menu-item-target-<?php echo esc_attr($item_id); ?>" value="_blank" name="menu-item-target[<?php echo esc_attr($item_id); ?>]"<?php checked( $item->target, '_blank' ); ?> />
							<?php esc_html_e( 'Open link in a new window/tab', 'fevr' ); ?>
						</label>
					</p>
					<p class="field-css-classes description description-thin">
						<label for="edit-menu-item-classes-<?php echo esc_attr($item_id); ?>">
							<?php esc_html_e( 'CSS Classes (optional)', 'fevr' ); ?><br />
							<input type="text" id="edit-menu-item-classes-<?php echo esc_attr($item_id); ?>" class="widefat code edit-menu-item-classes" name="menu-item-classes[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( implode(' ', $item->classes ) ); ?>" />
						</label>
					</p>
					<p class="field-xfn description description-thin">
						<label for="edit-menu-item-xfn-<?php echo esc_attr($item_id); ?>">
							<?php esc_html_e( 'Link Relationship (XFN)', 'fevr' ); ?><br />
							<input type="text" id="edit-menu-item-xfn-<?php echo esc_attr($item_id); ?>" class="widefat code edit-menu-item-xfn" name="menu-item-xfn[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->xfn ); ?>" />
						</label>
					</p>
					<p class="field-description description description-wide">
						<label for="edit-menu-item-description-<?php echo esc_attr($item_id); ?>">
							<?php esc_html_e( 'Description', 'fevr' ); ?><br />
							<textarea id="edit-menu-item-description-<?php echo esc_attr($item_id); ?>" class="widefat edit-menu-item-description" rows="3" cols="20" name="menu-item-description[<?php echo esc_attr($item_id); ?>]"><?php echo esc_html( $item->description ); // textarea_escaped ?></textarea>
						</label>
					</p>
	
					<p class="field-move hide-if-no-js description description-wide">
						<label>
							<span><?php esc_html_e( 'Move', 'fevr' ); ?></span>
							<a href="#" class="menus-move-up"><?php esc_html_e( 'Up one', 'fevr' ); ?></a>
							<a href="#" class="menus-move-down"><?php esc_html_e( 'Down one', 'fevr' ); ?></a>
							<a href="#" class="menus-move-left"></a>
							<a href="#" class="menus-move-right"></a>
							<a href="#" class="menus-move-top"><?php esc_html_e( 'To the top', 'fevr' ); ?></a>
						</label>
					</p>
	
					<?php if(fevr_check_luvoption('header-position', 'left', '!=')):?>
					<?php
					 if($depth == 0){
						$this->mega_menu_parent = $item_id;
					 }
					?>
					<div class="menu-item-luv-megamenu">
						<p class="description">	
							<label for="luv-megamenu-status[<?php echo esc_attr($item_id);?>]">
								<?php esc_html_e('Enable Megamenu', 'fevr');?>					
								<input type="checkbox" name="luv-megamenu-status[<?php echo esc_attr($item_id);?>]" id="luv-megamenu-status[<?php echo esc_attr($item_id);?>]" data-menuid="<?php echo esc_attr($item_id);?>" class="enable-megamenu-switch" value="enabled" <?php checked($item->fevr_megamenu_status, 'enabled')?>>
							</label>
						</p>
					</div>
					<div class="megamenu-background-image-container luv-required" data-required-name="luv-megamenu-status[<?php echo esc_attr($item_id);?>]" data-required-value="enabled" data-required-compare="=">
						<p class="description">	
							<?php esc_html_e('Megamenu Background image', 'fevr');?>	
							<div class="luv-media-upload-container media-image">
								<input type="hidden" name="luv-megamenu-background[<?php echo esc_attr($item_id);?>]" class="luv-media-upload-url" value="<?php echo (!empty($item->fevr_megamenu_background) ? $item->fevr_megamenu_background : '');?>" <?php disabled($item->fevr_megamenu_status, '')?>>
								<div class="luv-gallery-img-container"><img src="<?php echo (!empty($item->fevr_megamenu_background) ? esc_url($item->fevr_megamenu_background) : '');?>" class="luv-media-upload-preview <?php echo (empty($item->fevr_megamenu_background) ? 'is-hidden' : '');?>"></div>
								<div class="luv-media-buttons">
									<span class="button media_upload_button luv-media-upload"><?php echo (empty($item->fevr_megamenu_background) ? esc_html__('Upload', 'fevr') : esc_html__('Modify', 'fevr'));?></span>
									<span class="button remove-image luv-media-upload-reset <?php echo (empty($item->fevr_megamenu_background) ? 'is-hidden' : '');?>"><?php esc_html_e('Remove', 'fevr')?></span>
								</div>
								<select name="luv-megamenu-background_size[<?php echo esc_attr($item_id);?>]" <?php disabled($item->fevr_megamenu_status, '')?>>
									<option value="cover"<?php selected($item->fevr_megamenu_background_size,'cover')?>><?php esc_html_e('Cover', 'fevr');?></option>
									<option value="contain"<?php selected($item->fevr_megamenu_background_size,'contain')?>><?php esc_html_e('Contain', 'fevr');?></option>
								</select>
								<select name="luv-megamenu-background_position[<?php echo esc_attr($item_id);?>]" <?php disabled($item->fevr_megamenu_status, '')?>>
									<option value="left top"<?php selected($item->fevr_megamenu_background_position,'left top')?>><?php esc_html_e('Left top', 'fevr');?></option>
									<option value="left center"<?php selected($item->fevr_megamenu_background_position,'left center')?>><?php esc_html_e('Left center', 'fevr');?></option>
									<option value="left bottom"<?php selected($item->fevr_megamenu_background_position,'left bottom')?>><?php esc_html_e('Left bottom', 'fevr');?></option>
									<option value="right top"<?php selected($item->fevr_megamenu_background_position,'right top')?>><?php esc_html_e('Right top', 'fevr');?></option>
									<option value="right center"<?php selected($item->fevr_megamenu_background_position,'right center')?>><?php esc_html_e('Right center', 'fevr');?></option>
									<option value="right bottom"<?php selected($item->fevr_megamenu_background_position,'right bottom')?>><?php esc_html_e('Right bottom', 'fevr');?></option>
									<option value="center top"<?php selected($item->fevr_megamenu_background_position,'center top')?>><?php esc_html_e('Center top', 'fevr');?></option>
									<option value="center center"<?php selected($item->fevr_megamenu_background_position,'center center')?>><?php esc_html_e('Center center', 'fevr');?></option>
									<option value="center bottom"<?php selected($item->fevr_megamenu_background_position,'center bottom')?>><?php esc_html_e('Center bottom', 'fevr');?></option>
								</select>
							</div>
						</p>
					</div>
					<div class="menu-item-luv-megamenu-widgetarea l-megamenu-<?php echo esc_attr($this->mega_menu_parent);?>">
						<p class="description">
						<label>
							<?php esc_html_e('Use Widget Area', 'fevr');?>
							<input type="checkbox" id="luv-megamenu-use-widgetarea-<?php echo esc_attr($item_id); ?>" name="luv-megamenu-use-widgetarea[<?php echo esc_attr($item_id); ?>]" value="enabled" <?php checked($item->fevr_megamenu_use_widgetarea, 'enabled')?>>
						</label>
					</div>
					<div class="menu-item-luv-megamenu-select-widgetarea l-megamenu-<?php echo esc_attr($this->mega_menu_parent);?> luv-required" data-required-name="luv-megamenu-use-widgetarea[<?php echo esc_attr($item_id); ?>]" data-required-value="enabled" data-required-compare="=" <?php disabled($item->fevr_megamenu_use_widgetarea, '')?>>
						<select id="luv-megamenu-widgetarea-<?php echo esc_attr($item_id); ?>" class="luv-megamenu-widgetarea" name="luv-megamenu-widget-area[<?php echo esc_attr($item_id); ?>]">
							<option value=""><?php esc_html_e( 'Select Widget Area', 'fevr' ); ?></option> 
							<?php
							global $wp_registered_sidebars;
							if(is_array( $wp_registered_sidebars ) && !empty($wp_registered_sidebars)):
								foreach( $wp_registered_sidebars as $widget_area ):
							?>
							<option value="<?php echo esc_attr($widget_area['id']); ?>" <?php selected( $item->fevr_megamenu_widget_area, $widget_area['id'] ); ?>><?php echo esc_html($widget_area['name']); ?></option>
							<?php 
								endforeach;
							endif;
							?>
						</select>
					</div>
					<div class="menu-item-luv-megamenu-columns l-megamenu-<?php echo esc_attr($this->mega_menu_parent);?>">
						<p class="description">
							<label for="luv-megamenu-columns[<?php echo esc_attr($item_id);?>]">
							<?php esc_html_e('Mega menu columns', 'fevr');?>
							</label>					
							<select name="luv-megamenu-columns[<?php echo esc_attr($item_id);?>]" id="luv-megamenu-columns[<?php echo esc_attr($item_id);?>]">
								<option value="1" <?php selected($item->fevr_megamenu_columns, 1)?>>1/12</option>
								<option value="2" <?php selected($item->fevr_megamenu_columns, 2)?>>2/12</option>
								<option value="3" <?php selected($item->fevr_megamenu_columns, 3)?>>3/12</option>
								<option value="4" <?php selected($item->fevr_megamenu_columns, 4)?>>4/12</option>
								<option value="5" <?php selected($item->fevr_megamenu_columns, 5)?>>5/12</option>
								<option value="6" <?php selected($item->fevr_megamenu_columns, 6)?>>6/12</option>
								<option value="7" <?php selected($item->fevr_megamenu_columns, 7)?>>7/12</option>
								<option value="8" <?php selected($item->fevr_megamenu_columns, 8)?>>8/12</option>
								<option value="9" <?php selected($item->fevr_megamenu_columns, 9)?>>9/12</option>
								<option value="10" <?php selected($item->fevr_megamenu_columns, 10)?>>10/12</option>
								<option value="11" <?php selected($item->fevr_megamenu_columns, 11)?>>11/12</option>
								<option value="12" <?php selected($item->fevr_megamenu_columns, 12)?>>12/12</option>
								<option value="custom" <?php selected($item->fevr_megamenu_columns, 'custom')?>><?php esc_html_e('Custom', 'fevr');?></option>
							</select>
						</p>
					</div>
					<div class="menu-item-luv-megamenu-custom-columns l-megamenu-<?php echo esc_attr($this->mega_menu_parent);?> luv-required" data-required-name="luv-megamenu-columns[<?php echo esc_attr($item_id);?>]" data-required-value="custom" data-required-compare="=">
						<p class="description">
							<label for="luv-megamenu-custom-column[<?php echo esc_attr($item_id);?>]">
							<?php esc_html_e('Custom column width (%)', 'fevr');?>
							</label>					
							<input name="luv-megamenu-custom-column[<?php echo esc_attr($item_id);?>]" id="luv-megamenu-custom-column[<?php echo esc_attr($item_id);?>]" value="<?php echo esc_attr($item->fevr_megamenu_custom_column);?>">
						</p>
					</div>
					<?php endif;?>
	
					<div class="menu-item-actions description-wide submitbox">
						<?php if( 'custom' != $item->type && $original_title !== false ) : ?>
							<p class="link-to-original">
								<?php printf( esc_html__('Original: %s', 'fevr'), '<a href="' . esc_attr( $item->url ) . '">' . esc_html( $original_title ) . '</a>' ); ?>
							</p>
						<?php endif; ?>
						<a class="item-delete submitdelete deletion" id="delete-<?php echo esc_attr($item_id); ?>" href="<?php
						echo esc_url(
							wp_nonce_url(
								add_query_arg(
									array(
										'action' => 'delete-menu-item',
										'menu-item' => $item_id,
									),
									admin_url( 'nav-menus.php' )
								),
								'delete-menu_item_' . $item_id
							)
						);
						?>"><?php esc_html_e( 'Remove', 'fevr' ); ?></a> <span class="meta-sep hide-if-no-js"> | </span> <a class="item-cancel submitcancel hide-if-no-js" id="cancel-<?php echo esc_attr($item_id) ?>" href="<?php echo esc_url( add_query_arg( array( 'edit-menu-item' => $item_id, 'cancel' => time() ), admin_url( 'nav-menus.php' ) ) );
							?>#menu-item-settings-<?php echo esc_attr($item_id); ?>"><?php esc_html_e('Cancel', 'fevr'); ?></a>
					</div>
	
					<input class="menu-item-data-db-id" type="hidden" name="menu-item-db-id[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr($item_id); ?>" />
					<input class="menu-item-data-object-id" type="hidden" name="menu-item-object-id[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->object_id ); ?>" />
					<input class="menu-item-data-object" type="hidden" name="menu-item-object[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->object ); ?>" />
					<input class="menu-item-data-parent-id" type="hidden" name="menu-item-parent-id[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->menu_item_parent ); ?>" />
					<input class="menu-item-data-position" type="hidden" name="menu-item-position[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->menu_order ); ?>" />
					<input class="menu-item-data-type" type="hidden" name="menu-item-type[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->type ); ?>" />
				</div><!-- .menu-item-settings-->
				<ul class="menu-item-transport"></ul>
			<?php
			$output .= ob_get_clean();
		}
	}
	
endif;

if (!class_exists('Fevr_Mega_Menu_Frontend')):
	class Fevr_Mega_Menu_Frontend extends Walker_Nav_Menu {
		
		public $parent;

		/**
		 * What the class handles.
		 *
		 * @see Walker::$tree_type
		 * @since 3.0.0
		 * @var string
		 */
		public $tree_type = array( 'post_type', 'taxonomy', 'custom' );
		
		/**
		 * Database fields to use.
		 *
		 * @see Walker::$db_fields
		 * @since 3.0.0
		 * @var array
		*/
		public $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );
		
		/**
		 * Starts the list before the elements are added.
		 *
		 * @see Walker::start_lvl()
		 *
		 * @since 3.0.0
		 *
		 * @param string $output Passed by reference. Used to append additional content.
		 * @param int    $depth  Depth of menu item. Used for padding.
		 * @param array  $args   An array of arguments. @see wp_nav_menu()
		*/
		public function start_lvl( &$output, $depth = 0, $args = array() ) {
			$classes[] = 'sub-menu'; 
			if ($depth == 0 && isset($this->parent->fevr_megamenu_background) && !empty($this->parent->fevr_megamenu_background)){
				$size = (isset($this->parent->fevr_megamenu_background_size) ? $this->parent->fevr_megamenu_background_size : 'cover');
				$position = (isset($this->parent->fevr_megamenu_background_position) ? $this->parent->fevr_megamenu_background_position : 'left top');
				$classes[] = fevr_enqueue_inline_css(array('style'=>'background-image:url("'.$this->parent->fevr_megamenu_background.'") !important;background-size:'.$size.' !important;background-position:' . $position . ' !important', 'child' => array(' ul' => 'background:transparent !important;')));
			}
			$indent = str_repeat("\t", $depth);
			$output .= "\n$indent<ul class=\"".join(' ', $classes)."\">\n";
		}
		
		/**
		 * Ends the list of after the elements are added.
		 *
		 * @see Walker::end_lvl()
		 *
		 * @since 3.0.0
		 *
		 * @param string $output Passed by reference. Used to append additional content.
		 * @param int    $depth  Depth of menu item. Used for padding.
		 * @param array  $args   An array of arguments. @see wp_nav_menu()
		 */
		public function end_lvl( &$output, $depth = 0, $args = array() ) {
			$indent = str_repeat("\t", $depth);
			$output .= "$indent</ul>\n";
		}
		
		/**
		 * Start the element output.
		 *
		 * @see Walker::start_el()
		 *
		 * @since 3.0.0
		 *
		 * @param string $output Passed by reference. Used to append additional content.
		 * @param object $item   Menu item data object.
		 * @param int    $depth  Depth of menu item. Used for padding.
		 * @param array  $args   An array of arguments. @see wp_nav_menu()
		 * @param int    $id     Current item ID.
		 */
		public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {			
			$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		
			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
			$classes[] = 'menu-item-' . $item->ID;
		
			/**
			 * Add Fevr Megamenu class
			 */
				$is_widget = (isset($this->parent) && $this->parent->fevr_megamenu_status == 'enabled' && isset($item->fevr_megamenu_widget_area) && !empty($item->fevr_megamenu_widget_area) && fevr_check_luvoption('header-position', 'left', '!=') ? true : false);
				
				if ($depth == 0 && $item->fevr_megamenu_status == 'enabled' && fevr_check_luvoption('header-position', 'left', '!=')){
					$classes[] = 'l-megamenu';
				}
				
				elseif (($depth == 1 || $depth == 2) && isset($this->parent) && $this->parent->fevr_megamenu_status == 'enabled' && fevr_check_luvoption('header-position', 'left', '!=')){
					$classes[] = 'l-megamenu-' . $item->fevr_megamenu_columns;
					if ($classes[] = $item->fevr_megamenu_columns == 'custom'){
						$classes[] = fevr_enqueue_inline_css(array('parent' => '[data-header-position="default"] #main-header ','style' => 'width: ' . esc_attr($item->fevr_megamenu_custom_column) . '% !important'));
					}
				}
				
				if ($is_widget){
					$classes[] = 'l-megamenu-widget';
				}
			
			/**
			 * Filter the CSS class(es) applied to a menu item's list item element.
			 *
			 * @since 3.0.0
			 * @since 4.1.0 The `$depth` parameter was added.
			 *
			 * @param array  $classes The CSS classes that are applied to the menu item's `<li>` element.
			 * @param object $item    The current menu item.
			 * @param array  $args    An array of {@see wp_nav_menu()} arguments.
			 * @param int    $depth   Depth of menu item. Used for padding.
			 */
			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
			$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
			
			/**
			 * Filter the ID applied to a menu item's list item element.
			 *
			 * @since 3.0.1
			 * @since 4.1.0 The `$depth` parameter was added.
			 *
			 * @param string $menu_id The ID that is applied to the menu item's `<li>` element.
			 * @param object $item    The current menu item.
			 * @param array  $args    An array of {@see wp_nav_menu()} arguments.
			 * @param int    $depth   Depth of menu item. Used for padding.
			 */
			$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
			$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
		
			$output .= $indent . '<li' . $id . $class_names .'>';
		
			$atts = array();
			$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
			$atts['target'] = ! empty( $item->target )     ? $item->target     : '';
			$atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
			$atts['href']   = ! empty( $item->url )        ? $item->url        : '';
		
			/**
			 * Filter the HTML attributes applied to a menu item's anchor element.
			 *
			 * @since 3.6.0
			 * @since 4.1.0 The `$depth` parameter was added.
			 *
			 * @param array $atts {
			 *     The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
			 *
			 *     @type string $title  Title attribute.
			 *     @type string $target Target attribute.
			 *     @type string $rel    The rel attribute.
			 *     @type string $href   The href attribute.
			 * }
			 * @param object $item  The current menu item.
			 * @param array  $args  An array of {@see wp_nav_menu()} arguments.
			 * @param int    $depth Depth of menu item. Used for padding.
			 */
			$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );
		
			$attributes = '';
			foreach ( $atts as $attr => $value ) {
				if ( ! empty( $value ) ) {
					$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
					$attributes .= ' ' . $attr . '="' . $value . '"';
				}
			}
			
			$item_output = (isset($args->before) ? $args->before : '');
				
			if ($is_widget){
				if( is_active_sidebar( $item->fevr_megamenu_widget_area ) ) {
					ob_start();
					dynamic_sidebar( $item->fevr_megamenu_widget_area );
					$item_output .= ob_get_clean();
				}
			}
			else{
				$item_output .= '<a'. $attributes .'>';
				/** This filter is documented in wp-includes/post-template.php */
				$item_output .= (isset($args->link_before) ? $args->link_before : '') . apply_filters( 'the_title', $item->title, $item->ID ) . (isset($args->link_after) ? $args->link_after : '');
				$item_output .= '</a>';
			}
			
			$item_output .= (isset($args->after) ? $args->after : '');
	
			
			/**
			 * Filter a menu item's starting output.
			 *
			 * The menu item's starting output only includes `$args->before`, the opening `<a>`,
			 * the menu item's title, the closing `</a>`, and `$args->after`. Currently, there is
			 * no filter for modifying the opening and closing `<li>` for a menu item.
			 *
			 * @since 3.0.0
			 *
			 * @param string $item_output The menu item's starting HTML output.
			 * @param object $item        Menu item data object.
			 * @param int    $depth       Depth of menu item. Used for padding.
			 * @param array  $args        An array of {@see wp_nav_menu()} arguments.
			 */
			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
			
			//Set the parent element id for megamenu
			if ($depth == 0){
				$this->parent = $item;
			}
		}
		
		/**
		 * Ends the element output, if needed.
		 *
		 * @see Walker::end_el()
		 *
		 * @since 3.0.0
		 *
		 * @param string $output Passed by reference. Used to append additional content.
		 * @param object $item   Page data object. Not used.
		 * @param int    $depth  Depth of page. Not Used.
		 * @param array  $args   An array of arguments. @see wp_nav_menu()
		 */
		public function end_el( &$output, $item, $depth = 0, $args = array() ) {
			$output .= "</li>\n";
		}

}
endif;

if (!class_exists('Fevr_Mega_Menu_Framework')):
	class Fevr_Mega_Menu_Framework {

		/**
		 * Set the framework hooks
		 */
		public function __construct(){
			add_action( 'wp_update_nav_menu_item', array( $this, 'save_custom_fields' ), 10, 3 );
		
			add_filter( 'wp_edit_nav_menu_walker', array($this, 'add_custom_walker') );
			add_filter( 'wp_setup_nav_menu_item', array( $this, 'add_custom_fields_to_menu' ) );
		}
		
		
		/**
		 * Add custom fields data to the menu
		 *
		 * @return object Add custom fields data to the menu object
		 */
		public function add_custom_fields_to_menu($menu_item) {
			$menu_item->fevr_megamenu_status = get_post_meta($menu_item->ID, 'fevr_menu_item_megamenu_status', true);
			$menu_item->fevr_megamenu_background = get_post_meta($menu_item->ID, 'fevr_menu_item_megamenu_background', true);
			$menu_item->fevr_megamenu_background_size = get_post_meta($menu_item->ID, 'fevr_menu_item_megamenu_background_size', true);
			$menu_item->fevr_megamenu_background_position = get_post_meta($menu_item->ID, 'fevr_menu_item_megamenu_background_position', true);
			$menu_item->fevr_megamenu_columns = get_post_meta($menu_item->ID, 'fevr_menu_item_megamenu_columns', true);
			$menu_item->fevr_megamenu_custom_column = get_post_meta($menu_item->ID, 'fevr_menu_item_megamenu_custom_column', true);
			$menu_item->fevr_megamenu_use_widgetarea = get_post_meta($menu_item->ID, 'fevr_menu_item_megamenu_use_widgetarea', true);
			$menu_item->fevr_megamenu_widget_area = get_post_meta($menu_item->ID, 'fevr_menu_item_megamenu_widget_area', true);
			return $menu_item;
		
		}
		

		/**
		 * Add the custom fields menu item data to fields in database
		 *
		 * @return void
		 */
		public function save_custom_fields( $id, $menu_item_id, $args ) {
			if ($args['menu-item-parent-id'] == 0){				
				// Megamenu status
				$megamenu_status = (isset($_REQUEST['luv-megamenu-status'][$menu_item_id]) && $_REQUEST['luv-megamenu-status'][$menu_item_id] == 'enabled' ? 'enabled' : '');		
				update_post_meta( $menu_item_id, 'fevr_menu_item_megamenu_status', $megamenu_status);
				
				// Megamenu background
				if (isset($_REQUEST['luv-megamenu-background'][$menu_item_id])){
					update_post_meta( $menu_item_id, 'fevr_menu_item_megamenu_background', $_REQUEST['luv-megamenu-background'][$menu_item_id]);
					update_post_meta( $menu_item_id, 'fevr_menu_item_megamenu_background_size', $_REQUEST['luv-megamenu-background_size'][$menu_item_id]);
					update_post_meta( $menu_item_id, 'fevr_menu_item_megamenu_background_position', $_REQUEST['luv-megamenu-background_position'][$menu_item_id]);
				}
				else{
					update_post_meta( $menu_item_id, 'fevr_menu_item_megamenu_background', '');
					update_post_meta( $menu_item_id, 'fevr_menu_item_megamenu_background_size', '');
					update_post_meta( $menu_item_id, 'fevr_menu_item_megamenu_background_position', '');
				}
			}
			else{
				// Megamenu column width. Set 3(/12) by default on depth 1 and 12(/12) on depth 2
				$default_width = ($args['menu-item-parent-id'] == 1 ? 3 : 12); 
				$megamenu_columns			= (isset($_REQUEST['luv-megamenu-columns'][$menu_item_id]) ? $_REQUEST['luv-megamenu-columns'][$menu_item_id] : $default_width);
				$megamenu_custom_column	= (isset($_REQUEST['luv-megamenu-custom-column'][$menu_item_id]) ? (int)$_REQUEST['luv-megamenu-custom-column'][$menu_item_id] : '25');
				$megamenu_use_widget_area	= (isset($_REQUEST['luv-megamenu-use-widgetarea'][$menu_item_id]) && $_REQUEST['luv-megamenu-use-widgetarea'][$menu_item_id] == 'enabled' ? 'enabled' : '');
				$megamenu_widget_area		= ($megamenu_use_widget_area == 'enabled' && isset($_REQUEST['luv-megamenu-widget-area'][$menu_item_id]) ? $_REQUEST['luv-megamenu-widget-area'][$menu_item_id] : '');
				update_post_meta( $menu_item_id, 'fevr_menu_item_megamenu_columns', $megamenu_columns);
				update_post_meta( $menu_item_id, 'fevr_menu_item_megamenu_custom_column', $megamenu_custom_column);
				update_post_meta( $menu_item_id, 'fevr_menu_item_megamenu_use_widgetarea', $megamenu_use_widget_area);
				update_post_meta( $menu_item_id, 'fevr_menu_item_megamenu_widget_area', $megamenu_widget_area);
			}
		}
		
		
		/**
		 * Return with custom menu walker for nav menu editor
		 * @return string
		 */
		public function add_custom_walker() {
			return 'Fevr_Mega_Menu';
		}
	}
	
	//Init the Mega menu framework
	$Fevr_Mega_Menu_Framework = new Fevr_Mega_Menu_Framework();
	
endif;
?>