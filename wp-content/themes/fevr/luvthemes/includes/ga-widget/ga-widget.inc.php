<div id="embed-api-auth-container"></div>
<div id="chart-container"></div>
<select class="luv-ga-metric">
	<option value="sessions"><?php esc_html_e('Sessions', 'fevr')?></option>
	<option value="pageviews"><?php esc_html_e('Page Views', 'fevr')?></option>
	<option value="bounceRate"><?php esc_html_e('Bounce Rate', 'fevr')?></option>
	<option value="users"><?php esc_html_e('Users', 'fevr')?></option>
</select>
