if (pagenow == 'dashboard'){
	(function(w,d,s,g,js,fs){
	  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
	  js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
	  js.src='https://apis.google.com/js/platform.js';
	  fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
	}(window,document,'script'));
	
	
	
	gapi.analytics.ready(function() {
	  gapi.analytics.auth.authorize({
	    container: 'embed-api-auth-container',
	    clientid: fevr_ga_widget.clientid
	  });
	  var dataChart = new gapi.analytics.googleCharts.DataChart({
	    query: {
	      'ids': 'ga:101759784',
	      metrics: 'ga:sessions',
	      dimensions: 'ga:date',
	      'start-date': '30daysAgo',
	      'end-date': 'yesterday'
	    },
	    chart: {
	      container: 'chart-container',
	      type: 'LINE',
	      options: {
	        width: '100%'
	      }
	    }
	  });
	  dataChart.set({query: {ids: 'ga:101759784'}}).execute();
		jQuery(document).on('change', '.luv-ga-metric', function(){
			  var dataChart = new gapi.analytics.googleCharts.DataChart({
				    query: {
				      'ids': 'ga:' + fevr_ga_widget.vid,
				      metrics: 'ga:' + jQuery(this).val(),
				      dimensions: 'ga:date',
				      'start-date': '30daysAgo',
				      'end-date': 'yesterday'
				    },
				    chart: {
				      container: 'chart-container',
				      type: 'LINE',
				      options: {
				        width: '100%'
				      }
				    }
				  });
	
				  dataChart.set({query: {ids: 'ga:101759784'}}).execute();
		})
	  
	});
}