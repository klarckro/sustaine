<div class="luvthemes-templates-container">
	<h1><?php esc_html_e('Add Page from Layout Template', 'fevr')?></h1>
	<?php if (isset($_GET['group']) && !empty($_GET['group'])):?>
	<a class="luv-btn luv-btn-blue luv-group-btn on-hover" href="<?php echo esc_url(add_query_arg(array('post_type' => 'page', 'page' => 'fevr_use_template'),admin_url('edit.php')))?>"><span><?php echo esc_html($_GET['group']);?></span><i class="dashicons dashicons-no-alt"></i></a>
	<?php endif;?>
	<input type="text" class="luvthemes-templates-search" placeholder="<?php esc_attr_e('Search', 'fevr'); ?>" data-group="<?php echo (isset($_GET['group']) && !empty($_GET['group']) ? esc_attr($_GET['group']) : '')?>">
	<p><?php esc_html_e('Please select a template, and click to the "Add Page" button. After you selected the template WordPress will redirect you to the editor page.', 'fevr'); ?></p>
	<div class="luvthemes-templates-content">
		<div class="luvthemes-templates-loader"><i class="fa fa-spinner fa-pulse"></i></div>
	</div>
</div>