<?php

if (! class_exists ( 'WP_List_Table' )) {
	require_once (ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}


// Create submenu page
add_action ( 'admin_menu', 'fevr_feature_request_menu', 11 );
function fevr_feature_request_menu() {
	$fevr_purchase_key = fevr_purchase_key();
	if (!empty($fevr_purchase_key)){
		fevr_add_fevr_submenu_page ( 'fevr-dashboard', esc_html__ ( 'Feature Request', 'fevr' ), esc_html__ ( 'Feature Request', 'fevr' ), 'manage_options', 'fevr_feature_request', 'fevr_feature_request' );
	}
}

function fevr_feature_request() {
	$fevr_purchase_key = fevr_purchase_key();
	
	$table_data = '';
	
	if (isset($_GET['action']) && isset($_GET['vote']) && $_GET['action'] == 'vote'){
		$response = wp_remote_post ( esc_url(FEVR_API_URL . 'vote_feature/' . $_GET['vote']), array (
				'timeout' => 10,
				'sslverify' => false,
				'user-agent' => 'luv',
				'headers' => array (
						'X-ENVATO-PURCHASE-KEY' => trim ( $fevr_purchase_key )
				),
				'body' => array (
						'site' => home_url ()
				)
		) );
		
		if (is_wp_error ( $response )) {
			$admin_notice['type'] = 'error';
			$admin_notice['message'] = esc_html__ ( 'Couldn\'t connect to server, please try again.', 'fevr' );
		}
		else {
			$response = json_decode ( $response ['body'], true );
			if ($response ['error'] === true) {
				$admin_notice['type'] = 'error';
				$admin_notice['message'] = $response['response'];
			}
			else{
				$admin_notice['type'] = 'updated';
				$admin_notice['message'] = $response['response'];
			}
		}
	}
	else if (isset($_GET['action']) && $_GET['action'] == 'create-form'){
		$response = wp_remote_post ( FEVR_API_URL . 'create_feature_ability/', array (
				'timeout' => 10,
				'sslverify' => false,
				'user-agent' => 'luv',
				'headers' => array (
						'X-ENVATO-PURCHASE-KEY' => trim ( $fevr_purchase_key )
				),
				'body' => array (
						'site' => home_url ()
				)
		) );
		
		if (is_wp_error ( $response )) {
			$admin_notice['type'] = 'error';
			$admin_notice['message'] = esc_html__ ( 'Couldn\'t connect to server, please try again.', 'fevr' );
		}
		else {
			$response = json_decode ( $response ['body'], true );
			if ($response ['error'] === true) {
				$admin_notice['type'] = 'error';
				$admin_notice['message'] = $response['response'];
			}
			else{
				$create_form = '<form method="post" action="'.esc_url(add_query_arg('action', 'create' ,menu_page_url('fevr_feature_request', false))).'"><div id="poststuff"><div id="post-body" class="metabox-holder columns-1"><div id="post-body-content" style="position: relative;"><div id="titlediv"><div id="titlewrap"><input type="text" id="title" placeholder="'.esc_html__('Feature title', 'fevr').'" name="new-feature-title"><br><textarea name="new-feature-details" id="content"  placeholder="'.esc_html__('Feature description', 'fevr').'"  autocomplete="off" style="width:100%; height: 300px; margin-top: 7px;" class="wp-editor-area"></textarea><br><button class="button button-primary button-large">'.esc_html__('Send', 'fevr').'</button></div></div></div></div></div></form>';
			}
		}
	}
	else if (isset($_GET['action']) && $_GET['action'] == 'create'){
		if (isset($_POST['new-feature-title'])){
			$response = wp_remote_post ( FEVR_API_URL . 'create_feature/', array (
					'timeout' => 10,
					'sslverify' => false,
					'user-agent' => 'luv',
					'headers' => array (
							'X-ENVATO-PURCHASE-KEY' => trim ( $fevr_purchase_key )
					),
					'body' => array (
							'site' => home_url (),
							'feature' => array(
								'name' => $_POST['new-feature-title'],
								'details' => isset($_POST['new-feature-details']) ? $_POST['new-feature-details'] : ''								
							)
					)
			) );
	
			if (is_wp_error ( $response )) {
				$admin_notice['type'] = 'error';
				$admin_notice['message'] = esc_html__ ( 'Couldn\'t connect to server, please try again.', 'fevr' );
			}
			else {
				$response = json_decode ( $response ['body'], true );
				if ($response ['error'] === true) {
					$admin_notice['type'] = 'error';
					$admin_notice['message'] = $response['response'];
				}
				else{
					$admin_notice['type'] = 'updated';
					$admin_notice['message'] = $response['response'];
				}
			}
		}
		else{
			$admin_notice['type'] = 'error';
			$admin_notice['message'] = esc_html__ ( 'Title couldn\'t be empty', 'fevr' );
		}
	}
	
	$response = wp_remote_post ( FEVR_API_URL . 'get_features/', array (
			'timeout' => 10,
			'sslverify' => false,
			'user-agent' => 'luv',
			'headers' => array (
					'X-ENVATO-PURCHASE-KEY' => trim ( $fevr_purchase_key ) 
			),
			'body' => array (
					'site' => home_url () 
			) 
	) );
	
	if (is_wp_error ( $response )) {
		$admin_notice['type'] = 'error';
		$admin_notice['message'] = esc_html__ ( 'Couldn\'t connect to server, please try again.', 'fevr' );
	} 
	else {
		$response = json_decode ( $response ['body'], true );
		if ($response ['error'] === true) {
			$admin_notice['type'] = 'error';
			$admin_notice['message'] = $response['response'];
		}
		else {
			$table_data = $response ['response']['features'];
			
		}
	}
	
	$Fevr_Feature_Request_Table = new Fevr_Feature_Request_Table ( $table_data );
	if (isset($admin_notice['message'])){
		echo '<div class="'.esc_attr($admin_notice['type']).'"><p>'.fevr_kses($admin_notice['message']).'</p></div>';
	}
	echo '<div class="wrap"><h1>'.esc_html__('Feature Request', 'fevr').'</h1>';
	echo '<a class="luv-btn luv-btn-blue" href="'.esc_url(add_query_arg('action', 'create-form',menu_page_url('fevr_feature_request', false))).'">'.esc_html__('Suggest New Feature', 'fevr').'</a>';
	$Fevr_Feature_Request_Table->prepare_items();
	if (isset($create_form)){
		echo fevr_kses($create_form);
	}
	$Fevr_Feature_Request_Table->display();
	echo '</div>';
	
}

/**
 * Class for Feature request table
 *
 */
class Fevr_Feature_Request_Table extends WP_List_Table {
	
	/**
	 * Table content
	 * @var array
	 */
	public $table_data = array ();
	
	/**
	 * Construct for list table
	 * @param array $table_data
	 */
	function __construct($table_data) {
		global $status, $page;		

		parent::__construct ( array (
				'singular' => esc_html__ ( 'Feature Request', 'fevr' ),
				'plural' => esc_html__ ( 'Feature Requests', 'fevr' ),
				'ajax' => false 
		) );
		
		$this->table_data = $table_data;
	}
	
	/**
	 * Colunm name
	 * @param array $item
	 * @param string $column_name
	 * @return string
	 */
	public function column_default($item, $column_name) {
		switch ($column_name) {
			case 'feature' :
			case 'created' :
			case 'votes' :
				return $item [$column_name];
		}
	}
	
	/**
	 * Get all columns and set names to show
	 * @return array
	 */
	public function get_columns() {
		$columns = array (
				'votes' => esc_html__('Votes', 'fevr' ),
				'feature' => esc_html__ ( 'Feature', 'fevr' ),
				'created' => esc_html__ ( 'Created', 'fevr' ) 
				);
		return $columns;
	}
	
	/**
	 * Get sortable columns
	 * @return array
	 */
	public function get_sortable_columns() {
		$sortable_columns = array (
				'feature' => array (
						'feature',
						false 
				),
				'created' => array (
						'created',
						false 
				),
				'votes' => array (
						'votes',
						true
				)
		);
		return $sortable_columns;
	}
	
	/**
	 * Get column type
	 * @return string
	 */
	public function get_column_type($column) {
		switch ($column){
			case 'name':
				return 'string';				
			case 'created':
				return 'date';			
			case 'votes':
				return 'number';
			default:
				return 'string';
		}
	}
	
	/**
	 * Sorting function
	 * @param string $a
	 * @param string $b
	 * @return array
	 */
	public function usort_reorder( $a, $b ) {
		// If no sort, default to name
		$orderby = ( ! empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'name';
		// If no order, default to asc
		$order = ( ! empty($_GET['order'] ) ) ? $_GET['order'] : 'asc';
		// Determine column type and sort order 
		if ($this->get_column_type($orderby) == 'number'){
			// Number
			$result = ($a[$orderby] > $b[$orderby] ? 1 : ($a[$orderby] == $b[$orderby] ? 0 : -1 ));
		}	
		else if ($this->get_column_type($orderby) == 'date'){
			// Date
			$result = (strtotime($a[$orderby]) > strtotime($b[$orderby]) ? 1 : (strtotime($a[$orderby]) == strtotime($b[$orderby]) ? 0 : -1 ));
		}
		else{	
			// String
			$result = strcmp( $a[$orderby], $b[$orderby] );
		}
		// Send final sort direction to usort
		return ( $order === 'asc' ) ? $result : -$result;
	}
	
	/**
	 * Actions and rewrites for column "Title"
	 * @param array $item
	 */
	public function column_feature($item) {
		$name = (strlen($item['name']) > 50 && (!isset($_GET['action']) || !isset($_GET['id']) || $this->current_action() != 'expand' || $_GET['id'] != $item['id']) ? substr($item['name'],0,47) . '...' : $item['name']);
		if (isset($_GET['action']) && isset($_GET['id']) && $this->current_action() == 'expand' && $_GET['id'] == $item['id']){
			$action = 'hide';
			$details = $item['details'];
			$action_name = esc_html__('Hide', 'fevr');
		}
		else if(strlen($item['details']) > 50) {
			$details = substr($item['details'],0,47) . '...';
			$action = 'expand';
			$action_name = esc_html__('Expand', 'fevr');
		}
		else{
			$details = $item['details'];
		}
		
		if (isset($action)){
			$actions = array(
					'view' => '<a href="' . esc_url(add_query_arg(array('id' => (int)$item['id'], 'action' => $action), menu_page_url('fevr_feature_request', false))). '">'.esc_html($action_name).'</a>',
			);
			return ('<div>'.esc_html($name).'</div><div>'.esc_html($details).'</div>' . $this->row_actions($actions));
		}
		else{
			return ('<div>'.esc_html($name).'</div><div>'.esc_html($details).'</div>');
		}
		
	}
	
	/**
	 * Actions and rewrites for column "Created"
	 * @param array $item
	 */
	public function column_created($item) {
		return sprintf('%1$s', date('d-m-Y', $item['created']));
	}
	
	/**
	 * Actions and rewrites for column "Details"
	 * @param array $item
	 */
	public function column_details($item) {
		if (isset($_GET['action']) && isset($_GET['id']) && $this->current_action() == 'expand' && $_GET['id'] == $item['id']){
			$action = 'hide';
			$details = $item['details'];
			$action_name = esc_html__('Hide', 'fevr');
		}
		else if(strlen($item['details']) > 50) {
			$details = substr($item['details'],0,47) . '...';
			$action = 'expand';
			$action_name = esc_html__('Expand', 'fevr');
		}
		else{
			$details = $item['details'];
		}
		
		if (isset($action)){
			$actions = array(
					'view' => '<a href="'.esc_url(add_query_arg(array('action' => $action, 'id' => (int)$item['id']),menu_page_url('fevr_feature_request', false))).'">'.esc_html($action_name).'</a>',
			);
			return sprintf('%1$s %2$s', $details, $this->row_actions($actions) );
		}
		else{
			return sprintf('%1$s', $details );
		}
	
	}

	/**
	 * Actions and rewrites for column "Votes"
	 * @param array $item
	 */
	public function column_votes($item) {
		return $item['votes'] . ' <a href="'.esc_url(add_query_arg(array('action' =>'vote','vote' => (int)$item['id']),menu_page_url('fevr_feature_request', false))).'" class="luv-btn">' . esc_html__('Vote', 'fevr'). '</a>';
	}
	
	/**
	 * Prepare items to show
	 */
	public function prepare_items() {
		$columns = $this->get_columns();
		$hidden = array ();
		$sortable = $this->get_sortable_columns ();
		$this->_column_headers = array (
				$columns,
				$hidden,
				$sortable 
		);
		if (!empty($this->table_data)){
			usort ( $this->table_data, array (
					&$this,
					'usort_reorder' 
			) );
			
			$per_page = 10;
			$current_page = $this->get_pagenum();
			$total_items = count($this->table_data);
			
			$this->table_data = array_slice($this->table_data, (($current_page - 1) * $per_page), $per_page);
			
			$this->set_pagination_args ( array (
					'total_items' => $total_items,
					'per_page' => $per_page )
			);
		}
		$this->items = $this->table_data;
	}
} // Fevr_Feature_Request_Table

?>
