<?php 

/**
 * Update checker for Luvthemes
 *
 */
class Fevr_Updater{
	
	/**
	 * Create the updater object
	 * @param array $fevr_options
	 */
	public function __construct($fevr_purchase_key){
		$this->purchase_code = $fevr_purchase_key; 
		
		//Force WP Filesystem to use direct method
		add_filter('filesystem_method', array(&$this, 'force_direct_filesystem_method'));
	}
	
	/**
	 * Check the updates
	 */
	public function check_updates(){
		$response = wp_remote_post ( FEVR_API_URL . 'updater_info/', array (
				'timeout' => 10,
				'sslverify' => false,
				'user-agent' => 'luv',
				'headers' => array (
						'X-ENVATO-PURCHASE-KEY' => trim ( $this->purchase_code)
				),
				'body' => array (
						'site' => home_url ()
				)
		) );
		
		// Download failed
		if (is_wp_error($response)){
			throw new Exception($response->get_error_message());
		}
		else{
			$update_info		= json_decode($response['body'], true); 
			$last_update_info	= get_option('fevr_update',array()); 
			
			if (isset($update_info['version']) && version_compare(FEVR_THEME_VER, $update_info['version'], '<') && (!isset($last_update_info['version']) || $last_update_info['version'] != $update_info['version'])){
				update_option('fevr_update', $update_info);
			}
		}	
	}
	
	/**
	 * Download the latest version and update the theme
	 */
	public function update(){
		//Return if there isn't any updates
		$update_info = get_option('fevr_update',array());
		
		if (empty($update_info) || version_compare(FEVR_THEME_VER, $update_info['version'], '>=')){
			return false;
		}
		
		// Prevent PHP timeout
		set_time_limit(3600);
		$timeout = ini_get('max_execution_time');
		
		$download = wp_remote_post ( FEVR_API_URL . 'download/', array (
				'timeout' => $timeout,
				'sslverify' => false,
				'user-agent' => 'luv',
				'headers' => array (
						'X-ENVATO-PURCHASE-KEY' => trim ( $this->purchase_code)
				),
				'body' => array (
						'site' => home_url ()
				)
		) );

		// Download failed
		if (is_wp_error($download)){
			throw new Exception($download->get_error_message());
		}
		else if (!isset($download['headers']['luvthemes-filename'])){
			throw new Exception(esc_html__('API error, please try again later.', 'fevr'));
		}
		else{
			// Save temporary zip file
			WP_Filesystem();
			global $wp_filesystem;
			$wp_upload_dir = wp_upload_dir();
			$template_dir = get_template_directory();
			$filename = trailingslashit($wp_upload_dir['path']) . $download['headers']['luvthemes-filename'];
			
			// Check permissions
			if (!is_writable($wp_upload_dir['path'])){
				throw new Exception(esc_html__('The upload directory isn\'t writable, please change the permissions.', 'fevr'));
			}
			if (!is_writable(dirname($template_dir))){
				throw new Exception(esc_html__('The theme directory isn\'t writable, please change the permissions.', 'fevr'));
			}
				
			// Save the downloaded zip file
			if ( ! $wp_filesystem->put_contents( $filename, $download['body'], FS_CHMOD_FILE) ) {
				throw new Exception(esc_html__('File I/O error while creating file: ', 'fevr') . $filename);
			}
			else{
				// Unzip the downloaded theme
				$temp_dir = trailingslashit($wp_upload_dir['path']) . trailingslashit(hash('crc32',time()));
				$wp_filesystem->mkdir($temp_dir);
				$unzip = unzip_file($filename, $temp_dir);
				if (is_wp_error($unzip)){
					throw new Exception($unzip->get_error_message());
				}
				else{
					// Backup dynamic.css
					$dynamic_css = $wp_filesystem->get_contents(trailingslashit(get_template_directory()) . 'css/dynamic.css');
					
					// Empty the current theme directory
					$wp_filesystem->rmdir($template_dir, true);
					$wp_filesystem->mkdir($template_dir);
					
					//Move the temporary theme files to the theme directory
					foreach (scandir($temp_dir . basename($download['headers']['luvthemes-filename'], '.zip')) as $file){
						$this->rcopy($temp_dir . trailingslashit(basename($download['headers']['luvthemes-filename'], '.zip')) . $file, trailingslashit($template_dir) . $file);
					}
					
					//Remove temporary files
					$wp_filesystem->rmdir($temp_dir, true);
					unlink ($filename);
					
					// Restore dynamic.css 
					$wp_filesystem->put_contents(trailingslashit(get_template_directory()) . 'css/dynamic.css', $dynamic_css, FS_CHMOD_FILE);
					
					//Remove update info from options
					delete_option('fevr_update');
					return true;						
				}
			}
		}	
	}
	
	/**
	 * Recursive copy
	 * @param string $source
	 * @param string $destination
	 */
	public function rcopy($source, $destination){
		global $wp_filesystem;
		if ($wp_filesystem->is_dir($source)){
			$wp_filesystem->mkdir($destination);
			foreach (scandir($source) as $file){
				if ($file != "." && $file != ".."){
					$this->rcopy(trailingslashit($source) . $file, trailingslashit($destination) . $file);
				}
			}
		}
		else{
			$wp_filesystem->copy($source, $destination);
		}
	}
	
	/**
	 * Force WP_Filesystem to use direct method
	 */
	public function force_direct_filesystem_method(){
		return 'direct';
	}
	
}

?>