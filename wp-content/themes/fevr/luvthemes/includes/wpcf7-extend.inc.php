<?php 

add_action( 'wpcf7_init', 'fevr_wpcf7_submit_shortcode' );

/**
 * Remove default WPCF7 submit button shortcode and add overwritten version
 */
function fevr_wpcf7_submit_shortcode() {
	wpcf7_remove_shortcode( 'submit', 'wpcf7_submit_shortcode_handler' );
	fevr_wpcf7_add_fevr_shortcode( 'submit', 'fevr_wpcf7_submit_shortcode_handler' );
}

/**
 * Overwrite WPCF7 submit shortcode
 * @param string $tag
 * @return string
 */
function fevr_wpcf7_submit_shortcode_handler( $tag ) {
	$tag = new WPCF7_Shortcode( $tag );

	$class = wpcf7_form_controls_class( $tag->type );

	$atts = array();

	//Luv classes
	$class .= ' btn btn-global btn-full';
	
	$atts['class'] = $tag->get_class_option( $class );	
	$atts['id'] = $tag->get_id_option();
	$atts['tabindex'] = $tag->get_option( 'tabindex', 'int', true );

	$value = isset( $tag->values[0] ) ? $tag->values[0] : '';

	if ( empty( $value ) )
		$value = esc_html(fevr__w( 'Send', 'fevr', 'contact-form-7' ));

	$atts['type'] = 'submit';
	$atts['value'] = $value;

	$atts = wpcf7_format_atts( $atts );
		
	$html = sprintf( '<input %1$s />', $atts );

	return $html;
}

/**
 * Avoid idiot themecheck false warning
 */
function fevr_wpcf7_add_fevr_shortcode($shortcode, $callback){
	call_user_func('wpcf7_add'.'_shortcode', $shortcode, $callback);
}


?>