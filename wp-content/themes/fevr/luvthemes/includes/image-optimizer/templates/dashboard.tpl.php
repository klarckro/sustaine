<div class="wrap">
	<h2><?php esc_html_e('Image Optimizer', 'fevr')?></h2>
	<div class="image-optimizer-wrapper">
		<h4><?php esc_html_e('Optimize images', 'fevr')?></h4>
		<p>
			<a class="luv-btn luv-btn-blue" id="luv-optimize-images" href="#"><?php esc_html_e('Start', 'fevr')?></a>
		</p>
		<div id="luv-optimize-images-progressbar-container" class="luv-hidden">
			<div id="luv-optimize-images-progress"></div>
			<div id="luv-optimize-images-ratio"></div>
			<div class="media-progress-bar">
				<div id="luv-optimize-images-progressbar" style="width: 0%"></div>
			</div>
		</div>
	</div>
</div>