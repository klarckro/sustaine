<?php

class Fevr_Image_Optimizer{
	
	public $api_url;
	
	public $api_key;
	
	public $upload_dir;
	
	public $original_size = 0;
	
	public $compressed_size = 0;
	
	public $localize = array();
	
	/**
	 * Create Image Optimizer Object
	 */
	public function __construct(){
		$this->upload_dir = wp_upload_dir();
		
		// Init optimizer
		add_action('init', array($this, 'init'),11);
		
		// Optimize uploaded images
		add_action('wp_handle_upload', array($this, 'handle_upload'), 10, 2);
		
		// Single
		add_filter( 'attachment_fields_to_edit', array($this, 'optimize_single'), 10, 2 );
	}
	

	/**
	 * Set API URL, purchase code, create admin menu
	 */
	public function init(){
		if (is_admin()){		
			// Set API URL
			$this->api_url = FEVR_API_URL;
		
			// Set purchase key
			$this->api_key = fevr_purchase_key();
			
			// Localization
			$this->localize['i18n'] = array(
					'Preparing...' => esc_html__('Preparing...', 'fevr'),
					'Done' => esc_html__('Done', 'fevr'),
					'Restart' => esc_html__('Restart', 'fevr'),
					'Running' => esc_html__('Running', 'fevr'),
					'All of your images are already optimized' => esc_html__('All of your images are already optimized', 'fevr'),
			);
			
			if (!empty($this->api_key)){
				add_action('admin_menu', array($this, 'admin_menu'));
				add_action('admin_enqueue_scripts', array($this, 'enqueue_assets'));
				
				add_action('wp_ajax_fevr_image_optimizer', array($this, 'ajax_handler'));
			}
		}
	}
	
	
	/**
	 * Ajax handler
	 */
	public function ajax_handler(){
		$action = isset($_POST['fevr_action']) ? $_POST['fevr_action'] : '';
		$images = array('count' => 0);
		switch ($action){
			case 'compress':
				if (isset($_POST['id'])){
					$size = isset($_POST['size']) ? $_POST['size'] : '__original'; 
					$this->optimize($_POST['id'], $size);
					wp_send_json(array(
							'original' => $this->original_size,
							'compressed' => $this->compressed_size
					));
				}
				break;
			case 'get_file_list':
			default:
				if (isset($_POST['image-id']) && !empty($_POST['image-id'])){
					$attachments = array(get_post($_POST['image-id']));
				}
				else {
					$attachments = get_posts(array( 
						'post_type' => 'attachment',
						'posts_per_page' => -1,
						'post_status' => 'any',
						'post_mime_type' => array( 'image/jpeg', 'image/gif', 'image/png' ),
						'meta_query' => array(
								array(
										'key'		=> 'fevr_compressed',
										'compare'	=> 'NOT EXISTS',
										'value'		=> ''
								)
							)
						)
					);
				}
				if ( $attachments ) {
					foreach ((array)$attachments as $attachment) {
						$metadata = wp_get_attachment_metadata($attachment->ID);
						
						$count = 1;
						if (isset($metadata['sizes'])){
							foreach ((array)$metadata['sizes'] as $key=>$sizes){
								$images['items'][$attachment->ID . $key]['id']		= $attachment->ID;
								$images['items'][$attachment->ID . $key]['size']	= $key;
								$count++;
							}
						}

						$images['items'][$attachment->ID . '__original']['id']		= $attachment->ID;
						$images['items'][$attachment->ID . '__original']['size']	= '__original';
						
						$images['count'] += $count; 
					}
				}
				wp_send_json($images);
				break;
		}
	}
	
	/**
	 * Call API
	 * @param string $function
	 * @param array $args
	 */
	public function api($function = '', $args = array()){
		$response = wp_remote_post (
			$this->api_url . $function ,array(
					'timeout' => 10,
					'sslverify' => false,
					'user-agent' => 'luv',
					'headers' => array (
							'X-ENVATO-PURCHASE-KEY' => trim ($this->api_key)
					),
					'body' => array (
							'site' => trailingslashit(home_url()),
							'args' => $args
					)
			)
		);
		if (is_wp_error($response)){
			$this->error = esc_html__('Couldn\'t connect to API server: ', 'fevr') . $response->get_error_message();
		}
		else{
			$response = json_decode($response['body'], true);
			if ($response['error'] === true){
				$this->error = esc_html__('API error: ', 'fevr') . $response['response'];
			}
			else{
				return $response['response'];
			}
		}
	}
	
	/**
	 * Optimize original images and all sizes
	 * @param int $id
	 * @param string $size
	 */
	public function optimize($id, $size){
		
		if ($size == '__original'){
			// Get the path for the image
			$filepath = get_attached_file((int)$id);
			
			// Check is file exists
			if (file_exists($filepath)){
			
				// Get the original size
				$this->original_size = (filesize($filepath)/1024/1024);
				
				// Compress the original file
				$this->compress($filepath);
				
				// Get compressed size
				$this->compressed_size = (filesize($filepath)/1024/1024);
				
			}
			update_post_meta($id, 'fevr_compressed', 1);
		}
		else{
			// Compress all sizes
			$metadata = wp_get_attachment_metadata($id);
			if (isset($metadata['sizes'][$size])){				
				// Build file path for resized image
				$filepath = $this->upload_dir['basedir'] .'/'.dirname($metadata['file']) .'/'. $metadata['sizes'][$size]['file'];
				
				// Check is file exists
				if (file_exists($filepath)){
				
					// Get the original size
					$this->original_size = (filesize($filepath)/1024/1024);
					
					// Compress
					$this->compress($filepath);
					
					// Get compressed size
					$this->compressed_size = (filesize($filepath)/1024/1024);
				}
			}
		}
	}
	
	/**
	 * Compress the image using API
	 * @param string $file file path
	 */
	public function compress($file){
		global $wp_filesystem;
		WP_Filesystem();
		
		// Compress file
		$new_image = fevr_b64decode($this->api('compress_image', array(
				'data' => $wp_filesystem->get_contents($file)
		)));
		
		// Create temporary file for checkings
		$test_img = $this->upload_dir['basedir'] . '/test-image_' . mt_rand(0,PHP_INT_MAX);
		$wp_filesystem->put_contents($test_img, $new_image);

		// Check the resized image
		@$check = getimagesize($test_img);
		
		// If image seems ok overwrite the original image
		if ($check !== false && isset($check[0]) && $check[0] > 0){
			$wp_filesystem->put_contents($file, $new_image);
		}
		
		// Remove temporary file
		$wp_filesystem->delete($test_img);
	}
	
	public function handle_upload($upload, $action){
		if (!empty($this->api_key) && fevr_check_luvoption('optimize-uploaded-images', 1)){
			$this->compress($upload['file']);
		}
		return $upload;
	}
	
	/**
	 * Create menu
	 */
	public function admin_menu(){
		fevr_add_fevr_submenu_page( 'upload.php', 'Image Optimizer', 'Image Optimizer','manage_options', 'luvthemes-optimize-images', array($this, 'dashboard'));
	}
	
	public function optimize_single($fields, $post) {
	    $html = '<a href="#" class="button luv-optimize-single-image" data-image-id="'.$post->ID.'">'.esc_html__('Optimize image', 'fevr').'</a>'.
	 	    	'<div id="luv-optimize-images-progressbar-container" class="luv-hidden">'.
				'	<div id="luv-optimize-images-progress"></div>'.
				'	<div id="luv-optimize-images-ratio"></div>'.
				'	<div class="media-progress-bar">'.
				'		<div id="luv-optimize-images-progressbar" style="width: 0%"></div>'.
				'	</div>'.
				'</div>';
	    $fields["fevr-optimize-image"] = array(
	        "label"	=> esc_html__('Image Optimizer', 'fevr'),
	        "input"	=> "html",
	        "html"	=> $html
	    );
	    return $fields;
	}
	
	/**
	 * Display dashboard
	 */
	public function dashboard() {
		include_once trailingslashit(get_template_directory()) . 'luvthemes/includes/image-optimizer/templates/dashboard.tpl.php';
	}
	
	/**
	 * Enqueue assets
	 */
	public function enqueue_assets(){
		wp_enqueue_script('fevr-image-optimizer', trailingslashit(get_template_directory_uri()) . 'luvthemes/includes/image-optimizer/js/optimizer.js', array('jquery'), FEVR_THEME_VER, true );
		wp_localize_script('fevr-image-optimizer', 'fevr_image_optimizer', $this->localize);
		wp_enqueue_style('fevr-image-optimizer', trailingslashit(get_template_directory_uri()) . 'luvthemes/includes/image-optimizer/css/admin.css');
	}
	
}

?>