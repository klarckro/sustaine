(function(){	
	var original_size = 0;
	var compressed_size = 0;
	
	jQuery(function(){
		jQuery(document).on('click', '#luv-optimize-images, .luv-optimize-single-image', function(e){
			e.preventDefault();
			var image_id = (typeof jQuery(this).attr('data-image-id') != 'undefined' ? jQuery(this).attr('data-image-id') : '');
			jQuery('#luv-optimize-images').removeClass('luv-btn-blue').addClass('luv-btn-gray').prop('disabled','disabled').text(__('Running...'));
			jQuery('#luv-optimize-images-progressbar').css('width', '0%');
			jQuery('#luv-optimize-images-progressbar-container').removeClass('luv-hidden');
			jQuery.post(fevr_admin_ajax.ajax_url, {'action' : 'fevr_image_optimizer', 'image-id' : image_id}, function(response){
				var count	= response.count;
				var percent = 0;
				
				jQuery('#luv-optimize-images-progress').empty().text(__('Preparing...'));
				
				percent = fevr_compress_image(response, count, 0);
				
			});
		});
	});
	
	/**
	 * Compress the images
	 * @param array images
	 * @param int count
	 * @param int c
	 */
	function fevr_compress_image(images, count, c){
		if (count > 0){
			var id		= images['items'][Object.keys(images['items'])[0]]['id'];
			var size	= images['items'][Object.keys(images['items'])[0]]['size'];
			jQuery.post(fevr_admin_ajax.ajax_url, {'action' : 'fevr_image_optimizer', 'fevr_action' : 'compress', 'id' : id, 'size': size}, function(response){
				percent = parseInt(c/count*100);
				c++;
				delete images['items'][Object.keys(images['items'])[0]];
				
				try {
					original_size += response.original;
					compressed_size += response.compressed;
					
					// Refresh the progress
					jQuery('#luv-optimize-images-progressbar').css('width', format_number(percent, 0, '%'));
					jQuery('#luv-optimize-images-progress').empty().text(c + '/' + count + ' (' + format_number(percent, 0, '%') + ')');
					jQuery('#luv-optimize-images-ratio').empty().text(format_number(original_size, 2, ' Mb') + '/' + format_number(compressed_size, 2, ' Mb') + ' (' + format_number((1-(compressed_size/original_size))*100, 2, '%') + ')');
				}
				catch(e){
					// Silent fail
				}
				
				// Compress next image
				if (c < count){
					fevr_compress_image(images, count, c);
				}
				else{
					jQuery('#luv-optimize-images').addClass('luv-btn-blue').removeClass('luv-btn-gray').removeAttr('disabled').text(__('Restart'));
					jQuery('#luv-optimize-images-progressbar').css('width', '100%');
					jQuery('#luv-optimize-images-progress').empty().text(__('Done. ') + c + '/' + count + ' (100%)');
				}
				
			});
		}
		else{
			jQuery('#luv-optimize-images').addClass('luv-btn-blue').removeClass('luv-btn-gray').removeAttr('disabled').text(__('Restart'));

			jQuery('#luv-optimize-images-progressbar').css('width', '100%');
			jQuery('#luv-optimize-images-progress').empty().text(__('All of your images are already optimized'));
		}
	}
	
	/**
	 * Format number, add unit if necessary
	 * @param int|float number
	 * @param int decimals
	 * @param string unit
	 */
	function format_number(number, decimals, unit){
		decimals = decimals || 0;
		unit = unit || '';
		number = parseFloat(Math.round(number * 100) / 100).toFixed(decimals);
		return (isNaN(number) ? 0 : number) + unit;
	}
	
	/**
	 * Localization
	 * @param string text
	 * @return string
	 */
	function __(text){
		if (typeof fevr_image_optimizer.i18n[text] !== 'undefined'){
			return fevr_image_optimizer.i18n[text];
		}
		else {
			return text;
		}
	}
})();