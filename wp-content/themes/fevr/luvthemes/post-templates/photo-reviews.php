<?php
	global $fevr_reviews_title_typography_classes;
	
	$product_id = get_post_meta( get_the_ID(), '_product_id', true);
	
	$product_feedback = get_post_meta( get_the_ID(), '_fevr_feedback', true);
	
	$review_media = get_attached_media( 'image' );
?>

<div class="photo-review-media-container">
	<?php if(count($review_media) == 1): ?>
	
	<div class="post-featured-img">
		<div class="photo-review-overlay"></div>
		<?php 
			foreach($review_media as $item) {
				echo wp_get_attachment_image($item->ID, 'fevr_reviews_img');
				
				$larger_link = wp_get_attachment_image_src($item->ID, 'full');
			}
		?>
	</div>
	
	<?php elseif(count($review_media) > 1): ?>
	
	<div class="post-gallery-container">
		<ul class="post-gallery" data-gallery-arrows="true" data-gallery-loop="false">
			<?php
				foreach($review_media as $item):
				$larger_link = wp_get_attachment_image_src($item->ID, 'full');
			?>
			<li class="luv-gallery-item">
				<?php echo wp_get_attachment_image($item->ID, 'large'); ?>
				<div class="photo-review-overlay"></div>
				<div class="button-wrapper">
					<a href="<?php echo esc_url($larger_link[0]); ?>" class="btn btn-global btn-full btn-s open-portfolio" rel="photoreview-attachment"><?php esc_html_e('View Larger', 'fevr'); ?></a>
				</div>
			</li>
			
			<?php endforeach; ?>
		</ul>
	</div>
	
	<?php endif; ?>
	
	<?php if(fevr_check_luvoption('woocommerce-photo-reviews-rating', 1, '!=')):
			if($product_feedback == -1) {
				$feedback_classes = 'fa-thumbs-o-down';
			}
			else if($product_feedback == 1) {
				$feedback_classes = 'fa-thumbs-o-up';
			}
			else{
				$feedback_classes = '';
			}
	?>
	<i class="fa <?php echo esc_attr($feedback_classes); ?>"></i>
	<?php endif; ?>
	
	<?php if(count($review_media) == 1): ?>
	<div class="button-wrapper">
		<a href="<?php echo esc_url($larger_link[0]); ?>" class="btn btn-global btn-full btn-s open-portfolio" rel="photoreview-attachment"><?php esc_html_e('View Larger', 'fevr'); ?></a>
	</div>
	<?php endif; ?>
</div>

<div class="post-content">
	<h2 class="post-title<?php fevr_echo($fevr_reviews_title_typography_classes);?>"><?php the_title(); ?></h2>
	<?php the_content(); ?>
	
	<div class="post-meta">
		<?php
			if(fevr_check_luvoption('woocommerce-photo-reviews-date', '1', '!=')) {
				the_time(get_option('date_format'));
			}
		?>
		<?php
			if(fevr_check_luvoption('woocommerce-photo-reviews-likes', '1', '!=')) {
				fevr_get_like_icon();
			}
		?>
	</div>
	
	<?php
		$product = new WC_Product($product_id);	
	?>
	<div class="photo-review-related-product">
		<a href="<?php echo esc_url($product->get_permalink()); ?>">
			<?php echo fevr_kses($product->get_image()); ?><?php echo esc_html($product->get_title()); ?>
		</a>
	</div>
</div>