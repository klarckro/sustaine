<?php
	//Set is woocommerce as true
	add_filter('is_woocommerce', '__return_true');
	
	$fevr_meta_fields = get_post_meta( get_the_ID(), 'fevr_meta', true);
?>

<ul class="products">
	<?php
		$paged = get_query_var('paged');
		$args = array(
			'post_type' => 'product',
			'post__in' => $fevr_meta_fields['collection-items'],
			'paged' => !empty($paged) ? $paged : 1
		);
		$loop = new WP_Query( $args );
		
		// Override global wp_query for pagination
		global $wp_query;
		$_wp_query = $wp_query;
		$wp_query = $loop;
		
		if ( $loop->have_posts() ) {
			while ( $loop->have_posts() ) : $loop->the_post();
				wc_get_template_part( 'content', 'product' );
			endwhile;
		} else {
			echo esc_html__( 'No products found', 'fevr' );
		}
		wp_reset_postdata();
	?>
</ul><!--/.products-->
<?php
	get_template_part( 'luvthemes/luv-templates/pagination' );
	// Backup the global wp_query
	$wp_query = $_wp_query; 
?>