<?php
	// Comments
	$show_comments = fevr_check_luvoption('blog-comments-feature',1, '!=') && fevr_check_luvoption('blog-comments-meta', 'hide-on-archive', '!=') && fevr_check_luvoption('blog-comments-meta', 'hide-on-both', '!=');
	
	// Author
	$show_author = fevr_check_luvoption('blog-author-meta', 'hide-on-archive', '!=') && fevr_check_luvoption('blog-author-meta', 'hide-on-both', '!=');
	
	// Categories
	$show_categories = fevr_check_luvoption('blog-categories-meta', 'hide-on-archive', '!=') && fevr_check_luvoption('blog-categories-meta', 'hide-on-both', '!=') && fevr_check_luvoption('blog-layout-style', 'masonry', '!=') && fevr_check_luvoption('blog-layout-style','timeline', '!=');
	
	// Date
	$show_date = fevr_check_luvoption('blog-date-meta', 'hide-on-archive', '!=') && fevr_check_luvoption('blog-date-meta', 'hide-on-both', '!=') && (fevr_check_luvoption('blog-layout-style', 'standard') || fevr_check_luvoption('blog-layout-style', 'titles-only'));
	
	// Like
	$show_likes = fevr_check_luvoption('blog-likes-meta', 'hide-on-archive', '!=') && fevr_check_luvoption('blog-likes-meta', 'hide-on-both', '!=') && fevr_check_luvoption('blog-layout-style', 'standard');

?>

<?php if($show_comments || $show_author || $show_categories || $show_date || $show_likes): ?>
<div class="post-meta">
	<?php
		if($show_likes)  {
			fevr_get_like_icon();
		}

		if($show_author) {
			echo '<span>';
			esc_html_e('Posted by ', 'fevr'); the_author_posts_link();
			echo '</span>';
		}

		if($show_date) {
			echo '<span class="post-meta-date"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">';
			the_time(get_option('date_format'));
			echo '</a></span>';
		}

		if($show_categories) {
			echo '<span>';
			the_category(', ', '');
			echo '</span>';
		}
	?>
	
	<?php if($show_comments): ?>
		<span><a href="<?php the_permalink(); ?>" title="<?php esc_html_e('Comments of ', 'fevr'); ?> <?php the_title(); ?>"><?php comments_number( esc_html__('No Comments', 'fevr'), esc_html__('One Comment', 'fevr'), esc_html__('% Comments', 'fevr') ); ?></a></span>
	<?php endif; ?>
</div>
<?php endif; ?>
