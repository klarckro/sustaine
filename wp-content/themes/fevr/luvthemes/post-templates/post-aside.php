<?php
	global $fevr_expiration_message, $fevr_blog_title_typography_classes;
	$show_date = fevr_check_luvoption('blog-date-meta', 'hide-on-archive', '!=') && fevr_check_luvoption('blog-date-meta', 'hide-on-both', '!=');
?>

<?php if ( has_post_thumbnail() ): ?>
<div class="post-featured-img">
	<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>">
		<?php		
			if(fevr_check_luvoption('blog-layout-style', 'masonry') && fevr_check_luvoption('blog-masonry-crop-images', 1) && (!is_single() || fevr_is_luv_shortcode())) {
				the_post_thumbnail('fevr_post_thumb');
			} elseif(fevr_check_luvoption('blog-masonry-layout', 'standard') && fevr_check_luvoption('blog-layout-style','masonry') && (!is_single() || fevr_is_luv_shortcode())) {
				the_post_thumbnail('fevr_wide');
			} else {
				the_post_thumbnail('fevr_featured_img');
			}
		?>
	</a>
</div>
<?php endif; ?>

<div class="post-content">
	<?php
		if(is_home() || is_archive() || fevr_is_luv_shortcode()) {
			get_template_part( 'luvthemes/post-templates/post-archive', 'meta');
		}
	?>
	
	<?php if(is_home() || is_archive() || fevr_is_luv_shortcode()): ?>
	<h2 class="post-title<?php fevr_echo($fevr_blog_title_typography_classes);?>"><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>"><?php the_title(); ?></a></h2>
	<?php endif; ?>
	
	<?php
		if(is_archive() || is_home() || fevr_is_luv_shortcode()):
	?>
		<div class="post-exp-content">
		<?php the_excerpt(); ?>
		</div>
		
		<div class="post-exp-message">
			<a href="<?php the_permalink(); ?>"><?php echo fevr_kses($fevr_expiration_message); ?></a>
		</div>
	<?php else:
			the_content();
		endif;
	?>
	
	<?php
		// When the blog layout is masonry.
		if((!is_single() || fevr_is_luv_shortcode()) && fevr_check_luvoption('blog-layout-style', 'masonry') && fevr_check_luvoption('blog-masonry-layout', 'standard') && $show_date): ?>
		<div class="post-meta-bottom">
			<div class="post-meta">
				<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>"><?php the_time(get_option('date_format')); ?></a>
			</div>
			<?php
				if(fevr_check_luvoption('blog-likes-meta', 'hide-on-archive', '!=') && fevr_check_luvoption('blog-likes-meta', 'hide-on-both', '!=')) {
					fevr_get_like_icon();
				}
			?>
		</div>
	<?php endif; ?>
</div>