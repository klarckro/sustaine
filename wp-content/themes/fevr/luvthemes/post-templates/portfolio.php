<?php
	global $fevr_meta_fields, $fevr_portfolio_title_typography_classes;
	
	$project_link = isset($fevr_meta_fields['portfolio-custom-link']) && !empty($fevr_meta_fields['portfolio-custom-link']) ? $fevr_meta_fields['portfolio-custom-link'] : get_the_permalink($post->ID);
	
	// Classes for post-outer
	$fevr_post_content_outer_classes = array();
	$fevr_post_content_outer_styles = array();
	$fevr_post_content_outer_child_styles = array();
	
	// Background color when 'Show content' is active
	if(isset($fevr_meta_fields['portfolio-masonry-show-content']) && $fevr_meta_fields['portfolio-masonry-show-content'] == 'enabled' &&
			isset($fevr_meta_fields['portfolio-masonry-content']) && !empty($fevr_meta_fields['portfolio-masonry-content']) &&
			isset($fevr_meta_fields['portfolio-masonry-accent-color']) && !empty($fevr_meta_fields['portfolio-masonry-accent-color']) &&
			fevr_check_luvoption('portfolio-masonry-layout', 'meta-overlay'))
	{
		$fevr_post_content_outer_styles[] = 'background-color: '.$fevr_meta_fields['portfolio-masonry-accent-color'];
	}
	
	// Overlay background color
	if (fevr_check_luvoption('portfolio-masonry-hover-style', 'masonry-color-overlay') || fevr_check_luvoption('portfolio-masonry-hover-style', 'masonry-color-overlay-text') && isset($fevr_meta_fields['portfolio-masonry-accent-color']) && !empty($fevr_meta_fields['portfolio-masonry-accent-color'])){
		$fevr_post_content_outer_child_styles[':after'] = 'background-color: '.$fevr_meta_fields['portfolio-masonry-accent-color'] . ' !important';
	}
	
	// Overlay border color
	if( fevr_check_luvoption('portfolio-masonry-hover-style', 'masonry-box-border') &&
		fevr_check_luvoption('portfolio-masonry-auto-text-color', 1, '!=') &&
		isset($fevr_meta_fields['portfolio-masonry-text-color']) && !empty($fevr_meta_fields['portfolio-masonry-text-color']))
	{
		$fevr_post_content_outer_child_styles[':before'] = 'border-color: '.$fevr_meta_fields['portfolio-masonry-text-color'].' !important;';
	}
	
	$fevr_post_content_outer_classes[] = fevr_enqueue_inline_css(array('style' => implode(';',$fevr_post_content_outer_styles), 'child' => $fevr_post_content_outer_child_styles));
	
	// Classes for .post-inner
	$fevr_post_inner_classes = array();
	$fevr_post_inner_child_styles = array();
	$fevr_post_inner_styles = array();
	
	// Extra padding between items
	if (fevr_check_luvoption('portfolio-item-padding', '', '!=')){
		$padding = trim(fevr_get_luvoption('portfolio-item-padding'));
		$fevr_post_inner_styles[] = 'padding: ' . $padding . (!preg_match('~(px|%)$~', $padding) ? 'px' : '');
	}
	
	if(!empty($fevr_post_inner_styles) || !empty($fevr_post_inner_child_styles)) {
		$fevr_post_inner_classes[] = fevr_enqueue_inline_css(array('style' => implode(';', $fevr_post_inner_styles), 'child' => $fevr_post_inner_child_styles));
	}
		
	
	// Classes for .post-content
	$fevr_post_content_classes = array();
	$fevr_post_content_styles = array();
	$fevr_post_content_child_styles = array();
	
	// Horizontal text alignment
	if(isset($fevr_meta_fields['portfolio-masonry-h-text-alignment']) && !empty($fevr_meta_fields['portfolio-masonry-h-text-alignment'])) {
		$fevr_post_content_classes[] = $fevr_meta_fields['portfolio-masonry-h-text-alignment'];
	} else {
		$fevr_post_content_classes[] = 'is-left';
	}
	
	// Vertical text alignment
	if (fevr_check_luvoption('portfolio-masonry-layout', 'meta-overlay')){
		$fevr_post_content_classes[] = (isset($fevr_meta_fields['portfolio-masonry-v-text-alignment']) && !empty($fevr_meta_fields['portfolio-masonry-v-text-alignment']) ? $fevr_meta_fields['portfolio-masonry-v-text-alignment'] : 'vertical-bottom');
	}
	
	// Text color when the automatic text color turned off or the hover style is 'masonry-style-title-bottom' or the 'show content' is enabled
	if (isset($fevr_meta_fields['portfolio-masonry-text-color']) && !empty($fevr_meta_fields['portfolio-masonry-text-color']) &&
		(fevr_check_luvoption('portfolio-masonry-auto-text-color', 1, '!=') ||
		 fevr_check_luvoption('portfolio-masonry-hover-style', 'masonry-style-title-bottom') ||
		(isset($fevr_meta_fields['portfolio-masonry-show-content']) && $fevr_meta_fields['portfolio-masonry-show-content'] == 'enabled')) &&
		fevr_check_luvoption('portfolio-masonry-layout', 'meta-overlay'))
	{
		$fevr_post_content_styles[] = 'color: '.$fevr_meta_fields['portfolio-masonry-text-color'].' !important';
		$fevr_post_content_child_styles[' a'] = 'color: '.$fevr_meta_fields['portfolio-masonry-text-color'].' !important';
	}
	
	// Background color
	if ((!isset($fevr_meta_fields['portfolio-masonry-show-content']) || $fevr_meta_fields['portfolio-masonry-show-content'] != 'enabled') &&
		isset($fevr_meta_fields['portfolio-masonry-accent-color']) && !empty($fevr_meta_fields['portfolio-masonry-accent-color']) &&
		fevr_check_luvoption('portfolio-masonry-hover-style', 'masonry-style-title-bottom') &&
		fevr_check_luvoption('portfolio-masonry-layout', 'meta-overlay'))
	{
		$fevr_post_content_styles[] = 'background-color: '.$fevr_meta_fields['portfolio-masonry-accent-color'].' !important';
	}
	
	if(!empty($fevr_post_content_styles)) {
		$fevr_post_content_classes[] = fevr_enqueue_inline_css(array('style' => implode(';', $fevr_post_content_styles), 'child' => $fevr_post_content_child_styles));
	}
	
	// View button text on standard layout
	if(isset($fevr_meta_fields['portfolio-video']) && $fevr_meta_fields['portfolio-video'] == 'enabled') {
		$fevr_preview_text = esc_html__('View Video', 'fevr');	
	} else {
		$fevr_preview_text = esc_html__('View Larger', 'fevr');	
	}
	
	// Preview Link
	if(!empty($fevr_meta_fields['portfolio-video-mp4']) && (isset($fevr_meta_fields['portfolio-video-source']) && $fevr_meta_fields['portfolio-video-source'] == 'file')) {
		$fevr_preview_link = $fevr_meta_fields['portfolio-video-mp4'];
	} else if(!empty($fevr_meta_fields['portfolio-video-ogv']) && (isset($fevr_meta_fields['portfolio-video-source']) && $fevr_meta_fields['portfolio-video-source'] == 'file')) {
		$fevr_preview_link = $fevr_meta_fields['portfolio-video-ogv'];
	} else if(isset($fevr_meta_fields['portfolio-video-embedded']) && !empty($fevr_meta_fields['portfolio-video-embedded']) && isset($fevr_meta_fields['portfolio-video-source']) && $fevr_meta_fields['portfolio-video-source'] == 'embedded') {
			preg_match('~src=("|\')([^"\']*)~', $fevr_meta_fields['portfolio-video-embedded'], $src);
			if (isset($src[2]) && !empty($src[2])){
				$fevr_preview_link = $src[2];
			}
			else{
				$fevr_preview_link = $fevr_meta_fields['portfolio-video-embedded'];
			}
	} else {
		$_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
		$fevr_preview_link = $_image[0];
	}
?>

<div class="post-inner <?php echo esc_attr(implode(' ', $fevr_post_inner_classes)); ?>">
	<div class="post-content-outer <?php echo esc_attr(implode(' ', $fevr_post_content_outer_classes));?>">
		<?php
			// If the user uses content, instead of image we hide the featured image
			if((!isset($fevr_meta_fields['portfolio-masonry-show-content']) || $fevr_meta_fields['portfolio-masonry-show-content'] != 'enabled') || fevr_check_luvoption('portfolio-masonry-layout', 'meta-overlay', '!=')):
		?>
		<div class="post-featured-img">
			<a href="<?php echo esc_url($project_link); ?>" title="<?php echo esc_attr(get_the_title()); ?>">
				<?php
					if(has_post_thumbnail()) { 
						if(fevr_check_luvoption('portfolio-masonry-crop-images', 1) && fevr_check_luvoption('portfolio-masonry-layout', 'meta-overlay')) {
							$masonry_size = isset($fevr_meta_fields['portfolio-masonry-size']) && !empty($fevr_meta_fields['portfolio-masonry-size']) ? $fevr_meta_fields['portfolio-masonry-size'] : '';
							the_post_thumbnail($masonry_size);
						} else if(fevr_check_luvoption('portfolio-masonry-crop-images', 1)) {
							the_post_thumbnail('fevr_post_thumb');
						} else {
							the_post_thumbnail('fevr_featured_img');
						}
					}
				?>
			</a>
			<?php if(fevr_check_luvoption('portfolio-masonry-layout','standard') && fevr_check_luvoption('portfolio-item-overlay', 1)): ?>
				<?php if(!empty($fevr_preview_link)): ?>
				<div class="portfolio-overlay"></div>
				<div class="button-wrapper">
					<a href="<?php echo esc_url($fevr_preview_link); ?>" class="btn btn-global btn-full btn-s open-portfolio" rel="portfolio-attachment"><?php echo fevr_kses($fevr_preview_text); ?></a>
				</div>
				<?php endif; ?>
			<?php endif; ?>
		</div>
		<?php endif; ?>
		
		<?php
			if (fevr_check_luvoption('portfolio-masonry-layout', 'meta-overlay') &&
				fevr_check_luvoption('portfolio-masonry-hover-style', 'masonry-color-overlay')):
		?>
			<div class="post-overlay-icon">
			<?php if (isset($fevr_meta_fields['portfolio-masonry-overlay-icon']) && !empty($fevr_meta_fields['portfolio-masonry-overlay-icon'])):?>
				<?php echo do_shortcode('[luv_icon icon="' . $fevr_meta_fields['portfolio-masonry-overlay-icon'] . '"]')?>
			<?php else:?>
			+
			<?php endif;?>			
			</div>
		<?php endif; ?>
		<div class="post-content <?php echo esc_attr(implode(' ', $fevr_post_content_classes)); ?>">
			<?php
				// If the user uses content, instead of image we hide the featured image
				if(isset($fevr_meta_fields['portfolio-masonry-show-content']) && $fevr_meta_fields['portfolio-masonry-show-content'] == 'enabled' && isset($fevr_meta_fields['portfolio-masonry-content']) && !empty($fevr_meta_fields['portfolio-masonry-content']) && fevr_check_luvoption('portfolio-masonry-layout', 'meta-overlay')):
					echo do_shortcode($fevr_meta_fields['portfolio-masonry-content']);
				else:
			?>
	
				<h2 class="post-title<?php fevr_echo($fevr_portfolio_title_typography_classes); ?>">
					<a href="<?php echo esc_url($project_link); ?>" title="<?php echo esc_attr(get_the_title()); ?>"><?php echo esc_html(get_the_title()); ?></a>
					<?php
						if(fevr_check_luvoption('portfolio-likes-meta', 'hide-on-archive', '!=') && fevr_check_luvoption('portfolio-likes-meta', 'hide-on-both', '!=') && fevr_check_luvoption('portfolio-masonry-layout', 'standard')) {
							fevr_get_like_icon();
						}
					?>
				</h2>
	
				<?php if(fevr_check_luvoption('portfolio-masonry-content', 'title', '!=')): ?>
				<div class="post-meta">
					<?php
						if(fevr_check_luvoption('portfolio-masonry-content', 'title-date')) {
							the_time(get_option('date_format'));
						} else if(fevr_check_luvoption('portfolio-masonry-content', 'title-category')) {
							$items = array();
							$categories = get_the_terms( $post->ID, 'luv_portfolio_categories' );
							foreach($categories as $category) {
								$items[] = $category->name;
							}
							$category = implode(", ",$items);
							echo esc_html($category);
						} elseif(isset($fevr_meta_fields['portfolio-excerpt']) && !empty($fevr_meta_fields['portfolio-excerpt'])) {
							echo fevr_kses($fevr_meta_fields['portfolio-excerpt']);
						}
					?>
				</div>
				<?php endif; ?>
			<?php endif; ?>
		</div>
		<?php if(fevr_check_luvoption('portfolio-masonry-layout', 'meta-overlay')): ?>
			<?php if(!isset($fevr_meta_fields['portfolio-masonry-show-content']) || $fevr_meta_fields['portfolio-masonry-show-content'] != 'enabled'): ?>
				<a class="post-link" href="<?php echo esc_url($project_link); ?>" title="<?php echo esc_attr(get_the_title()); ?>"></a>
			<?php endif; ?>
		<?php endif; ?>
	</div>
</div>