<?php
	// Enqueue OwlCarousel
	wp_enqueue_script( 'fevr-owlcarousel' );
	
	$fevr_meta_fields = get_post_meta( get_the_ID(), 'fevr_meta', true);
	global $fevr_featured_class, $fevr_show_thumbnail, $fevr_blog_title_typography_classes;
	
	$show_date = fevr_check_luvoption('blog-date-meta', 'hide-on-archive', '!=') && fevr_check_luvoption('blog-date-meta', 'hide-on-both', '!=');
	
	// When the blog layout is masonry or timeline.
	$show_bottom_meta = false;
	if((fevr_check_luvoption('blog-layout-style', 'masonry') && fevr_check_luvoption('blog-masonry-layout', 'standard')) || fevr_check_luvoption('blog-layout-style', 'timeline')) {
		$show_bottom_meta = true;
	}
	
	$show_bottom_meta = ((!is_single() || fevr_is_luv_shortcode()) && $show_bottom_meta && $show_date);
	$post_content_spacing = ($show_bottom_meta ? 'post-content-spacing' : '');
	
	// Data for .post-gallery
	$fevr_post_gallery_data = array();
	// Gallery autoplay
	if(isset($fevr_meta_fields['post-gallery-autoplay']) && $fevr_meta_fields['post-gallery-autoplay'] == 'enabled') {
		$fevr_post_gallery_data[] = 'data-gallery-autoplay="true"';
	}
	
	// Gallery arrows
	if(isset($fevr_meta_fields['post-gallery-arrows']) && $fevr_meta_fields['post-gallery-arrows'] == 'enabled') {
		$fevr_post_gallery_data[] = 'data-gallery-arrows="true"';
	}
	
	// Gallery dots
	if(isset($fevr_meta_fields['post-gallery-dots']) && $fevr_meta_fields['post-gallery-dots'] == 'enabled') {
		$fevr_post_gallery_data[] = 'data-gallery-dots="true"';
	}
?>
	
<?php if((!is_single() || fevr_is_luv_shortcode()) || $fevr_show_thumbnail): ?>
	<?php if($fevr_featured_class && !empty($fevr_featured_class)): ?>
		<div class="post-featured-img <?php echo esc_attr($fevr_featured_class); ?>">
			<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>">
				<?php		
					if(fevr_check_luvoption('blog-layout-style', 'masonry') && fevr_check_luvoption('blog-masonry-crop-images', 1) && (!is_single() || fevr_is_luv_shortcode())) {
						the_post_thumbnail('fevr_post_thumb');
					} elseif(fevr_check_luvoption('blog-masonry-layout', 'standard') && fevr_check_luvoption('blog-layout-style','masonry') && (!is_single() || fevr_is_luv_shortcode())) {
						the_post_thumbnail('fevr_wide');
					} else {
						the_post_thumbnail('fevr_featured_img');
					}
				?>
			</a>
		</div>
	<?php else: ?>
		<div class="post-gallery-container">
			<?php if($fevr_show_thumbnail && isset($fevr_meta_fields['post-gallery'])): ?>
			<ul class="post-gallery" <?php echo esc_attr(implode(' ', $fevr_post_gallery_data)); ?>>
				<?php foreach((array)$fevr_meta_fields['post-gallery'] as $item):
						$item_id = fevr_get_attachment_id($item);
						$item_size = is_single() && !fevr_is_luv_shortcode() ? 'fevr_featured_img' : 'fevr_post_thumb';
				?>
					<li class="luv-gallery-item"><a href="<?php the_permalink(); ?>"><?php echo wp_get_attachment_image($item_id, $item_size ); ?></a></li>
				<?php endforeach; ?>
			</ul>
			<?php endif; ?>
		</div>
	<?php endif; ?>
<?php endif; ?>

<?php
	if(is_home() || is_archive() || fevr_is_luv_shortcode()) {
		// When masonry standard is active
		get_template_part( 'luvthemes/post-templates/post-archive', 'category');
	}
?>
<div class="post-content<?php fevr_echo($post_content_spacing); ?>">
	<?php
		if(is_home() || is_archive() || fevr_is_luv_shortcode()) {
			get_template_part( 'luvthemes/post-templates/post-archive', 'meta');
		}
	?>

	<?php if(is_home() || is_archive() || fevr_is_luv_shortcode()): ?>
	<h2 class="post-title<?php fevr_echo($fevr_blog_title_typography_classes);?>"><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>"><?php the_title(); ?></a></h2>
	<?php endif; ?>
	
	<?php
		if((is_archive() || is_home() || fevr_is_luv_shortcode()) && fevr_check_luvoption('blog-excerpt', 1)) {
			echo '<p>';
			echo fevr_excerpt();
			echo '</p>';
		} else {
			the_content();
		}
	?>
</div>

<?php
	if($show_bottom_meta): ?>
	<div class="post-meta-bottom">
		<div class="post-meta">
			<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>"><?php the_time(get_option('date_format')); ?></a>
		</div>
		<?php
			if(fevr_check_luvoption('blog-likes-meta', 'hide-on-archive', '!=') && fevr_check_luvoption('blog-likes-meta', 'hide-on-both', '!=')) {
				fevr_get_like_icon();
			}
		?>
	</div>
<?php endif; ?>
