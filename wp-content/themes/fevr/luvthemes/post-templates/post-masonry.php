<?php
	global $fevr_meta_fields, $fevr_blog_title_typography_classes;
	
	// Classes for post-outer
	$fevr_post_content_outer_classes = array();
	$fevr_post_content_outer_styles = array();
	$fevr_post_content_outer_child_styles = array();
	
	// Background color when 'Show content' is active
	if(isset($fevr_meta_fields['post-masonry-show-content']) && $fevr_meta_fields['post-masonry-show-content'] == 'enabled' &&
			isset($fevr_meta_fields['post-masonry-content']) && !empty($fevr_meta_fields['post-masonry-content']) &&
			isset($fevr_meta_fields['post-masonry-accent-color']) && !empty($fevr_meta_fields['post-masonry-accent-color']))
	{
		$fevr_post_content_outer_styles[] = 'background-color: '.$fevr_meta_fields['post-masonry-accent-color'];
	}
	
	// Overlay background color
	if (fevr_check_luvoption('blog-masonry-hover-style', 'masonry-color-overlay') || fevr_check_luvoption('blog-masonry-hover-style', 'masonry-color-overlay-text') && isset($fevr_meta_fields['post-masonry-accent-color']) && !empty($fevr_meta_fields['post-masonry-accent-color'])){
		$fevr_post_content_outer_child_styles[':after'] = 'background-color: '.$fevr_meta_fields['post-masonry-accent-color'] . ' !important';
	}
	
	// Overlay border color
	if(fevr_check_luvoption('blog-masonry-hover-style', 'masonry-box-border') &&
			fevr_check_luvoption('blog-masonry-auto-text-color', 1, '!=') &&
			isset($fevr_meta_fields['post-masonry-text-color']) && !empty($fevr_meta_fields['post-masonry-text-color']))
	{
		$fevr_post_content_outer_child_styles[':before'] = 'border-color: '.$fevr_meta_fields['post-masonry-text-color'].' !important;';
	}
	
	$fevr_post_content_outer_classes[] = fevr_enqueue_inline_css(array('style' => implode(';',$fevr_post_content_outer_styles), 'child' => $fevr_post_content_outer_child_styles));
	
	
	// Classes for .post-inner
	$fevr_post_inner_classes = array();
	$fevr_post_inner_child_styles = array();
	$fevr_post_inner_styles = array();
	
	// Extra padding between items
	if (fevr_check_luvoption('blog-item-padding', '', '!=')){
		$padding = trim(fevr_get_luvoption('blog-item-padding'));
		$fevr_post_inner_styles[] = 'padding: ' . $padding . (!preg_match('~(px|%)$~', $padding) ? 'px' : '');
	}
	
	if(!empty($fevr_post_inner_styles) || !empty($fevr_post_inner_child_styles)) {
		$fevr_post_inner_classes[] = fevr_enqueue_inline_css(array('style' => implode(';', $fevr_post_inner_styles), 'child' => $fevr_post_inner_child_styles));
	}
	
	// Classes for .post-content
	$fevr_post_content_classes = array();
	// Horizontal text alignment
	if(isset($fevr_meta_fields['post-masonry-h-text-alignment']) && !empty($fevr_meta_fields['post-masonry-h-text-alignment'])) {
		$fevr_post_content_classes[] = $fevr_meta_fields['post-masonry-h-text-alignment'];
	} else {
		$fevr_post_content_classes[] = 'is-left';
	}
	
	// Vertical text alignment
	if(isset($fevr_meta_fields['post-masonry-v-text-alignment']) && !empty($fevr_meta_fields['post-masonry-v-text-alignment'])) {
		$fevr_post_content_classes[] = $fevr_meta_fields['post-masonry-v-text-alignment'];
	} else {
		$fevr_post_content_classes[] = 'vertical-bottom';
	}
	
	// Text color when the automatic text color turned off or the hover style is 'masonry-style-title-bottom' or the 'show content' is enabled
	if (isset($fevr_meta_fields['post-masonry-text-color']) && !empty($fevr_meta_fields['post-masonry-text-color']) &&
		(fevr_check_luvoption('blog-masonry-auto-text-color', 1, '!=') ||
		(fevr_check_luvoption('blog-masonry-hover-style', 'masonry-style-title-bottom')) ||
		(isset($fevr_meta_fields['post-masonry-show-content']) && $fevr_meta_fields['post-masonry-show-content'] == 'enabled')) &&
		fevr_check_luvoption('blog-masonry-layout', 'meta-overlay'))
	{
		$fevr_post_content_classes[] = fevr_enqueue_inline_css(array('style' => 'color: '.$fevr_meta_fields['post-masonry-text-color'].' !important', 'child' =>array(' *' => 'color: '.$fevr_meta_fields['post-masonry-text-color'].' !important')));
	}
	
	// Background color
	if ((!isset($fevr_meta_fields['post-masonry-show-content']) || $fevr_meta_fields['post-masonry-show-content'] != 'enabled') &&
		isset($fevr_meta_fields['post-masonry-accent-color']) && !empty($fevr_meta_fields['post-masonry-accent-color']) &&
		fevr_check_luvoption('blog-masonry-hover-style', 'masonry-style-title-bottom') &&
		fevr_check_luvoption('blog-masonry-layout', 'meta-overlay'))
	{
		$fevr_post_content_classes[] = fevr_enqueue_inline_css(array('style' => 'background-color: '.$fevr_meta_fields['post-masonry-accent-color']));
	}
	
?>

<div class="post-inner <?php echo esc_attr(implode(' ', $fevr_post_inner_classes)); ?>">
	<div class="post-content-outer <?php echo esc_attr(implode(' ', $fevr_post_content_outer_classes))?>">
	<?php
		// If the user uses content, instead of image we hide the featured image
		if(!isset($fevr_meta_fields['post-masonry-show-content']) || $fevr_meta_fields['post-masonry-show-content'] != 'enabled'):
	?>
	<div class="post-featured-img">
	<?php
		if(has_post_thumbnail()) {
			if(fevr_check_luvoption('blog-masonry-crop-images', 1)) {
				$masonry_size = isset($fevr_meta_fields['post-masonry-size']) && !empty($fevr_meta_fields['post-masonry-size']) ? $fevr_meta_fields['post-masonry-size'] : 'fevr_normal';
				the_post_thumbnail($masonry_size);
			} else {
				the_post_thumbnail('fevr_featured_img');
			}
		}
	?>
	</div>
	<?php endif; ?>
	<?php
		if (fevr_check_luvoption('blog-masonry-layout', 'meta-overlay') &&
			fevr_check_luvoption('blog-layout-style', 'masonry') &&
			fevr_check_luvoption('blog-masonry-hover-style', 'masonry-color-overlay')):
	?>
			<div class="post-overlay-icon">
			<?php if (isset($fevr_meta_fields['post-masonry-overlay-icon']) && !empty($fevr_meta_fields['post-masonry-overlay-icon'])):?>
				<?php echo do_shortcode('[luv_icon icon="' . $fevr_meta_fields['post-masonry-overlay-icon'] . '"]')?>
			<?php else:?>
			+
			<?php endif;?>			
			</div>
	<?php endif; ?>
	<div class="post-content <?php echo esc_attr(implode(' ', $fevr_post_content_classes)); ?>">
		<?php
			// If the user uses content, instead of image we hide the featured image
			if(isset($fevr_meta_fields['post-masonry-show-content']) && $fevr_meta_fields['post-masonry-show-content'] == 'enabled' && isset($fevr_meta_fields['post-masonry-content']) && !empty($fevr_meta_fields['post-masonry-content'])):
				echo do_shortcode($fevr_meta_fields['post-masonry-content']);
			else:
		?>
		
			<h2 class="post-title<?php fevr_echo($fevr_blog_title_typography_classes);?>"><?php the_title(); ?></h2>
		
			<?php if(fevr_check_luvoption('blog-masonry-content', 'title', '!=')): ?>
			<div class="post-meta">
				<?php
					if(fevr_check_luvoption('blog-masonry-content', 'title-date')) {
						the_time(get_option('date_format'));
					} else if(fevr_check_luvoption('blog-masonry-content', 'title-category')) {
						$items = array();
						$categories = get_the_category();
						foreach($categories as $category) {
							$items[] = $category->name;
						}
						$category = implode(", ",$items);
						echo esc_html($category);
					} elseif(fevr_check_luvoption('blog-excerpt', 1)) {
						echo fevr_excerpt();
					}
				?>
			</div>
			<?php endif; ?>
		
		<?php endif; ?>
	</div>
	
	<?php if(!isset($fevr_meta_fields['post-masonry-show-content']) || $fevr_meta_fields['post-masonry-show-content'] != 'enabled'): ?>
	<a class="post-link" href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>"></a>
	<?php endif; ?>
	</div>
</div>