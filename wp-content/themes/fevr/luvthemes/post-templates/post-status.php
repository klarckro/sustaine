<div class="post-content">
	<div class="post-content-inner">
		<?php the_content(); ?>
	</div>
	<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>" class="post-link"></a>
</div>

