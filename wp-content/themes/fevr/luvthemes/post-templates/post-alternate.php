<?php
	global $fevr_blog_title_typography_classes;
?>

<?php if(get_post_format() == 'status' || get_post_format() == 'link' || get_post_format() == 'quote'): ?>
	<?php get_template_part( 'luvthemes/post-templates/post', get_post_format() ); ?>
<?php else: ?>
<div class="l-grid-row">
	<div class="l-grid-6">
	<?php if ( has_post_thumbnail() ): ?>
	<div class="post-featured-img"><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>"><?php the_post_thumbnail('fevr_wide'); ?></a></div>
	<?php endif; ?>
	</div>
	
	<div class="l-grid-6">
		<div class="post-content">
			<?php get_template_part( 'luvthemes/post-templates/post-archive', 'meta'); ?>
			
			<h2 class="post-title<?php fevr_echo($fevr_blog_title_typography_classes);?>"><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>"><?php the_title(); ?></a></h2>
			
			<?php
				if((is_archive() || is_home() || fevr_is_luv_shortcode()) && fevr_check_luvoption('blog-excerpt', 1)) {
					echo '<p>';
					echo fevr_excerpt(fevr_get_luvoption('blog-excerpt-length', '35'));
					echo '</p>';
				} else {
					the_content();
				}
			?>
			
			
			<div class="post-meta alternate-bottom-meta">
				<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>"><?php the_time(get_option('date_format')); ?></a>
				<?php
					if(fevr_check_luvoption('blog-likes-meta', 'hide-on-archive', '!=') && fevr_check_luvoption('blog-likes-meta', 'hide-on-both', '!=')) {
						fevr_get_like_icon();
					}
				?>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>