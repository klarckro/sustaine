<?php
	// Categories
	$show_categories = fevr_check_luvoption('blog-categories-meta', 'hide-on-archive', '!=') && fevr_check_luvoption('blog-categories-meta', 'hide-on-both', '!=') && (fevr_check_luvoption('blog-layout-style', 'masonry') || fevr_check_luvoption('blog-layout-style', 'timeline'));
	
?>

<?php if($show_categories): ?>
<div class="post-meta-cat post-meta">
	<span>
		<?php the_category(' ', ''); ?>
	</span>
</div>
<?php endif; ?>