<?php if ( has_post_thumbnail() ): ?>
<div class="post-featured-img">
	<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>">
		<?php the_post_thumbnail('fevr_post_thumb');?>
	</a>
</div>
<?php endif; ?>

<?php if (fevr_check_luvoption('search-hide-pt',1,'!=')):?>
<div class="post-meta-cat post-meta">
	<span>
		<span><?php echo get_post_type(); ?></span>
	</span>
</div>
<?php endif; ?>

<div class="post-content">	
	<h2 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>"><?php the_title(); ?></a></h2>
	<?php echo fevr_excerpt(); ?>
</div>
	
<div class="post-meta-bottom">
	<div class="post-meta">
		<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>"><?php the_time(get_option('date_format')); ?></a>
	</div>
</div>
