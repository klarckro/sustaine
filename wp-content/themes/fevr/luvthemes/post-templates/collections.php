<?php
	//Set is woocommerce as true
	add_filter('is_woocommerce', '__return_true');
 
	global $fevr_meta_fields, $fevr_collections_title_typography_classes;

	
	// Classes for post-outer
	$fevr_post_content_outer_classes = array();
	$fevr_post_content_outer_styles = array();
	$fevr_post_content_outer_child_styles = array();
	
	// Background color when 'Show content' is active
	if(isset($fevr_meta_fields['collections-masonry-show-content']) && $fevr_meta_fields['collections-masonry-show-content'] == 'enabled' &&
			isset($fevr_meta_fields['collections-masonry-content']) && !empty($fevr_meta_fields['collections-masonry-content']) &&
			isset($fevr_meta_fields['collections-masonry-accent-color']) && !empty($fevr_meta_fields['collections-masonry-accent-color']) &&
			fevr_check_luvoption('woocommerce-collections-masonry-layout', 'meta-overlay'))
	{
		$fevr_post_content_outer_styles[] = 'background-color: '.$fevr_meta_fields['collections-masonry-accent-color'];
	}
	
	// Overlay background color
	if (fevr_check_luvoption('woocommerce-collections-masonry-hover-style', 'masonry-color-overlay') || fevr_check_luvoption('woocommerce-collections-masonry-hover-style', 'masonry-color-overlay-text') && isset($fevr_meta_fields['post-masonry-accent-color']) && !empty($fevr_meta_fields['post-masonry-accent-color'])){
		$fevr_post_content_outer_child_styles[':after'] = 'background-color: '.$fevr_meta_fields['collections-masonry-accent-color'] . ' !important';;
	}
	
	// Overlay border color
	if(fevr_check_luvoption('woocommerce-collections-masonry-hover-style','masonry-box-border') &&
			(fevr_check_luvoption('woocommerce-collections-masonry-auto-text-color', 1, '!=')) &&
			isset($fevr_meta_fields['collections-masonry-text-color']) && !empty($fevr_meta_fields['collections-masonry-text-color']))
	{
		$fevr_post_content_outer_child_styles[':before'] = 'border-color: '.$fevr_meta_fields['collections-masonry-text-color'].' !important;';
	}
	
	$fevr_post_content_outer_classes[] = fevr_enqueue_inline_css(array('style' => implode(';',$fevr_post_content_outer_styles), 'child' => $fevr_post_content_outer_child_styles));
	
	// Classes for .post_content
	$fevr_post_content_classes = array();
	// Vertical text alignment
	if(isset($fevr_meta_fields['collections-masonry-v-text-alignment']) && !empty($fevr_meta_fields['collections-masonry-v-text-alignment'])) {
		$fevr_post_content_classes[] = $fevr_meta_fields['collections-masonry-v-text-alignment'];
	} else {
		$fevr_post_content_classes[] = 'is-left';
	}
	
	// Horizontal text alignment
	if(isset($fevr_meta_fields['collections-masonry-h-text-alignment']) && !empty($fevr_meta_fields['collections-masonry-h-text-alignment'])) {
		$fevr_post_content_classes[] = $fevr_meta_fields['collections-masonry-h-text-alignment'];
	} else {
		$fevr_post_content_classes[] = 'vertical-bottom';
	}
	
	// Background color for text (on hover)
	if(isset($fevr_meta_fields['collections-masonry-accent-color']) && !empty($fevr_meta_fields['collections-masonry-accent-color'])) {
		$fevr_post_content_classes[] = fevr_enqueue_inline_css(array('style' => 'background-color: '.$fevr_meta_fields['collections-masonry-accent-color'].' !important'));
	}

	// Text color
	if(isset($fevr_meta_fields['collections-masonry-text-color']) && !empty($fevr_meta_fields['collections-masonry-text-color'])) {
		$fevr_post_content_classes[] = fevr_enqueue_inline_css(array('style' => 'color: '.$fevr_meta_fields['collections-masonry-text-color'].' !important', 'child' => array(' .post-title' => 'color: '.$fevr_meta_fields['collections-masonry-text-color'].' !important')));
	}
?>
<div class="post-inner">
	<div class="post-content-outer <?php echo esc_attr(implode(' ', $fevr_post_content_outer_classes));?>">
	<?php if ( has_post_thumbnail() ): ?>
	<div class="post-featured-img">
		<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>">
			<?php the_post_thumbnail('fevr_featured_img'); ?>
		</a>
	</div>
	<?php endif; ?>
	
	<?php
		if (fevr_check_luvoption('woocommerce-collections-masonry-hover-style', 'masonry-color-overlay')):
	?>
			<div class="post-overlay-icon">
			<?php if (isset($fevr_meta_fields['collections-masonry-overlay-icon']) && !empty($fevr_meta_fields['collections-masonry-overlay-icon'])):?>
				<?php echo do_shortcode('[luv_icon icon="' . $fevr_meta_fields['collections-masonry-overlay-icon'] . '"]')?>
			<?php else:?>
			+
			<?php endif;?>			
			</div>

	<?php endif; ?>
	
	<div class="post-content <?php echo fevr_check_luvoption('woocommerce-collections-masonry-hover-style', 'masonry-box-shadow', '!=') ? esc_attr(implode(' ', $fevr_post_content_classes)) : ''; ?>">
		<h2 class="post-title<?php fevr_echo($fevr_collections_title_typography_classes);?> <?php echo fevr_check_luvoption('woocommerce-collections-masonry-hover-style', 'masonry-box-shadow') ? esc_attr(implode(' ', $fevr_post_content_classes)) : ''; ?>"><?php the_title(); ?></h2>
		
		
		<?php if(fevr_check_luvoption('woocommerce-collections-masonry-content', 'title-excerpt') && isset($fevr_meta_fields['collections-excerpt']) && !empty($fevr_meta_fields['collections-excerpt'])): ?>
		<div class="post-meta <?php echo fevr_check_luvoption('woocommerce-collections-masonry-hover-style', 'masonry-box-shadow') ? esc_attr(implode(' ', $fevr_post_content_classes)) : ''; ?>">
			<?php echo fevr_kses($fevr_meta_fields['collections-excerpt']); ?>
		</div>
		<?php endif; ?>
	</div>
	<a class="post-link" href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>"></a>
	</div>
</div>

<ul class="collection-related-products">
	<?php 
		$products = (array)$fevr_meta_fields['collection-items'];
		shuffle($products);
		
		for($i=0; $i<3; $i++):
		if (isset($products[$i])):
			$product = new WC_Product($products[$i]);
		?>
			<li>
				<a href="<?php echo esc_url($product->get_permalink()); ?>">
					<?php echo fevr_kses($product->get_image()); ?>
				</a>
			</li>
		<?php endif?>
	<?php endfor;?>
</ul>