<?php
	global $fevr_show_thumbnail, $fevr_featured_class;
	$fevr_meta_fields = get_post_meta( get_the_ID(), 'fevr_meta', true);
?>

<?php if(!is_single() || fevr_is_luv_shortcode() || $fevr_show_thumbnail): ?>
	<?php if ( has_post_thumbnail() ): ?>
	<div class="post-featured-img <?php echo (isset($fevr_featured_class) && !empty($fevr_featured_class) ? $fevr_featured_class : ''); ?>">
		<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>">
			<?php		
				if(fevr_check_luvoption('blog-layout-style', 'masonry') && fevr_check_luvoption('blog-masonry-crop-images', 1) && (!is_single() || fevr_is_luv_shortcode())) {
					the_post_thumbnail('fevr_post_thumb');
				} elseif(fevr_check_luvoption('blog-masonry-layout', 'standard') && fevr_check_luvoption('blog-layout-style','masonry') && (!is_single() || fevr_is_luv_shortcode())) {
					the_post_thumbnail('fevr_wide');
				} else {
					the_post_thumbnail('fevr_featured_img');
				}
			?>
		</a>
	</div>
	<?php endif; ?>
<?php endif; ?>

<?php if(is_single() || fevr_check_luvoption('blog-layout-style', 'masonry', '!=') || fevr_check_luvoption('blog-masonry-layout', 'standard','!=')): ?>
	<div class="post-content">
	<?php
		if((is_archive() || is_home() || fevr_is_luv_shortcode()) && fevr_check_luvoption('blog-excerpt', 1)) {
			echo '<p>';
			echo fevr_excerpt();
			echo '</p>';
		} else {
			the_content();
		}
	?>
	</div>
<?php endif; ?>