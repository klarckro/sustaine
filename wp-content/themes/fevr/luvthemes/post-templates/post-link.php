<?php
	global $fevr_blog_title_typography_classes;
	$fevr_meta_fields = get_post_meta( get_the_ID(), 'fevr_meta', true);
?>

<div class="post-content">
	<div class="post-content-inner">
		<h2 class="post-title<?php fevr_echo($fevr_blog_title_typography_classes);?>"><?php the_title(); ?></h2>
		<?php if(isset($fevr_meta_fields['post-link']) && !empty($fevr_meta_fields['post-link'])): ?>
			<small><?php echo esc_html($fevr_meta_fields['post-link']); ?></small>
		<?php endif; ?>
		<?php if(isset($fevr_meta_fields['post-link']) && !empty($fevr_meta_fields['post-link'])): ?>
			<a class="post-link" href="<?php echo esc_url($fevr_meta_fields['post-link']); ?>" title="<?php echo esc_attr(get_the_title()); ?>"></a>
		<?php endif; ?>
	</div>
	<span class="post-icon"><i class="fa fa-link"></i></span>
</div>