<?php
	$fevr_meta_fields = get_post_meta( get_the_ID(), 'fevr_meta', true);
	
	// Classes for .l-grid-row
	$fevr_grid_classes = array();
	// Sidebar
	if(isset($fevr_meta_fields['portfolio-sidebar']) && $fevr_meta_fields['portfolio-sidebar'] == 'enabled' && isset($fevr_meta_fields['portfolio-sidebar-position']) && $fevr_meta_fields['portfolio-sidebar-position'] != 'portfolio-sidebar-bottom') {
		$fevr_grid_classes[] = 'has-sidebar';
	}
	
	// Sidebar position
	if(isset($fevr_meta_fields['portfolio-sidebar-position']) && $fevr_meta_fields['portfolio-sidebar-position'] == 'portfolio-sidebar-left') {
		$fevr_grid_classes[] = 'left-sidebar';
	}
	
	// Attributes for <video>
	$fevr_video_attributes = array();
	
	// Autoplay
	if(isset($fevr_meta_fields['portfolio-video-autoplay']) && $fevr_meta_fields['portfolio-video-autoplay'] == 'enabled') {
		$fevr_video_attributes[] = 'autoplay';
	}
	
	// Loop
	if(isset($fevr_meta_fields['portfolio-video-loop']) && $fevr_meta_fields['portfolio-video-loop'] == 'enabled') {
		$fevr_video_attributes[] = 'loop';
	}
	
	// Mute
	if(isset($fevr_meta_fields['portfolio-video-mute']) && $fevr_meta_fields['portfolio-video-mute'] == 'enabled') {
		$fevr_video_attributes[] = 'muted';
	}
	
	// Controls
	if(isset($fevr_meta_fields['portfolio-video-controls']) && $fevr_meta_fields['portfolio-video-controls'] == 'enabled') {
		$fevr_video_attributes[] = 'controls';
	}
?>
<div class="l-grid-row <?php echo esc_attr(implode(' ', $fevr_grid_classes)); ?>">
	<div id="portfolio-inner">
		<?php if(fevr_check_luvoption('portfolio-featured-image', 1, '!=') && (isset($fevr_meta_fields['portfolio-hide-featured-image']) && $fevr_meta_fields['portfolio-hide-featured-image'] != 'enabled')): ?>
		
			<?php if(isset($fevr_meta_fields['portfolio-video']) && $fevr_meta_fields['portfolio-video'] == 'enabled'): ?>
			<div class="post-video">
				<?php
						// Video from File
						if((!empty($fevr_meta_fields['portfolio-video-mp4']) || !empty($fevr_meta_fields['portfolio-video-ogv'])) && (isset($fevr_meta_fields['portfolio-video-source']) && $fevr_meta_fields['portfolio-video-source'] == 'file')):
				?>
					
							<video preload="auto" <?php echo esc_attr(implode(' ', $fevr_video_attributes)); ?>>
								<?php if(!empty($fevr_meta_fields['portfolio-video-mp4'])): ?>
								<source src="<?php echo esc_url($fevr_meta_fields['portfolio-video-mp4']); ?>" type="video/mp4">
								<?php endif; ?>
								
								<?php if(!empty($fevr_meta_fields['portfolio-video-ogv'])): ?>
								<source src="<?php echo esc_url($fevr_meta_fields['portfolio-video-ogv']); ?>" type="video/ogg">
								<?php endif; ?>
								
								<?php esc_html_e('Your browser does not support the video tag.', 'fevr'); ?>
							</video>
							
					<?php
						// Embedded Video
						elseif(isset($fevr_meta_fields['portfolio-video-embedded']) && !empty($fevr_meta_fields['portfolio-video-embedded']) && isset($fevr_meta_fields['portfolio-video-source']) && $fevr_meta_fields['portfolio-video-source'] == 'embedded'):
						
							// If the field contains URL
							if(!filter_var($fevr_meta_fields['portfolio-video-embedded'], FILTER_VALIDATE_URL) === false):
					?>
					
						<iframe src="<?php echo esc_url(fevr_url_to_embedded($fevr_meta_fields['portfolio-video-embedded'])); ?>" frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen></iframe>
					
					<?php
							else:
								echo fevr_kses($fevr_meta_fields['portfolio-video-embedded']);
							endif;
					?>
				
					<?php endif; ?>
			</div>
			
			<?php else: ?>
			
				<?php if ( has_post_thumbnail() ): ?>
				<div class="post-featured-img">
					<?php the_post_thumbnail('full'); ?>
				</div>
				<?php endif; ?>
				
			<?php endif; ?>
		<?php endif; ?>
		
		<div class="post-content">
			<?php the_content(); ?>
		</div>
	</div>
	
	<?php if(isset($fevr_meta_fields['portfolio-sidebar']) && $fevr_meta_fields['portfolio-sidebar'] == 'enabled'): ?>
	<div id="portfolio-sidebar">
		
		<?php
			$fevr_like = (fevr_check_luvoption('portfolio-likes-meta', 'hide-on-single', '!=') && fevr_check_luvoption('portfolio-likes-meta', 'hide-on-both', '!='));
			$fevr_share = fevr_check_luvoption('portfolio-social-meta', 1, '!=');
		?>
		
		<?php
			if(($fevr_like || $fevr_share) && fevr_check_luvoption('portfolio-social-position', 1)) {
				get_template_part('luvthemes/luv-templates/social');
			}
		?>
								
		<?php if(isset($fevr_meta_fields['portfolio-color-palette']) && $fevr_meta_fields['portfolio-color-palette'] == 'enabled'): ?>
			<div id="portfolio-color-palette"></div>
		<?php endif; ?>
		
		<?php
			if(isset($fevr_meta_fields['portfolio-details']) && !empty($fevr_meta_fields['portfolio-details'])) {
				echo apply_filters('the_content', $fevr_meta_fields['portfolio-details']);
			}
		?>
		
		<?php
			$project_tags = get_the_terms($post->ID, 'luv_portfolio_tags');
			if(!empty($project_tags)):
		?>
			<ul id="project-tags">
				<?php foreach($project_tags as $tag): ?>
					<li><?php echo fevr_kses($tag->name); ?></li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
	</div>
	<?php endif; ?>
</div>
