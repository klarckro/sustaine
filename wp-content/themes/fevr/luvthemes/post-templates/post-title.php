<?php 
	global $fevr_blog_title_typography_classes;
	$fevr_meta_fields = get_post_meta( get_the_ID(), 'fevr_meta', true);
?>
<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>" class="post-title-only" style="display:block;width:100%">
	<?php if (fevr_check_luvoption('blog-display-featured-image', 1)):?>
	<div class="post-featured-img">
	<?php the_post_thumbnail('fevr_featured_img');?>
	</div>
	<?php endif;?>
	<h3 class="<?php fevr_echo($fevr_blog_title_typography_classes, '');?>"><?php the_title(); ?></h3>
	<?php get_template_part( 'luvthemes/post-templates/post-archive', 'meta');?>
</a>