<?php
	$fevr_meta_fields = get_post_meta( get_the_ID(), 'fevr_meta', true);
?>
	
<div class="post-content">
	<span class="post-icon"><i class="fa fa-quote-right"></i></span>
	<div class="post-content-inner">
		<?php the_content(); ?> <?php echo isset($fevr_meta_fields['post-quote-author']) && !empty($fevr_meta_fields['post-quote-author']) ? '<small>'.fevr_kses($fevr_meta_fields['post-quote-author']).'</small>' : ''; ?>
	</div>
	<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>" class="post-link"></a>
</div>