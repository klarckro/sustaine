<?php
	
//======================================================================
// Define constants
//======================================================================

if (!defined('FEVR_THEME_VER')){
	define('FEVR_THEME_VER', '1.1.1');
}

if (!defined('FEVR_API_URL')){
	define('FEVR_API_URL','http://api.luvt.co/');
}

if (!defined('FEVR_DEMO_URL')){
	define('FEVR_DEMO_URL','http://fevr.luvthemes.com/');
}

?>
