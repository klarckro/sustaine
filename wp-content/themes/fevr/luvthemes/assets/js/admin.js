/* global media_uploader, wp, fevr_admin_ajax */
/* jshint -W020 */
/* jshint unused:false */
/* jshint latedef:false */
/* jshint devel:true */
if (jQuery('.luv-dashboard-wrap, .fevr_page_fevr_feature_request').length > 0){
	jQuery( 'div.updated, div.error, div.notice' ).addClass('below-h2');
}

/*----------------------------------------------------------------------------------*/
/* luvthemes object for localization
/*----------------------------------------------------------------------------------*/

var LuvthemesAdmin = function(){
	// Dashicon objects
	this.dashicons = {
						'yes' : jQuery('<i>',{
							'class' : 'dashicons dashicons-yes'
						})
					};
	// Localization
	this.translate = function(text){
		if (typeof fevr_admin_ajax.messages[text] !== 'undefined'){
			return fevr_admin_ajax.messages[text];
		}
		else {
			return text;
		}
	};
	
	this.decodeHtmlEntity = function(str) {
	  return str.replace(/&#(\d+);/g, function(match, dec) {
	    return String.fromCharCode(dec);
	  });
	};
};

var luvthemes_admin = new LuvthemesAdmin();

/**
 * Autoresize element
 * @param id
 */
function autoResize(id){
    var newheight;
    var newwidth;

    if(document.getElementById){
        newheight = document.getElementById(id).contentWindow.document.body.scrollHeight;
        newwidth = document.getElementById(id).contentWindow.document.body.scrollWidth;
    }

    document.getElementById(id).height = (newheight)+50 + "px";
    document.getElementById(id).width = (newwidth) + "px";
}

/*----------------------------------------------------------------------------------*/
/* Customize wpColorPicker
/*----------------------------------------------------------------------------------*/

// Backup original function
jQuery.fn._wpColorPicker = jQuery.fn.wpColorPicker;

/**
 * Override default palettes for wpColorPicker
 */
jQuery.fn.wpColorPicker = function(options){
	options = options || {};
	options.palettes = fevr_admin_ajax.colors;
	jQuery(this)._wpColorPicker(options);
}


/*----------------------------------------------------------------------------------*/
/* Ajax function to get items from database
/*----------------------------------------------------------------------------------*/

function get_collection_items() {
	var exclude = '';
	var prod_cat = '';
	jQuery('[name="fevr_meta[collection-items][]"]').each(function(){	
		exclude += jQuery(this).val()+',';
	});
	jQuery('[name="luvthemes_product_cat[]"]:checked').each(function(){	
		prod_cat += jQuery(this).val()+',';
	});
	jQuery('.luv-products').addClass('is-loading');
	jQuery('.luv-products').prepend('<div class="luv-loading"></div>');
	jQuery('.luv-products .luv-product-outer').load(fevr_admin_ajax.ajax_url, {'action' : 'get_collection_items', 'exclude' : exclude, 's' : jQuery('.luv-product-search').val(), 'paged' : jQuery('.luv-products .luv-product-outer').attr('data-paged'), 'prod_cat' : prod_cat, 'wp_nonce' : fevr_admin_ajax.wp_nonce}, function(){
		initalize_draggable_items();
		jQuery('.luv-products').removeClass('is-loading');
		jQuery('.luv-loading').remove();
	});
}

/*----------------------------------------------------------------------------------*/
/* Function to setup draggable items
/*----------------------------------------------------------------------------------*/

function initalize_draggable_items() {
	var $draggable = jQuery('.luv-products ul.luv-product-container li').draggabilly();
	// container position
	var container_left = jQuery('.luv-selected-items ul').offset().left;
	var container_right = jQuery(window).width() - (container_left + jQuery('.luv-selected-items ul').outerWidth());
		
	// On drag start
	$draggable.on( 'dragStart', function() {
		jQuery('.luv-drop-zone').show();
		jQuery('.luv-selected-items').addClass('luv-dragging');
	});
	
	// On drag end
	$draggable.on( 'dragEnd', function(event, pointer) {
		
		if(pointer.clientX >= container_left && pointer.clientX <= container_right) {

			var item = jQuery('.luv-dummy-item').clone();
			var original = jQuery(this).clone();
			
			// Change ID
			item.attr('data-product-id', original.attr('data-product-id'));
			
			// Change Name
			item.children('.luv-product-title').text(original.children('.luv-product-title').text());
			
			// Add Image
			item.append(original.children('img'));
			
			// Hidden Input
			item.children('[name="fevr_meta[collection-items][]"]').val(original.attr('data-product-id'));
			
			// Remove Input
			item.children('.remove-product').attr('data-product-id', original.attr('data-product-id'));
			
			item.removeClass('luv-dummy-item');
			
			jQuery(item).insertAfter('.luv-drop-zone');
			
			jQuery(this).remove();
			get_collection_items();
			jQuery('.luv-products .luv-product-outer').attr('data-paged', jQuery(this).data('page'));
		} else {
			jQuery(this).attr('style', 'position: relative');
		}
		
		jQuery('.luv-drop-zone').hide();
		jQuery('.luv-selected-items').removeClass('luv-dragging');
	});
}

/*----------------------------------------------------------------------------------*/
/* Update megamenu settings (show/hide settings based on depth and parent menu level)
/*----------------------------------------------------------------------------------*/

function update_luvthemes_megamenu(){
	var luvthemes_megamenu_enabled = false;
	
	jQuery('.menu-item').each(function(){
		//Check is megamenu enabled
	    if (jQuery(this).find('.enable-megamenu-switch').prop('checked') === true && jQuery(this).hasClass('menu-item-depth-0')){
	      luvthemes_megamenu_enabled = true;
	    }
	    else if (jQuery(this).find('.enable-megamenu-switch').prop('checked') === false && jQuery(this).hasClass('menu-item-depth-0')){
	      luvthemes_megamenu_enabled = false;
	    }
	    
	    // Show/hide megamenu options
	    var default_width = (jQuery(this).hasClass('menu-item-depth-1') ? 3 : 12);
	    if ((jQuery(this).hasClass('menu-item-depth-1') || jQuery(this).hasClass('menu-item-depth-2')) && luvthemes_megamenu_enabled === true){
	      jQuery(this).find('.menu-item-luv-megamenu-widgetarea').removeClass('menu-item-megamenu-hidden');
	      jQuery(this).find('.menu-item-luv-megamenu-columns').removeClass('menu-item-megamenu-hidden');
	    }
	    else {
	      jQuery(this).find('.menu-item-luv-megamenu-widgetarea').addClass('menu-item-megamenu-hidden');
	      jQuery(this).find('.menu-item-luv-megamenu-columns').addClass('menu-item-megamenu-hidden');
	    }
	});
}

// Trigger mega menu on load
jQuery(function(){
	update_luvthemes_megamenu();
});


/*----------------------------------------------------------------------------------*/
/* For Post Format Display Function
/*----------------------------------------------------------------------------------*/

function display_post_format_meta_box() {
	jQuery('#post-body div[id^=luv-post-format-meta-box-]').hide();
	var post_format = jQuery('#post-formats-select input:checked').attr('value');
	jQuery('#post-body #luv-post-format-meta-box-' + post_format + '').stop(true, true).fadeIn(400);
}

/*----------------------------------------------------------------------------------*/
/* Bindings
/*----------------------------------------------------------------------------------*/

jQuery(document).ready(function(){
	
	// Toggle Framework Sections
	jQuery('.luv-section-header').click(function(){
		jQuery(this).toggleClass('luv-section-header-hidden');
		jQuery(this).nextUntil('.luv-section-header').toggle();
	});
	
	jQuery('#page-header-filter, #slide-filter').change(function(){
		var current_filter = jQuery(this).val();
		if(jQuery('.page-header-bg_container .luv-media-upload-preview, .slide-image_container .luv-media-upload-preview').length > 0) {
			jQuery('.page-header-bg_container .media-image > img, .slide-image_container .media-image > img').removeClass(
				function (index, css) {
					return (css.match (/(^|\s)filter-\S+/g) || []).join(' ');
				}
			).addClass(current_filter);
		}
	});
	
	jQuery('#page-header-filter, #slide-filter').keypress(function(){
		jQuery(this).trigger('change');
	});

	/*----------------------------------------------------------------------------------*/
	/* Custom Redux Checkboxes
	/*----------------------------------------------------------------------------------*/
	
	jQuery('.redux-container-checkbox > input[type="checkbox"]').each(function(){
		jQuery('<label class="switch-style" for="'+jQuery(this).attr('id')+'"></label>').insertAfter(jQuery(this));	
		jQuery(this).addClass('switch-style');
	});
	
	/*----------------------------------------------------------------------------------*/
	/* Display Meta Boxes For Post Formats
	/*----------------------------------------------------------------------------------*/
	
	display_post_format_meta_box();
	
	//jQuery('.post-format-aside').text('Expiring');
	
	jQuery('#post-formats-select input').on('change', display_post_format_meta_box);
	

	jQuery('.wp-color-picker').each(function(){
		var field = jQuery(this);
		jQuery(this).wpColorPicker({
			change: function(event, ui) {
		        // event = standard jQuery event, produced by whichever control was changed.
		        // ui = standard jQuery UI object, with a color member containing a Color.js object
		
		        // change the headline color
		        setTimeout(function(){
			   		jQuery(field).trigger('keyup');
			    }, 100);
		    }
		});
	});

	/*----------------------------------------------------------------------------------*/
	/* Display Sidebar Metabox 
	/*----------------------------------------------------------------------------------*/
	
	jQuery(document).on('change','#page_template', function(){
		jQuery('li[data-tab="header-promo"]').stop(true, true).hide();
		jQuery('li[data-tab="page-sidebar"]').stop(true, true).hide();
			
		if (jQuery(this).val() == 'page-templates/page-sidebar.php'){
			jQuery('li[data-tab="page-sidebar"]').stop(true, true).fadeIn(400);
		}
		else if (jQuery(this).val() == 'page-templates/page-promo.php'){
			jQuery('li[data-tab="header-promo"]').stop(true, true).fadeIn(400);
		}
	});
	
	// Show metabox on initial pageload if necessary
	jQuery('#page_template').trigger('change');
	
	/*----------------------------------------------------------------------------------*/
	/* Megamenu Functions
	/*----------------------------------------------------------------------------------*/
	
	// Update megamenu settings
	jQuery(document).on('change','.enable-megamenu-switch',function(){
		update_luvthemes_megamenu();
	});

	// Update megamenu settings on menu-to-edit content's change
	jQuery('.ui-sortable').bind('sortstop', function(event, ui) {
		setTimeout(function(){
			if (typeof jQuery(ui.item).attr('data-depth') === 'undefined' || jQuery(ui.item).attr('data-depth') == '' || !jQuery(ui.item).hasClass(jQuery(ui.item).attr('data-depth'))){
				jQuery(ui.item).removeAttr('data-depth');
			}
			update_luvthemes_megamenu();
		},100); 
	});
	
	jQuery('.ui-sortable').bind('sortstart', function(event, ui) {
		var current_depth = jQuery.grep(jQuery(ui.item).attr('class').split(" "), function(v, i){
		       return v.indexOf('menu-item-depth-') === 0;
		}).join();
		jQuery(ui.item).attr('data-depth', current_depth);
		
	});
	
	if (jQuery('body.nav-menus-php').length > 0){
		jQuery( document ).ajaxSuccess(function( event, xhr, settings ) {
			 if (settings.data.match('action=add-menu-item')){
				 init_luv_required();
			 }
		});
	}
	
	/*----------------------------------------------------------------------------------*/
	/* Repeatable Metabox
	/*----------------------------------------------------------------------------------*/
	
	jQuery(document).on('click', '.luv-repeat-field', function(){
		var field = jQuery(this).closest('.luv-repeatable > div').clone();
		field.find('img').attr('src', '');
		field.find('input').attr('value', '');
		field.find('img').addClass('is-hidden');
		field.find('.luv-repeat-field').addClass('is-hidden');
		field.find('.luv-media-upload').text(luvthemes_admin.translate('Upload'));
		jQuery(field).insertAfter(jQuery(this).closest('.luv-repeatable > div'));
	});
	
	jQuery(document).on('click', '.luv-repeatable .luv-media-upload-reset', function(){
		if(jQuery('.luv-repeatable > div').length > 2) {
			jQuery(this).closest('.luv-media-upload-container').remove();
		}
	});
	
	jQuery('.luv-sortable').sortable({
		
	});
	
	/*----------------------------------------------------------------------------------*/
	/* Collection
	/*----------------------------------------------------------------------------------*/
	 
	if(jQuery('.luv-products').length > 0) {
		
		jQuery('[name="luvthemes_product_cat[]"]').prop('checked', false); 
		
		initalize_draggable_items();
		
		jQuery('.luv-selected-items ul').sortable({
			forcePlaceholderSize: true,
			dropOnEmpty: true
		});
		
		jQuery('.luv-product-search').on('change keyup', function(){
			jQuery('.luv-products .luv-product-outer').attr('data-paged', '1');
			get_collection_items();
		});
		
		jQuery('[name="luvthemes_product_cat[]"]').on('change', function(){
			jQuery('.luv-products .luv-product-outer').attr('data-paged', '1');
			get_collection_items();
		});
		
		jQuery(document).on('click', '.collection-pages a', function(e) {
			e.preventDefault();
			jQuery('.luv-products .luv-product-outer').attr('data-paged', jQuery(this).data('page'));
			get_collection_items();
		});
		
		jQuery('.category-dropdown-toggle').click(function(e){
			e.preventDefault();
			jQuery('.luv-category-container ul').slideToggle();
		});
		
		// Remove Product
		jQuery(document).on('click', '.remove-product', function(e){
			e.preventDefault();
			jQuery('.luv-selected-items li[data-product-id=' + jQuery(this).data('product-id') + ']').remove();
			get_collection_items();
		});
	}
	
	/*----------------------------------------------------------------------------------*/
	/* Luv Slider
	/*----------------------------------------------------------------------------------*/
	
	// Sortable fields for slider
	jQuery('.luv-slides-container').sortable({
		stop: function(){
			reorder_slides();
		}
	});
	
	// Add new slide
	jQuery('.add-new-slide').click(function(e){
		e.preventDefault();
		var slide = jQuery('.luv-slides-container li.is-hidden').clone().removeClass('is-hidden');
		jQuery('.luv-slides-container li.is-hidden').before(slide);
		slide.click();
		
		// On new sliders the editor is hidden, we should make it visible
		if(jQuery('#slide-meta-boxes').is(':hidden')) {
			jQuery('#slide-meta-boxes').show();
		}
		
		reorder_slides();
	});
	
	// Remove slide
	jQuery('.remove-slide').click(function(e){
		e.preventDefault();
		jQuery('.luv-slides-container li.slide-active').remove();
		jQuery('.luv-slides-container li:first-child').click();
		
		reorder_slides();
	});
	
	// Load data from the first preview
	if(jQuery('.luv-slides-container li:not(.is-hidden)').length > 0) {
		jQuery('.luv-slides-container li:first-child').addClass('slide-active');
		
		load_slide_data();
	} else {
		jQuery('#slide-meta-boxes').hide();
	}
	
	
	// Load data from temporary containers to previews
	jQuery(document).on('click', '.wp-picker-holder', function(){
		jQuery(this).prev('.wp-picker-input-wrap').find('.wp-color-picker').trigger('change');
	});

	jQuery(document).on('mouseup', '.wp-picker-clear', function(){
		var that = jQuery(this);
		setTimeout(function(){jQuery(that).prev().trigger('change')},100);
	});
	
	jQuery(document).on('change keyup click', '#slide-meta-boxes input, #slide-meta-boxes textarea, #slide-meta-boxes select, #slide-meta-boxes textarea', function(){
		var item = jQuery(this).attr('data-id') || jQuery(this).attr('id');
		//var item = ( (!jQuery('#'+item).is('input[type="checkbox"]') && !jQuery('#'+item).is('input[type="radio"]')) || !jQuery('#'+item).prop('checked') === true ? jQuery(this).val() : '');
		
		// Inputs has similar classes, so the saving is easy
	
		if((jQuery(this).prop('type') === 'checkbox' || jQuery(this).prop('type') === 'radio') && !jQuery(this).prop('checked')) {
			// If the input is a checkbox and doesn't checked we have to remove the data from the preview's hidden input
			jQuery('.slide-active .luv-'+item).val('');
		} else {
			
			// Exception for layers
			if(jQuery(this).attr('name') == '_fevr_meta[slide-parallax-layer-list][]') {
				load_slide_layer_data('container');
			} else {
				jQuery('.slide-active .luv-'+item).val(jQuery(this).val());
			}
		}
		
		if(jQuery(this).prop('disabled')) {
			jQuery('.slide-active .luv-'+item).prop('disabled', true);
		} else {
			jQuery('.slide-active .luv-'+item).removeProp('disabled');
		}
		
		if(jQuery('#slide-image').val() != '') {
			var slide_image_src = jQuery('#slide-image').val();
			
			jQuery('.slide-active').css('background-image', 'url(' + slide_image_src + ')');
		}
		
	});
	
	// Load data from previews to editor
	jQuery(document).on('click', '.luv-slides-container li', function(){
		/* 
		 * Reset repetable fields
		 */
		
		// Layers
		jQuery('.luv-repeatable .luv-media-upload-container').not(':first').remove();
		var first_container = jQuery('.luv-repeatable .luv-media-upload-container');
		first_container.find('[name="_fevr_meta[slide-parallax-layer-list][]"]').val('');
		first_container.find('.luv-media-upload-preview').addClass('is-hidden');
		first_container.find('.luv-media-buttons span').addClass('is-hidden');
		first_container.find('.media_upload_button').removeClass('is-hidden');
		first_container.find('.media_upload_button').text(luvthemes_admin.translate('Upload')); 
		
		// Select active slide
		jQuery('.luv-slides-container li').removeClass('slide-active');
		jQuery(this).addClass('slide-active');
		
		// Load slide data to editor
		
		load_slide_data();
	});
	
	// Load slide layer data on sortstop to reorder layers in hidden containers if necessary
	jQuery(document).on('sortstop','.slide-parallax-layer-list_container .luv-sortable',function(){
		load_slide_layer_data('container');
	});
	
	// Function to load data from previews
	function load_slide_data() {
		jQuery('.slide-active > input, .slide-active > textarea').each(function() {
			
			var target = jQuery('#'+jQuery(this).attr('class').replace('luv-', ''));

			//Exception for layers
			if (jQuery(this).hasClass('luv-slide-parallax-layer-list')){
				load_slide_layer_data('editor', jQuery(this));
			}
			else if (jQuery(this).hasClass('luv-slide-heading-font-weight') || jQuery(this).hasClass('luv-slide-caption-font-weight')){
				jQuery(target).val(jQuery(this).val());
				jQuery(target).attr('data-value',jQuery(this).val());
			}
			else if(jQuery('[data-id='+jQuery(this).attr('class').replace('luv-', '')+']').length > 0) {
				jQuery('[value="'+jQuery(this).val()+'"]').prop('checked', true);
				// Trigger change to show/hide required fields
				var that = jQuery(this);
				setTimeout(function(){
					jQuery('[value="'+that.val()+'"]').trigger('change');
				}, 100);
				
			} else if(target.prop('type') === 'checkbox' && jQuery(this).val() === '') {
				target.removeProp('checked');
			} else if(target.prop('type') === 'checkbox' && jQuery(this).val() !== '') {
				target.prop('checked', true);
			} else {
				target.val(jQuery(this).val());
			}
			
		});
			
		if(jQuery('.slide-active').find('.luv-slide-image').val()) {
			jQuery('.slide-image_container .luv-media-upload-preview').attr('src', jQuery('.slide-active').find('.luv-slide-image').val()).removeClass('is-hidden');
			jQuery('.slide-image_container .luv-media-upload').text(luvthemes_admin.translate('Modify'));
			jQuery('.slide-image_container .luv-media-upload-reset').removeClass('is-hidden');
		} else {
			jQuery('.slide-image_container .luv-media-upload-preview').addClass('is-hidden');
			jQuery('.slide-image_container .luv-media-upload').text(luvthemes_admin.translate('Upload'));
			jQuery('.slide-image_container .luv-media-upload-reset').addClass('is-hidden');
		}
		
		if(jQuery('.slide-active').find('.luv-slide-video-mp4').val()) {
			jQuery('.slide-video-mp4_container .luv-media-upload').text(luvthemes_admin.translate('Modify'));
			jQuery('.slide-video-mp4_container .luv-media-upload-reset').removeClass('is-hidden');
		} else {
			jQuery('.slide-video-mp4_container .luv-media-upload').text(luvthemes_admin.translate('Upload'));
			jQuery('.slide-video-mp4_container .luv-media-upload-reset').addClass('is-hidden');
		}
		
		if(jQuery('.slide-active').find('.luv-slide-video-ogv').val()) {
			jQuery('.slide-video-ogv_container .luv-media-upload').text(luvthemes_admin.translate('Modify'));
			jQuery('.slide-video-ogv_container .luv-media-upload-reset').removeClass('is-hidden');
		} else {
			jQuery('.slide-video-ogv_container .luv-media-upload').text(luvthemes_admin.translate('Upload'));
			jQuery('.slide-video-ogv_container .luv-media-upload-reset').addClass('is-hidden');
		}
	}
	
	// Handler for layers (we convert array to JSON and JSON to array)
	function load_slide_layer_data(target, object){
		// Editor to contaimer
		if (target == 'container'){
			var layers = [];
			jQuery('[name="_fevr_meta[slide-parallax-layer-list][]"]').each(function(){
				layers.push(jQuery(this).val());
			});
			if (layers.length > 0){
				jQuery('.slide-active .luv-slide-parallax-layer-list').val(JSON.stringify(layers));
			}
		}
		// From container to editor
		else{
			try{
				var images = JSON.parse(object.val());
				if (images.length > 0){
					for (var i in images){	
						if (images[i] != ''){
							console.log(i);
							console.log(images[i]);
							var field = jQuery('.slide-parallax-layer-list_container .luv-repeatable .luv-media-upload-container:last').clone();
							field.find('img').attr('src', images[i]);
							field.find('img').removeClass('is-hidden');
							field.find('input').attr('value', images[i]);
							field.find('.media_upload_button').text(luvthemes_admin.translate('Modify'));
							// Manipulate buttons on last image
							if (i < images.length){
								field.find('.luv-media-buttons span').removeClass('is-hidden');
							}
							jQuery(field).insertAfter('.slide-parallax-layer-list_container .luv-repeatable .luv-media-upload-container:last');
						}
					}
					// Remove the first empty entry 
					if (jQuery('.slide-parallax-layer-list_container .luv-repeatable .luv-media-upload-container').length > 1){
						jQuery('.slide-parallax-layer-list_container .luv-repeatable .luv-media-upload-container:first').remove();
					}
				}
			}
			catch(e){
				//Silent fail if JSON is malformed
			}
		}
	}
	
	// Function to re-order 'name' attribute values
	function reorder_slides() {
		jQuery('.luv-slides-container li').each(function(){
			var index = jQuery(this).index('.luv-slides-container li');
			jQuery(this).find('input').each(function(){
				jQuery(this).attr('name', jQuery(this).attr('name').replace(/\[([0-9]*)?\]/,'['+index+']'));
			});
		});
	}
	
	reorder_slides();
	
	/*----------------------------------------------------------------------------------*/
	/* Manage Photo Reviews
	/*----------------------------------------------------------------------------------*/
	
	// Remove photo review image
	jQuery(document).on('click', '.remove-photo-review-image', function(e){
		e.preventDefault();
		var container = jQuery(this).parent();
		jQuery.post(fevr_admin_ajax.ajax_url, {'action' : 'fevr_photo_review_remove_image', 'photo_review_image_id' : jQuery(this).attr('data-id'), 'wp_nonce': fevr_admin_ajax.wp_nonce},function(){
			container.remove();
		});
	});
	
	// Quick publish
	jQuery(document).on('click', '.ext-reviews-quick-publish', function(e){
		e.preventDefault();
		var button = jQuery(this);
		jQuery.post(fevr_admin_ajax.ajax_url, {'action' : 'fevr_accept_photo_review', 'photo_review_id' : jQuery(this).attr('data-id'), 'wp_nonce': fevr_admin_ajax.wp_nonce},function(){
			button.closest('tr').find('.post-state').empty().text(luvthemes_admin.translate('Approved'));
			button.find('span').text(luvthemes_admin.translate('Approved'));
			button.removeClass('ext-reviews-quick-publish luv-btn-blue').addClass('luv-btn-green on-hover');
		});
	});
	
	jQuery(document).on('click', '.luv-admin-notice-dismiss', function(e){
		e.preventDefault();
		var that = jQuery(this);
		jQuery.post(fevr_admin_ajax.ajax_url, {'action':'fevr_dismiss_notice', 'dismiss' : jQuery(that).attr('data-dismiss'), 'wp_nonce': fevr_admin_ajax.wp_nonce}, function(){
			jQuery(that).closest('.luv-admin-notice').remove();
		});
	});
	
	/*----------------------------------------------------------------------------------*/
	/* Masonry Settings for WP Gallery
	/*----------------------------------------------------------------------------------*/

	
	jQuery(document).ready(function(){

	 jQuery(document).on('click','button.media-button-gallery', init_luv_required);
		
      _.extend(wp.media.gallery.defaults, {
        masonry: ''
      });
      
      wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend({
        template: function(view){
          return wp.media.template('gallery-settings')(view)
               + wp.media.template('masonry')(view);
        }
      });

    });
	
	/*----------------------------------------------------------------------------------*/
	/* LuvStock
	/*----------------------------------------------------------------------------------*/
	
	var luvstock_type_timeout;
	
	if (jQuery('body#media-upload')){
		jQuery('.luvstock-container .luvstock-content').load(ajaxurl + '?action=fevr_get_luvstock_items&wp_nonce=' + fevr_admin_ajax.wp_nonce);
	}
	
	jQuery(document).on('click','.luvstock-trigger', function(e){
		e.preventDefault();
		if (jQuery(this).parents('.media-modal').length == 0){
		    wp.media({
		        frame:    "post",
		        state:    "insert",
		        multiple: false,
		    }).open();
		}

		jQuery('.media-menu').find(':contains("LuvStock")').click();
	
	});
	
	jQuery(document).on('click', '.luvstock-list li', function(){
		jQuery(this).toggleClass('details');

		if (jQuery('.luvstock-list li.details').length > 0){
			jQuery('.luvstock-download').removeProp('disabled');
			jQuery('.luvstock-download').removeClass('luv-btn-blue');
			jQuery('.luvstock-download').addClass('luv-btn-green');
		}
		else{
			jQuery('.luvstock-download').prop('disabled','disabled');
			jQuery('.luvstock-download').removeClass('luv-btn-green');
			jQuery('.luvstock-download').addClass('luv-btn-blue');
		}
	});
	
	jQuery(document).on('click', '.luvstock-download', function(e){
		e.preventDefault();
		var items = [];
		
		jQuery('.luvstock-download').addClass('luv-hidden');
		
		jQuery('.luvstock-list li.details').each(function(){
			items.push(jQuery(this).attr('data-item'));
		});
		
		jQuery('.luvstock-container .luvstock-content').empty().append('<div class="luvstock-download-progress-container"><div class="media-progress-bar"><div class="luvstock-download-bar" style="width: 2%" data-width="2"></div></div></div>');
		setTimeout(function(){luvstock_progress(parseInt(100/items.length));},1000);
		
		jQuery.post(ajaxurl, {'action': 'fevr_download_luvstock_items', 'items': items, 'wp_nonce' : fevr_admin_ajax.wp_nonce}, function(){
			if (pagenow == 'media_page_fevr_luvstock'){
				document.location.href = fevr_admin_ajax.upload_page;
			}
			else{
	            var wp = parent.wp;
	            wp.media.frame.setState('insert');
	            if( wp.media.frame.content.get() !== null) {
	                wp.media.frame.content.get().collection.props.set({ignore: (+ new Date())});
	                wp.media.frame.content.get().options.selection.reset();
	            } else {
	                wp.media.frame.library.props.set ({ignore: (+ new Date())});
	            }
	            
	            jQuery('.luvstock-download').removeClass('luv-hidden');
			}
		});
	});
	
	jQuery(document).on('keyup', '.luvstock-search', function(){
		luvstock_type_timeout = setTimeout(luvstock_search,500);
	});
	
	jQuery(document).on('keydown', '.luvstock-search', function(){
		clearTimeout(luvstock_type_timeout);
	});

	jQuery(document).on('click', '.luvstock-pagination a', function(e){
		e.preventDefault();
		var page = jQuery(this).attr('data-page');
		jQuery('.luvstock-container .luvstock-content').load(ajaxurl, {'action' : 'fevr_get_luvstock_items', 'keyword' : jQuery('.luvstock-search').val(), 'start' : page, 'wp_nonce' : fevr_admin_ajax.wp_nonce});
	});
	
	function luvstock_search(){
		jQuery('.luvstock-container .luvstock-content').empty().append('<div class="luvstock-loader"><i class="fa fa-spinner fa-pulse"></i></div>');
		jQuery('.luvstock-container .luvstock-content').load(ajaxurl, {'action' : 'fevr_get_luvstock_items', 'keyword' : jQuery('.luvstock-search').val(), 'wp_nonce' : fevr_admin_ajax.wp_nonce});
	}
	
	function luvstock_progress(status){
		var current_width	= jQuery('.luvstock-download-bar').attr('data-width');
		
		var div 		= ((current_width*1) + (status*1) < 40 ? status : ((current_width*1) + (status*1) < 70 ? status/2 : status/4));
		var new_width	= (current_width*1) + (div*1);
		
		
		if (new_width < 90){
			jQuery('.luvstock-download-bar').attr('data-width', new_width);
			jQuery('.luvstock-download-bar').css('width', new_width + '%');
			setTimeout(function(){luvstock_progress(status);},1000);
		}
	}
	
	/*----------------------------------------------------------------------------------*/
	/* Luvthemes Template
	/*----------------------------------------------------------------------------------*/
	
	var luvthemes_templates_type_timeout;
	
	if (pagenow == 'pages_page_fevr_use_template'){
		var group = jQuery('.luvthemes-templates-search').attr('data-group');
		jQuery('.luvthemes-templates-container .luvthemes-templates-content').load(ajaxurl, {'action' : 'fevr_get_luvthemes_templates', 'group': group, 'wp_nonce' : fevr_admin_ajax.wp_nonce}, function(){
			var template_grid = jQuery('.luvthemes-templates-list').isotope({
				layoutMode: 'packery',
				itemSelector: '.luvthemes-templates-list li',
				packery: {
					gutter: jQuery('.luvthemes-templates-list').width()*0.02,
				}
			});
			
			jQuery('.luvthemes-templates-filter li a').on( 'click', function(e) {
				e.preventDefault();
				jQuery('.luvthemes-templates-filter li a').removeClass('active-filter');
				jQuery(this).addClass('active-filter');
				var filterValue = jQuery(this).attr('data-filter');
	
				template_grid.isotope({ filter: filterValue });
			});
			
			template_grid.imagesLoaded().progress( function() {
				template_grid.isotope('layout');
			});
		});
	}

	
	// Refresh opener if it is luvthemes setup
	try{
		if (window.opener && window.opener.location.href.match('page=fevr_setup')){
			window.opener.location.reload();
		}
	}
	catch(e){
		// Silent fail if window opener doesn't accessable
	}
	
	jQuery(document).on('click', '.use-luvthemes-template', function(e){
		e.preventDefault();
		var id = jQuery(this).closest('li').attr('data-item');
		var group = jQuery(this).closest('li').attr('data-group');
		
		jQuery('.luvthemes-templates-container .luvthemes-templates-content').empty().append('<div class="luvthemes-templates-loader"><i class="fa fa-spinner fa-pulse"></i></div>');
		jQuery('.use-luvthemes-template').addClass('luv-hidden');
		
		jQuery.post(fevr_admin_ajax.ajax_url, {'action': 'fevr_use_luvthemes_template', 'id': id, 'group': group, 'wp_nonce' : fevr_admin_ajax.wp_nonce}, function(response){
			if (response.post_id){
				document.location.href = luvthemes_admin.decodeHtmlEntity(response.edit_url);
			}
			else{
				jQuery('.luvthemes-templates-container .luvthemes-templates-content').empty().append('<div>' + luvthemes_admin.translate('An error occured. Please try again later.') + '</div>');
			}
		});

	});
	
	jQuery(document).on('keyup', '.luvthemes-templates-search', function(){
		luvstock_type_timeout = setTimeout(luvthemes_templates_search,500);
	});
	
	jQuery(document).on('keydown', '.luvthemes-templates-search', function(){
		clearTimeout(luvthemes_templates_type_timeout);
	});

	jQuery(document).on('click', '.luvthemes-templates-pagination a', function(e){
		e.preventDefault();
		var page = jQuery(this).attr('data-page');
		var group = jQuery(this).attr('data-group');
		jQuery('.luvthemes-templates-container .luvthemes-templates-content').load(ajaxurl, {'action' : 'fevr_get_luvthemes_templates', 'keyword' : jQuery('.luvthemes-templates-search').val(), 'start' : page, 'group': group, 'wp_nonce' : fevr_admin_ajax.wp_nonce});
	});
	
	function luvthemes_templates_search(){
		var group	= jQuery('.luvthemes-templates-search').attr('data-group');
		var keyword = jQuery('.luvthemes-templates-search').val();
		jQuery('.luvthemes-templates-container .luvthemes-templates-content').empty().append('<div class="luvthemes-templates-loader"><i class="fa fa-spinner fa-pulse"></i></div>');
		jQuery('.luvthemes-templates-container .luvthemes-templates-content').load(ajaxurl, {'action' : 'fevr_get_luvthemes_templates', 'keyword' : keyword, 'group': group, 'wp_nonce' : fevr_admin_ajax.wp_nonce});
	}
		
	/*----------------------------------------------------------------------------------*/
	/* Meta Tabs
	/*----------------------------------------------------------------------------------*/
	
	// First tab onload
	jQuery(window).on('load', function(){
		if(jQuery('.luv-meta-tabs').length > 0){
			jQuery('.luv-meta-tabs li:not(.divider):first').click();
		}		
	});
	
	jQuery('.luv-meta-container.has-tabs ul li:not(.divider)').click(function(e){
		e.preventDefault();
		var tab = jQuery(this).attr('data-tab');
		jQuery('.luv-meta-container.has-tabs ul li').removeClass('active');
		jQuery(this).addClass('active');
		jQuery(this).closest('.luv-meta-container.has-tabs').find('div[data-tab]').removeClass('active');
		jQuery(this).closest('.luv-meta-container.has-tabs').find('div[data-tab="' + tab + '"]').addClass('active');
	});
	
	
	/*----------------------------------------------------------------------------------*/
	/* Tour pointers
	/*----------------------------------------------------------------------------------*/
	for (var i in fevr_admin_ajax.tips){
		jQuery(fevr_admin_ajax.tips[i].selector).pointer({
			content: fevr_admin_ajax.tips[i].content,
		    position: fevr_admin_ajax.tips[i].position,
		    close: function() {
		        jQuery.post(fevr_admin_ajax.ajax_url, {action: 'fevr_dismiss_tip', id: fevr_admin_ajax.tips[i].id, 'wp_nonce': fevr_admin_ajax.wp_nonce});
		    },
		}).pointer('open');
	};
});
