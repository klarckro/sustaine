jQuery(function(){
	"use strict";
	
	var shortcodeable_fields = ['top-bar-content','footer-custom-content', 'footer-content'];
	jQuery.each(shortcodeable_fields, function(){
			jQuery('[data-id="' + this + '"]').find('.wp-editor-tools').prepend(jQuery('<a>',{
				'class' : 'button redux-luv-shortcode-generator',
				'href'	: '#',
				'text'	: 'Luvthemes Shortcodes'
			}));	
	});
	
	jQuery('<input>',{'class': 'luv-redux-search', 'type':'text', 'placeholder': __('Search option')}).insertBefore(jQuery('#redux-sticky .redux-action_bar'));
	jQuery('<div id="9999_section_group" class="redux-group-tab" data-rel="99999"><table id="redux-search-results" class="form-table"></table></div>').insertAfter('#redux_ajax_overlay');
		
	jQuery(document).on('click', '.luv-redux-search-result',function(e){
		e.preventDefault();
		jQuery('#redux-search-results').empty();
		var tab = jQuery(this).attr('data-tab');
		var selector = jQuery(this).attr('data-selector');
		jQuery('#' + tab + '_li_a').click();
		setTimeout(function(){
			jQuery('html, body').animate({
				scrollTop: jQuery(selector).offset().top-100
			}, 100);
			jQuery(selector).focus();
		},100);
		return false;
	});
	
	jQuery(document).on('keyup', '.luv-redux-search', function(e){
		var element = jQuery(this);
		var keyword = jQuery(this).val();
	
		if (e.which == 13){
			return false;
		}
		else if(e.which == 0){
			jQuery(this).val('');
			jQuery(this).blur();
			jQuery('#redux-search-results').empty();
		}
		else{
			jQuery('#redux-search-results').empty();
			jQuery('.redux-group-tab-link-li').removeClass('active');
			jQuery('.redux-group-tab').css('display','none');
			jQuery('#9999_section_group').css('display','block');
			if (keyword == ''){
				return;
			}
			var tabs = [];
			var results = jQuery('<div>');
			var fevr_options = fevr_search_object['fevr_options'];
				for (var i in fevr_options){
					 if (
							 ((typeof fevr_options[i]['title'] != 'undefined') && fevr_options[i]['title'].match(new RegExp(keyword,'i'))) ||
							 ((typeof fevr_options[i]['subtitle'] != 'undefined') && fevr_options[i]['subtitle'].match(new RegExp(keyword,'i'))) ||
							 ((typeof fevr_options[i]['desc'] != 'undefined') && fevr_options[i]['desc'].match(new RegExp(keyword,'i')))
						)
					 {
						  var result = jQuery('#fevr_options-' + fevr_options[i]['id']);
						  var tab_id = jQuery(result).parents('.redux-group-tab').attr('id');
						  var selector = jQuery(result).getPath();
						  var clone = jQuery(result).parents('tr').clone();
						  jQuery(clone).addClass('luv-redux-search-result');
						  jQuery(clone).attr('data-tab', tab_id);
						  jQuery(clone).attr('data-selector', selector);
						  
						  if (!jQuery(result).parents('tr.fold').hasClass('hide')){
							  jQuery('#redux-search-results').append(jQuery(clone));				  
						  }
					  }
				}
		}
	});
	
	jQuery(document).on('click','.redux-sidebar', function(){
		jQuery('.luv-redux-search').val('');
		jQuery('#redux-search-results').empty();
	});
	
	/**
	 * Get element path
	 * @return string 
	 */
	jQuery.fn.getPath = function () {
	    if (this.length != 1) throw __('Requires one element.');
	
	    var path, node = this;
	    while (node.length) {
	        var realNode = node[0], name = realNode.localName;
	        if (!name) break;
	        name = name.toLowerCase();
	
	        var parent = node.parent();
	
	        var siblings = parent.children(name);
	        if (siblings.length > 1) { 
	            name += ':eq(' + siblings.index(realNode) + ')';
	        }
	
	        path = name + (path ? '>' + path : '');
	        node = parent;
	    }
	
	    return path;
	};
	
	function __(text){
		if (typeof fevr_admin_ajax !== 'undefined' && typeof fevr_admin_ajax.messages[text] !== 'undefined'){
			return fevr_admin_ajax.messages[text];
		}
		else {
			return text;
		}
	};
});