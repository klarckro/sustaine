<?php
	switch(get_post_type()) {
		case 'luv_portfolio':
		
			$fevr_like = (fevr_check_luvoption('portfolio-likes-meta', 'hide-on-single', '!=') && fevr_check_luvoption('portfolio-likes-meta', 'hide-on-both', '!='));
			$fevr_share = fevr_check_luvoption('portfolio-social-meta', 1, '!=');
		
			break;
			
		case 'post':
		
			$fevr_like = (fevr_check_luvoption('blog-likes-meta', 'hide-on-single', '!=') && fevr_check_luvoption('blog-likes-meta', 'hide-on-both', '!='));
			$fevr_share = fevr_check_luvoption('blog-social-meta', 1, '!=');				
			
			break;
			
		case 'product':
			
			$fevr_like = false;
			$fevr_share = fevr_check_luvoption('woocommerce-social-meta', 1, '!=');
				
			
			break;
			
		default:
			
			$fevr_share = true;
			$fevr_like = true;
	}
?>

<?php if ($fevr_share == true || $fevr_like == true):?>
<div class="social-share-container">
	<div class="luv-social-buttons">
	<?php 
		if($fevr_like) {
			fevr_get_like_icon(false);
		}	
	?>
	
	<?php
		if($fevr_share) {
			fevr_get_social_share_icons();
		}
	?>
	</div>
</div>
<?php endif;?>