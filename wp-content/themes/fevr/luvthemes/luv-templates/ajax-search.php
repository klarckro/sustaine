<?php 
	$fevr_meta_fields = get_post_meta( get_the_ID(), 'fevr_meta', true);
?>
<div class="luv-ajax-result">
	<div class="luv-ajax-results-meta">
	<?php
		$post_type = get_post_type();
		switch ($post_type) {
			case 'luv_portfolio':
				$post_type_name = esc_html__('Portfolio', 'fevr');
				break;
			
			case 'luv_collections':
				$post_type_name = esc_html__('Collection', 'fevr');
				break;
				
			case 'post':
				$post_type_name = esc_html__('Blog Post', 'fevr');
				break;
				
			default:
				$post_type_name = esc_html($post_type);
		}
		echo esc_html($post_type_name).' | ';
		echo get_the_time(get_option('date_format'));
	?>
	</div>
	<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>" class="post-title-only" style="display:block;width:100%">
		<?php if ( has_post_thumbnail() ): ?>
			<div class="post-featured-img">
				<?php the_post_thumbnail('thumb');?>
			</div>
		<?php endif; ?>
		<h3 class="post-title"><?php the_title(); ?></h3>
	</a>
</div>