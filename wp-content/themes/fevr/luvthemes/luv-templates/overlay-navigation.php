<div id="overlay-navigation-overlay">
	<div id="overlay-navigation">
		<a href="#" class="overlay-navigation-trigger"><i class="ion-close"></i></a>
		<div id="overlay-navigation-inner" <?php echo (fevr_check_luvoption('overlay-navigation-only-nav', 1) && fevr_check_luvoption('overlay-navigation', 1)) ? 'class="primary-overlay-navigation"' : ''; ?>>
			
			<div class="container">
				<?php do_action('fevr_overlay_nav_content');?>
			</div>
		</div>
	</div>
</div>