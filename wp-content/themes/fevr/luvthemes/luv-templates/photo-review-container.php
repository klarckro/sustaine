<div class="woocommerce">
	<div class="photo-review-popup">
		<a href="#" class="photo-review-close"><i class="ion-close"></i></a>
		<div class="photo-review-message is-hidden"></div>
		<div class="photo-review-container"></div>
	</div>
</div>	
