<form id="photo-review-upload-form" class="photo-review-forms" method="POST">
	<input type="file" class="to-empty" id="review-file-select" name="photo_review_photos[]" multiple>
	<input type="hidden" name="action" value="photo_review_image_upload">
	<input type="hidden" id="photo-review-product-id" name="product_id" value="<?php echo esc_attr($_POST['product_id']);?>">
	<input type="hidden" id="photo-review-id" name="photo_review_id" value="">
	
	<button type="submit" id="photo-review-upload"><?php esc_html_e('Upload', 'fevr'); ?></button>
	
	<div id="photo-review-upload-text">
		<p><i class="fa fa-cloud-upload"></i></p>
		<p><?php esc_html_e('Drag and drop a file here or click', 'fevr'); ?></p>
	</div>
</form>

<div id="photo-review-image-container"></div>

<form id="photo-review-content-form" class="photo-review-forms" method="POST">
	<input type="text" class="to-empty" name="photo_review_title" placeholder="<?php esc_html_e('Title', 'fevr'); ?>">
	<textarea class="to-empty" name="photo_review_content" placeholder="<?php esc_html_e('What do you think about this?', 'fevr'); ?>"></textarea>
	
	<?php if(fevr_check_luvoption('woocommerce-photo-reviews-rating', 1, '!=')): ?>
	<div class="photo-review-feedback-container">
		<label><?php esc_html_e('Your Rating', 'fevr'); ?></label>
		<input type="radio" id="feedback-positive" name="feedback" class="photo-review-feedback positive" value="1">
		<label for="feedback-positive"><i class="fa fa-thumbs-o-up"></i></label>
		<input type="radio" id="feedback-negative" name="feedback" class="photo-review-feedback negative" value="-1">
		<label for="feedback-negative"><i class="fa fa-thumbs-o-down"></i></label>
	</div>
	<?php endif; ?>
	<input type="hidden" name="action" value="fevr_photo_review_content">
	<?php wp_nonce_field('fevr','wp_nonce');?>
	<input type="hidden" class="photo-review-product-id" name="product_id" value="<?php echo esc_attr($_POST['product_id']);?>">
	<input type="hidden" class="photo-review-id" name="photo_review_id" value="">
	<button type="submit" class="btn btn-global btn-full" id="photo-review-send"><?php esc_html_e('Upload', 'fevr'); ?></button>
</form>