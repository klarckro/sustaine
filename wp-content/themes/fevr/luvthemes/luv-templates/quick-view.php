<?php 
global $product, $post, $woocommerce;
$product = get_product((int)$_POST['product_id'] );
$post = get_post((int)$_POST['product_id']);

$product_cats = wp_get_post_terms( (int)$_POST['product_id'], 'product_cat' );
     if (!empty($product_cats)){
     foreach ((array)$product_cats as $cat){
     	$product_categories[] = '<a href="' . esc_url(get_term_link( $cat->term_id, 'product_cat')) .'" class="woocommerce-quick-view-category">'.esc_html($cat->name).'</a>';
	}
}	

do_action('fevr_woocommerce_quick_view');

?>

<?php do_action('fevr_before_woocommerce_quick_view'); ?>

<div class="wc-quick-view-image">
	<?php echo fevr_kses($product->get_image('shop_single'));?>
</div>

<div class="wc-quick-view-content">
	<?php do_action( 'woocommerce_single_product_summary' ); ?>
	
</div>