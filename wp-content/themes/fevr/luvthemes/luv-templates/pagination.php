<?php
	// Use shortcode attributes
	if (fevr_is_luv_shortcode()){
		global $fevr_pagination_shortcode_atts, $fevr_meta_fields, $fevr_shortcode_post_type;
		$post_type = $fevr_shortcode_post_type;
	}
	// Use global settings
	else {
		$post_type = get_post_type();
	}
	switch($post_type) {
		case 'luv_portfolio':
		
			$fevr_pagination = fevr_get_luvoption('portfolio-pagination');
		
			break;
		
		case 'luv_collections':
		
			$fevr_pagination = fevr_get_luvoption('woocommerce-collections-pagination');
		
			break;
			
		case 'fevr_ext_reviews':
		
			$fevr_pagination = fevr_get_luvoption('woocommerce-photo-reviews-pagination');
		
			break;
			
		default:
		
			$fevr_pagination = fevr_get_luvoption('blog-pagination');
	}
	
	if ($fevr_pagination == 'infinite-scroll'){
		wp_enqueue_script( 'fevr-infinite-scroll', trailingslashit(get_template_directory_uri()) . 'js/min/jquery.infinitescroll.min.js', array('jquery'), FEVR_THEME_VER, true );
	}
?>
<?php if ($fevr_pagination == 'infinite-scroll'):?>
<div class="infinite-loader-container"></div>
<?php endif;?>
<div class="pagination-container">
<?php if($fevr_pagination == 'prev-next'):?>
		<?php posts_nav_link(' ','<span class="page-numbers">&larr;</span>','<span class="page-numbers">&rarr;</span>'); ?>

<?php elseif($fevr_pagination == 'standard' || $fevr_pagination == 'infinite-scroll'):
	$big = PHP_INT_MAX;
	
	echo ($fevr_pagination == 'infinite-scroll' ? '<div class="is-hidden pagination-infinite-scroll">' : '');
	echo paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'prev_text'          => '&larr;',
		'next_text'          => '&rarr;',
		'current' => max( 1, get_query_var('paged') ),
	) );
	echo ($fevr_pagination == 'infinite-scroll' ? '</div>' : '');

endif; ?>
</div>