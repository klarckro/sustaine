<?php	
	if (fevr_is_luv_shortcode()){
		global $fevr_shortcode_categories, $fevr_shortcode_taxonomy;
		$taxonomy = $fevr_shortcode_taxonomy;
		
		// Filter categories
		foreach ($fevr_shortcode_categories as $cat){
			$categories[] = get_term_by('name', $cat, $taxonomy);
		}
		
		$filter_background = fevr_get_luvoption('shortcode-masonry-filter-background');
	}
	else {
		$filter_background = 0;
		switch (get_post_type()) {
			case 'luv_portfolio':
					
				$taxonomy = 'luv_portfolio_categories';
				$filter_background = fevr_get_luvoption('portfolio-masonry-filter-background');
					
				break;
					
			default:
				
				$taxonomy = 'category';
				$filter_background = fevr_get_luvoption('blog-masonry-filter-background');
		}
		
		$categories = get_categories(array('taxonomy' => $taxonomy));
	}
	
?>

<?php if (count($categories) > 1):?>
<div id="masonry-filter" class="<?php echo (fevr_is_luv_shortcode() ? fevr_enqueue_inline_css(array('style' => 'margin-top: 0 !important;')): ''); ?> <?php echo ($filter_background == 1 ? 'has-background' : ''); ?>">
	<span id="masonry-filter-term"><?php esc_html_e('All', 'fevr'); ?></span>
	<ul id="masonry-filter-options">
		<li data-masonry-filter="*" class="active-term"><?php esc_html_e('All', 'fevr'); ?></li>
		<?php foreach($categories as $category): ?>
		<li data-masonry-filter=".<?php echo esc_attr($taxonomy); ?>-<?php echo esc_attr($category->slug); ?>"><?php echo esc_html($category->name); ?></li>
		<?php endforeach; ?>
	</ul>
</div>
<?php endif;?>