<div id="off-canvas-menu-overlay"></div>
<div id="off-canvas-menu">
	<div id="off-canvas-menu-inner">
		<a href="#" class="off-canvas-menu-trigger <?php if(fevr_check_luvoption('off-canvas-menu-close-btn', 1)) { echo 'is-block'; } ?>"><i class="ion-close"></i></a>
		<?php do_action('fevr_off_canvas_nav_content');?>
	</div>
</div>