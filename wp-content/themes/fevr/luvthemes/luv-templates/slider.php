<?php
	global $fevr_slider_atts;

	if(is_numeric($fevr_slider_atts['id'])) {
		$_slider = $fevr_slider_atts['id'];
	} else {
		global $wpdb;
		$_slider = $wpdb->get_var($wpdb->prepare('SELECT ID FROM ' . $wpdb->posts . ' WHERE post_name = %s AND post_type = "luv_slider" LIMIT 1', $fevr_slider_atts['id']));
	}
	
	$slider = get_post_meta( $_slider, 'fevr_meta', true );
	
	$infinite_slider = isset($slider['slider-infinite']) && $slider['slider-infinite'] == 'enabled' ? 'true' : 'false';
	$nav_slider = isset($slider['slider-nav']) && $slider['slider-nav'] == 'enabled' ? 'true' : 'false';
	$dots_slider = isset($slider['slider-dots']) && $slider['slider-dots'] == 'enabled' ? 'true' : 'false';
	$autoplay_slider = isset($slider['slider-autoplay']) && $slider['slider-autoplay'] == 'enabled' ? 'true' : 'false';
	$autoplay_timeout_slider = isset($slider['slider-autoplay-timeout']) && !empty($slider['slider-autoplay-timeout']) ? $slider['slider-autoplay-timeout'] : 5000;
	$autoplay_pause_slider = isset($slider['slider-autoplay-pause']) && $slider['slider-autoplay-pause'] == 'enabled' ? 'true' : 'false';
	$height_slider = isset($slider['slider-height']) && $slider['slider-height'] == 'custom' && isset($slider['slider-height-custom']) && !empty($slider['slider-height-custom']) ? $slider['slider-height-custom'] : '';
	$transition_type_slider = isset($slider['slider-transition-type']) && !empty($slider['slider-transition-type']) && $slider['slider-transition-type'] != 'slide' ? $slider['slider-transition-type'] : '';
	$full_height_slider = isset($slider['slider-height']) && $slider['slider-height'] == 'full_height' ? 'true' : '';
	$parallax_slider = isset($slider['slider-parallax']) && $slider['slider-parallax'] == 'parallax-enabled' ? 'true' : '';
?>
<div class="luv-slider-wrapper">
	<ul class="luv-slider luv-page-slider" data-luv-slider-infinite="<?php echo esc_attr($infinite_slider); ?>" data-luv-slider-nav="<?php echo esc_attr($nav_slider); ?>" data-luv-slider-dots="<?php echo esc_attr($dots_slider); ?>" data-luv-slider-autoplay="<?php echo esc_attr($autoplay_slider); ?>" data-luv-slider-autoplay-timeout="<?php echo esc_attr($autoplay_timeout_slider); ?>" data-luv-slider-autoplay-pause="<?php echo esc_attr($autoplay_pause_slider); ?>" <?php echo !empty($transition_type_slider) ? 'data-luv-slider-transition-type="'.esc_attr($transition_type_slider).'"' : ''; ?> <?php echo ($full_height_slider == 'true' ? 'data-luv-slider-full-height="true"' : 'data-luv-slider-full-height="false"'); ?> <?php echo ($parallax_slider == 'true' ? 'data-luv-slider-parallax="true"' : ''); ?>>
		<?php foreach((array)$slider['slider'] as $slide): ?>
			<?php
				$parallax_layers_slider = isset($slide['slide-parallax-layer-list']) ? $slide['slide-parallax-layer-list'] : '';
				
				// Heading Typography
				
				$slide_heading_font_family = isset($slide['slide-heading-font-family']) ? $slide['slide-heading-font-family'] : '';
				$slide_heading_font_size = isset($slide['slide-heading-font-size']) ? $slide['slide-heading-font-size'] : '';
				$slide_heading_resposive_font_size = isset($slide['slide-heading-responsive-font-size']) ? 'true' : 'false';
				$slide_heading_line_height = isset($slide['slide-heading-line-height']) ? $slide['slide-heading-line-height'] : '';
				$slide_heading_font_weight = isset($slide['slide-heading-font-weight']) ? $slide['slide-heading-font-weight'] : '';
				$slide_heading_text_transform = isset($slide['slide-heading-text-transform']) ? $slide['slide-heading-text-transform'] : '';

				$heading_typography_stlye = apply_filters( FEVR_VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, '', 'slide_heading', array(
						'font_family' => $slide_heading_font_family,
						'font_size' => $slide_heading_font_size,
						'responsive_font_size' => $slide_heading_resposive_font_size,
						'line_height' => $slide_heading_line_height,
						'font_weight' => $slide_heading_font_weight,
						'text_transform' => $slide_heading_text_transform
				));
				
				// Caption typography
				$slide_caption_font_family = isset($slide['slide-caption-font-family']) ? $slide['slide-caption-font-family'] : '';
				$slide_caption_font_size = isset($slide['slide-caption-font-size']) ? $slide['slide-caption-font-size'] : '';
				$slide_caption_resposive_font_size = isset($slide['slide-caption-responsive-font-size']) ? 'true' : 'false';
				$slide_caption_line_height = isset($slide['slide-caption-line-height']) ? $slide['slide-caption-line-height'] : '';
				$slide_caption_font_weight = isset($slide['slide-caption-font-weight']) ? $slide['slide-caption-font-weight'] : '';
				$slide_caption_text_transform = isset($slide['slide-caption-text-transform']) ? $slide['slide-caption-text-transform'] : '';
				
				$caption_typography_stlye = apply_filters( FEVR_VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, '', 'slide_caption', array(
						'font_family' => $slide_caption_font_family,
						'font_size' => $slide_caption_font_size,
						'responsive_font_size' => $slide_caption_resposive_font_size,
						'line_height' => $slide_caption_line_height,
						'font_weight' => $slide_caption_font_weight,
						'text_transform' => $slide_caption_text_transform
				));
				
				// Content typography
				$slide_content_font_family = isset($slide['slide-content-font-family']) ? $slide['slide-content-font-family'] : '';
				$slide_content_font_size = isset($slide['slide-content-font-size']) ? $slide['slide-content-font-size'] : '';
				$slide_content_resposive_font_size = isset($slide['slide-content-responsive-font-size']) ? 'true' : 'false';
				$slide_content_font_weight = isset($slide['slide-content-font-weight']) ? $slide['slide-content-font-weight'] : '';
				$slide_content_text_transform = isset($slide['slide-content-text-transform']) ? $slide['slide-content-text-transform'] : '';
				
				$content_typography_stlye = apply_filters( FEVR_VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, '', 'slide_content', array(
						'font_family' => $slide_content_font_family,
						'font_size' => $slide_content_font_size,
						'responsive_font_size' => $slide_content_resposive_font_size,
						'font_weight' => $slide_content_font_weight,
						'text_transform' => $slide_content_text_transform
				));
				
				// Classes for slider item
				$slide_item_classes = array();
				$slide_item_styles = array();
				$slide_item_child_styles = array();
				$slide_overlay_styles = array();
				
				// Filter
				if(isset($slide['slide-filter']) && !empty($slide['slide-filter'])) {
					$slide_item_classes[] = $slide['slide-filter'];
				}
				
				// Alignment
				if(isset($slide['slide-content-alignment']) && !empty($slide['slide-content-alignment'])) {
					$slide_item_classes[] = $slide['slide-content-alignment'];
				}
				
				// Height
				if(!empty($height_slider)) {
					$slide_item_styles[] = 'height: '.$height_slider.'px';
					$slide_item_child_styles[' .container'] = 'height: '.$height_slider.'px';
				}

				// Color
				if(!empty($slide['slide-text-color'])) {
					$slide_item_styles[] = 'color: '.$slide['slide-text-color'];
					$slide_item_child_styles[' h3'] = 'color: '.$slide['slide-text-color'];
					$slide_item_child_styles[' a'] = 'color: '.$slide['slide-text-color'];
				}
				
				// Overlay
				if(isset($slide['slide-overlay-color']) && !empty($slide['slide-overlay-color'])) {
					$slide_overlay_styles[] = 'background-color: '.$slide['slide-overlay-color'].';';
					if(isset($slide['slide-overlay-color-opacity']) && !empty($slide['slide-overlay-color-opacity'])) {
						$slide_overlay_styles[] = ' opacity: '.$slide['slide-overlay-color-opacity'].';';
					}
				}
				
				// Background image
				if(!empty($slide['slide-image'])) {
					$slide_item_styles[] = 'background-image: url('.$slide['slide-image'].')';	
				}
				
				$slide_item_classes[] = fevr_enqueue_inline_css(array('style' => implode(';', $slide_item_styles), 'child' => $slide_item_child_styles));
				
				// Data attributes for items
				$slide_data_attr = array();
				
				// Slide Skin
				if(isset($slide['slide-skin']) && !empty($slide['slide-skin']) && $slide['slide-skin'] != 'default') {
					$slide_data_attr[] = 'data-header-skin="'.esc_attr(trim($slide['slide-skin'],'"')).'"';
				} else {
					$slide_data_attr[] = 'data-header-skin="'. esc_attr(fevr_get_luvoption('header-skin', 'dark')).'"';
				}
				
				// Classes for luv-slider-animation
				$slide_item_animation_classes = array();
				
				// Display Effect
				if(isset($slide['slide-effect']) && !empty($slide['slide-effect']) && $slide['slide-effect'] != 'none') {
					$slide_item_animation_classes[] = $slide['slide-effect'];
				} else {
					$slide_item_animation_classes[] = 'fade-in';
				}
				
				$slide_item_animation_classes[] = 'animated';
				
			?>
			<li class="<?php echo esc_attr(implode(' ', $slide_item_classes)); ?>" <?php echo fevr_kses(implode(' ', $slide_data_attr)); ?>>
				<?php if(!empty($slide_overlay_styles)): ?>
				<div class="luv-slider-overlay <?php echo fevr_enqueue_inline_css(array('style' => implode(';', $slide_overlay_styles))); ?>"></div>
				<?php endif; ?>
				
				<?php
					// Parallax scene (layers)
					if(!empty($parallax_layers_slider)):
					// Enqueue parallax jquery plugin 
					wp_enqueue_script( 'fevr-parallax', trailingslashit(get_template_directory_uri()) . '/js/min/jquery.parallax-min.js', array('jquery'), FEVR_THEME_VER, true );
				?>
					<ul class="parallax-scene">
						<?php
							$i = 0;
							foreach((array)$parallax_layers_slider as $layer) {
								$i++;
								echo '<li class="layer" data-depth="'.($i > 10 ? 1 : $i/10).'"><div class="parallax-layer-img '.fevr_enqueue_inline_css(array('style' => 'background-image:url('.$layer.');')).'"></li>';
							}
						?>
					</ul>
				<?php endif; ?>
				
				<?php
					// Video background
					if(isset($slide['slide-type']) && $slide['slide-type'] == 'video' && (!empty($slide['slide-video-mp4']) || !empty($slide['slide-video-ogv']))):
				?>
						<video preload="auto" autoplay loop muted webkit-playsinline>
							<?php if(!empty($slide['slide-video-mp4'])): ?>
							<source src="<?php echo esc_url($slide['slide-video-mp4']); ?>" type="video/mp4">
							<?php endif; ?>
							
							<?php if(!empty($slide['slide-video-ogv'])): ?>
							<source src="<?php echo esc_url($slide['slide-video-ogv']); ?>" type="video/ogg">
							<?php endif; ?>
							
							<?php esc_html_e('Your browser does not support the video tag.', 'fevr'); ?>
						</video>
				<?php
					// Embedded background
					elseif(isset($slide['slide-type']) && $slide['slide-type'] == 'video' && isset($slide['slide-video-embedded']) && !empty($slide['slide-video-embedded'])):
						if(!filter_var($slide['slide-video-embedded'], FILTER_VALIDATE_URL) === false):
				?>
					<iframe width="560" height="315" src="<?php echo esc_url(fevr_url_to_embedded($slide['slide-video-embedded'])); ?>" frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen mute></iframe>
				<?php
						else:
							echo fevr_kses($slide['slide-video-embedded']);
						endif;
				?>
				<?php endif; ?>
					
				<?php if( (isset($slide['slide-heading']) && !empty($slide['slide-heading'])) || (isset($slide['slide-caption']) && !empty($slide['slide-caption'])) || (isset($slide['slide-content']) && !empty($slide['slide-content'])) ): ?>
				<div class="container">
					<div class="luv-slider-inner">
						<div class="luv-slider-animation <?php echo esc_attr(implode(' ', $slide_item_animation_classes)); ?>">
							<?php if(isset($slide['slide-heading']) && !empty($slide['slide-heading'])): ?>
							<h3 class="luv-slider-title<?php fevr_echo($heading_typography_stlye)?>"><?php echo fevr_kses($slide['slide-heading']); ?></h3>
							<?php endif; ?>
							
							<?php if(isset($slide['slide-caption']) && !empty($slide['slide-caption'])): ?>
							<div class="luv-slider-caption<?php fevr_echo($caption_typography_stlye)?>"><?php echo fevr_kses($slide['slide-caption']); ?></div>
							<?php endif; ?>
							
							<?php if(isset($slide['slide-content']) && !empty($slide['slide-content'])): ?>
							<div class="luv-slider-content<?php fevr_echo($content_typography_stlye)?>"><?php echo fevr_kses(do_shortcode($slide['slide-content'])); ?></div>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<?php endif; ?>
				
				<?php if(isset($slide['slide-link']) && !empty($slide['slide-link'])): ?>
				<a href="<?php echo esc_url($slide['slide-link']); ?>" class="slide-link"></a>
				<?php endif; ?>
				
				<?php if(isset($slide['slide-mouse-icon']) && $slide['slide-mouse-icon'] == 'enabled' && isset($slide['slide-mouse-icon-type']) && !empty($slide['slide-mouse-icon-type'])): ?>
					<?php if($slide['slide-mouse-icon-type'] == 'arrow'): ?>
						<div class="header-scroll header-scroll-arrow">
							<i class="ion-ios-arrow-down"></i>
						</div>
					<?php elseif($slide['slide-mouse-icon-type'] == 'arrow2'): ?>
						<div class="header-scroll header-scroll-arrow header-scroll-arrow-style2">
							<i class="ion-ios-arrow-down"></i>
						</div>
					<?php else: ?>
						<div class="header-scroll">
							<span class="header-scroll-mouse">
								<span class="fa fa-angle-down"></span>
							</span>
							<span class="header-scroll-caption"><?php esc_html_e('Scroll', 'fevr'); ?></span>
						</div>
					<?php endif; ?>
				<?php endif; ?>
			</li>
			
		<?php endforeach; ?>
	</ul>
</div>