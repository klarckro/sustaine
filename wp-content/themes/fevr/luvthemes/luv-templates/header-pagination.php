<?php
	switch(get_post_type()) {
		case 'luv_portfolio':
			
			$fevr_pagination_position = fevr_get_luvoption('portfolio-pagination-position');
			$fevr_pagination_hide = fevr_get_luvoption('hide-portfolio-pagination', 0);
			$grid_url = get_post_type_archive_link('luv_portfolio');
			
			break;
		
		case 'luv_collections':

			$fevr_pagination_position = fevr_get_luvoption('woocommerce-collections-pagination-position');
			$fevr_pagination_hide = fevr_get_luvoption('hide-woocommerce-collections-pagination', 0);
			$grid_url = get_post_type_archive_link('luv_collections');
			
			break;
			
		default:
		
			$fevr_pagination_position = fevr_get_luvoption('blog-pagination-position');
			$fevr_pagination_hide = fevr_get_luvoption('hide-blog-pagination',0);
			$fevr_blog_page = fevr_get_luvoption('blog-page',0);
			$page_for_posts = get_option('page_for_posts');
			
			if($page_for_posts != 0) {
				$grid_url = get_permalink($page_for_posts);
			} elseif($fevr_blog_page  != 0) {
				$grid_url = get_permalink($fevr_blog_page);
			} else {
				$grid_url = site_url();
			}
	}
?>


<?php if($fevr_pagination_hide == 0 && $fevr_pagination_position == 'header' && is_single() && (!function_exists('is_woocommerce') || !is_woocommerce()) && (!function_exists('is_bbpress') || !is_bbpress())): ?>
<div id="page-header-nav">
	<?php previous_post_link('%link', '<i class="ion-ios-arrow-left"></i>'); ?>
	<a href="<?php echo esc_url($grid_url); ?>"><i class="ion-android-apps"></i></a>
	<?php next_post_link('%link', '<i class="ion-ios-arrow-right"></i>'); ?>
</div>
<?php endif; ?>