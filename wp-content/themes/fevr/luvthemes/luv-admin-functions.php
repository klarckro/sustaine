<?php 
//======================================================================
// Load TGM Activation
//======================================================================
require_once(trailingslashit(get_template_directory()) . 'luvthemes/plugins/tgm/tgm.php');

//======================================================================
// Installer
//======================================================================

if (!defined('FEVR_WHITELABEL')){
	require_once trailingslashit(get_template_directory()) . 'luvthemes/includes/installer/installer.php';
	$fevr_installer = new Fevr_Installer('fevr_installer');
	
	// Create submenu for installer
	add_action ( 'admin_menu', 'fevr_installer_menu', 12 );
}

function fevr_installer_menu(){
	global $fevr_installer;
	fevr_add_fevr_submenu_page('fevr-dashboard', esc_html__ ( 'Demo Content', 'fevr' ), esc_html__ ( 'Demo Content', 'fevr' ), 'manage_options', 'fevr_installer', array($fevr_installer, 'init'));
}


//======================================================================
// Setup wizard
//======================================================================

if (!defined('FEVR_WHITELABEL')){
	require_once trailingslashit(get_template_directory()) . 'luvthemes/includes/setup/setup.php';
	$fevr_setup = new Fevr_Setup();
	
	// Create submenu for installer
	add_action ( 'admin_menu', 'fevr_setup_menu', 12 );
}

function fevr_setup_menu(){
	global $fevr_setup;
	fevr_add_fevr_submenu_page('fevr-dashboard', esc_html__ ( 'Setup Wizard', 'fevr' ), esc_html__ ( 'Setup Wizard', 'fevr' ), 'manage_options', 'fevr_setup', array($fevr_setup, 'pseudo'));
}

/**
 * Localize site_url and upload dir in post content
 * @param string $template
 * @return string
 */
function fevr_localize_template($template){
	$uploaddir = wp_upload_dir();

	$template = str_replace('{UPLOAD_DIR_URL}', $uploaddir['url'], $template);
	$template = str_replace('{SITE_URL}', site_url(), $template);
	$template = str_replace('{SITE_HOST}', preg_replace('~https?://~', '',site_url()), $template);
	$template = str_replace('{ADMIN_EMAIL}', get_option('admin_email'), $template);
	return $template;
}


//======================================================================
// Image Optimizer
//======================================================================

require_once trailingslashit(get_template_directory()) . 'luvthemes/includes/image-optimizer/image-optimizer.php';
new Fevr_Image_Optimizer();

//======================================================================
// LuvStock
//======================================================================

add_action('admin_menu', 'fevr_add_luvstock_button');

/**
 * Add LuvStock menu
 */
function fevr_add_luvstock_button(){
	$fevr_purchase_key = fevr_purchase_key();
	if (!empty($fevr_purchase_key) && fevr_check_luvoption('module-luvstock', 1)){
		fevr_add_fevr_submenu_page('upload.php', esc_html__ ( 'LuvStock', 'fevr' ), esc_html__ ( 'LuvStock', 'fevr' ), 'edit_pages', 'fevr_luvstock', 'fevr_media_luvstock_tab');
	}
}

add_filter('media_upload_tabs', 'fevr_media_tabs_handler');
/**
 * Add LuvStock Tab
 * @param array $tabs
 * @return array
 */
function fevr_media_tabs_handler($tabs) {
	$fevr_purchase_key = fevr_purchase_key();
	if (!empty($fevr_purchase_key) && fevr_check_luvoption('module-luvstock', 1)){
		$tabs['luvstock'] = 'LuvStock';
		return $tabs;
	}
}

// Create LuvStock hook for media uploader
add_action('wp_ajax_fevr_luvstock', 'fevr_media_luvstock_tab');


/**
 * Display LuvStock tab
 */
function fevr_media_luvstock_tab() {
	global $pagenow;
	if ($pagenow == 'upload.php'){
		echo '<h1>' . esc_html__ ( 'LuvStock', 'fevr' ) . '</h1>';
	}
	echo '<div class="luvstock-container"><input type="text" class="luvstock-search" placeholder="'.esc_html__('Search', 'fevr').'"><div class="luvstock-content"><div class="luvstock-loader"><i class="fa fa-spinner fa-pulse"></i></div></div><div class="luvstock-toolbar"><button class="luvstock-download luv-btn luv-btn-blue" disabled>' . esc_html__('Download to Media Libary', 'fevr') . '</button></div>';
	echo '<p>';
	esc_html_e('LuvStock images are licensed under Creative Commons Zero (CC0) license. This means the pictures are completely free to be used for any legal purpose. You can copy, modify, distribute and use the photos for free, including commercial purposes, without asking permission.', 'fevr');
	echo '</p>';
}

// Create LuvStock hook for media uploader
add_action('media_upload_luvstock', 'fevr_media_luvstock_handler');

/**
 * Render LuvStock Tab
 */
function fevr_media_luvstock_handler() {
	wp_iframe('fevr_media_luvstock_tab');
}

// Create ajax hook for get LuvStock items
add_action( 'wp_ajax_fevr_get_luvstock_items', 'fevr_get_luvstock_items');

/**
 * Ajax action to list LuvStock items
 * Connects to Luvthemes API, get the excepted items and print them
 */
function fevr_get_luvstock_items(){
	// Check if our nonce is set.
	if ( ! isset( $_REQUEST['wp_nonce'] ) || ! wp_verify_nonce( $_REQUEST['wp_nonce'], 'fevr' ) || !current_user_can('edit_posts')) {
		return;
	}

	$start = isset($_POST['start']) ? $_POST['start'] : 0;
	$keyword = isset($_POST['keyword']) ? $_POST['keyword'] : '';
	$items_per_page = 50;

	$response = wp_remote_post (
			FEVR_API_URL . 'get_luvstock_items',
			array(
					'timeout' => 10,
					'sslverify' => false,
					'user-agent' => 'luv',
					'headers' => array (
							'X-ENVATO-PURCHASE-KEY' => fevr_purchase_key()
					),
					'body' => array (
							'site' => trailingslashit(home_url()),
							'args' => array('keyword' => $keyword, 'start' => $start)
					)
			)
			);
	if (is_wp_error($response)){
		echo esc_html__('Couldn\'t connect to API server: ', 'fevr') . $response->get_error_message();
	}
	else{
		$response = json_decode($response['body'], true);
		if (!empty($response['error'])){
			echo fevr_kses($response['error']);
		}
		else{
			$pagination = '' ;
			$total		= (int)$response['response']['total'];
			$page_count = ($total % $items_per_page) > 0 ? (int)($total/$items_per_page) + 1 : (int)($total/$items_per_page);
			$current	= (int)$start/$items_per_page;
			$show_pages = ($page_count > 1 ? true : false);
				
			$first	= '<li><a href="#" data-page="'. 0 . '" class="luvstock-prev">'.esc_html__('First', 'fevr').'</a></li>';
			$last	= '<li><a href="#" data-page="'. (($page_count -1) * $items_per_page).'" class="luvstock-next">'.esc_html__('Last', 'fevr').'</a></li>';

				
			// Prev
			if ($start >= $items_per_page){
				$pagination .= $first;
				$pagination .= '<li><a href="#" data-page="'. ($start - $items_per_page > 0 ? $start - $items_per_page : 0).'" class="luvstock-prev">'.esc_html__('Previous', 'fevr').'</a></li>';
			}
				
			// Page numbers
			if ($show_pages){
				$min_page = ($current - 5 > 0 ? $current - 5 : 0);
				$min_page = ($current + 5 > $page_count ? $page_count - 10 : $min_page);
				$min_page = ($min_page < 0 ? 0 : $min_page);
				$max_page = ($min_page + 10 <= $page_count ? $min_page + 10 : $page_count);

				for ($i=$min_page; $i < $max_page; $i++){
					$pagination .= '<li'.($i == $current ? ' class="current"' : '').'><a href="#" data-page="'. ($i * $items_per_page).'" class="luvstock-next">'. ($i + 1) .'</a></li>';
				}
			}
				
			// Next
			if ($start + $items_per_page < (int)$response['response']['total']){
				$pagination .= '<li><a href="#" data-page="'. ($start + $items_per_page).'" class="luvstock-next">'.esc_html__('Next', 'fevr').'</a></li>';
				$pagination .= $last;
			}
				
			echo '<h3>'.sprintf(esc_html__('%s items', 'fevr'), (int)$response['response']['total']).'</h3>';
			echo '<ul class="luvstock-pagination">';
			echo fevr_kses($pagination);
			echo '</ul>';
			echo '<ul class="luvstock-list">';
			for($i = 0; $i < $items_per_page; $i++){
				if (isset($response['response']['items'][$i]['image'])){
					echo '<li class="attachment" data-item="'.esc_attr($response['response']['items'][$i]['image']).'"><div class="luvstock-item" style="background-image:url('. esc_url(FEVR_API_URL . $response['response']['items'][$i]['thumb']).')"></div> </li>';
				}
			}
			echo '</ul>';
			echo '<ul class="luvstock-pagination">';
			echo fevr_kses($pagination);
			echo '</ul>';
				
		}
	}

	wp_die();
}

// Create download LuvStock item ajax hook
add_action( 'wp_ajax_fevr_download_luvstock_items', 'fevr_download_luvstock_items');

/**
 * Download selected LuvStock items
 * This function uses $_POST['items'] array, which is a list of selected images' URL
 */
function fevr_download_luvstock_items(){
	WP_Filesystem();
	global $wp_filesystem;

	// Check if our nonce is set.
	if ( ! isset( $_REQUEST['wp_nonce'] ) || ! wp_verify_nonce( $_REQUEST['wp_nonce'], 'fevr' ) || !current_user_can('edit_posts') ) {
		return;
	}
	$uploaddir = wp_upload_dir();

	if ( !function_exists('media_handle_upload') ) {
		require_once(ABSPATH . "wp-admin" . '/includes/image.php');
		require_once(ABSPATH . "wp-admin" . '/includes/file.php');
		require_once(ABSPATH . "wp-admin" . '/includes/media.php');
	}

	foreach((array)$_POST['items'] as $item) {
		$url = esc_url(FEVR_API_URL . '/' . $item);
			
		$tmp = wp_tempnam($url);
		if ( ! $tmp ){
			return new WP_Error('http_no_file', esc_html__('Could not create Temporary file.', 'fevr'));
		}

		$response = wp_remote_post (
				$url,
				array(
						'timeout' => 60,
						'sslverify' => false,
						'user-agent' => 'luv',
						'headers' => array (
								'X-ENVATO-PURCHASE-KEY' => fevr_purchase_key()
						),
						'body' => array (
								'site' => trailingslashit(home_url()),
						)
				)
		);
		
		if ( is_wp_error( $response ) ) {
			unlink( $tmp );
			return $response;
		}
		else{
			$wp_filesystem->put_contents($tmp, $response['body']);
			
			$file_array = array();

			preg_match('/[^\?]+\.(jpg|jpe|jpeg|gif|png)/i', $url, $matches);
			$file_array['name'] = basename($matches[0]);
			$file_array['tmp_name'] = $tmp;

			media_handle_sideload( $file_array, 0);
		}
	}
	wp_die();
}

//======================================================================
// Luvthemes Page Templates
//======================================================================

// Add page action links
if (!defined('FEVR_WHITELABEL')){
	add_filter('page_row_actions', 'fevr_page_related_layout_action_links', 10, 2);
}

/**
 * Add action links to pages list
 * @param array $actions
 * @param WP_Post $post
 */
function fevr_page_related_layout_action_links($actions, $post) {
	$group = get_post_meta($post->ID, 'luv_template_group', true);
	if (!empty($group)){
		$actions['edit_badges'] = '<span class="luv-similar-templates"><a href="'.esc_url(add_query_arg(array('post_type' => 'page', 'page' => 'fevr_use_template', 'group' => $group),admin_url('edit.php'))).'">'.esc_html__('Related Layout Templates', 'fevr').'</a></span>';
	}
	return $actions;
}

if (!defined('FEVR_WHITELABEL')){
	add_action('edit_form_after_title', 'fevr_related_layout_button');
}

/**
 * Add related layout button
 */
function fevr_related_layout_button(){
	if (!defined('FEVR_WHITELABEL')){
		global $post;
		$group = get_post_meta($post->ID, 'luv_template_group', true);
		if (!empty($group)){
			echo ' <a href="'.esc_url(add_query_arg(array('post_type' => 'page', 'page' => 'fevr_use_template', 'group' => $group),admin_url('edit.php'))).'" class="luv-btn luv-btn-blue">'.esc_html__('Related Layout Templates', 'fevr').'</a>';
		}
	}
}

if (!defined('FEVR_WHITELABEL')){
	add_action('admin_menu', 'fevr_add_use_template_button');
}

/**
 * Add Use Template button
 */
function fevr_add_use_template_button(){
	$fevr_purchase_key = fevr_purchase_key();
	if (!empty($fevr_purchase_key)){
		fevr_add_fevr_submenu_page('edit.php?post_type=page', esc_html__ ( 'Layout Template', 'fevr' ), esc_html__ ( 'Layout Template', 'fevr' ), 'edit_pages', 'fevr_use_template', 'fevr_list_luvthemes_templates');
	}
}


/**
 * Display Luvthemes Templates
 */
function fevr_list_luvthemes_templates() {
	include trailingslashit(get_template_directory()).'luvthemes/includes/use-template/use-template.tpl.php';
}

// Create ajax hook for get templates
add_action( 'wp_ajax_fevr_get_luvthemes_templates', 'fevr_get_luvthemes_templates');

/**
 * Ajax action to list Luvthemes Templates
 * Connects to Luvthemes API, get the excepted items and print them
 */
function fevr_get_luvthemes_templates(){
	// Check if our nonce is set.
	if ( ! isset( $_REQUEST['wp_nonce'] ) || ! wp_verify_nonce( $_REQUEST['wp_nonce'], 'fevr' ) || !current_user_can('edit_posts')) {
		return;
	}

	$start = isset($_POST['start']) ? $_POST['start'] : 0;
	$keyword = isset($_POST['keyword']) ? $_POST['keyword'] : '';
	$group = isset($_POST['group']) ? $_POST['group'] : '';
	$items_per_page = 50;

	$response = wp_remote_post (
			FEVR_API_URL . 'get_templates',
			array(
					'timeout' => 10,
					'sslverify' => false,
					'user-agent' => 'luv',
					'headers' => array (
							'X-ENVATO-PURCHASE-KEY' => fevr_purchase_key()
					),
					'body' => array (
							'site' => trailingslashit(home_url()),
							'args' => array('keyword' => $keyword, 'start' => $start, 'group' => $group)
					)
			)
			);
	if (is_wp_error($response)){
		echo esc_html__('Couldn\'t connect to API server: ', 'fevr') . $response->get_error_message();
	}
	else{
		$response = json_decode($response['body'], true);
		if (!empty($response['error'])){
			echo fevr_kses($response['error']);
		}
		else{
			$pagination = '' ;
			$total		= (int)$response['response']['total'];
			$page_count = ($total % $items_per_page) > 0 ? (int)($total/$items_per_page) + 1 : (int)($total/$items_per_page);
			$current	= (int)$start/$items_per_page;
			$show_pages = ($page_count > 1 ? true : false);

			$first	= '<li><a href="#" data-page="'. 0 . '" class="luvstock-prev">'.esc_html__('First', 'fevr').'</a></li>';
			$last	= '<li><a href="#" data-page="'. (($page_count -1) * $items_per_page).'" class="luvstock-next">'.esc_html__('Last', 'fevr').'</a></li>';

			// Prev
			if ($start >= $items_per_page){
				$pagination .= $first;
				$pagination .= '<li><a href="#" data-page="'. ($start - $items_per_page > 0 ? $start - $items_per_page : 0).'" class="luvstock-prev">'.esc_html__('Previous', 'fevr').'</a></li>';
			}

			// Page numbers
			if ($show_pages){
				$min_page = ($current - 5 > 0 ? $current - 5 : 0);
				$min_page = ($current + 5 > $page_count ? $page_count - 10 : $min_page);
				$min_page = ($min_page < 0 ? 0 : $min_page);
				$max_page = ($min_page + 10 <= $page_count ? $min_page + 10 : $page_count);

				for ($i=$min_page; $i < $max_page; $i++){
					$pagination .= '<li'.($i == $current ? ' class="current"' : '').'><a href="#" data-page="'. ($i * $items_per_page).'" class="luvstock-next">'. ($i + 1) .'</a></li>';
				}
			}

			// Next
			if ($start + $items_per_page < (int)$response['response']['total']){
				$pagination .= '<li><a href="#" data-page="'. ($start + $items_per_page).'" class="luvstock-next">'.esc_html__('Next', 'fevr').'</a></li>';
				$pagination .= $last;
			}

			// Masonry filter
			$masonry_filters = array();
			foreach($response['response']['items'] as $item){
				$masonry_filters[$item['category']] = '<li><a href="#" data-filter=".'.esc_attr($item['category']).'">'.esc_html($item['category']).'</a></li>';
			}
			
			echo '<ul class="luvthemes-templates-pagination">';
			echo fevr_kses($pagination);
			echo '</ul>';
			echo '<ul class="luvthemes-templates-filter"><li><a href="#" data-filter="*">All</a></li>'.implode($masonry_filters).'</ul>';
			echo '<ul class="luvthemes-templates-list">';
			for($i = 0; $i < $items_per_page; $i++){
				if (isset($response['response']['items'][$i]['image'])){
					echo '<li class="attachment '.esc_attr($response['response']['items'][$i]['category']).'" data-item="'.esc_attr($response['response']['items'][$i]['id']).'"><span class="luvthemes-template-name">'.esc_html($response['response']['items'][$i]['name']).'</span><div class="luvthemes-template-image-wrapper"><img src="'.esc_url(FEVR_API_URL . $response['response']['items'][$i]['image']).'"></div><div class="luvthemes-template-buttons-wrapper"><a class="luv-btn luv-btn-blue" href="'.esc_url($response['response']['items'][$i]['preview']).'" target="_blank">'.esc_html__('Preview', 'fevr').'</a><a class="luv-btn luv-btn-blue" href="'.esc_url(add_query_arg(array('post_type' => 'page', 'page' => 'fevr_use_template', 'group' => $response['response']['items'][$i]['template_group']),admin_url('edit.php'))).'">'.esc_html__('Similar Pages', 'fevr').'</a><button class="use-luvthemes-template luv-btn luv-btn-green">'.esc_html__('Add Page', 'fevr').'</button></div></li>';
				}
			}
			echo '</ul>';
			echo '<ul class="luvthemes-templates-pagination">';
			echo fevr_kses($pagination);
			echo '</ul>';

		}
	}

	wp_die();
}

// Create use template ajax hook
add_action( 'wp_ajax_fevr_use_luvthemes_template', 'fevr_use_luvthemes_template');

/**
 * Download selected Luvthemes Template
 * This function uses $_POST['items'] array, which is a list of selected images' URL
 */
function fevr_use_luvthemes_template(){
	WP_Filesystem();
	global $wp_filesystem;

	// Check if our nonce is set.
	if ( ! isset( $_REQUEST['wp_nonce'] ) || ! wp_verify_nonce( $_REQUEST['wp_nonce'], 'fevr' ) || !current_user_can('edit_posts') ) {
		return;
	}

	$response = wp_remote_post (
			FEVR_API_URL . 'get_template',
			array(
					'timeout' => 10,
					'sslverify' => false,
					'user-agent' => 'luv',
					'headers' => array (
							'X-ENVATO-PURCHASE-KEY' => fevr_purchase_key()
					),
					'body' => array (
							'site' => trailingslashit(home_url()),
							'args' => array('id' => (int)$_POST['id'])
					)
			)
			);
	if (is_wp_error($response)){
		echo esc_html__('Couldn\'t connect to API server: ', 'fevr') . $response->get_error_message();
	}
	else{
		$response = json_decode($response['body'], true);
		if (!empty($response['error'])){
			echo fevr_kses($response['error']);
		}
		else{
			$post = json_decode(fevr_localize_template($response['response']), true);
			
			
			// Install attached posts (eg contact form)
			$localized_post_ids = array();
			if (isset($post['attached_posts'])){
				foreach ((array)$post['attached_posts'] as $_attached_post_key => $_attached_post_value){
					// Localize post data
					$_attached_post_value['data']['ID'] = 0;
					$_attached_post_value['data']['post_author'] = get_current_user_id();
					$_attached_post_value['data']['post_status'] = 'draft';
					unset($_attached_post_value['data']['post_date']);
					unset($_attached_post_value['data']['post_date_gmt']);
						
					// Insert post
					$_attached_post_id = $localized_post_ids[$_attached_post_key] = wp_insert_post($_attached_post_value['data']);
					
					// Insert post meta
					foreach ($_attached_post_value['meta'] as $meta_key => $meta_value){
						update_post_meta($_attached_post_id, $meta_key, $meta_value);
					}
				}
			}
			
			// Localize post data
			$post['data']['ID'] = 0;
			$post['data']['post_author'] = get_current_user_id();
			$post['data']['post_status'] = 'draft';
			unset($post['data']['post_date']);
			unset($post['data']['post_date_gmt']);
			
			// Localized Post IDs
			foreach ($localized_post_ids as $_lid_key => $_lid_value){
				$post['data']['post_content'] = str_replace($_lid_key, $_lid_value, $post['data']['post_content']); 
			}
			
			// Empty ids attribute from shortcodes
			$post['data']['post_content'] = preg_replace('~ids="([\d,]*)"~', 'ids=""', $post['data']['post_content']);
				
			// Empty image attribute from shortcodes
			$post['data']['post_content'] = preg_replace('~image="\d*"~', 'image=""', $post['data']['post_content']);
			
			// Insert post
			$post_id = wp_insert_post($post['data']);
				
			// Insert post meta
			foreach ($post['meta'] as $meta_key => $meta_value){
				update_post_meta($post_id, $meta_key, $meta_value);
			}
				
				
			// Insert media
			$uploaddir = wp_upload_dir();
				
			foreach ($post['media'] as $media){
				$url = esc_url(FEVR_API_URL . 'templates/images/' . $media);
				$filename = $uploaddir['path'] . '/' . basename($url);

				$response = wp_remote_post (
						$url,
						array(
								'timeout' => 60,
								'sslverify' => false,
								'user-agent' => 'luv',
								'headers' => array (
										'X-ENVATO-PURCHASE-KEY' => fevr_purchase_key()
								),
								'body' => array (
										'site' => trailingslashit(home_url()),
								)
						)
						);
					
				if ( is_wp_error( $response ) ) {
					@unlink( $filename );
					return $response;
				}
				else{
					$wp_filesystem->put_contents($filename, $response['body']);
					$filetype = wp_check_filetype( basename( $filename ), null );
					$attachment = array(
							'guid'           => esc_url($uploaddir['url'] . '/' . basename( $filename )),
							'post_mime_type' => $filetype['type'],
							'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
							'post_content'   => '',
							'post_status'    => 'inherit'
					);
						
					// Insert the attachment.
					$attach_id = wp_insert_attachment( $attachment, $filename, 0);
						
					// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
					require_once( ABSPATH . 'wp-admin/includes/image.php' );
						
					// Generate the metadata for the attachment, and update the database record.
					$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
					wp_update_attachment_metadata( $attach_id, $attach_data );
				}
			}
				
			// Send JSON
			wp_send_json(array('post_id' => $post_id, 'edit_url' => esc_url(admin_url('post.php?post=' . $post_id . '&action=edit'))));
		}
	}
	wp_die();
}

//======================================================================
// Dashboard Widgets
//======================================================================

add_action( 'wp_dashboard_setup', 'fevr_init_dashboard_widgets' );
/**
 * Initialize WordPress Dashboard Widgets
 */
function fevr_init_dashboard_widgets() {
	if (fevr_check_luvoption('ga-dashboard-widget', 1) && fevr_check_luvoption('ga-cid', '', '!=') && fevr_check_luvoption('ga-vid', '', '!=')){
		wp_add_dashboard_widget('fevr_google_analytics_widget', esc_html__('Google Analytics', 'fevr'), 'fevr_google_analytics_widget');
	}
}

/**
 * Google Analytics Widget
 */
function fevr_google_analytics_widget() {
	include_once trailingslashit(get_template_directory()) . 'luvthemes/includes/ga-widget/ga-widget.inc.php';
}

//======================================================================
// Fevr Mobile
//======================================================================

function fevr_get_fevr_mobile_data(){
	$response = wp_remote_post (
			FEVR_API_URL . 'get_mobile_app_info',
			array(
					'timeout' => 10,
					'sslverify' => false,
					'user-agent' => 'luv',
					'headers' => array (
							'X-ENVATO-PURCHASE-KEY' => fevr_purchase_key()
					),
					'body' => array (
							'site' => trailingslashit(home_url()),
					)
			)
			);
	if (is_wp_error($response)){
		fevr_print_admin_notice(array('class' => 'error', 'message' => esc_html__('Couldn\'t connect to API server: ', 'fevr') . $response->get_error_message()));
	}
	else{
		$response = json_decode($response['body'], true);
		return $response['response'];
	}
}
?>