<?php
/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.6.1 for parent theme Fevr - Multi-Purpose Theme for publication on ThemeForest
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */

/**
 * Include the Fevr_TGM_Plugin_Activation class.
 *
 */
require_once trailingslashit(get_template_directory()) . 'luvthemes/plugins/tgm/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'fevr_theme_register_required_plugins' );
/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * Fevr_TGM_Plugin_Activation class constructor.
 */
function fevr_theme_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		array(
				'name'               => esc_html('Luvthemes Core', 'fevr'),
				'slug'               => 'luvthemes-core',
				'source'             => get_template_directory() . '/luvthemes/plugins/luvthemes-core.zip',
				'required'           => true,
				'version'            => '1.1.1',
				'force_activation'   => false,
				'force_deactivation' => false,
				'is_automatic'		 => true,
				'external_url'       => '',
		),
			
		array(
            'name'               => esc_html('WPBakery Visual Composer', 'fevr'),
            'slug'               => 'js_composer',
            'source'             => get_template_directory() . '/luvthemes/plugins/js_composer.zip',
            'required'           => true,
            'version'            => '4.12',
            'force_activation'   => false,
            'force_deactivation' => false,
            'is_automatic'		 => true,
            'external_url'       => '',
        ),
        
        array(
            'name'               => esc_html('Slider Revolution', 'fevr'),
            'slug'               => 'revslider',
            'source'             => get_template_directory() . '/luvthemes/plugins/slider_revolution/revslider.zip',
            'required'           => false,
            'version'            => '5.2.5.4',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => '',
        ),
        
        array(
            'name'               => esc_html('LayerSlider', 'fevr'),
            'slug'               => 'LayerSlider',
            'source'             => get_template_directory() . '/luvthemes/plugins/layerslider/layersliderwp-5.6.8.installable.zip',
            'required'           => false,
            'version'            => '5.6.8',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => '',
        ),
        
        array(
            'name'               => esc_html('Swift Security - Hide WordPress, Firewall, Code Scanner', 'fevr'),
            'slug'               => 'SwiftSecurity',
            'source'             => get_template_directory() . '/luvthemes/plugins/swift-security-bundle-hide-wordpress-firewall-code-scanner.zip',
            'required'           => false,
            'version'            => '1.4.2.13',
            'force_activation'   => false,
            'force_deactivation' => false,
            'external_url'       => '',
        ),
        
	    array(
            'name'      => esc_html('WooCommerce', 'fevr'),
            'slug'      => 'woocommerce',
            'required'  => false,
        ),
        
        array(
            'name'      => esc_html('Contact Form 7', 'fevr'),
            'slug'      => 'contact-form-7',
            'required'  => false,
        ),

	);

	/*
	 * Array of configuration settings. Amend each line as needed.
	 *
	 * TGMPA will start providing localized text strings soon. If you already have translations of our standard
	 * strings available, please help us make TGMPA even better by giving us access to these translations or by
	 * sending in a pull-request with .po file(s) with the translations.
	 *
	 * Only uncomment the strings in the config array if you want to customize the strings.
	 */
	$config = array(
		'id'           => 'fevr',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'luvthemes-install-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.
	);

	tgmpa( $plugins, $config );
}

?>
