<?php 

//======================================================================
// Admin related functions
//======================================================================
include_once trailingslashit(get_template_directory()) . 'luvthemes/luv-admin-functions.php';

//======================================================================
// Extend WPCF7
//======================================================================

if (class_exists('WPCF7')){
	require_once trailingslashit(get_template_directory()) . 'luvthemes/includes/wpcf7-extend.inc.php';
}

//======================================================================
// Google Fonts
//======================================================================

include_once trailingslashit(get_template_directory()) . 'luvthemes/includes/google-fonts.inc.php';


//======================================================================
// Visual Composer
//======================================================================

//Remove welcome screen
remove_action( 'init', 'vc_page_welcome_redirect' );
remove_action( 'vc_activation_hook', 'vc_page_welcome_set_redirect' );
remove_action( 'admin_init', 'vc_page_welcome_redirect' );

add_action('vc_before_init', 'fevr_set_vc_as_theme');
/**
 * Set VC theme integration mode
 */
function fevr_set_vc_as_theme() {
	vc_set_as_theme(true);
}

$wpb_lma = (defined('WPB_VC_VERSION') ? WPB_VC_VERSION : 99999);
$_COOKIE['vchideactivationmsg_vc11'] = $wpb_lma;

add_action('admin_menu','fevr_remove_menu_pages',11);

/**
 * Remove menu pages
 * VC removed
 * Redux removed
 */
function fevr_remove_menu_pages(){
	remove_menu_page( 'vc-general' );
	remove_menu_page( 'vc-welcome' );
	remove_submenu_page( 'tools.php', 'redux-about' );
}


/**
 * Set VC default post types
 */
if(function_exists('vc_set_default_editor_post_types')):
	vc_set_default_editor_post_types (
		array(
		    'page',
		    'post',
			'luv_snippets',
			'luv_portfolio',
			'product',
		)
	);
endif;

/**
 * Override Visual Composer Updater to work with TGM
 */
add_filter( 'upgrader_pre_download', 'fevr_vc_tgm_update_fix', 0, 3);
function fevr_vc_tgm_update_fix($reply, $package, $updater){
	if (isset( $updater->skin->plugin ) && function_exists('vc_plugin_name') && $updater->skin->plugin === vc_plugin_name()){
		remove_all_filters('upgrader_pre_download');
		return get_stylesheet_directory() . '/luvthemes/plugins/js_composer.zip';
	}
	return $reply;
}


// Avoid PHP notices if VC isn't activated yet 
// VC is required plugin, it should be activated
if(!defined('FEVR_VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG')){
	define('FEVR_VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG', 'vc_shortcodes_css_class');
}

//======================================================================
// Revolution Slider
//======================================================================

// Set Revolution Slider as theme
if(!defined('REV_SLIDER_AS_THEME')){
	define('REV_SLIDER_AS_THEME', true);
}

//======================================================================
// Layer Slider
//======================================================================

add_action('layerslider_ready', 'fevr_layerslider_overrides');
/**
 * Turn off auto updates for Layer Slider
 */
function fevr_layerslider_overrides() {
	// Disable auto-updates
	$GLOBALS['lsAutoUpdateBox'] = false;
}

//======================================================================
// URL to Embedded Video
//======================================================================

/**
 * Create embed code from a single youtube or vimeo URL
 * @param string $url
 * @return string
 */
function fevr_url_to_embedded($url) {
	$vimeo = '/(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?((.*)\/)?([0-9]+)/i';
	$youtube = '/(?:http?s?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/i';

	// Vimeo
	if(preg_match($vimeo, $url, $matches)) {
		return esc_url('//player.vimeo.com/video/'.$matches[3].'?autoplay=1&loop=1&title=0&byline=0&portrait=0&autopause=0');
	
	// Youtube	
	} elseif(preg_match($youtube, $url, $matches)) {
		return esc_url('//www.youtube.com/embed/'.$matches[1].'?autoplay=1&loop=1&playlist='.$matches[1].'&autohide=1&controls=0&disablekb=1&wmode=transparent&modestbranding=1&showinfo=0');
	}
}
 
//======================================================================
// Woocommerce minimum cart total
//======================================================================

add_filter('woocommerce_get_settings_checkout', 'fevr_wc_minimum_order_settings', 10, 1);

/**
 * Set minimum order amount
 * @param array $settings
 * @return array
 */
function fevr_wc_minimum_order_settings($settings){
	$option = array(
			'title'         => esc_html_x( 'Minimum Order', 'Settings group label', 'fevr' ),
			'desc'          => esc_html__( 'Minimum order (cart total)', 'fevr' ),
			'desc_tip'      => esc_html__( 'You can define a minimum order amount for checkout. You can use currency symbols here.', 'fevr' ),
			'id'            => 'fevr_woocommerce_minimum_order',
			'type'          => 'text',
			'autoload'      => false
	);
	array_splice( $settings, 2, 0, array($option));

	return $settings;
}

add_action( 'woocommerce_checkout_process', 'fevr_wc_minimum_order_checking' );

/**
 * Check minimum order amount
 */
function fevr_wc_minimum_order_checking() {
	$minimum = get_option('fevr_woocommerce_minimum_order');
	if ( floatval(preg_replace( '~[^\d.]~', '', WC()->cart->get_cart_total())) < floatval(preg_replace( '~[^\d.]~', '',$minimum))) {
		wc_add_notice( sprintf( esc_html__('You must have an order with a minimum of %s to place your order.', 'fevr') , $minimum ), 'error');
	}
}

//======================================================================
// Woocommerce minimum cart total
//======================================================================

add_filter( 'woocommerce_product_tabs', 'fevr_woocommerce_disable_reviews_tab', PHP_INT_MAX);

function fevr_woocommerce_disable_reviews_tab($tabs) {
	if (fevr_check_luvoption('woocommerce-disable-reviews', 1)){
		unset($tabs['reviews']);
	}
	return $tabs;
}

//======================================================================
// WooCommerce Ajax Cart
//======================================================================

// Ensure cart contents update when products are added to the cart via AJAX
add_filter( 'woocommerce_add_to_cart_fragments', 'fevr_woocommerce_header_add_to_cart_fragment' );
/**
 * Get WooCommerce Cart fragments for refresh cart via ajax 
 * @param array $fragments
 */
function fevr_woocommerce_header_add_to_cart_fragment( $fragments ) {
	
	$fragments['a.cart-contents span'] = (WC()->cart->cart_contents_count > 0 ? '<span data-count="'.WC()->cart->cart_contents_count.'"><i class="ion-bag"></i></span>' : '<span><i class="ion-bag"></i></span>');
	
	return $fragments;
}

add_filter('widget_title', 'fevr_trim_wc_widget_title',10,3);
/**
 * Trim WooCommerce Cart Widget title to avoid invalid empty <h2> tags
 * @param string $title
 * @param WP_Widget $instance
 * @param string $base
 * @return string
 */
function fevr_trim_wc_widget_title($title = '', $instance = null, $base = ''){
	if ($base == 'woocommerce_widget_cart'){
		return trim($title);
	}
	return $title;
}


//======================================================================
// WooCommerce Custom Fields
//======================================================================

add_action('init', 'fevr_woocommerce_custom_fields_init');
/**
 * Add extra meta field for WooCommerce products if wc-style-3 is set
 * Fields:
 * 		- Product Grid Description (extra description for product box on hover)
 */
function fevr_woocommerce_custom_fields_init() {
		add_action( 'woocommerce_product_options_general_product_data', 'fevr_custom_woocommerce_fields' );
		add_action( 'woocommerce_process_product_meta', 'fevr_custom_woocommerce_fields_save' );
}

/**
 * Display extra meta fields for WooCommerce products
 * Fields:
 * 		- Product Grid Description (extra description for product box on hover)
 */
function fevr_custom_woocommerce_fields() {

  global $woocommerce, $post;

  // Create nonce
  woocommerce_wp_hidden_input(array(
  		'id' => 'fevr_meta_box_nonce',
  		'value' => wp_create_nonce('fevr_meta_box')
  ));
  
  // Masonry size
  echo '<div class="options_group">';
  woocommerce_wp_select(
  		array(
  				'id'          => '_fevr_product_masonry_size',
  				'label'       => esc_html__( 'Masonry Item Size', 'fevr' ),
  				'desc_tip'    => 'false',
  				'options'	  => array(
					'fevr_normal' => esc_html__('Normal', 'fevr'),
					'fevr_wide' => esc_html__('Wide', 'fevr'),
					'fevr_tall' => esc_html__('Tall', 'fevr'),
					'fevr_wide_tall' => esc_html__('Wide & Tall', 'fevr'),
					'fevr_full_size' => esc_html__('Full', 'fevr'),
				),
  		)
  		);
  echo '</div>';
  
  //Masonry image
  $masonry_image_id		= get_post_meta($post->ID, '_fevr_product_masonry_image', true);
  $masonry_image		= (!empty($masonry_image_id) ? wp_get_attachment_image_src($masonry_image_id, 'thumbnail') : '');
  $masonry_image_src	= (isset($masonry_image[0]) ? $masonry_image[0] : ''); 
  echo '<p class="form-field">';
  echo '<label>' . esc_html__('Masonry Image', 'fevr').'</label>';
  echo '<span class="luv-media-upload-container media-image">';
  woocommerce_wp_hidden_input(array(
  		'id' => '_fevr_product_masonry_image',
  		'value' => $masonry_image_id,
  		'class' => 'luv-media-upload-url'
  ));
  echo '<img src="'.esc_url($masonry_image_src).'" class="luv-media-upload-preview luv-hidden">';
  echo '<span class="luv-media-buttons">';
  echo '<span class="button media_upload_button luv-media-upload-by-id">Upload</span>';
  echo '<span class="button remove-image luv-media-upload-reset is-hidden">Remove</span>';
  echo '</span></span>';
  echo '</p>';
  
  
  // Short description
  echo '<div class="options_group">';
		woocommerce_wp_textarea_input( 
			array( 
				'id'          => '_fevr_product_grid_description', 
				'label'       => esc_html__( 'Description in the product box on hover', 'fevr' ), 
				'desc_tip'    => 'false',
			)
		);
  echo '</div>';
}

/**
 * Save extra product meta fields
 * @param int $post_id
 */
function fevr_custom_woocommerce_fields_save( $post_id ){
	// Check if our nonce is set.
	if ( ! isset( $_POST['fevr_meta_box_nonce'] ) || ! wp_verify_nonce( $_POST['fevr_meta_box_nonce'], 'fevr_meta_box' ) ) {
		return;
	}
	
	// Check the user's permissions.
	if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
	}

	// Update meta
	update_post_meta( $post_id, '_fevr_product_masonry_image', esc_html( $_POST['_fevr_product_masonry_image'] ) );
	update_post_meta( $post_id, '_fevr_product_masonry_size', esc_html( $_POST['_fevr_product_masonry_size'] ) );
	update_post_meta( $post_id, '_fevr_product_grid_description', esc_html( $_POST['_fevr_product_grid_description'] ) );
	
	
}

//======================================================================
// Custom Body Class
//======================================================================

add_filter( 'body_class', 'fevr_custom_woocommerce_body_class' );
add_filter( 'login_body_class', 'fevr_custom_woocommerce_body_class' );

/**
 * Add 'woocommerce' to body classes for luv_collections page
 * @param array $classes
 */
function fevr_custom_woocommerce_body_class( $classes ){
	if( is_singular( 'luv_collections' )) {
		$classes[] = 'woocommerce';
	}
	
	if (fevr_is_headless()){
		$classes[] = 'is-headless';
	}
	
	if (fevr_is_mobile()){
		$classes[] = 'is-mobile-app';
	}
	return $classes;
}

//======================================================================
// WooCommerce Actions
//======================================================================

// Items in cart
add_action('wp_head', 'fevr_collect_cart_items');

/**
 * Set global $is_item_in_cart for later use (eg change color/buttons for items which are already in cart) 
 */
function fevr_collect_cart_items(){
	if (class_exists('woocommerce') && !is_admin() && is_object(WC())){
		global $fevr_is_item_in_cart;
		foreach ((array)WC()->cart->get_cart() as $item){
			$fevr_is_item_in_cart[$item['product_id']] = $item['product_id'];
		}
	}
}

/**
 * Get is item in cart
 * @param int $product_id
 * @return boolean
 */
function fevr_is_item_in_cart($product_id){
	global $fevr_is_item_in_cart;
	
	return in_array($product_id, (array)$fevr_is_item_in_cart);
}

// WooCommerce actions
add_action('init', 'fevr_wc_actions', 11);

/**
 * Build WooCoomerce layouts with WC hooks
 * Add Wrapper for Quickview button
 * Add Whishlist button for single/quick view
 * Add Single price
 * Add Categories for single/quick view
 */
function fevr_wc_actions() {
	// Wrapper for quickview buttons
	add_action('woocommerce_single_product_summary', 'fevr_wc_wrapper_start', 29);
	add_action('woocommerce_single_product_summary', 'fevr_wc_wrapper_end', 31);
	
	// Wishlist button on single/quick view
	add_action('woocommerce_after_add_to_cart_button', 'fevr_get_woocommerce_wishlist_fevr_button', 39);
}

// Variations
add_action('woocommerce_before_single_product', 'fevr_wc_custom_variable', 0);
add_action('fevr_before_woocommerce_quick_view', 'fevr_wc_custom_variable', 0);

/**
 * Add elements (whislist, single price) for variations 
 */
function fevr_wc_custom_variable() {
	global $product, $available_variations;
	
	if($product->is_type('variable')) {
		remove_action('woocommerce_after_add_to_cart_button', 'fevr_get_woocommerce_wishlist_fevr_button', 39);
		add_action('fevr_wc_variable_wishlist', 'fevr_get_woocommerce_wishlist_fevr_button', 39);
		
		$checking_array = array();
		foreach($product->get_available_variations() as $variation) {
			if($variation['is_purchasable']) {
				$checking_array[$variation['display_price']] = true;
				
			}
		}
		
		if(count($checking_array) > 1) {
			remove_action('woocommerce_before_add_to_cart_button', 'woocommerce_template_single_price');
			add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 6);
		}
	}
}



//======================================================================
// Woocommerce Product Box Styles
//======================================================================	
			
/**
 * Wrapper layouts for different WC styles
 */
function fevr_wc_wrapper_start() {
	global $fevr_wc_wrapper_index;
	
	switch(current_action()) {
		case 'woocommerce_single_product_summary':
			echo '<div class="product-add-to-cart-container">';
			break;
	}
	
	// Style 1 (default)
	if(fevr_check_luvoption('woocommerce-product-box-style', 'wc-style-1') || fevr_check_luvoption('woocommerce-product-box-style', '')) {
		switch(current_action()) {
			case 'woocommerce_before_shop_loop_item_title':
				echo '<div class="button-wrapper">';
				break;
				
			case 'woocommerce_shop_loop_item_title':
				echo '<div class="title-wrapper">';
				break;
						
			case 'woocommerce_after_shop_loop_item_title':
				echo '<div class="meta-wrapper">';
				break;
				
			case 'woocommerce_before_shop_loop_item':
				echo '<div class="product-wrapper">';
				break;
		}
		
	// Style 2	
	} else if(fevr_check_luvoption('woocommerce-product-box-style', 'wc-style-2')) {
		switch(current_action()) {
			case 'woocommerce_shop_loop_item_title':
				echo '<div class="title-wrapper">';
				break;
						
			case 'woocommerce_after_shop_loop_item':
				echo '<div class="cart-wrapper">';
				break;
				
			case 'woocommerce_before_shop_loop_item':
				echo '<div class="product-wrapper">';
				break;
		}
		
	// Style 3
	} else if(fevr_check_luvoption('woocommerce-product-box-style', 'wc-style-3')) {
		switch(current_action()) {
			case 'woocommerce_before_shop_loop_item_title':
				if (!isset($fevr_wc_wrapper_index['woocommerce_before_shop_loop_item_title']) || empty($fevr_wc_wrapper_index['woocommerce_before_shop_loop_item_title'])){
					echo '<div class="item-wrapper">';
					$fevr_wc_wrapper_index['woocommerce_before_shop_loop_item_title'] = 'item-wrapper';
					break;
				}
				else if ($fevr_wc_wrapper_index['woocommerce_before_shop_loop_item_title'] == 'item-wrapper'){
					echo '<div class="product-button-wrapper">';
					$fevr_wc_wrapper_index['woocommerce_before_shop_loop_item_title'] = 'product-button-wrapper';			
					break;
				}
				else if ($fevr_wc_wrapper_index['woocommerce_before_shop_loop_item_title'] == 'product-button-wrapper'){
					echo '<div class="product-details-wrapper">';
					$fevr_wc_wrapper_index['woocommerce_before_shop_loop_item_title'] = '';
					break;
				}
			case 'woocommerce_before_shop_loop_item':
				echo '<div class="product-wrapper">';
				break;
		}
	// Style 4
	} else if(fevr_check_luvoption('woocommerce-product-box-style', 'wc-style-4')) {
		switch(current_action()) {
			case 'woocommerce_before_shop_loop_item_title':
					echo '<div class="item-wrapper">';
					break;
					
			case 'woocommerce_before_shop_loop_item':
				echo '<div class="product-wrapper">';
				break;
		}
	// Style 5
	} else if(fevr_check_luvoption('woocommerce-product-box-style', 'wc-style-5')) {
		switch(current_action()) {
			case 'woocommerce_before_shop_loop_item_title':
					echo '<div class="item-wrapper">';
					break;
					
			case 'woocommerce_before_shop_loop_item':
				echo '<div class="product-wrapper">';
				break;
		}
	// Style 6
	} else if(fevr_check_luvoption('woocommerce-product-box-style', 'wc-style-6')) {
		switch(current_action()) {
			case 'woocommerce_before_shop_loop_item_title':
					echo '<div class="item-wrapper">';
					break;
					
			case 'woocommerce_before_shop_loop_item':
				echo '<div class="product-wrapper">';
				break;
		}
	}
}

/**
 * Print wrapper start
 */
function fevr_wc_item_wrapper_start() {
	echo '<div class="item-wrapper">';
}

/**
 * Print wrapper end
 */
function fevr_wc_wrapper_end() {
	echo '</div>';
}

// Product Wrapper
add_action('woocommerce_before_shop_loop_item', 'fevr_wc_wrapper_start',PHP_INT_MAX);
add_action('woocommerce_shop_loop_item_title', 'fevr_wc_wrapper_end',-PHP_INT_MAX);


//======================================================================
// Product Categories
//======================================================================

/**
 * Print product categories
 */
function fevr_wc_product_categories() {
	global $product;
	echo '<div class="product-categories">'.fevr_kses($product->get_categories()).'</div>';
}

//======================================================================
// WooCommerce Buttons
//======================================================================

// Init buttons 
add_action('init', 'fevr_init_wc_styles');

/**
 * Initialize product box layouts and buttons for WooCommerce
 */
function fevr_init_wc_styles() {
	remove_action('woocommerce_before_shop_loop_item','woocommerce_template_loop_product_link_open');
	remove_action('woocommerce_after_shop_loop_item','woocommerce_template_loop_product_link_close', 5);
	
	// Style 1
	if(fevr_check_luvoption('woocommerce-product-box-style', 'wc-style-1') || fevr_check_luvoption('woocommerce-product-box-style', '')) {
		
		// Wrapper
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_start');
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_end', 12);
		
		// Quickview
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_get_woocommerce_quickview_fevr_button');
		
		// Wishlist
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_get_woocommerce_wishlist_fevr_button');
		
		// Wrapper
		add_action('woocommerce_shop_loop_item_title', 'fevr_wc_wrapper_start');
		add_action('woocommerce_shop_loop_item_title', 'fevr_wc_wrapper_end', 13);
		
		// Title
		remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title');
		add_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);
		
		// Add to Cart
		remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
		add_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 12);
		
		// Wrapper
		add_action('woocommerce_after_shop_loop_item_title', 'fevr_wc_wrapper_start');
		add_action('woocommerce_after_shop_loop_item_title', 'fevr_wc_wrapper_end', 12);
		
		// Price, Rating
		remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
		remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);
		add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price');
		add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 11);
	
	// Style 2
	} else if(fevr_check_luvoption('woocommerce-product-box-style', 'wc-style-2')) {
		
		// Quick View
		add_action('woocommerce_after_shop_loop_item', 'fevr_get_woocommerce_quickview_fevr_button', 13);
		
		// Wrapper
		add_action('woocommerce_shop_loop_item_title', 'fevr_wc_wrapper_start', 9);
		add_action('woocommerce_after_shop_loop_item_title', 'fevr_wc_wrapper_end', 11);
		
		// Wrapper
		add_action('woocommerce_after_shop_loop_item', 'fevr_wc_wrapper_start', 10);
		add_action('woocommerce_after_shop_loop_item', 'fevr_wc_wrapper_end', 14);
		
		// Cart
		remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
		add_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 11);
		
		// Wishlist
		add_action('woocommerce_after_shop_loop_item', 'fevr_get_woocommerce_wishlist_fevr_button', 12);
	
	// Style 3
	} else if(fevr_check_luvoption('woocommerce-product-box-style', 'wc-style-3')) {
		// Wrapper
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_start');
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_end', 12);
		
		// Title
		remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title');
		add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_title');
		
		// Price
		remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);
		add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_price', 11);
		
		// Wrapper
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_start', 12);
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_end', 15);
		
		// Cart
		remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
		add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 12);
		
		// Wishlist
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_get_woocommerce_wishlist_fevr_button', 13);
		
		// Quickview
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_get_woocommerce_quickview_fevr_button', 14);
		
		//
		// Hidden Content
		//
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_start', 15);
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_end', 21);
		
		// Title
		add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_title', 16);
		
		// Price
		add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_price', 17);
		
		// Categories
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_product_categories', 18);
		
		// Rating
		remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
		add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_rating', 19);
		
		// Excerpt
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_custom_description_field', 20);		

	// Style 4
	} else if(fevr_check_luvoption('woocommerce-product-box-style', 'wc-style-4')) {
		
		// Wrapper
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_start');
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_end', 14);
		
		// Price
		remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price');
		add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_price');
		
		// Category
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_product_categories');
		
		// Rating
		remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
		add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_rating', 11);
		
		// Wishlist
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_get_woocommerce_wishlist_fevr_button', 12);
		
		// Quickview
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_get_woocommerce_quickview_fevr_button', 13);
	
	// Style 5
	} else if(fevr_check_luvoption('woocommerce-product-box-style', 'wc-style-5')) {
		
		// Wrapper
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_start');
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_end', 14);
			
		// Cart
		remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
		add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 12);
		
		// Quickview
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_get_woocommerce_quickview_fevr_button', 9);
		
		// Rating
		remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
		
	// Style 6
	} else if(fevr_check_luvoption('woocommerce-product-box-style', 'wc-style-6')) {
		
		// Wrapper
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_start');
		add_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_end', 14);
		
		// Title
		remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title');
		add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_title');
		
		// Cart
		remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
		add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 14);
		
		// Price
		remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price');
		add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_price', 13);
		
		// Rating
		remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
	}
}

/**
 * Reset WooCommerce product box styles to the default
 */
function fevr_reset_wc_styles(){

	// Remove custom WooCommerce hooks

	remove_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_start');
	remove_action('woocommerce_after_shop_loop_item_title', 'fevr_wc_wrapper_end', 11);
	remove_action('woocommerce_after_shop_loop_item_title', 'fevr_wc_wrapper_end', 12);
	remove_action('woocommerce_after_shop_loop_item_title', 'fevr_wc_wrapper_start');
	remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price');
	remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 11);
	remove_action('woocommerce_after_shop_loop_item', 'fevr_get_woocommerce_quickview_fevr_button', 13);
	remove_action('woocommerce_after_shop_loop_item', 'fevr_get_woocommerce_wishlist_fevr_button', 12);
	remove_action('woocommerce_after_shop_loop_item', 'fevr_wc_wrapper_end', 14);
	remove_action('woocommerce_after_shop_loop_item', 'fevr_wc_wrapper_start', 10);
	remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 11);
	remove_action('woocommerce_before_shop_loop_item_title', 'fevr_custom_description_field', 20);
	remove_action('woocommerce_before_shop_loop_item_title', 'fevr_get_woocommerce_quickview_fevr_button', 13);
	remove_action('woocommerce_before_shop_loop_item_title', 'fevr_get_woocommerce_quickview_fevr_button', 14);
	remove_action('woocommerce_before_shop_loop_item_title', 'fevr_get_woocommerce_quickview_fevr_button', 9);
	remove_action('woocommerce_before_shop_loop_item_title', 'fevr_get_woocommerce_quickview_fevr_button');
	remove_action('woocommerce_before_shop_loop_item_title', 'fevr_get_woocommerce_wishlist_fevr_button', 12);
	remove_action('woocommerce_before_shop_loop_item_title', 'fevr_get_woocommerce_wishlist_fevr_button', 13);
	remove_action('woocommerce_before_shop_loop_item_title', 'fevr_get_woocommerce_wishlist_fevr_button');
	remove_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_product_categories', 18);
	remove_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_product_categories');
	remove_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_end', 12);
	remove_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_end', 14);
	remove_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_end', 15);
	remove_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_end', 21);
	remove_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_start', 12);
	remove_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_start', 15);
	remove_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_wrapper_start');
	remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 12);
	remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_price', 11);
	remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_price', 17);
	remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_price');
	remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_title', 16);
	remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_title');
	remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_rating', 11);
	remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_rating', 19);
	remove_action('woocommerce_shop_loop_item_title', 'fevr_wc_wrapper_end', 13);
	remove_action('woocommerce_shop_loop_item_title', 'fevr_wc_wrapper_start', 9);
	remove_action('woocommerce_shop_loop_item_title', 'fevr_wc_wrapper_start');
	remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 12);
	remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);
	remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 14);
	remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_price', 13);
	
	// Add default WooCommerce hooks

	add_action('woocommerce_before_shop_loop_item','woocommerce_template_loop_product_link_open');
	add_action('woocommerce_after_shop_loop_item','woocommerce_template_loop_product_link_close', 5);
	add_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title');
	add_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
	add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
	add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);

	// Reinit product box styles
	fevr_init_wc_styles();
}

/**
 * Print description field
 */
function fevr_custom_description_field() {
	global $post;

	$fevr_woocommerce_description = get_post_meta($post->ID, '_fevr_product_grid_description', true);

	if(!empty($fevr_woocommerce_description)) {
		echo '<div itemprop="description">'.fevr_kses($fevr_woocommerce_description).'</div>';
	} else {
		woocommerce_template_single_excerpt();
	}

}


/**
 * Create photo review button for purchased items
 */
function fevr_get_woocommerce_photo_review_button(){
	global $product;

	//If the user purchased the product we add the photo review button
	$current_user = wp_get_current_user();
	if ( fevr_customer_can_photo_review($current_user->user_email, $current_user->ID, get_the_ID())){
		return '<a data-product-id="'.esc_attr(get_the_ID()).'" class="button luv-photo-review btn-s btn btn-full" href="#">'.esc_html__('Add Photo Review', 'fevr').'</a>';
	}
}

/**
 * Quick View button
 * Print Qiuck View button for archive page based on selected product box style
 */
function fevr_get_woocommerce_quickview_fevr_button(){
	if (!is_single() || did_action('woocommerce_after_single_product_summary')){
		
		$wc_box_style = fevr_get_luvoption('woocommerce-product-box-style');
		
		if($wc_box_style && in_array($wc_box_style, array('wc-style-2','wc-style-3', 'wc-style-5')) && !did_action('fevr_woocommerce_quick_view') && (!is_single() || did_action('woocommerce_after_single_product_summary'))) {
			echo '<a href="#" class="luv-product-quick-view '.($wc_box_style != 'wc-style-3' ? ' luv-wc-btn' : '').'" data-product-id="'.esc_attr(get_the_ID()).'"><i class="fa fa-external-link"></i></a>';
		} else {
			echo '<a href="#" class="luv-product-quick-view luv-wc-btn" data-product-id="'.esc_attr(get_the_ID()).'">'.esc_html__('Quick View', 'fevr').'</a>';
		}
	}
}

/**
 * Wishlist button
 * Print Wishlist button for archive page based on selected product box style
 * If an item is already on user's whishlist it displays "Remove wishlist" otherwise "Add to wishlist"
 * Items on wishlist are stored in cookies 
 * @see fevr_wc_wishlist
 */
function fevr_get_woocommerce_wishlist_fevr_button(){
	// Don't show button if it is disabled
	if (fevr_check_luvoption('woocommerce-disable-wishlist', 1)){
		return false;
	}
	
	//Set wishlist class and button text based on user's wishlist
	if(isset($_COOKIE['luv-woocommerce-wishlist']) && in_array(get_the_ID(), (array)json_decode(stripslashes($_COOKIE['luv-woocommerce-wishlist']),true))){
		$on_wishlist_class = ' on-my-wishlist';
		$icon = 'fa fa-heart';
		$button_text = esc_html__('Remove from Wishlist', 'fevr');
	}
	else{
		$on_wishlist_class = '';
		$icon = 'fa fa-heart-o';
		$button_text = esc_html__('Add to Wishlist', 'fevr');
	}

	$wc_box_style = fevr_get_luvoption('woocommerce-product-box-style');
	
	if($wc_box_style && in_array($wc_box_style, array('wc-style-2','wc-style-3')) && !did_action('fevr_woocommerce_quick_view') && (!is_single() || did_action('woocommerce_after_single_product_summary'))) {
		echo '<a href="#" rel="nofollow" data-product-id="'.esc_attr(get_the_ID()).'" data-product-title="'.esc_attr(get_the_title()).'" class="luv-wc-wishlist luv-wishlist-icon'.($wc_box_style != 'wc-style-3' ? ' luv-wc-btn' : '').$on_wishlist_class.'"><i class="'.$icon.'"></i></a>';
	} else {
		echo '<a href="#" rel="nofollow" data-product-id="'.esc_attr(get_the_ID()).'" data-product-title="'.esc_attr(get_the_title()).'" class="button luv-wc-btn luv-wc-wishlist'.$on_wishlist_class.'">'.$button_text.'</a>';
	}
}

//======================================================================
// Woocommerce Quick View AJAX
//======================================================================

add_action('wp_ajax_fevr_product_quick_view','fevr_product_quick_view');
add_action('wp_ajax_nopriv_fevr_product_quick_view','fevr_product_quick_view');

/**
 * Use quick-view template to load quick view popup via ajax
 */
function fevr_product_quick_view(){
	get_template_part('luvthemes/luv-templates/quick-view');
	wp_die();
}

//======================================================================
// Woocommerce Photo Reviews
//======================================================================

/**
 * Check ability for sending photo review. 
 * Buyers can send photo reviews only one time and only for items that are purchased by the user
 * We use this function to check should we show/hide the photo review buttons, and when the user send the review 
 * @param string $email
 * @param int $user_id
 * @param int $product_id
 * @return boolean
 */
function fevr_customer_can_photo_review($email, $user_id, $product_id){

	$args = array(
			'post_type'		=> 'luv_ext_reviews',
			'author'		=> $user_id,
			'post_status' 	=> 'pending,publish',
			'meta_key'   	=> '_product_id',
			'meta_value' 	=> $product_id
	);
	$wc_ext_review_query = new WP_Query( $args );
	return (wc_customer_bought_product($email, $user_id, $product_id) && $wc_ext_review_query->post_count == 0);
}

// Add extra WooCommerce e-mail template
add_filter('woocommerce_email_classes', 'fevr_add_woocommerce_email_templates');

if (!function_exists('fevr_add_woocommerce_email_templates')):
/**
 * Add extra e-mail templates for WooCommerce 
 * @param array $templates
 */
function fevr_add_woocommerce_email_templates($templates){
	// Coupon code template
	$templates['Fevr_WC_Email_Coupon_Code'] = include(trailingslashit(get_template_directory()) . 'luvthemes/includes/woocommerce/emails/class-wc-email-coupon-code.php');
	return $templates;
}	
endif;

// Add extra WooCommerce email actions
add_filter('woocommerce_email_actions','fevr_add_woocommerce_email_actions');

if (!function_exists('fevr_add_woocommerce_email_actions')):
/**
 * Add extra WooCommerce email actions
 * @param array $actions
 */
function fevr_add_woocommerce_email_actions($actions){
	// Coupon code notification
	$actions[] = 'woocommerce_coupon_code_notification';
	return $actions;
}
endif;

// Review form ajax
add_action('wp_ajax_fevr_photo_review', 'fevr_photo_review');

/**
 * Use photo review template to load photo review popup via ajax
 */
function fevr_photo_review(){
	get_template_part('luvthemes/luv-templates/photo-review');
	wp_die();
}

// Image upload
add_action('wp_ajax_fevr_photo_review_image_upload','fevr_photo_review_image_upload');

/**
 * Frontend image upload for photo reviews 
 * This function will create a new post for the review and attach the uploaded images to this post
 */
function fevr_photo_review_image_upload() {
	// Check wp-nonce
	if (!isset($_POST['wp_nonce']) || !wp_verify_nonce( $_POST['wp_nonce'], '_fevr' ) ) {
		esc_html_e('Unknown error occured', 'fevr');
		die();
	}

	if (!isset($_POST['photo_review_id']) || empty($_POST['photo_review_id'])){
		$user = wp_get_current_user();
		$user_id = $user->ID;
		// Return if user didn't purchase the item
		if (!fevr_customer_can_photo_review($user->user_email, $user->ID, $_POST['product_id'])){
			esc_html_e('Unknown error occured', 'fevr');
			die();
		}
		$review = array(
				'post_title'    => sanitize_title($_POST['product_id'].'_'.$user_id.'_draft_' . time()),
				'post_content'  => ' ',
				'post_type'     => 'luv_ext_reviews',
				'post_status'   => 'draft',
				'post_author'   => (int)$user_id
		);

		// Insert the draft post into the database
		$review_id = wp_insert_post( $review );
		add_post_meta($review_id, '_product_id', (int)$_POST['product_id']);
	}
	else{
		$review_id = (int)$_POST['photo_review_id'];
	}

	$data = array();
	if(isset($_FILES['photo_review_photos'])) {
		$error = false;
		$photos = array();
		$message = '';
		$uploaddir = wp_upload_dir();

		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		require_once( ABSPATH . 'wp-admin/includes/media.php' );

		$files = $_FILES["photo_review_photos"];
		foreach($files['name'] as $key => $value) {
			if (isset($files['name'][$key])) {
				$file = array(
						'name' => $files['name'][$key],
						'type' => $files['type'][$key],
						'tmp_name' => $files['tmp_name'][$key],
						'error' => $files['error'][$key],
						'size' => $files['size'][$key]
				);
				$_FILES = array ("photo_review_photos" => $file);
				foreach ($_FILES as $file => $array) {
					// Allow images only
					if(getimagesize($array['tmp_name']) > 0) {
						$attachment_id = media_handle_upload( $file, $review_id );

						if ( is_wp_error( $attachment_id ) ) {
							$error = true;
							$message = esc_html__('There was an error uploading the image: ', 'fevr') . $attachment_id->get_error_message();
						} else {
							$photos[] = array('id' => $attachment_id, 'src' => wp_get_attachment_url($attachment_id));
						}
					}
					else{
						$error = true;
						$message = esc_html__('There was an error uploading the image.', 'fevr');
					}
				}
			}
		}
	}
	$data = array(
			'error' => $error,
			'message' => $message,
			'photos' => $photos,
			'review_id' => $review_id
	);
	echo json_encode($data);
	wp_die();
}

// Remove image
add_action('wp_ajax_fevr_photo_review_remove_image','fevr_photo_review_remove_image');

/**
 * Remove previously uploaded images for photo review
 */
function fevr_photo_review_remove_image() {
	// Check wp-nonce
	if ( ! wp_verify_nonce( $_POST['wp_nonce'], '_fevr' ) ) {
		esc_html_e('Unknown error occured', 'fevr');
		die();
	}

	$post = get_post((int)$_POST['photo_review_image_id']);
	if ($post->post_author == get_current_user_id() || current_user_can('edit_posts')){
		wp_delete_attachment( (int)$_POST['photo_review_image_id'], true);
	}
	wp_die();
}

// Add rewiev
add_action('wp_ajax_fevr_photo_review_content','fevr_photo_review_content');

/**
 * Create the photo review
 * This function will create the post if it wasn't created on image upload, otherwise it will update the previously created post
 */
function fevr_photo_review_content() {
	// Check wp-nonce
	if ( ! wp_verify_nonce( $_POST['wp_nonce'], 'fevr' ) ) {
		die(esc_html__('Unknown error occured', 'fevr'));
	}

	// Return if user didn't purchase the item
	$user = wp_get_current_user();
	if (!fevr_customer_can_photo_review($user->user_email, $user->ID, $_POST['product_id'])){
		die(esc_html__('Unknown error occured', 'fevr'));
	}

	// If there isn't any image the post isn't exists, so we create it
	if (!isset($_POST['photo_review_id']) || empty($_POST['photo_review_id'])){
		$user = wp_get_current_user();
		$user_id = $user->ID;
		//Return if user didn't purchase the item
		if (!fevr_customer_can_photo_review($user->user_email, $user->ID, $_POST['product_id'])){
			esc_html_e('Unknown error occured', 'fevr');
			die();
		}
		$review = array(
				'post_title'    => esc_attr($_POST['photo_review_title']),
				'post_content'  => sanitize_text_field($_POST['photo_review_content']),
				'post_type'     => 'luv_ext_reviews',
				'post_status'   => 'pending',
				'post_author'   => $user_id
		);

		// Insert the draft post into the database
		$review_id	= wp_insert_post( $review );
		
		// Feedback
		$feedback	= isset($_POST['feedback']) ? (int)$_POST['feedback'] : 0; 
		add_post_meta($review_id, '_product_id', (int)$_POST['product_id']);
		add_post_meta($review_id, '_fevr_feedback', $feedback);
	}
	else{
		$review_id = (int)$_POST['photo_review_id'];
		$post = get_post($review_id);

		if ($post->post_author == get_current_user_id()){
			$review = array(
					'ID'          	=> $review_id,
					'post_title'  	=> esc_attr($_POST['photo_review_title']),
					'post_content'	=> sanitize_text_field($_POST['photo_review_content']),
					'post_status'	=> 'pending'
			);

			// Feedback
			$feedback	= isset($_POST['feedback']) ? (int)$_POST['feedback'] : 0;
			add_post_meta($review_id, '_fevr_feedback', $feedback);
			
			//Finalize review
			wp_update_post($review);
		}
	}
	
	$args = array(
			'post_type'		=> 'luv_ext_reviews',
			'post_status' 	=> 'pending',
	);
	$pending_reviews_query = new WP_Query( $args );
	update_option('_fevr_pending_photo_reviews', $pending_reviews_query->post_count);

	echo json_encode(array('error'=>0));
	wp_die();
}


// Accept photo Reviews
add_action('save_post','fevr_accept_photo_review');

/**
 * Accept Photo Reviews
 * Generate WooCommerce Coupon on accept photo review if coupon is enabled
 * @param int $post_id
 */
function fevr_accept_photo_review($post_id) {
	// Check post type
	if (get_post_type($post_id) !== 'luv_ext_reviews'){
		return;
	}
	
	// Check if our nonce is set.
	if (!isset($_POST['wp_nonce']) || !wp_verify_nonce( $_POST['wp_nonce'], 'fevr' )) {
		return;
	}
	
	// Check permissions
	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}
	
	$args = array(
			'post_type'		=> 'luv_ext_reviews',
			'post_status' 	=> 'pending',
	);
	$pending_reviews_query = new WP_Query( $args );
	update_option('_fevr_pending_photo_reviews', $pending_reviews_query->post_count);
	
	
	if (fevr_check_luvoption('woocommerce-photo-reviews-coupon','','!=')){
		$post = get_post($post_id);
	
		// Run only on photo review post type
		if ($post->post_type == 'luv_ext_reviews'){
			$is_coupon_applied = get_post_meta($post_id, 'is_coupon_applied',true);
			$user_billing_emails = get_user_meta($post->post_author,'billing_email',true);
	
			if ($post->post_status == 'publish' && $is_coupon_applied != true && (int)fevr_get_luvoption('woocommerce-photo-reviews-coupon') > 0){
				$coupon_users = get_post_meta(fevr_get_luvoption('woocommerce-photo-reviews-coupon'), 'customer_email',true);
				if (!in_array($user_billing_emails, $coupon_users)){
					$coupon_users[] = $user_billing_emails;
					update_post_meta($post_id, 'is_coupon_applied',true);
					update_post_meta(fevr_get_luvoption('woocommerce-photo-reviews-coupon'), 'customer_email', $coupon_users);
					$_coupon = get_post( fevr_get_luvoption('woocommerce-photo-reviews-coupon') );
					$coupon = array(
							'code'			=> $_coupon->post_title,
							'description'	=> $_coupon->post_excerpt,
							'expiry'		=> get_post_meta(fevr_get_luvoption('woocommerce-photo-reviews-coupon'), 'expiry_date', true)
					);
	
					$wc_email = WC_Emails::instance();
					$email = $wc_email->emails['Fevr_WC_Email_Coupon_Code'];
						
					// Send mail
					$email->trigger($post->post_author, $coupon['code'], $coupon['description'], $coupon['expiry']);
	
				}
			}
		}
	}
}

//======================================================================
// Woocommerce Wishlist
//======================================================================

// Wishlist ajax actions
add_action('wp_ajax_fevr_wc_wishlist', 'fevr_wc_wishlist');
add_action('wp_ajax_nopriv_fevr_wc_wishlist', 'fevr_wc_wishlist');

/**
 * Add/Remove item to/from wishlist
 * The function will print -1 on remove event, and 1 on add event.
 * We use this printed values in javascript to change the buttons after add/remove action
 */
function fevr_wc_wishlist(){
	$wishlist = isset($_COOKIE['luv-woocommerce-wishlist']) ? (array)json_decode(stripslashes($_COOKIE['luv-woocommerce-wishlist']), true) : array();
	$product = (int)$_POST['product'];

	// Remove from wishlist
	if(in_array($product, $wishlist)){
		unset($wishlist[$product]);
		$response = -1;
	}
	// Add to wishlist
	else{
		$wishlist[$product] = $product;
		$response = 1;
	}
	
	$wishlist = array_filter(array_unique($wishlist));
	
	//Save wishlist to db for logged in users
	if (is_user_logged_in()){
		update_user_meta(get_current_user_id(), 'luv-woocommerce-wishlist', $wishlist);
	}
	
	setcookie("luv-woocommerce-wishlist", json_encode($wishlist), time()+(10 * 365 * 24 * 60 * 60), SITECOOKIEPATH);
	wp_die($response);
}

// Merge wishlist cookie with previously added products on login
add_action('wp_login', 'fevr_wc_wishlist_login_merge', 10, 2);

/**
 * This function merge current cookies with previously stored user meta
 * User's wishlist basically is stored in cookies, but we also save them in user meta for logged in users
 * @param string $user_login
 * @param WP_User_Object $user
 */
function fevr_wc_wishlist_login_merge($user_login, $user){
	if (isset($_COOKIE['luv-woocommerce-wishlist'])){
		$cookie_wishlist = json_decode(stripslashes($_COOKIE['luv-woocommerce-wishlist']), true);
		$_COOKIE['luv-woocommerce-wishlist'] = array_filter(array_unique(array_merge((array)get_user_meta($user->ID, 'luv-woocommerce-wishlist', true), $cookie_wishlist)));
		setcookie("luv-woocommerce-wishlist", json_encode($_COOKIE['luv-woocommerce-wishlist']), time()+(10 * 365 * 24 * 60 * 60), SITECOOKIEPATH);
	}
}

add_action('wp_logout', 'fevr_wc_wishlist_logout_clear');

/**
 * Clear WC wishlist cookies on logout if they was set
 */ 
function fevr_wc_wishlist_logout_clear(){
	if (isset($_COOKIE['luv-woocommerce-wishlist'])){
		setcookie("luv-woocommerce-wishlist", '[]', 0, SITECOOKIEPATH);
	}
}

//======================================================================
// Woocommerce Catalog Product Gallery
//======================================================================

add_action('woocommerce_before_shop_loop_item_title', 'fevr_wc_catalog_product_gallery', 10);
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );

/**
 * Extend Woocommerce Catalog Product Gallery for wc-style-3 product box style 
 */
function fevr_wc_catalog_product_gallery() {
	global $fevr_is_wc_masonry, $fevr_wc_masonry_product_size;
	
	echo (fevr_check_luvoption('woocommerce-product-box-style', 'wc-style-3', '!=') ? '<a href="'.esc_url(get_permalink()).'">' : '');
	echo '<div class="catalog-product-gallery-container">';
	
	if ($fevr_is_wc_masonry === true){
		$size = $fevr_wc_masonry_product_size;
		echo fevr_woocommerce_get_product_thumbnail($size);
	}
	else{
		$size = 'shop_catalog';
		echo woocommerce_get_product_thumbnail();
	}
	
	global $post, $product, $woocommerce;

	$attachment_ids = $product->get_gallery_attachment_ids();

	if ($attachment_ids ) {
		echo '<div class="catalog-product-gallery">';
	
			foreach ( $attachment_ids as $attachment_id ) {
				$image_title 	= esc_attr( get_the_title( $attachment_id ) );
				$image_caption 	= esc_attr( get_post_field( 'post_excerpt', $attachment_id ) );
	
				$image       = wp_get_attachment_image( $attachment_id, $size, 0, $attr = array(
					'title'	=> $image_title,
					'alt'	=> $image_title
					) );
	
				echo fevr_kses($image);
			}
	
		echo '</div>';
	}
	
	echo '</div>';
	echo (fevr_check_luvoption('woocommerce-product-box-style', 'wc-style-3', '!=') ? '</a>' : '');
}

//======================================================================
// Woocommerce Custom Variable Button
//======================================================================

remove_action( 'woocommerce_single_variation', 'woocommerce_single_variation', 10 );
remove_action( 'woocommerce_single_variation', 'woocommerce_single_variation_add_to_cart_button', 20 );
add_action('woocommerce_single_variation','fevr_woocommerce_single_variation_add_to_cart_button', 10);

if ( ! function_exists( 'fevr_woocommerce_single_variation_add_to_cart_button' ) ):
/**
 * Output the add to cart button for variations.
 */
function fevr_woocommerce_single_variation_add_to_cart_button() {
	global $product;
	?>
	<div class="variations_button">
				<?php woocommerce_quantity_input( array( 'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : 1 ) ); ?>
			<input type="hidden" name="add-to-cart"
		value="<?php echo absint( $product->id ); ?>" /> <input type="hidden"
		name="product_id" value="<?php echo absint( $product->id ); ?>" /> <input
			type="hidden" name="variation_id" class="variation_id" value="" />
	</div>
	<div class="add-to-cart-inner">
	<button type="submit" class="single_add_to_cart_button button alt">
		<i class="ion-bag"></i> <?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
		
		<?php do_action('fevr_wc_variable_wishlist'); ?>
	</div>
<?php
}
endif;


//======================================================================
// Woocommerce Sale Flash
//======================================================================

add_filter('woocommerce_sale_flash', 'fevr_wc_sale_flash', 10, 3);

/**
 * Overwrite Sale Flash for porduct boxes
 * @param string $html
 * @param WP_Post_Object $post
 * @param WC_Product_Object $product
 * @return string
 */
function fevr_wc_sale_flash($html, $post, $product) {
	if(empty($product->regular_price)){
		return $html;
	}
	
	$discount = number_format(100-($product->sale_price/$product->regular_price)*100, 0);
	return '<span class="onsale"><span>' . esc_html(fevr__w('Sale!', 'fevr','woocommerce')) . ' -'.esc_html($discount).'%</span></span>';
}

//======================================================================
// WooCommerce Cart Remove Icon
//======================================================================

add_filter('woocommerce_cart_item_remove_link', 'fevr_wc_mini_cart_remove_icon');

/**
 * Overwrite defalunt remove cart icon with ion-icon ion-close
 * @param string $html
 * @return string
 */
function fevr_wc_mini_cart_remove_icon($html) {
	 return str_replace('&times;', '<i class="ion-close"></i>', $html);
}

//======================================================================
// WooCommerce Masonry Layout
//======================================================================

/**
 * Override Woocommerce Thumbnails for Masonry layout
 */
function fevr_woocommerce_template_loop_product_thumbnail(){
	global $fevr_wc_masonry_product_size;
	if (fevr_check_luvoption('woocommerce-product-box-style', 'wc-style-6', '!=')){
		echo fevr_woocommerce_get_product_thumbnail($fevr_wc_masonry_product_size);
	}
}

/**
 * Get product thumbnail for masonry
 */
function fevr_woocommerce_get_product_thumbnail( $size = 'shop_catalog', $deprecated1 = 0, $deprecated2 = 0 ) {
		global $post;

		$masonry_image_id = get_post_meta($post->ID, '_fevr_product_masonry_image', true);		
		if (!empty($masonry_image_id)){
			return wp_get_attachment_image($masonry_image_id, 'thumbnail');
		}
		else if ( has_post_thumbnail() ) {
			return get_the_post_thumbnail( $post->ID, $size );
		} elseif ( wc_placeholder_img_src() ) {
			return wc_placeholder_img( $size );
		}
}


//======================================================================
// Luv Like
//======================================================================


// Luv like ajax actions
add_action('wp_ajax_fevr_like', 'fevr_like');
add_action('wp_ajax_nopriv_fevr_like', 'fevr_like');

/**
 * Ajax function for luv like. 
 * Increase like count if user didn't like the post before
 * Decrease like count if user did like the post before
 * Print a json object which contains the current like count and the current event (like/unlike). 
 * Eg:
 * 		{
 * 			"like_count":12,
 * 			"like": 1
 * 		}
 * Use 1 for like event and -1 for unlike event. We will use this value to refresh all like buttons for the liked/unliked post on the current page.		
 */
function fevr_like(){
	$likes = isset($_COOKIE['luv-like']) ? (array)json_decode($_COOKIE['luv-like'], true) : array();
	$to_like = (int)$_POST['to-like'];
	$total_likes = get_post_meta($to_like, 'fevr_like_count', true);
	
	// Unlike
	if(in_array($to_like, $likes)){
		unset($likes[array_search($to_like, $likes)]);
		$like = -1;
	}
	// Like
	else{
		$likes[] = $to_like;
		$like = 1;
	}
	
	/*
	 * Count like/unlike
	 */
	
	// Prevent negative values
	$total_likes = ((int)$total_likes + $like >= 0 ? (int)$total_likes + $like : 0); 
	update_post_meta($to_like, 'fevr_like_count', $total_likes);
	
	// Create response JSON
	$response = json_encode(
		array(
			'like_count' => $total_likes,
			'like' => $like
		)
	);
	
	setcookie("luv-like", json_encode($likes), time()+(10 * 365 * 24 * 60 * 60), SITECOOKIEPATH);
	wp_die($response);
}

function fevr_get_like_icon($wrapper = true) {
	$wrapper_start = $wrapper_end = '';
	if ($wrapper){
		$wrapper_start	= '<div class="luv-social-buttons">';
		$wrapper_end	= '</div>';
	}
	$liked = (isset($_COOKIE['luv-like']) && in_array(get_the_ID(), (array)json_decode($_COOKIE['luv-like']), true) ? true :false);
	echo $wrapper_start . '<a href="#" class="luv-like'.($liked ? ' luv-liked' : '').'" data-to-like="'.get_the_ID().'"><i class="fa fa-heart'.($liked ? '' : '-o').'"></i><span class="luv-like-count" data-luv-like-post_id="'.get_the_ID().'"></span></a>' . $wrapper_end;
}


//======================================================================
// Page Header
//======================================================================


if(!function_exists('fevr_header')):
	/**
	 * This function print the header based on the current post id
	 * @param int $post_id
	 */
	function fevr_header() {
		global $post, $fevr_meta_fields, $custom_header, $fevr_header_title, $fevr_header_subtitle, $fevr_header_content, $fevr_header_background, $fevr_header_background_color, $fevr_header_video_mp4, $fevr_header_video_ogv, $fevr_header_video_embedded;

		$post_id = fevr_get_page_id();
		
		$fevr_navigation_borders = (fevr_check_luvoption('border-on-transparent-header', 1, '!=') && (!isset($fevr_meta_fields['page-header-nav-borders']) || $fevr_meta_fields['page-header-nav-borders'] != 1)) || isset($fevr_meta_fields['page-header-nav-borders']) && $fevr_meta_fields['page-header-nav-borders'] == 2 ? 'no-border' : '';
		$custom_header = false;
		$header_parallax = isset($fevr_meta_fields['page-header-parallax']) ? $fevr_meta_fields['page-header-parallax'] : '';
		$header_overlay = isset($fevr_meta_fields['page-header-overlay']) ? $fevr_meta_fields['page-header-overlay'] : '';
		$header_overlay_color = isset($fevr_meta_fields['page-header-overlay-color']) ? $fevr_meta_fields['page-header-overlay-color'] : '';
		$header_overlay_opacity = isset($fevr_meta_fields['page-header-overlay-color-opacity']) ? $fevr_meta_fields['page-header-overlay-color-opacity'] : '';
		$header_height = isset($fevr_meta_fields['page-header-height']) ? $fevr_meta_fields['page-header-height'] : '';
		$header_alignment = isset($fevr_meta_fields['page-header-alignment']) ? $fevr_meta_fields['page-header-alignment'] : '';
		$fevr_header_background = isset($fevr_meta_fields['page-header-bg']) ? $fevr_meta_fields['page-header-bg'] : '';
		$fevr_header_background_color = isset($fevr_meta_fields['page-header-bg-color']) ? $fevr_meta_fields['page-header-bg-color'] : '';
		$fevr_header_background_gradient_direction = isset($fevr_meta_fields['page-header-gradient-direction']) ? $fevr_meta_fields['page-header-gradient-direction'] : '';
		$fevr_header_background_gradient_1 = isset($fevr_meta_fields['page-header-gradient-color-1']) ? $fevr_meta_fields['page-header-gradient-color-1'] : '';
		$fevr_header_background_gradient_2 = isset($fevr_meta_fields['page-header-gradient-color-2']) ? $fevr_meta_fields['page-header-gradient-color-2'] : '';
		$header_font_color = isset($fevr_meta_fields['page-header-font-color']) ? $fevr_meta_fields['page-header-font-color'] : '';
		$fevr_header_title = isset($fevr_meta_fields['page-header-title']) ? $fevr_meta_fields['page-header-title'] : '';
		$fevr_header_title_font_family = isset($fevr_meta_fields['page-header-title-font-family']) ? $fevr_meta_fields['page-header-title-font-family'] : '';
		$fevr_header_title_font_size = isset($fevr_meta_fields['page-header-title-font-size']) ? $fevr_meta_fields['page-header-title-font-size'] : '';
		$fevr_header_title_line_height = isset($fevr_meta_fields['page-header-title-line-height']) ? $fevr_meta_fields['page-header-title-line-height'] : '';
		$fevr_header_title_resposive_font_size = isset($fevr_meta_fields['page-header-title-responsive-font-size']) ? 'true' : 'false';
		$fevr_header_title_font_weight = isset($fevr_meta_fields['page-header-title-font-weight']) ? $fevr_meta_fields['page-header-title-font-weight'] : '';
		$fevr_header_title_text_transform = isset($fevr_meta_fields['page-header-title-text-transform']) ? $fevr_meta_fields['page-header-title-text-transform'] : '';
		$fevr_header_title_effect = isset($fevr_meta_fields['page-header-title-effect']) ? $fevr_meta_fields['page-header-title-effect'] : '';
		$fevr_header_subtitle = isset($fevr_meta_fields['page-header-subtitle']) ? $fevr_meta_fields['page-header-subtitle'] : '';
		$fevr_header_subtitle_font_family = isset($fevr_meta_fields['page-header-subtitle-font-family']) ? $fevr_meta_fields['page-header-subtitle-font-family'] : '';
		$fevr_header_subtitle_font_size = isset($fevr_meta_fields['page-header-subtitle-font-size']) ? $fevr_meta_fields['page-header-subtitle-font-size'] : '';
		$fevr_header_subtitle_line_height = isset($fevr_meta_fields['page-header-subtitle-line-height']) ? $fevr_meta_fields['page-header-subtitle-line-height'] : '';
		$fevr_header_subtitle_resposive_font_size = isset($fevr_meta_fields['page-header-subtitle-responsive-font-size']) ? 'true' : 'false';
		$fevr_header_subtitle_font_weight = isset($fevr_meta_fields['page-header-subtitle-font-weight']) ? $fevr_meta_fields['page-header-subtitle-font-weight'] : '';
		$fevr_header_subtitle_text_transform = isset($fevr_meta_fields['page-header-subtitle-text-transform']) ? $fevr_meta_fields['page-header-subtitle-text-transform'] : '';
		$fevr_header_content = isset($fevr_meta_fields['page-header-content']) ? $fevr_meta_fields['page-header-content'] : '';
		$fevr_header_content_font_family = isset($fevr_meta_fields['page-header-content-font-family']) ? $fevr_meta_fields['page-header-content-font-family'] : '';
		$fevr_header_content_font_size = isset($fevr_meta_fields['page-header-content-font-size']) ? $fevr_meta_fields['page-header-content-font-size'] : '';
		$fevr_header_content_line_height = isset($fevr_meta_fields['page-header-content-line-height']) ? $fevr_meta_fields['page-header-content-line-height'] : '';
		$fevr_header_content_resposive_font_size = isset($fevr_meta_fields['page-header-content-responsive-font-size']) ? 'true' : 'false';
		$fevr_header_content_font_weight = isset($fevr_meta_fields['page-header-content-font-weight']) ? $fevr_meta_fields['page-header-content-font-weight'] : '';
		$hide_title = isset($fevr_meta_fields['page-header-hide-title']) ? $fevr_meta_fields['page-header-hide-title'] : '';
		$header_height_custom = isset($fevr_meta_fields['page-header-height-custom']) && !empty($fevr_meta_fields['page-header-height-custom']) ? $fevr_meta_fields['page-header-height-custom'] : '250';
		$parallax_layers = isset($fevr_meta_fields['page-header-parallax-layer-list']) ? $fevr_meta_fields['page-header-parallax-layer-list'] : '';
		$header_bg_type = isset($fevr_meta_fields['header-background-type']) ? $fevr_meta_fields['header-background-type'] : '';
		$fevr_header_video_mp4 = isset($fevr_meta_fields['header-video-mp4']) ? $fevr_meta_fields['header-video-mp4'] : '';
		$fevr_header_video_ogv = isset($fevr_meta_fields['header-video-ogv']) ? $fevr_meta_fields['header-video-ogv'] : '';
		$fevr_header_video_embedded = isset($fevr_meta_fields['header-video-embedded']) ? $fevr_meta_fields['header-video-embedded'] : '';
		$header_video_preview = isset($fevr_meta_fields['header-video-preview']) ? $fevr_meta_fields['header-video-preview'] : '';
		$header_filter = isset($fevr_meta_fields['page-header-filter']) ? $fevr_meta_fields['page-header-filter'] : '';
		$comments_feature = fevr_get_luvoption('blog-comments-feature');
		$show_comments = fevr_check_luvoption('blog-comments-feature', 1, '!=') && fevr_check_luvoption('blog-comments-meta', 'hide-on-single', '!=') && fevr_check_luvoption('blog-comments-meta', 'hide-on-both', '!=');
		$show_author = fevr_check_luvoption('blog-author-meta', 'hide-on-single', '!=') && fevr_check_luvoption('blog-author-meta', 'hide-on-both', '!=');
		$show_categories = fevr_check_luvoption('blog-categories-meta', 'hide-on-single', '!=') && fevr_check_luvoption('blog-categories-meta', 'hide-on-both', '!=');
		$show_date = fevr_check_luvoption('blog-date-meta', 'hide-on-single', '!=') && fevr_check_luvoption('blog-date-meta', 'hide-on-both', '!=');
		$content_full_width = isset($fevr_meta_fields['page-header-content-width']) && $fevr_meta_fields['page-header-content-width'] == 'full_width' ? 'container-fluid' : 'container';
		$breadcrumbs_meta = isset($fevr_meta_fields['page-breadcrumbs']) ? $fevr_meta_fields['page-breadcrumbs'] : 'default';
		$arrow = isset($fevr_meta_fields['header-mouse-icon']) ? $fevr_meta_fields['header-mouse-icon'] : '';
		$arrow_style = isset($fevr_meta_fields['header-mouse-icon-type']) ? $fevr_meta_fields['header-mouse-icon-type'] : '';
		$header_skin = (isset($fevr_meta_fields['page-header-skin']) && $fevr_meta_fields['page-header-skin'] != 'default') ? $fevr_meta_fields['page-header-skin'] : fevr_get_luvoption('header-skin', 'default');
		
		// Breadcrumbs
		
		// Use shortcode attributes
		if (fevr_is_luv_shortcode()){
			global $fevr_pagination_shortcode_atts, $fevr_meta_fields, $fevr_shortcode_post_type;
			$post_type = $fevr_shortcode_post_type;
		}
		// Use global settings
		else {
			$post_type = get_post_type();
		}
		switch($post_type) {
			case 'luv_portfolio':
		
				$breadcrumbs = fevr_get_luvoption('portfolio-breadcrumbs');
		
				break;
		
			case 'luv_collections':
		
				$breadcrumbs = fevr_get_luvoption('woocommerce-collections-breadcrumbs');
		
				break;
					
			case 'luv_ext_reviews':
		
				$breadcrumbs = fevr_get_luvoption('woocommerce-photo-reviews-breadcrumbs');
		
				break;
				
			case 'product':
			
				$breadcrumbs = fevr_get_luvoption('woocommerce-breadcrumbs');
			
				break;
				
			default:
		
				$breadcrumbs = fevr_get_luvoption('blog-breadcrumbs');
		}
		
		// Check meta field override for breadcrumbs

		$breadcrumbs = (($breadcrumbs == 1 && $breadcrumbs_meta != 'disabled') || $breadcrumbs_meta == 'enabled' ? true : false);
		
		$fevr_page_header_custom_styles = array();
		
		// Header Background
		
		if (!empty($fevr_header_background) && $header_bg_type == 'image'){
			$fevr_page_header_custom_styles[] = 'background-image: url('.$fevr_header_background.')';
		}
		
		if (!empty($header_video_preview) && $header_bg_type == 'video'){
			$fevr_page_header_custom_styles[] = 'background-image: url('.$header_video_preview.')';
		}
				
		if (!empty($fevr_header_background_color) && $header_bg_type != 'gradient'){
			$fevr_page_header_custom_styles[] = 'background-color: '.$fevr_header_background_color;
		}

		// Header Gradient
		
		if (!empty($fevr_header_background_gradient_direction) && !empty($fevr_header_background_gradient_1) && !empty($fevr_header_background_gradient_2) && $header_bg_type == 'gradient'){
			if($fevr_header_background_gradient_direction == 'left-to-right') {
				$fevr_page_header_custom_styles[] = 'background: '.$fevr_header_background_gradient_1.';
													background: -moz-linear-gradient(left, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													background: -webkit-gradient(left top, right top, color-stop(0%, '.$fevr_header_background_gradient_1.'), color-stop(100%, '.$fevr_header_background_gradient_2.'));
													background: -webkit-linear-gradient(left, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													background: -o-linear-gradient(left, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													background: -ms-linear-gradient(left, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													background: linear-gradient(to right, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\''.$fevr_header_background_gradient_1.'\', endColorstr=\''.$fevr_header_background_gradient_2.'\', GradientType=1 );';
			} elseif($fevr_header_background_gradient_direction == 'top-to-bottom') {
				$fevr_page_header_custom_styles[] = 'background: '.$fevr_header_background_gradient_1.';
													background: -moz-linear-gradient(top, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													background: -webkit-gradient(left top, left bottom, color-stop(0%, '.$fevr_header_background_gradient_1.'), color-stop(100%, '.$fevr_header_background_gradient_2.'));
													background: -webkit-linear-gradient(top, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													background: -o-linear-gradient(top, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													background: -ms-linear-gradient(top, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													background: linear-gradient(to bottom, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\''.$fevr_header_background_gradient_1.'\', endColorstr=\''.$fevr_header_background_gradient_2.'\', GradientType=0 );';
			
			} elseif($fevr_header_background_gradient_direction == 'left-top-to-right-bottom') {
				$fevr_page_header_custom_styles[] = 'background: '.$fevr_header_background_gradient_1.';
													background: -moz-linear-gradient(-45deg, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													background: -webkit-gradient(left top, right bottom, color-stop(0%, '.$fevr_header_background_gradient_1.'), color-stop(100%, '.$fevr_header_background_gradient_2.'));
													background: -webkit-linear-gradient(-45deg, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													background: -o-linear-gradient(-45deg, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													background: -ms-linear-gradient(-45deg, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													background: linear-gradient(135deg, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\''.$fevr_header_background_gradient_1.'\', endColorstr=\''.$fevr_header_background_gradient_2.'\', GradientType=1 );';
			
			} elseif($fevr_header_background_gradient_direction == 'left-bottom-to-right-top') {
				$fevr_page_header_custom_styles[] = 'background: '.$fevr_header_background_gradient_1.';
													background: -moz-linear-gradient(45deg, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													background: -webkit-gradient(left bottom, right top, color-stop(0%, '.$fevr_header_background_gradient_1.'), color-stop(100%, '.$fevr_header_background_gradient_2.'));
													background: -webkit-linear-gradient(45deg, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													background: -o-linear-gradient(45deg, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													background: -ms-linear-gradient(45deg, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													background: linear-gradient(45deg, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\''.$fevr_header_background_gradient_1.'\', endColorstr=\''.$fevr_header_background_gradient_2.'\', GradientType=1 );';
			
			} elseif($fevr_header_background_gradient_direction == 'ellipse') {
				$fevr_page_header_custom_styles[] = 'background: '.$fevr_header_background_gradient_1.';
													background: -moz-radial-gradient(center, ellipse cover, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%, '.$fevr_header_background_gradient_1.'), color-stop(100%, '.$fevr_header_background_gradient_2.'));
													background: -webkit-radial-gradient(center, ellipse cover, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													background: -o-radial-gradient(center, ellipse cover, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													background: -ms-radial-gradient(center, ellipse cover, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													background: radial-gradient(ellipse at center, '.$fevr_header_background_gradient_1.' 0%, '.$fevr_header_background_gradient_2.' 100%);
													filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\''.$fevr_header_background_gradient_1.'\', endColorstr=\''.$fevr_header_background_gradient_2.'\', GradientType=1 );';
			
			}
		}
		
		// Header Typography
		$fevr_page_header_title_fonts_styles = apply_filters( FEVR_VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, '', 'page_header_title', array(
				'font_family' => $fevr_header_title_font_family,
				'font_size' => $fevr_header_title_font_size,
				'line_height' => $fevr_header_title_line_height,
				'responsive_font_size' => $fevr_header_title_resposive_font_size,
				'font_weight' => $fevr_header_title_font_weight,
				'text_transform' => $fevr_header_title_text_transform
		));

		$fevr_page_header_subtitle_fonts_styles = apply_filters( FEVR_VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, '', 'page_header_subtitle', array(
				'font_family' => $fevr_header_subtitle_font_family,
				'font_size' => $fevr_header_subtitle_font_size,
				'line_height' => $fevr_header_subtitle_line_height,
				'responsive_font_size' => $fevr_header_subtitle_resposive_font_size,
				'font_weight' => $fevr_header_subtitle_font_weight,
				'text_transform' => $fevr_header_subtitle_text_transform
		));
		
		
		$fevr_page_header_content_fonts_styles = apply_filters( FEVR_VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, '', 'page_header_content', array(
				'font_family' => $fevr_header_content_font_family,
				'font_size' => $fevr_header_content_font_size,
				'line_height' => $fevr_header_content_line_height,
				'responsive_font_size' => $fevr_header_content_resposive_font_size,
				'font_weight' => $fevr_header_content_font_weight,
		));
		
		
		// Custom responsive header height
		$header_height_class = '';
		if (isset($fevr_meta_fields['page-responsive-header-height-custom']) && $fevr_meta_fields['page-responsive-header-height-custom'] == 'enabled' && isset($fevr_meta_fields['page-responsive-header-heights']) && !empty($fevr_meta_fields['page-responsive-header-heights'])){
			$header_height = 'responsive';
			$responsive_header_heights = json_decode(str_replace("'",'"',$fevr_meta_fields['page-responsive-header-heights']), true);
			
			$header_height_class = implode(' ',array(
				fevr_enqueue_inline_css(array(
						'style'		=> 'height: ' . $responsive_header_heights['desktop'] . 'px !important;max-height: none !important;',
						'media'		=> '(min-width: 1480px)'
				)),
				fevr_enqueue_inline_css(array(
						'style'		=> 'height: ' . $responsive_header_heights['laptop'] . 'px !important;max-height: none !important;',
						'media'		=> '(min-width: 992px) and (max-width: 1479px)'
				)),
				fevr_enqueue_inline_css(array(
						'style'		=> 'height: ' . $responsive_header_heights['tablet-landscape'] . 'px !important;max-height: none !important;',
						'media'		=> '(min-width: 768px) and (max-width: 991px)'
				)),
				fevr_enqueue_inline_css(array(
						'style'		=> 'height: ' . $responsive_header_heights['tablet-portrait'] . 'px !important;max-height: none !important;',
						'media'		=> '(min-width: 460px) and (max-width: 767px)'
				)),
				fevr_enqueue_inline_css(array(
						'style'		=> 'height: ' . $responsive_header_heights['mobile'] . 'px !important;max-height: none !important;',
						'media'		=> '(max-width: 459px)'
				))
			));				
		}
		// Non-responsive custom header height
		else if ($header_height == 'custom' && !empty($header_height_custom)) {
				$header_height_class = fevr_enqueue_inline_css(array('style'=> 'height: '.$header_height_custom.'px; '));
		}
		
		
		// If we show the pagination in the header we should add a custom class for padding
		
		if(get_post_type() != 'page') {
			switch(get_post_type()) {
				case 'luv_portfolio':
				
					$fevr_pagination_position = fevr_get_luvoption('portfolio-pagination-position');
					$fevr_pagination_hide =  fevr_get_luvoption('hide-portfolio-pagination', 0);
					
					break;
				
				case 'luv_collections':
				
					$fevr_pagination_position = fevr_get_luvoption('woocommerce-collections-pagination-position');
					$fevr_pagination_hide =  fevr_get_luvoption('woocommerce-hide-collections-pagination', 0);
					
					break;
					
				default:
				
					$fevr_pagination_position = fevr_get_luvoption('blog-pagination-position');
					$fevr_pagination_hide =  fevr_get_luvoption('hide-blog-pagination', 0);
				}
			
			$header_extra_classes = ($fevr_pagination_hide == 0 && $fevr_pagination_position == 'header' ? 'header-pagination' : '');
		}
		else{
			$header_extra_classes = '';
		}
		
// If the user used the custom fields (title, subtitle, content) we add the related classes, attributes
if( !fevr_is_headless() && !fevr_is_mobile() && (!empty($fevr_header_title) || !empty($fevr_header_subtitle) || !empty($fevr_header_content) || !empty($fevr_header_background) || !empty($fevr_header_background_color) || !empty($fevr_header_video_mp4) || !empty($fevr_header_video_ogv) || !empty($fevr_header_video_embedded))):
		$custom_header = true;
		
		// Hide WooCommerce page title on archive
		if (function_exists('is_shop') && is_shop()){
			add_filter('woocommerce_show_page_title', '__return_false');
		}
?>
<div id="page-header-wrapper" <?php echo ($header_height == 'full_height' ? 'data-full-height-header="true"' : 'data-full-height-header="false"'); ?> class="<?php fevr_echo($header_height_class)?>">
<div id="page-header-custom" <?php echo (!empty($header_parallax) ? 'data-parallax-header="'.esc_attr($header_parallax).'"' : ''); ?> <?php echo ($header_height == 'full_height' ? 'data-full-height-header="true" ' : ''); ?> class="<?php fevr_echo($header_height_class)?><?php fevr_echo($header_filter);?> <?php echo ($header_bg_type == 'video' ? esc_attr($header_bg_type) : ''); ?> <?php echo !empty($header_video_preview)  ? 'video-preview-image' : ''; ?> <?php echo (!empty($header_alignment) ? 'is-'.esc_attr($header_alignment).'' : ''); ?> <?php echo fevr_enqueue_inline_css(array('style' => implode(';', $fevr_page_header_custom_styles)));?>" data-skin="<?php echo $header_skin; ?>">

		<?php
			// Overlay
			echo ($header_overlay == 'enabled' && !empty($header_overlay_color) ? '<div id="page-header-overlay" class="'.fevr_enqueue_inline_css(array('style'=> 'background-color: '.$header_overlay_color.'; '.(!empty($header_overlay_opacity) ? 'opacity: '.$header_overlay_opacity.';' : 'opacity: 0.8;'))).'"></div>' : '')	
		?>
		<?php
			// Parallax scene (layers)
			if(!empty($parallax_layers)):
			// Enqueue parallax jquery plugin 
			wp_enqueue_script( 'fevr-parallax', trailingslashit(get_template_directory_uri()) . '/js/min/jquery.parallax-min.js', array('jquery'), FEVR_THEME_VER, true );
		?>
			<ul class="parallax-scene">
				<?php
					$i = 0;
					foreach($parallax_layers as $layer) {
						$i++;
						echo '<li class="layer" data-depth="'.($i > 10 ? 1 : $i/10).'"><div class="parallax-layer-img '.fevr_enqueue_inline_css(array('style' => 'background-image:url('.$layer.');')).'"></div></li>';
					}
				?>
			</ul>
		<?php endif; ?>
		
		<?php
			// Video background
			if($header_bg_type == 'video' && (!empty($fevr_header_video_mp4) || !empty($fevr_header_video_ogv))):
		?>
				<video preload="auto" autoplay="autoplay" loop="loop" muted="muted">
					<?php if(!empty($fevr_header_video_mp4)): ?>
					<source src="<?php echo esc_url($fevr_header_video_mp4); ?>" type="video/mp4">
					<?php endif; ?>
					
					<?php if(!empty($fevr_header_video_ogv)): ?>
					<source src="<?php echo esc_url($fevr_header_video_ogv); ?>" type="video/ogg">
					<?php endif; ?>
					
					<?php esc_html_e('Your browser does not support the video tag.', 'fevr'); ?>
				</video>
		<?php
			// Embedded background
			elseif($header_bg_type == 'video' && !empty($fevr_header_video_embedded)):
				if(!filter_var($fevr_header_video_embedded, FILTER_VALIDATE_URL) === false):
		?>
			<iframe width="560" height="315" src="<?php echo esc_url(fevr_url_to_embedded($fevr_header_video_embedded)); ?>" frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen></iframe>
		<?php
				else:
					echo fevr_kses($fevr_header_video_embedded);
				endif;
		?>
		<?php endif; ?>
		<div class="<?php fevr_echo(esc_attr($content_full_width),''); ?><?php fevr_echo(esc_attr($header_height_class));?>" <?php echo ($header_height == 'full_height' ? 'data-full-height-header="true"' : ''); ?>>

			<div id="page-header-inner" class="<?php echo ($fevr_navigation_borders != 'no-border' ? 'relative-center' : ''); ?> <?php echo (isset($header_font_color) && !empty($header_font_color) ? fevr_enqueue_inline_css(array('style' => 'color: '.$header_font_color, 'child' => array(' a'=>'color: '.$header_font_color, ' .page-header-title'=>'color: '.$header_font_color.' !important', ' i'=>'color: '.$header_font_color.' !important'))) : ''); ?> <?php echo !empty($header_extra_classes) ? $header_extra_classes : ''; ?>">
				<div class="page-header-animation <?php echo ($fevr_header_title_effect != 'none'  && !empty($fevr_header_title_effect) ? $fevr_header_title_effect : 'fade-in'); ?>">
						
				<?php
					// If it's a post we have to display the meta section
					if(is_single() && get_post_type() == 'post'):
				?>
					<div class="page-header-meta">
						<?php
							if($show_author) {
								esc_html_e('Posted by ', 'fevr'); the_author_posts_link();
							}
						?>
						
						<?php
							if($show_date) {
								echo '<span>';
								the_time(get_option('date_format'));
								echo '</span>';
							}
						?>
						
						<?php
							if($show_categories) {
								echo '<span>';
								the_category(', ', '');
								echo '</span>';
							}
						?>
						
						<?php if($show_comments): ?>
							<span><a href="<?php the_permalink(); ?>" title="<?php esc_html_e('Comments of ', 'fevr'); ?> <?php the_title(); ?>"><?php comments_number( esc_html__('No Comments', 'fevr'), esc_html__('One Comment', 'fevr'), esc_html__('% Comments', 'fevr') ); ?></a></span>
						<?php endif; ?>
					</div>
				<?php endif; ?>
					
					<?php
						// Typewriter animation
						if($fevr_header_title_effect == 'typewriter') {
							// Enque typed jquery plugin
							wp_enqueue_script( 'ferv-typed', trailingslashit(get_template_directory_uri()) . '/js/min/typed-min.js', array('jquery'), FEVR_THEME_VER, true );
						}
					?>
					
					<?php if($hide_title != 'enabled'): ?>
					<h1 class="page-header-title<?php fevr_echo($fevr_page_header_title_fonts_styles )?>">
						<?php
							if(!empty($fevr_header_title)) {
								echo fevr_kses($fevr_header_title);
								
								if(is_author())	{
									echo ': ' . get_the_author();
								} else if(is_category()) {
									echo ': ' .single_cat_title('', false);
								} else if(is_tag()) {
									echo ': ' .single_tag_title('', false);
								} else if(is_date()) {
									if ( is_day() ) {
										echo ': ' . get_the_date();
									} elseif ( is_month() ) {
										echo ': ' . get_the_date( esc_html_e('F Y', 'fevr'));
									} elseif ( is_year() ) {
										echo ': ' . get_the_date( esc_html_e('Y', 'fevr'));
									} 
								}
							}
							else{
								if(is_author())	{
									printf( esc_html__('Posts by %s', 'fevr'), get_the_author());
								} else if(is_category()) {
									printf( esc_html__('Category Archives: %s', 'fevr'), single_cat_title('', false));
								} else if(is_tag()) {
									printf( esc_html__('Tag Archives: %s', 'fevr'), single_tag_title('', false));
								} else if(is_date()) {
									if ( is_day() ) {
										printf(esc_html__( 'Daily Archives: %s', 'fevr'), get_the_date());
									} elseif ( is_month() ) {
										printf(esc_html__('Monthly Archives: %s', 'fevr'), get_the_date( esc_html_e('F Y', 'fevr')));
									} elseif ( is_year() ) {
										printf(esc_html__('Yearly Archives: %s', 'fevr'), get_the_date( esc_html_e('Y', 'fevr')));
									} else {
										esc_html_e('Archives', 'fevr');
									}
								}
								else {
									echo get_the_title($post_id);
								}
							}
						?>
					</h1>
					<?php endif; ?>
					
					<?php if(!empty($fevr_header_subtitle)): ?>
					<div class="page-header-subtitle<?php fevr_echo($fevr_page_header_subtitle_fonts_styles )?>">
						<?php echo fevr_kses($fevr_header_subtitle);?>
					</div>
					<?php endif; ?>
	
					<?php if(!empty($fevr_header_content)): ?>
					<div class="page-header-content<?php fevr_echo($fevr_page_header_content_fonts_styles )?>">
						<?php echo fevr_kses(do_shortcode($fevr_header_content)); ?>
					</div>
					<?php endif; ?>
					
					<?php get_template_part('luvthemes/luv-templates/header-pagination'); ?>
				</div>
			</div>
			<?php if($arrow == 'enabled' && !empty($arrow_style)): ?>
				<?php if($arrow_style == 'arrow'): ?>
					<div class="header-scroll header-scroll-arrow">
						<i class="ion-ios-arrow-down"></i>
					</div>
				<?php elseif($arrow_style == 'arrow2'): ?>
					<div class="header-scroll header-scroll-arrow header-scroll-arrow-style2">
						<i class="ion-ios-arrow-down"></i>
					</div>
				<?php else: ?>
					<div class="header-scroll">
						<span class="header-scroll-mouse">
							<span class="fa fa-angle-down"></span>
						</span>
						<span class="header-scroll-caption"><?php esc_html_e('Scroll', 'fevr'); ?></span>
					</div>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
</div>

<?php elseif(($hide_title != 'enabled' || (is_category() || is_date() || is_author())) && !($post_id == 0 && is_home()) && (!function_exists('is_woocommerce') || !is_woocommerce())): ?>

<div class="<?php echo esc_attr($content_full_width); ?>">
	<div id="page-header-default" class="<?php fevr_echo(esc_attr($header_extra_classes),'');?><?php fevr_echo(($breadcrumbs ? 'has-breadcrumbs' : ''));?>">
		<?php if(is_single() && get_post_type() == 'post'): ?>
		<div class="page-header-meta">
			<?php
				if($show_author) {
					echo '<span>';
					esc_html_e('Posted by ', 'fevr'); the_author_posts_link();
					echo '</span>';
				}
			?>
			
			<?php
				if($show_date) {
					echo '<span>';
					the_time(get_option('date_format'));
					echo '</span>';
				}
			?>
			
			<?php
				if($show_categories) {
					echo '<span>';
					the_category(', ', '');
					echo '</span>';
				}
			?>
			
			<?php if($show_comments): ?>
				<span><a href="<?php the_permalink(); ?>" title="<?php esc_html_e('Comments of ', 'fevr'); ?> <?php the_title(); ?>"><?php comments_number( esc_html__('No Comments', 'fevr'), esc_html__('One Comment', 'fevr'), esc_html__('% Comments', 'fevr') ); ?></a></span>
			<?php endif; ?>
		</div>
		<?php endif; ?>
		
		<h1 class="page-header-title">
		<?php
			if(is_author())	{
				printf( esc_html__('Posts by %s', 'fevr'), get_the_author());
			} else if(is_category()) {
				printf( esc_html__('Category Archives: %s', 'fevr'), single_cat_title('', false));
			} else if(is_tag()) {
				printf( esc_html__('Tag Archives: %s', 'fevr'), single_tag_title('', false));
			} else if(is_date()){
				if ( is_day() ) {
					printf(esc_html__( 'Daily Archives: %s', 'fevr'), get_the_date());
				} elseif ( is_month() ) {
					printf(esc_html__('Monthly Archives: %s', 'fevr'), get_the_date( esc_html_e('F Y', 'fevr')));
				} elseif ( is_year() ) {
					printf(esc_html__('Yearly Archives: %s', 'fevr'), get_the_date( esc_html_e('Y', 'fevr')));
				} else {
					esc_html_e('Archives', 'fevr');
				}
			} else {
				echo get_the_title($post_id);
			}
		?>
		</h1>
		<?php get_template_part('luvthemes/luv-templates/header-pagination'); ?>
	</div>
</div>
<?php endif;
	
	if(!is_front_page() && !is_home()):
?>
	<?php if ($breadcrumbs):?>
		<div class="<?php echo ($content_full_width == 'container-fluid' || $custom_header ? 'container-fluid' : 'container'); ?>">
			<div class="luv-breadcrumbs <?php echo (!$custom_header ? 'has-margin' : ''); ?> <?php echo ($custom_header || $content_full_width == 'container-fluid' ? 'luv-breadcrumbs-bg' : ''); ?>">
				<div class="l-grid-row">
					<div class="l-grid-6">
						<h4 class="luv-breadcrumbs-title">
						<?php echo get_the_title(); ?>
						</h4>
					</div>
					<div class="l-grid-6 is-right">
						<?php fevr_breadcrumbs(); ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif?>
<?php
	endif;
}

endif;

//======================================================================
// Template Functions
//======================================================================

add_action('fevr_body_attributes', 'fevr_body_attributes');

/**
 * Return with body tag attributes
 */
function fevr_body_attributes(){
		global $fevr_meta_fields;
	
		// Header transparency
		$page_header_transparency	= (isset($fevr_meta_fields['page-header-transparency']) ? $fevr_meta_fields['page-header-transparency'] : 0);
		$is_transparent				= !fevr_is_headless() && !fevr_is_mobile() && (((fevr_check_luvoption('transparent-header', 1) && $page_header_transparency != 2) || $page_header_transparency == 1) && fevr_check_luvoption('header-layout', 'nav-under-logo', '!='));		
	
		$attr = array();
		
		// Layout
		$attr[] = 'data-layout="' . esc_attr(fevr_get_luvoption('page-layout')).'"';
		
		// Header Position
		$attr[] = 'data-header-position="' . esc_attr(fevr_get_luvoption('header-position')).'"';
		
		// Full width header
		if (fevr_check_luvoption('header-full-width', 1)){
			$attr[] = 'data-header-layout="full-width"';
		}
		
		// Header Layout
		if (fevr_check_luvoption('header-layout', 'default', '!=') && fevr_check_luvoption('header-position', 'left', '!=')){
			$attr[] = 'data-header-layout-style="'.esc_attr(fevr_get_luvoption('header-layout')).'"';
		}
		
		// Navigation Position
		if (fevr_check_luvoption('header-nav-position', 'right', '!=')){
			$attr[] = 'data-header-nav-position="' . esc_attr(fevr_get_luvoption('header-nav-position')).'"';
		}
		
		// Mobile Navigation Position 
		if (fevr_check_luvoption('mobile-navigation-position', 'default', '!=')){
			$attr[] = 'data-mobile-nav-position="' . esc_attr(fevr_get_luvoption('mobile-navigation-position')).'"';
		}
		
		// Header Transparency
		if ($is_transparent){
			$attr[] = 'data-transparent-header="true"';
		}

		// Navigation Transparency
		$attr[] = (fevr_check_luvoption('transparent-navigation', 1) ? ' data-transparent-menu="true"' : 'data-transparent-menu="false"');
		
		// Top bar
		$attr[] = (fevr_check_luvoption('top-bar', 1) && (!isset($fevr_meta_fields['page-hide-top-bar']) || $fevr_meta_fields['page-hide-top-bar'] != 'enabled') ? ' data-top-bar="true"' : '');
		
		// Off canvas menu
		if(fevr_check_luvoption('off-canvas-menu', 1)){
			$attr[] = 'data-off-canvas-menu="true"';
		}
		
		// Sticky header
		if(fevr_check_luvoption('header-sticky', 1)){
			$attr[] = 'data-sticky-header="true"';
			// Sticky Header Type
			$attr[] = 'data-sticky-header-type="' .str_replace('header-sticky-','',fevr_get_luvoption('header-sticky-type')) . '"';
		}
				
		// Layout whitespace
		if (fevr_check_luvoption('layout-whitespace', 1)) {
			$attr[] = 'data-whitespace="true"';
		}
		
		// Nice scroll
		if (fevr_check_luvoption('nice-scroll', 1)){
			$attr[] = 'data-nice-scroll="true"';
		}
		
		// One page navigation
		if (fevr_check_luvoption('one-page-navigation', 1)){
			$attr[] = 'data-one-page-navigation="true"';
		}
		
		// Under the Rug Footer effect
		if (fevr_check_luvoption('footer-under-the-rug', 1)){
			$attr[] = 'data-footer-under-the-rug="true"';
		}
		
		// Midnight JS
		if(fevr_check_luvoption('automatic-header-skin', 1)){
			$attr[] = 'data-auto-header-skin="true"';
		}
		
		if (isset($fevr_meta_fields['page-promo-sliding-content']) && $fevr_meta_fields['page-promo-sliding-content'] == 'enabled'){
			$attr[] = 'data-promo-sliding-content="true"';
		}
		
		
		
		echo implode(' ',$attr);
} 

add_action('fevr_custom_after_body_html', 'fevr_custom_after_body_html');

/**
 * Print Custom After Body HTML
 */
function fevr_custom_after_body_html(){
	echo fevr_get_luvoption('custom-after-body-html');
}

add_action('fevr_loader_animation', 'fevr_loader_animation');

/**
 * Print loading animation
 */
function fevr_loader_animation(){
	if(fevr_check_luvoption('loading-animation', 1) && !fevr_is_headless() && !fevr_is_mobile() && !fevr_is_pajax()){
		echo '<div id="loader-overlay"><div class="loader"><svg class="loader-circular" viewBox="25 25 50 50"><circle class="loader-path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div></div>';
	}	
}

add_action('fevr_header_search', 'fevr_header_search');

/**
 * Print header search filter
 */
function fevr_header_search(){
	if (fevr_check_luvoption('header-search', 1) && !fevr_is_headless() && !fevr_is_mobile()){
		echo '<div id="search-bar-overlay">'.
			 '<div class="search-bar-trigger"><i class="ion-close"></i></div>'.
			 '	<div id="search-bar">'.
			 '	<div class="container">';
					if(fevr_check_luvoption('search-filter', 1)){
						echo '<ul id="search-filter-container">'.
							 '	<li><input type="radio" name="search-filter-term" id="search-filter-all" checked><label for="search-filter-all">'.esc_html__('All', 'fevr').'</label></li>'.
							 	fevr_get_search_filter().
							 '</ul>';
					}
					echo '<div id="search-bar-inner">'.
							get_search_form(false).
						'</div>'.
					'</div>'.	
			'</div>'.
		'</div>';
	}
}

add_action('fevr_top_bar', 'fevr_top_bar');

/**
 * Print Top Bar
 */
function fevr_top_bar(){
	global $fevr_meta_fields;
	if(fevr_check_luvoption('top-bar', 1) && !fevr_is_headless() && !fevr_is_mobile() && (!isset($fevr_meta_fields['page-hide-top-bar']) || $fevr_meta_fields['page-hide-top-bar'] != 'enabled')){
		echo '<aside id="top-bar"'.(fevr_check_luvoption('top-bar-hide-on-small', 1) ? ' class="hidden-xs hidden-s"' : '') . '><div class="container">';
			if (fevr_check_luvoption('top-bar-content', '', '!=')){
				echo '<div id="top-bar-content">' . do_shortcode(fevr_get_luvoption('top-bar-content')) . '</div>'; 
			}
			echo (fevr_check_luvoption('top-bar-close', 1) ? '<div id="top-bar-close"><i class="ion-close"></i></div>' : ''); 
			echo '<ul id="top-bar-icons">';
				if (fevr_check_luvoption('top-bar-enable-social-icons',1)){
					fevr_print_social_icons('top-bar-social-media');
				}
				if(fevr_check_luvoption('woocommerce-cart-icon-top-bar', 1) && class_exists('woocommerce')){
					echo '<li><a href="'.esc_url(WC()->cart->get_cart_url()).'" title=""><i class="ion-bag"></i></a></li>';
				}
				if(fevr_check_luvoption('off-canvas-menu', 1) && fevr_check_luvoption('off-canvas-menu-btn-tb', 1)){
					echo '<li class="off-canvas-menu-trigger"><a href="#"><i class="ion-ios-keypad"></i></a></li>';
				}
			echo '</ul><nav id="top-bar-menu">';
			if(has_nav_menu('theme_top_navigation')){
				wp_nav_menu( array('theme_location' => 'theme_top_navigation', 'container' => false, 'depth' => -1, 'walker' => new Fevr_Mega_Menu_Frontend())); 
			}
			echo '</nav>';
			if (fevr_check_luvoption('top-bar-display-wpml', 1)){
				do_action('icl_language_selector');
			}
		echo '</div>'.
			 '</aside>';
	}
}

add_action('fevr_main_header_outer_start', 'fevr_main_header_outer_start');

/**
 * Print wrapper to keep the distance between content and header
 */
function fevr_main_header_outer_start(){
	global $fevr_options, $fevr_meta_fields;
	
	// Header transparency
	$page_header_transparency	= (isset($fevr_meta_fields['page-header-transparency']) ? $fevr_meta_fields['page-header-transparency'] : 0);
	$is_transparent				= !fevr_is_headless() && !fevr_is_mobile() && (((fevr_check_luvoption('transparent-header', 1) && $page_header_transparency != 2) || $page_header_transparency == 1) && fevr_check_luvoption('header-layout', 'nav-under-logo', '!='));
	
	echo (!$is_transparent && fevr_check_luvoption('header-position', 'left', '!=') ? '<div id="main-header-outer" class="'.(isset($fevr_options['header-height']['height']) && (int)$fevr_options['header-height']['height'] > 0 && (!isset($fevr_options['header-layout']) || $fevr_options['header-layout'] != 'nav-under-logo') ? fevr_enqueue_inline_css(array('parent' => 'html #l-wrapper-inner ', 'style' => 'height: '. $fevr_options['header-height']['height'], 'media' => '(min-width: 769px)')) : '').'">' : ''); 
}

add_action('fevr_main_header_outer_end', 'fevr_main_header_outer_end');

/**
 * Print wrapper to keep the distance between content and header
 */
function fevr_main_header_outer_end(){
	global $fevr_options, $fevr_meta_fields;

	// Header transparency
	$page_header_transparency	= (isset($fevr_meta_fields['page-header-transparency']) ? $fevr_meta_fields['page-header-transparency'] : 0);
	$is_transparent				= !fevr_is_headless() && !fevr_is_mobile() && (((fevr_check_luvoption('transparent-header', 1) && $page_header_transparency != 2) || $page_header_transparency == 1) && fevr_check_luvoption('header-layout', 'nav-under-logo', '!='));

	echo (!$is_transparent && fevr_check_luvoption('header-position', 'left', '!=') ? '</div>' : '');
}

add_action('fevr_mobile_nav', 'fevr_mobile_nav');

/**
 * Print mobile navigation in header
 */
function fevr_mobile_nav(){
	global $fevr_meta_fields;

	if(fevr_check_luvoption('overlay-navigation-only-nav', 1, '!=')) {
		echo '<nav id="mobile-nav">'.
			 '	<div id="mobile-nav-inner">'.
			 '		<a href="#" class="mobile-nav-trigger"><i class="ion-close"></i></a>';
		if (isset($fevr_meta_fields['custom-nav-menu']) && !empty($fevr_meta_fields['custom-nav-menu'])){
					echo '<ul>';
					wp_nav_menu( array('menu' => $fevr_meta_fields['custom-nav-menu'], 'items_wrap' => '%3$s', 'container' => false, 'link_after' => '<span class="menu-indicator"><i class="ion-chevron-down"></i></span>', 'depth' => 0, 'walker' => new Fevr_Mega_Menu_Frontend() ));
					if (fevr_check_luvoption('header-search',1)){
						echo '<li><a href="#" class="mobile-nav-search-trigger"><i class="ion-search"></i> '.esc_html__('Search', 'fevr').'</a></li>';
					}
					echo '</ul>';
		}
		else if (fevr_is_headless()){
					echo '<ul>';
					wp_nav_menu( array('theme_location' => 'woocommerce_headless', 'items_wrap' => '%3$s', 'container' => false, 'link_after' => '<span class="menu-indicator"><i class="ion-chevron-down"></i></span>', 'depth' => 0, 'walker' => new Fevr_Mega_Menu_Frontend() ));
					echo '</ul>';
		}
		else if (has_nav_menu('theme_main_navigation') || has_nav_menu('theme_main_mobile_navigation')){
					if(has_nav_menu('theme_main_mobile_navigation')) {
						$nav_location = 'theme_main_mobile_navigation';
					} else {
						$nav_location = 'theme_main_navigation';
					}
					
					echo '<ul>';
					wp_nav_menu( array('theme_location' => $nav_location, 'items_wrap' => '%3$s', 'container' => false, 'link_after' => '<span class="menu-indicator"><i class="ion-chevron-down"></i></span>', 'depth' => 0, 'walker' => new Fevr_Mega_Menu_Frontend() ));
					if (fevr_check_luvoption('header-search', 1)){
						echo '<li><a href="#" class="mobile-nav-search-trigger"><i class="ion-search"></i> '.esc_html__('Search', 'fevr').'</a></li>';
					}
					echo '</ul>';
		}
		
		if (fevr_check_luvoption('header-enable-social-media', 1) && fevr_check_luvoption('header-layout', 'centered-logo','!=') && fevr_check_luvoption('header-social-media','','!=')){
				echo '<ul class="social-media-icons">';
				fevr_print_social_icons('header-social-media');			
				echo '</ul>';
		}
				
		echo '<div id="mobile-nav-search">';
					if (fevr_check_luvoption('header-search', 1)){
						echo '<a href="#" class="mobile-nav-search-trigger"><i class="ion-ios-arrow-left"></i></a>';
						get_search_form();
					}
					echo '<span class="mobile-nav-search-title">'.esc_html__('Quick Links', 'fevr').'</span>';
					if (fevr_is_headless()){
						wp_nav_menu( array('theme_location' => 'woocommerce_headless', 'container' => false, 'menu_class' => 'nav-menu', 'link_before' => '<span>', 'link_after' => '</span><span class="menu-indicator"><i class="ion-chevron-right"></i></span>', 'depth' => 0, 'walker' => new Fevr_Mega_Menu_Frontend()));
					}
					else{
						wp_nav_menu( array('theme_location' => 'mobile_quick_navigation', 'container' => false));
					}
		echo '</div>'.
			'</div>'.
		'</nav>';
	}
}

add_action('fevr_overlay_nav', 'fevr_overlay_nav');

/**
 * Print overlay navigation in header
 */
function fevr_overlay_nav(){
	if(fevr_check_luvoption('overlay-navigation', 1) && !fevr_is_headless() && !fevr_is_mobile() ){
		get_template_part('luvthemes/luv-templates/overlay-navigation');
	}
}

add_action('fevr_overlay_nav_content', 'fevr_overlay_nav_content');

/**
 * Render Overlay navigation content
 */
function fevr_overlay_nav_content(){
	if(fevr_check_luvoption('overlay-navigation-only-nav', 1, '!=')){
		$overlay_columns = fevr_get_luvoption('overlay-navigation-columns',3);
						echo '<div class="l-grid-row">';
							for($i = 1; $i <= $overlay_columns; $i++){
								echo '<div class="l-grid-'.(12/$overlay_columns).'">';
									if ( fevr_func_exists('dynamic_sidebar') && is_active_sidebar('overlay-nav-widgets'.($i > 1 ? '-'.$i : '')) && dynamic_sidebar('overlay-nav-widgets'.($i > 1 ? '-'.$i : '')) ) {
										// Do the widgets
									} else if (current_user_can('manage_options')){
											echo '<div class="overlay-nav-widget widget">'.			
												 '<h4 class="widget-title">'.esc_html__('Overlay Navigation Widget Area', 'fevr') . ' '. (int)$i.'</h4>'.
												 '<p class="no-widgets"><a href="'.esc_url(admin_url('widgets.php')).'">'.esc_html__('Click here to assign widgets.', 'fevr').'></a></p>'.
												 '</div>';
									}
								echo '</div>';
							}
						echo '</div>';
	}
	else{	
		wp_nav_menu( array('theme_location' => 'theme_main_navigation', 'container' => 'nav', 'container_id' => 'overlay-nav-container', 'menu_class' => 'nav-menu-overlay', 'depth' => 0));
				
	}
}

add_action('fevr_off_canvas_nav', 'fevr_off_canvas_nav');

/**
 * Print off-canvas navigation in header
 */
function fevr_off_canvas_nav(){
	if(fevr_check_luvoption('off-canvas-menu', 1) && !fevr_is_headless() && !fevr_is_mobile()){
		get_template_part('luvthemes/luv-templates/off-canvas-navigation');
	}
}

add_action('fevr_off_canvas_nav_content', 'fevr_off_canvas_nav_content');

/**
 * Render Off-Canvas navigation content
 */
function fevr_off_canvas_nav_content(){
	if ( function_exists('dynamic_sidebar') && is_active_sidebar('off-canvas-menu-widgets') && dynamic_sidebar('off-canvas-menu-widgets') ) {
		// Do the widgets
	}
	else if (current_user_can('manage_options')){
		echo '<div class="off-canvas-menu-widget widget">'.			
			'<h4 class="widget-title">'.esc_html__('Off-Canvas Menu Widget Area', 'fevr').'</h4>'.
			'<p class="no-widgets"><a href="'.esc_url(admin_url('widgets.php')).'">'.esc_html__('Click here to assign widgets.', 'fevr').'</a></p>'.
			'</div>';
	}
}

add_action('fevr_header_custom_content','fevr_header_custom_content');

/**
 * Render header slider/snippet
 */
function fevr_header_custom_content(){
	global $fevr_meta_fields;
	if(isset($fevr_meta_fields['page-header-slider']) && !empty($fevr_meta_fields['page-header-slider'])) {
		$wrapper = array('','');
		if(isset($fevr_meta_fields['page-header-slider-hide-s']) && $fevr_meta_fields['page-header-slider-hide-s'] == 'enabled') {
			$wrapper[0] = '<div class='.fevr_enqueue_inline_css(array('style' => 'display:none', 'media' => '(max-width: 460px)')).'>';
			$wrapper[1] = '</div>';
		}
		echo $wrapper[0] . do_shortcode('[luv_slider id="'.$fevr_meta_fields['page-header-slider'].'"]') . $wrapper[1];
	}
	if(isset($fevr_meta_fields['page-header-snippet']) && !empty($fevr_meta_fields['page-header-snippet'])) {
		echo do_shortcode('[luv_snippet id="'.$fevr_meta_fields['page-header-snippet'].'"]');
	}
}

add_action('fevr_main_header_classes', 'fevr_main_header_classes');

/**
 * Print main header classes
 */
function fevr_main_header_classes(){
	global $fevr_options, $fevr_meta_fields;
	$classes = array();
	
	// Header transparency
	$page_header_transparency	= (isset($fevr_meta_fields['page-header-transparency']) ? $fevr_meta_fields['page-header-transparency'] : 0);
	$is_transparent				= !fevr_is_headless() && !fevr_is_mobile() && (((fevr_check_luvoption('transparent-header', 1) && $page_header_transparency != 2) || $page_header_transparency == 1) && fevr_check_luvoption('header-layout', 'nav-under-logo', '!='));

	// Navigation borders
	$fevr_navigation_borders = (fevr_check_luvoption('border-on-transparent-header', 1, '!=') && (!isset($fevr_meta_fields['page-header-nav-borders']) || $fevr_meta_fields['page-header-nav-borders'] != 1)) || isset($fevr_meta_fields['page-header-nav-borders']) && $fevr_meta_fields['page-header-nav-borders'] == 2 ? 'no-border' : '';
	
	if (!empty($fevr_navigation_borders)){
		$classes[] = $fevr_navigation_borders; 
	}
	
	// Logo right
	if (fevr_check_luvoption('header-logo-right', 1)){
		$classes[] = 'logo-right'; 
	}
	
	// Header height
	if (isset($fevr_options['header-height']['height']) && (int)$fevr_options['header-height']['height'] > 0 && (!isset($fevr_options['header-position']) || $fevr_options['header-position'] != 'left') && (!isset($fevr_options['header-layout']) || $fevr_options['header-layout'] != 'nav-under-logo')){
		$classes[] = fevr_enqueue_inline_css(array('parent' => 'html #l-wrapper-inner ', 'style' => 'height: '. $fevr_options['header-height']['height'] .' !important;line-height: '. $fevr_options['header-height']['height'], 'media' => '(min-width: 769px)')); 
	}
					
	if($is_transparent){
		$classes[] = 'is-transparent';
	}
	
	echo implode(' ',$classes);
}

add_action('fevr_main_header_attributes', 'fevr_main_header_attributes');

/**
 * Print main header attributes
 */
function fevr_main_header_attributes(){
	global $fevr_meta_fields;
	
	// Header transparency
	$page_header_transparency	= (isset($fevr_meta_fields['page-header-transparency']) ? $fevr_meta_fields['page-header-transparency'] : 0);
	$is_transparent				= !fevr_is_headless() && !fevr_is_mobile() && (((fevr_check_luvoption('transparent-header', 1) && $page_header_transparency != 2) || $page_header_transparency == 1) && fevr_check_luvoption('header-layout', 'nav-under-logo', '!='));
	
	// Page header skin
	$page_header_skin	= (isset($fevr_meta_fields['page-header-skin']) && $fevr_meta_fields['page-header-skin'] != 'default') ? $fevr_meta_fields['page-header-skin'] : fevr_get_luvoption('header-skin', 'default');
	$transparent_nav	= fevr_check_luvoption('transparent-navigation', 1);
	
	echo ($is_transparent && $transparent_nav && !empty($page_header_skin) ? 'data-header-skin="'.esc_attr($page_header_skin).'"' : '');
}

add_action('fevr_wc_cart', 'fevr_wc_cart');

/**
 * Print Cart icon in header
 */
function fevr_wc_cart(){
	if(fevr_check_luvoption('woocommerce-cart-style', 'cart-style-2')){
		echo '<div id="full-width-cart" class="nav-cart-list cart-style-2">';
		the_widget( 'WC_Widget_Cart', 'title= ' );
		echo '</div>';
	}
}

add_action('fevr_print_logo', 'fevr_print_logo');

/**
 * Print site logo
 */
function fevr_print_logo(){
	global $fevr_options;
	
	// Logo dimensions
	$fevr_logo_dimensions = fevr_get_luvoption('logo-height', array('height' => 56));
	if (isset($fevr_options['header-logo']['url']) && !empty($fevr_options['header-logo']['url']) && file_exists(str_replace(site_url(), ABSPATH, $fevr_options['header-logo']['url']))){
		// Keep aspect ratio
		$dimensions = getimagesize(str_replace(site_url(), ABSPATH, $fevr_options['header-logo']['url']));
		$fevr_logo_dimensions['height'] = (int)$fevr_logo_dimensions['height'];
		$fevr_logo_dimensions['width'] = (int)($dimensions[0]/$dimensions[1]*$fevr_logo_dimensions['height']);
	}
	else{
		$fevr_logo_dimensions['height'] = (int)$fevr_logo_dimensions['height'];
		$fevr_logo_dimensions['width'] = '';
	}
	
	if(!fevr_is_headless() && !fevr_is_mobile()){
		if(fevr_check_luvoption('image-for-logo', 1) && ((isset($fevr_options['header-logo']['url']) && !empty($fevr_options['header-logo']['url'])) || (isset($fevr_options['header-logo-dark']['url']) && !empty($fevr_options['header-logo-dark']['url'])) || (isset($fevr_options['header-logo-light']['url']) && !empty($fevr_options['header-logo-light']['url'])))){
			echo '<a href="'.esc_url(trailingslashit(site_url())).'" title="'.esc_attr(get_bloginfo('name')).'" class="main-header-logo">';
			
			if(isset($fevr_options['header-logo']['url']) && !empty($fevr_options['header-logo']['url'])){
				echo '<img class="header-logo-default" src="'.esc_url($fevr_options['header-logo']['url']).'" alt="'.esc_attr(get_bloginfo('name')).'" height="'.esc_attr($fevr_logo_dimensions['height']).'" width="'.esc_attr($fevr_logo_dimensions['width']).'">';
			}
			if(isset($fevr_options['header-logo-dark']['url']) && !empty($fevr_options['header-logo-dark']['url'])){
				echo '<img class="header-logo-dark" src="'.esc_url($fevr_options['header-logo-dark']['url']).'" alt="'.esc_attr(get_bloginfo('name')).'" height="'.esc_attr($fevr_logo_dimensions['height']).'" width="'.esc_attr($fevr_logo_dimensions['width']).'">';
			}
			if(isset($fevr_options['header-logo-light']['url']) && !empty($fevr_options['header-logo-light']['url'])){
				echo '<img class="header-logo-light" src="'.esc_url($fevr_options['header-logo-light']['url']).'" alt="'.esc_attr(get_bloginfo('name')).'" height="'.esc_attr($fevr_logo_dimensions['height']).'" width="'.esc_attr($fevr_logo_dimensions['width']).'">';
			}
			
			echo '</a>';
		}
		else{
			echo '<a href="'.esc_url(trailingslashit(site_url())).'" title="'.esc_attr(get_bloginfo('name')).'" class="main-header-logo">'.get_bloginfo('name').'</a>';
		}	
	}
}

add_action('fevr_nav_primary','fevr_nav_primary');

/**
 * Render the primary navigation
 */
function fevr_nav_primary(){
	echo '<nav id="nav-primary" '.(fevr_check_luvoption('navigation-link-style', '', '!=') && fevr_check_luvoption('navigation-link-style', 'none', '!=') && fevr_check_luvoption('header-position', 'left', '!=') ? 'class="style-'.esc_attr(fevr_get_luvoption('navigation-link-style')).'"' : '') .'>'; 
		if(fevr_check_luvoption('header-nav-disable', 1, '!=')){
			if (isset($fevr_meta_fields['custom-nav-menu']) && !empty($fevr_meta_fields['custom-nav-menu'])){
				wp_nav_menu( array('menu' => $fevr_meta_fields['custom-nav-menu'], 'container' => false, 'menu_class' => 'nav-menu', 'link_before' => '<span>', 'link_after' => '</span><span class="menu-indicator"><i class="ion-chevron-right"></i></span>', 'depth' => 0, 'walker' => new Fevr_Mega_Menu_Frontend()));
			}
			else if (fevr_is_headless()){
				wp_nav_menu( array('theme_location' => 'woocommerce_headless', 'container' => false, 'menu_class' => 'nav-menu', 'link_before' => '<span>', 'link_after' => '</span><span class="menu-indicator"><i class="ion-chevron-right"></i></span>', 'depth' => 0, 'walker' => new Fevr_Mega_Menu_Frontend()));
			}
			else if (fevr_is_mobile()){
				wp_nav_menu( array('theme_location' => 'mobile_app', 'container' => false, 'menu_class' => 'nav-menu', 'link_before' => '<span>', 'link_after' => '</span><span class="menu-indicator"><i class="ion-chevron-right"></i></span>', 'depth' => 0, 'walker' => new Fevr_Mega_Menu_Frontend()));
			}
			else if (has_nav_menu('theme_main_navigation')){
				wp_nav_menu( array('theme_location' => 'theme_main_navigation', 'container' => false, 'menu_class' => 'nav-menu', 'link_before' => '<span>', 'link_after' => '</span><span class="menu-indicator"><i class="ion-chevron-right"></i></span>', 'depth' => 0, 'walker' => new Fevr_Mega_Menu_Frontend()));
			} 
		}		
		if(fevr_check_luvoption('header-position', 'left') && fevr_check_luvoption('header-search',  1)){ 
			echo '<div id="left-header-search">'.get_search_form(false).'</div>';
		}
	
		if (fevr_check_luvoption('header-enable-social-media',1) && fevr_check_luvoption('header-layout', 'centered-logo', '!=' ) && fevr_check_luvoption('header-social-media','','!=')){
			echo '<ul class="social-media-icons">';
			fevr_print_social_icons('header-social-media');
			echo '</ul>';
		}
		
		echo '<ul class="nav-buttons">';
			if(fevr_check_luvoption('header-search', 1) && !fevr_is_headless()){
				echo '<li class="nav-search"><a href="#" class="search-bar-trigger"><i class="ion-search"></i></a></li>';
			}
			
			if(fevr_check_luvoption('overlay-navigation-only-nav', 1,'!=')){ 
				echo '<li class="nav-icon"><a href="#"><i class="ion-navicon-round"></i></a></li>';
			}
			
			if(fevr_check_luvoption('woocommerce-cart-icon-header', 1) && class_exists('woocommerce')){
				echo '<li class="nav-cart'.(fevr_check_luvoption('woocommerce-cart-style','','!=') ? ' '.esc_attr(fevr_get_luvoption('woocommerce-cart-style')) : '') . (WC()->cart->cart_contents_count == 0 ? ' is-hidden' : '').'">'.
					 '<a class="cart-contents" href="'.esc_url(WC()->cart->get_cart_url()).'" title="'.esc_html__( 'View your shopping cart', 'fevr' ).'">'.
					 	'<span '.(WC()->cart->cart_contents_count > 0 ? 'data-count="'.WC()->cart->cart_contents_count .'"' : '').'><i class="ion-bag"></i></span>'.
					 '</a>';
					
					if(fevr_check_luvoption('woocommerce-cart-style', 'cart-style-1')){
						echo '<div class="nav-cart-list">';
						the_widget( 'WC_Widget_Cart', 'title= ' );
						echo '</div>';
					}
					
				echo '</li>';
			}
			
			if(fevr_check_luvoption('overlay-navigation', 1) && !fevr_is_headless() && !fevr_is_mobile()){
				echo '<li class="overlay-navigation-trigger ' . (fevr_check_luvoption('overlay-navigation-only-nav', 1) && fevr_check_luvoption('overlay-navigation', 1) ? 'float-left' : '').'"><a href="#"><i class="ion-navicon-round"></i></a></li>';
			}
			
			if(fevr_check_luvoption('off-canvas-menu', 1) && fevr_check_luvoption('off-canvas-menu-btn-nav', 1) && !fevr_is_headless()){
				echo '<li class="off-canvas-menu-trigger"><a href="#"><i class="ion-ios-keypad"></i></a></li>';
			}
		echo '</ul>';
		
		if(has_nav_menu('theme_main_right_navigation') && fevr_check_luvoption('header-position', 'left', '!=') && fevr_check_luvoption('header-nav-disable', 1, '!=') && fevr_check_luvoption('header-layout', 'centered-logo')){
			wp_nav_menu( array('theme_location' => 'theme_main_right_navigation', 'container' => false, 'menu_class' => 'nav-menu nav-menu-right', 'link_after' => '<span class="menu-indicator"><i class="ion-chevron-right"></i></span>', 'depth' => 0, 'walker' => new Fevr_Mega_Menu_Frontend()));
		}
	echo '</nav>';
}

add_action('fevr_woocommerce_popups', 'fevr_woocommerce_popups');

/**
 * Render Woocommerce Popup Dialogs
 */
function fevr_woocommerce_popups(){
	// Display popup if the page is related to woocommerce
	if(class_exists('woocommerce') && (is_woocommerce() || is_checkout() || is_cart() || is_account_page() || fevr_is_luv_shortcode()) ){
		get_template_part('luvthemes/luv-templates/quick-view-container');
		get_template_part('luvthemes/luv-templates/photo-review-container');
	}
}

add_action('fevr_footer', 'fevr_footer');

/**
 * Render footer
 */
function fevr_footer(){
	global $fevr_meta_fields;
	if(!fevr_is_headless() && !fevr_is_mobile() && (!isset($fevr_meta_fields['page-footer-hide']) || $fevr_meta_fields['page-footer-hide'] != 'enabled')){
	echo '<footer id="footer">';
		if(fevr_check_luvoption('footer-custom', 1) && fevr_check_luvoption('footer-custom-content','', '!=')){
			echo '<div id="custom-footer-content">';
			echo (fevr_check_luvoption('footer-custom-content-grid', 1) ? '<div class="container">' : '');
			echo do_shortcode(fevr_get_luvoption('footer-custom-content'));
			echo (fevr_check_luvoption('footer-custom-content-grid', 1) ? '</div>' : '');
			echo '</div>';
		}
		
		if(!isset($fevr_meta_fields['page-footer-hide-widgets']) || $fevr_meta_fields['page-footer-hide-widgets'] != 'enabled'){
			echo '<div class="container">';
			if(fevr_check_luvoption('footer-widgets', 1)){ 
				$footer_columns = fevr_get_luvoption('footer-widget-columns',4);
			
				echo '<div id="footer-widgets" class="l-grid-row">';
						for($i = 1; $i <= $footer_columns; $i++){
							echo '<div class="l-grid-'.(12/$footer_columns).'">';
							if ( fevr_func_exists('dynamic_sidebar') && is_active_sidebar('footer-widgets'.($i > 1 ? '-' . $i : '')) && dynamic_sidebar('footer-widgets'.($i > 1 ? '-' . $i : '')) ) {
								// Do the widgets
							} 
							else if (current_user_can('edit_theme_options')){ 
							echo '<div class="footer-widget widget">'.			
									'<h4 class="widget-title">Footer Widget Area <?php echo (int)$i; ?></h4>'.
									'<p class="no-widgets"><a href="'.esc_url(admin_url('widgets.php')).'">'.esc_html__('Click here to assign widgets.', 'fevr').'</a></p>'.
									'</div>';
							}
						echo '</div>';
						}
				echo '</div>';
			}
			echo '</div>';
		}
		
		echo '<div id="footer-copyright">'.
			 '<div class="container">'.do_shortcode(fevr_get_luvoption('footer-content', '&copy; '.date('Y').' '.get_bloginfo('name').' '.esc_html__('All Rights Reserved.', 'fevr')));
				if (fevr_check_luvoption('footer-enable-social-media',1) && fevr_check_luvoption('footer-social-media','','!=')){
					echo '<ul class="social-media-icons">';
					fevr_print_social_icons('footer-social-media');
					echo '</ul>';
				}
		echo'</div>'.
		'</div>'.
	'</footer>';
	}
}

add_action('fevr_custom_footer_html', 'fevr_custom_footer_html');

/**
 * Print custom footer HTML
 */
function fevr_custom_footer_html(){
	if(fevr_check_luvoption('custom-footer-html', '', '!=')) {
		// Unfiltered HTML, needs administrator privileges to modify it
		echo fevr_get_luvoption('custom-footer-html');
	}
}

add_action('fevr_back_to_top_button', 'fevr_back_to_top_button');

/**
 * Render back to top button
 */
function fevr_back_to_top_button(){
	global $fevr_options;
	if(fevr_check_luvoption('back-to-top-btn', 1) && fevr_check_luvoption('back-to-top-btn-img', '', '!=')){
		echo '<a href="#" id="to-top" class="to-top-custom"><img src="'.$fevr_options['back-to-top-btn-img']['url'].'"></a>';
	} elseif(fevr_check_luvoption('back-to-top-btn', 1)){
		echo '<a href="#" id="to-top"><i class="fa fa-angle-up"></i></a>';
	}
}

add_action('template_redirect', 'fevr_get_page_meta');

/**
 * Get page meta and set global variable
 */
function fevr_get_page_meta(){
	global $fevr_meta_fields;
	// Get page specific meta values
	$fevr_meta_fields = get_post_meta( fevr_get_page_id(), 'fevr_meta', true);
}

add_action('init','fevr_top_bar_visibility',10);

/**
 * Hide top bar if visitor alredy closed it
 */
function fevr_top_bar_visibility(){
	global $fevr_options;
	if (isset($_COOKIE['fevr-top-bar-close']) && $_COOKIE['fevr-top-bar-close'] == true){
		unset($fevr_options['top-bar']);
	}
}

add_action('init','fevr_header_meta');

/**
 * Print default meta tags in header
 */
function fevr_header_meta(){
	if (apply_filters('fevr_header_meta', true)){
		fevr_late_add_header_meta('<meta charset="'.esc_attr(get_option( 'blog_charset')).'">');
		fevr_late_add_header_meta('<meta http-equiv="X-UA-Compatible" content="IE=edge">');
		fevr_late_add_header_meta('<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">');
	}
	if (fevr_check_luvoption('facebook-comments', 1) && fevr_check_luvoption('facebook-comments-admins', '', '!=')){
		foreach ((array)fevr_get_luvoption('facebook-comments-admins') as $admin){
			fevr_late_add_header_meta('<meta property="fb:admins" content="'.esc_attr($admin).'">');
		}
	}

}

/**
 * Print social icons in top bar
 * @param string location
 */
function fevr_print_social_icons($location = 'top-bar-social-media'){
	foreach((array)fevr_get_luvoption($location) as $key => $icon){
		if (fevr_check_luvoption('social-media-urls-'.$key,'','!=') && !empty($icon)){
			echo '<li><a href="'.esc_url(fevr_get_luvoption('social-media-urls-'.$key)).'"><i class="fa fa-'.esc_attr($key).'"></i></a></li>';
		}
	}
}

/**
 * Print search filter for header search
 */
function fevr_get_search_filter(){
	$html = '';
	$args = array('exclude_from_search' => false);
	$search_filter_post_types = fevr_get_luvoption('search-filter-post-types');
	foreach(get_post_types($args) as $post_type){
		if (!isset($search_filter_post_types[$post_type]) || $search_filter_post_types[$post_type]  != '1'){
			continue;
		}
		switch ($post_type) {
			case 'luv_portfolio':
				$post_type_name = esc_html__('Portfolio', 'fevr');
				break;

			case 'luv_collections':
				$post_type_name = esc_html__('Collection', 'fevr');
				break;
					
			default:
				$post_type_name = esc_html($post_type);
		}

		$html .= '<li><input type="radio" name="search-filter-term" data-search-in="'.esc_attr($post_type).'" id="search-filter-'.esc_attr($post_type).'"><label for="search-filter-'.esc_attr($post_type).'">'.esc_html($post_type_name).'</label></li>';
	}
	return $html;
}

/**
 * Generate Breadcrumbs
 */
function fevr_breadcrumbs(){
	$text['home']     = esc_html__('Home', 'fevr'); // text for the 'Home' link
	$text['category'] = esc_html__('Archive by Category "%s"', 'fevr'); // text for a category page
	$text['tax'] 	  = esc_html__('Archive for "%s"', 'fevr'); // text for a taxonomy page
	$text['search']   = esc_html__('Search Results for "%s" Query', 'fevr'); // text for a search results page
	$text['tag']      = esc_html__('Posts Tagged "%s"', 'fevr'); // text for a tag page
	$text['author']   = esc_html__('Articles Posted by %s', 'fevr'); // text for an author page
	$text['404']      = esc_html__('Error 404', 'fevr'); // text for the 404 page
	$show_current = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
	$show_on_home  = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
	$delimiter   = ' / '; // delimiter between crumbs
	$before      = '<span class="current">'; // tag before the current crumb
	$after       = '</span>'; // tag after the current crumb
	/* === END OF OPTIONS === */
	global $post;
	$home_link = trailingslashit(home_url());
	$link_before = '<span typeof="v:Breadcrumb">';
	$link_after = '</span>';
	$link_attr = ' rel="v:url" property="v:title"';
	$link = $link_before . '<a' . $link_attr . ' href="%1$s">%2$s</a>' . $link_after;
	if (!is_home() && !is_front_page()){
		echo '<div id="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">' . sprintf($link, esc_url($home_link), esc_html($text['home'])) . $delimiter;
		if ( is_category() ) {
			$this_cat = get_category(get_query_var('cat'), false);
			if ($this_cat->parent != 0) {
				$cats = get_category_parents($this_cat->parent, true, $delimiter);
				$cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
				$cats = str_replace('</a>', '</a>' . $link_after, $cats);
				echo fevr_kses($cats);
			}
			echo fevr_kses($before . sprintf($text['category'], single_cat_title('', false)) . $after);
		} else if( is_tax() ){
			$this_cat = get_category(get_query_var('cat'), false);
			if ($this_cat->parent != 0) {
				$cats = get_category_parents($this_cat->parent, true, $delimiter);
				$cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
				$cats = str_replace('</a>', '</a>' . $link_after, $cats);
				echo fevr_kses($cats);
			}
			echo fevr_kses($before . sprintf($text['tax'], single_cat_title('', false)) . $after);

		}
		else if ( is_search() ) {
			echo fevr_kses($before . sprintf($text['search'], get_search_query()) . $after);
		}
		else if ( is_day() ) {
			echo fevr_kses(sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter);
			echo fevr_kses(sprintf($link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $delimiter);
			echo fevr_kses($before . get_the_time('d') . $after);
		}
		else if ( is_month() ) {
			echo fevr_kses(sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter);
			echo fevr_kses($before . get_the_time('F') . $after);
		}
		elseif ( is_year() ) {
			echo fevr_kses($before . get_the_time('Y') . $after);
		}
		else if ( is_single() && !is_attachment() ) {
			if ( get_post_type() != 'post' ) {
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				printf($link, $home_link . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
				echo fevr_kses($delimiter . $before . get_the_title() . $after);
			}
			else {
				$cat = get_the_category(); $cat = $cat[0];
				$cats = get_category_parents($cat, true, $delimiter);
				$cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
				$cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
				$cats = str_replace('</a>', '</a> / ' . $link_after, $cats);
				echo fevr_kses($cats);
				echo fevr_kses($before . get_the_title() . $after);
			}
		}
		else if ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
			$post_type = get_post_type_object(get_post_type());
			echo fevr_kses($before . $post_type->labels->singular_name . $after);
		}
		else if ( is_attachment() ) {
			$parent = get_post($post->post_parent);
			$cat = get_the_category($parent->ID); $cat = $cat[0];
			$cats = get_category_parents($cat, TRUE, $delimiter);
			$cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
			$cats = str_replace('</a>', '</a>' . $link_after, $cats);
			echo fevr_kses($cats);
			printf($link, get_permalink($parent), $parent->post_title);
			echo fevr_kses($delimiter . $before . get_the_title() . $after);
		}
		else if ( is_page() && !$post->post_parent ) {
			echo fevr_kses($before . get_the_title() . $after);
		}
		else if ( is_page() && $post->post_parent ) {
			$parent_id  = $post->post_parent;
			$breadcrumbs = array();
			while ($parent_id) {
				$page = get_page($parent_id);
				$breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
				$parent_id  = $page->post_parent;
			}
			$breadcrumbs = array_reverse($breadcrumbs);
			for ($i = 0; $i < count($breadcrumbs); $i++) {
				echo fevr_kses($breadcrumbs[$i]);
				if ($i != count($breadcrumbs)-1){
					echo esc_html($delimiter);
				}
			}
			echo fevr_kses($delimiter . $before . get_the_title() . $after);
		}
		else if ( is_tag() ) {
			echo fevr_kses($before . sprintf($text['tag'], single_tag_title('', false)) . $after);
		}
		else if ( is_author() ) {
			global $author;
			$userdata = get_userdata($author);
			echo fevr_kses($before . sprintf($text['author'], $userdata->display_name) . $after);
		}
		else if ( is_404() ) {
			echo fevr_kses($before . $text['404'] . $after);
		}
		if ( get_query_var('paged') ) {
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ){
				echo ' (';
			}
			esc_html_e('Page', 'fevr') . ' ' . get_query_var('paged');
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ){
				echo ')';
			}
		}
		echo '</div>';
	}
}

//======================================================================
// Extend WP Gallery
//======================================================================

add_action('print_media_templates', 'fevr_wp_gallery_photo_settings');
/**
 * Add masonry and carousel options to Gallery settings
 */
function fevr_wp_gallery_photo_settings(){
  echo '<script type="text/html" id="tmpl-masonry">
			<label class="setting"><span>' . esc_html__('Animation', 'fevr') . '</span><select data-setting="animation"><option value="">'.esc_html__('None', 'fevr').'</option><option value="c-animate-fade-in">'.esc_html__('Fade in', 'fevr').'</option><option value="c-animate-top">'.esc_html__('Fade in from top', 'fevr').'</option><option value="c-animate-bottom">'.esc_html__('Fade in from bottom', 'fevr').'</option><option value="c-animate-left">'.esc_html__('Fade in from left', 'fevr').'</option><option value="c-animate-right">'.esc_html__('Fade in from right', 'fevr').'</option><option value="c-animate-zoom-in">'.esc_html__('Zoom in', 'fevr').'</option><option value="c-animate-zoom-in-spin">'.esc_html__('Zoom in & Spin', 'fevr').'</option></select></label>
			<label class="setting"><span>' . esc_html__('Masonry settings', 'fevr') . '</span><select data-setting="masonry"><option value=""></option><option value="masonry">'.esc_html__('Masonry', 'fevr').'</option><option value="masonry masonry-no-gap">'.esc_html__('Masonry no gap', 'fevr').'</option></select></label>
			<label class="setting"><span>' . esc_html__('Carousel', 'fevr') . '</span><input type="checkbox" data-setting="luv_carousel" name="gallery-carousel" value="true"></label>
			<label class="setting luv-required" data-required-value="true" data-required-compare="=" data-required-name="gallery-carousel"><span>' . esc_html__('Infinite', 'fevr') . '</span><input type="checkbox" data-setting="infinite" value="true"></label>
			<label class="setting luv-required" data-required-value="true" data-required-compare="=" data-required-name="gallery-carousel"><span>' . esc_html__('Navigation', 'fevr') . '</span><input type="checkbox" data-setting="nav" value="true"></label>
			<label class="setting luv-required" data-required-value="true" data-required-compare="=" data-required-name="gallery-carousel"><span>' . esc_html__('Dots', 'fevr') . '</span><input type="checkbox" data-setting="dots" value="true"></label>
			<label class="setting luv-required" data-required-value="true" data-required-compare="=" data-required-name="gallery-carousel"><span>' . esc_html__('Autoplay', 'fevr') . '</span><input type="checkbox" data-setting="autoplay" value="true"></label>
	 		<label class="setting luv-required" data-required-value="true" data-required-compare="=" data-required-name="gallery-carousel"><span>' . esc_html__('Autoplay Timeout', 'fevr') . '</span><input type="text" data-setting="autoplay_timeout" value="5000"></label>
			<label class="setting luv-required" data-required-value="true" data-required-compare="=" data-required-name="gallery-carousel"><span>' . esc_html__('Autoplay Pause on Hover', 'fevr') . '</span><input type="checkbox" data-setting="autoplay_pause"></label>
	 		<label class="setting luv-required" data-required-value="true" data-required-compare="=" data-required-name="gallery-carousel"><span>' . esc_html__('Transition Type', 'fevr') . '</span><select data-setting="transition_type"><option value="slide">Slide</option><option value="fadeOut">Fade Out</option><option value="flipInX">flipInX</option></select></label>
			<label class="setting luv-required" data-required-value="true" data-required-compare="=" data-required-name="gallery-carousel"><span>' . esc_html__('Items', 'fevr') . '</span></label>
			<label class="setting luv-required" data-required-value="true" data-required-compare="=" data-required-name="gallery-carousel"><span><i class="fa fa-desktop"></i></span><input type="number" data-gallery-responsive="1" data-setting="items-desktop">
			<label class="setting luv-required" data-required-value="true" data-required-compare="=" data-required-name="gallery-carousel"><span><i class="fa fa-laptop"></i></span><input type="number" data-gallery-responsive="1" data-setting="items-laptop">
			<label class="setting luv-required" data-required-value="true" data-required-compare="=" data-required-name="gallery-carousel"><span><i class="fa fa-tablet fa-rotate-90"></i></span><input type="number" data-gallery-responsive="1" data-setting="items-tablet-landscape">
			<label class="setting luv-required" data-required-value="true" data-required-compare="=" data-required-name="gallery-carousel"><span><i class="fa fa-tablet"></i></span><input type="number" data-gallery-responsive="1" data-setting="items-tablet-portrait">
			<label class="setting luv-required" data-required-value="true" data-required-compare="=" data-required-name="gallery-carousel"><span><i class="fa fa-mobile"></i></span><input type="number" data-gallery-responsive="1" data-setting="items-mobile">
 			<label class="setting luv-required" data-required-value="true" data-required-compare="=" data-required-name="gallery-carousel"><span>' . esc_html__('Margin Between Items', 'fevr') . '</span><input type="text" data-setting="margin" value="10"></label>
 			<label class="setting luv-required" data-required-value="true" data-required-compare="=" data-required-name="gallery-carousel"><span>' . esc_html__('Full Height', 'fevr') . '</span><input type="checkbox" data-setting="full_height" value="true"></label>
			</script>
			';
}

add_filter('post_gallery', 'fevr_post_gallery', 10, 3);
/**
 * Gallery shortcode overwrite
 * @param string $_ empty string from the default gallery shortcode filter
 * @param array $attr shortcode attributes 
 * @param number $instance number of instance
 * @return string
 */
function fevr_post_gallery($_, $attr, $instance = 0) {
	wp_enqueue_script( 'fevr-owlcarousel' );
	
	$post = get_post();

	$html5 = current_theme_supports ( 'html5', 'gallery' );
	$atts = shortcode_atts ( array (
			'order' => 'ASC',
			'orderby' => 'menu_order ID',
			'id' => $post ? $post->ID : 0,
			'itemtag' => $html5 ? 'figure' : 'dl',
			'icontag' => $html5 ? 'div' : 'dt',
			'captiontag' => $html5 ? 'figcaption' : 'dd',
			'columns' => 3,
			'size' => 'thumbnail',
			'include' => '',
			'exclude' => '',
			'link' => '',
			'animation' => '',
			'masonry' => '',
			'luv_carousel' => 'false',
			'infinite' => 'false',
			'nav' => 'false',
			'dots' => 'false',
			'autoplay' => 'false',
			'autoplay_timeout' => '5000',
			'autoplay_pause' => 'false',
			'transition_type' => '',
			'margin' => '',
			'parallax' => 'false',
			'full_height' => 'false',
			'items-desktop' => '1',
			'items-laptop' => '1',
			'items-tablet-landscape' => '1',
			'items-tablet-portrait' => '1',
			'items-mobile' => '1',

	), $attr, 'gallery' );
	
	$id = intval ( $atts ['id'] );
	
	if (! empty ( $atts ['include'] )) {
		$_attachments = get_posts ( array (
				'include' => $atts ['include'],
				'post_status' => 'inherit',
				'post_type' => 'attachment',
				'post_mime_type' => 'image',
				'order' => $atts ['order'],
				'orderby' => $atts ['orderby'] 
		) );
		
		$attachments = array ();
		foreach ( $_attachments as $key => $val ) {
			$attachments [$val->ID] = $_attachments [$key];
		}
	} elseif (! empty ( $atts ['exclude'] )) {
		$attachments = get_children ( array (
				'post_parent' => $id,
				'exclude' => $atts ['exclude'],
				'post_status' => 'inherit',
				'post_type' => 'attachment',
				'post_mime_type' => 'image',
				'order' => $atts ['order'],
				'orderby' => $atts ['orderby'] 
		) );
	} else {
		$attachments = get_children ( array (
				'post_parent' => $id,
				'post_status' => 'inherit',
				'post_type' => 'attachment',
				'post_mime_type' => 'image',
				'order' => $atts ['order'],
				'orderby' => $atts ['orderby'] 
		) );
	}
	
	if (empty ( $attachments )) {
		return '';
	}
	
	if (is_feed ()) {
		$output = "\n";
		foreach ( $attachments as $att_id => $attachment ) {
			$output .= wp_get_attachment_link ( $att_id, $atts ['size'], true ) . "\n";
		}
		return $output;
	}
	
	$itemtag = tag_escape ( $atts ['itemtag'] );
	$captiontag = tag_escape ( $atts ['captiontag'] );
	$icontag = tag_escape ( $atts ['icontag'] );
	$valid_tags = wp_kses_allowed_html ( 'post' );
	if (! isset ( $valid_tags [$itemtag] )) {
		$itemtag = 'dl';
	}
	if (! isset ( $valid_tags [$captiontag] )) {
		$captiontag = 'dd';
	}
	if (! isset ( $valid_tags [$icontag] )) {
		$icontag = 'dt';
	}
	
	$columns = intval ( $atts ['columns'] );
	$itemwidth = $columns > 0 ? floor ( 100 / $columns ) : 100;
	$float = is_rtl () ? 'right' : 'left';
	
	$selector = "gallery-{$instance}";
	
	$gallery_style = '';
	
	/**
	 * Filter whether to print default gallery styles.
	 *
	 * @since 3.1.0
	 *       
	 * @param bool $print
	 *        	Whether to print default gallery styles.
	 *        	Defaults to false if the theme supports HTML5 galleries.
	 *        	Otherwise, defaults to true.
	 */
	// Gallery container classes
	$gallery_class = '';
	if (apply_filters ( 'use_default_gallery_style', ! $html5 )) {
		$gallery_class = fevr_enqueue_inline_css(array('style' => 'margin:auto;', 'child'=>array(' .gallery-item' => 'float:' . $float . ';margin-top: 10px;text-align: center;width:'.$itemwidth.'%;',  ' img' => 'border:2px solid #cfcfcf;', ' .gallery-caption' => 'margin-left: 0;'))); 
	}
	
	//Gallery item classes
	$gallery_item_class = 'gallery-item';
	if (!empty($atts['animation'])){
		$gallery_item_class .= ' c-has-animation ' . esc_attr($atts['animation']); 
	}
	
	// Luv carousel data attributes
	if ($atts['luv_carousel'] == 'true'){
		$carousel_settings = array('infinite', 'nav', 'dots', 'autoplay', 'autoplay_timeout', 'autoplay_pause', 'transition_type', 'margin', 'parallax', 'full_height');
		foreach ($carousel_settings as $key){
			if (!empty($atts[$key])){
				$fevr_gallery_container_data[] = 'data-luv-carousel-' . esc_attr($key) . '="'.esc_attr($atts[$key]).'"';
			}
		}
		
		// Responsive items count
		$fevr_gallery_container_data[] = 'data-luv-carousel-items="{\'desktop\':\''.$atts['items-desktop'].'\',\'laptop\':\''.$atts['items-laptop'].'\',\'tablet-landscape\':\''.$atts['items-tablet-landscape'].'\',\'tablet-portrait\':\''.$atts['items-tablet-portrait'].'\',\'mobile\':\''.$atts['items-mobile'].'\'}"';
	}
	
	$size_class = sanitize_html_class ( $atts ['size'] );
	$gallery_div = "<div id='".esc_attr($selector)."' class='gallery".(!empty($atts['masonry']) ? ' ' . esc_attr($atts['masonry']) : '').(!empty($fevr_gallery_container_data) ? ' luv-carousel' : '')." galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class} ".esc_attr($gallery_class)."' ".(!empty($fevr_gallery_container_data) ? esc_html(implode(' ', $fevr_gallery_container_data)) : '').">";
	
	// Enqueue OwlCarousel
	if (!empty($atts['masonry'])){
		wp_enqueue_script( 'fevr-owlcarousel' );
	}
	
	/**
	 * Filter the default gallery shortcode CSS styles.
	 *
	 * @since 2.5.0
	 *       
	 * @param string $gallery_style
	 *        	Default CSS styles and opening HTML div container
	 *        	for the gallery shortcode output.
	 */
	$output = apply_filters ( 'gallery_style', $gallery_style . $gallery_div );
	
	$i = 0;
	foreach ( $attachments as $id => $attachment ) {
		
		$attr = (trim ( $attachment->post_excerpt )) ? array (
				'aria-describedby' => "$selector-$id" 
		) : '';
		if (! empty ( $atts ['link'] ) && 'file' === $atts ['link']) {
			$image_output = wp_get_attachment_link ( $id, $atts ['size'], false, false, false, $attr );
		} elseif (! empty ( $atts ['link'] ) && 'none' === $atts ['link']) {
			$image_output = wp_get_attachment_image ( $id, $atts ['size'], false, $attr );
		} else {
			$image_output = wp_get_attachment_link ( $id, $atts ['size'], true, false, false, $attr );
		}
		$image_meta = wp_get_attachment_metadata ( $id );
		
		$orientation = '';
		if (isset ( $image_meta ['height'], $image_meta ['width'] )) {
			$orientation = ($image_meta ['height'] > $image_meta ['width']) ? 'portrait' : 'landscape';
		}
		$output .= "<{$itemtag} class='{$gallery_item_class}'>";
		$output .= "
			<{$icontag} class='gallery-icon {$orientation}'>
			$image_output
	</{$icontag}>";
		if ($captiontag && trim ( $attachment->post_excerpt )) {
			$output .= "
	<{$captiontag} class='wp-caption-text gallery-caption' id='$selector-$id'>
	" . wptexturize ( $attachment->post_excerpt ) . "
	</{$captiontag}>";
		}
		$output .= "</{$itemtag}>";
		if (! $html5 && $columns > 0 && ++ $i % $columns == 0) {
			$output .= '<br style="clear: both" />';
		}
	}
	
	if (! $html5 && $columns > 0 && $i % $columns !== 0) {
		$output .= "
			<br style='clear: both' />";
	}
	
	$output .= "\n</div>\n";
	
	return $output;
}

//======================================================================
// Get Page ID 
//======================================================================


/**
 * Get the current page id
 */
function fevr_get_page_id(){
	if (is_singular() && (!fevr_func_exists('is_bbpress') || !is_bbpress())){
		return get_the_ID();
	} else {
		return fevr_get_archive_page_id();
	}
}

/**
 * Returns archive page id based on post type
 * @return int
 */
function fevr_get_archive_page_id() {
	
	if (is_search()){
		return 0;
	}

	if (is_404()){
		return 0;
	}
	
	if (is_category()){
		return fevr_check_luvoption('custom-header-category', '', '!=') ? fevr_get_luvoption('custom-header-category') : get_option('page_for_posts');
	}
	
	if (is_tag()){
		return fevr_check_luvoption('custom-header-tag', '', '!=') ? fevr_get_luvoption('custom-header-tag') : get_option('page_for_posts');
	}
	
	if (is_author()){
		return fevr_check_luvoption('custom-header-author', '', '!=') ? fevr_get_luvoption('custom-header-author') : get_option('page_for_posts');
	}
	
	if (is_date()){
		return fevr_check_luvoption('custom-header-date', '', '!=') ? fevr_get_luvoption('custom-header-date') : get_option('page_for_posts');
	}
	
	switch (get_post_type()){
		case 'product':
			return wc_get_page_id('shop');
		case 'luv_portfolio':	
			return fevr_get_luvoption('portfolio-page', 0); 
		case 'luv_collections':
			return fevr_get_luvoption('woocommerce-collections-page', 0);
		case 'luv_ext_reviews':
			return fevr_get_luvoption('woocommerce-photo-reviews-page', 0);
		case 'topic':
			return fevr_get_luvoption('bbpress-page', 0);
		case 'forum':
			return fevr_get_luvoption('bbpress-page', 0);
		default:	
			return get_option('page_for_posts');
	}
}

//======================================================================
// Get Social Share Icons
//======================================================================

/**
 * Print social icons
 */
function fevr_get_social_share_icons() {	
	if(fevr_check_luvoption('social-media-share', 1)) {
		$permalink = esc_attr(get_permalink());
		foreach((array)fevr_get_luvoption('social-media-share-icons') as $icon => $value) {
			if (!empty($value)){
				switch($icon) {
					case 'facebook':
						$share_url = esc_url('http://www.facebook.com/share.php?u='.urlencode($permalink));
					break;
					
					case 'twitter':
						$share_url = esc_url('http://twitter.com/intent/tweet?text='.urlencode(get_the_title().' '.$permalink));
					break;
					
					case 'google-plus':
						$share_url = esc_url('https://plus.google.com/share?url='.urlencode($permalink));
					break;
					
					case 'linkedin':
						$share_url = esc_url('http://www.linkedin.com/shareArticle?mini=true&url='.urlencode($permalink).'&title='.urlencode(get_the_title()));
					break;
					
					case 'pinterest':
						$image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full' );
						$share_url = esc_url('http://pinterest.com/pin/create/button/?url='.urlencode($permalink).'&media='.urlencode($image[0]).'&description='.urlencode(get_the_title()));
					break;
				}
				if (isset($share_url)){
					echo '<a href="'.esc_url($share_url).'" target="_blank" class="social-share-popup social-share-popup-'.esc_attr($icon).'"><i class="fa fa-'.esc_attr($icon).'"></i><span class="luv-share-count-tooltip" data-luv-share-count="'.esc_attr($icon).'" data-luv-share-url="'.urlencode(esc_url($permalink)).'"></span></a>';
				}
			}
		}
	}
}

// Create luv like count ajax hook
add_action('wp_ajax_fevr_load_like_count', 'fevr_load_like_count');
add_action('wp_ajax_nopriv_fevr_load_like_count', 'fevr_load_like_count');

/**
 * Load like count for post
 * This function uses $_POST['post_id'] to determine post 
 */
function fevr_load_like_count(){
	echo (int)get_post_meta((int)$_POST['post_id'], 'fevr_like_count', true);
	wp_die();
}

//======================================================================
// Rewrites
//======================================================================

add_action('template_redirect', 'fevr_canonical_redirect');

/**
 * Theme redirects
 * The user can create a new page, or select an existing page, on which you can set a custom header and content. In this case, on the archive page the set custom header and custom content will appear above the items.
 * This function redirects these pseudo pages to the archive page to prevent duplicate content
 *  - Portfolio
 *  - Collections
 *  - Photo Reviews
 *  - BB Press forums
 */
function fevr_canonical_redirect(){
	global $post, $wpdb;
	
	if(!is_object($post)) {
		return;
	}
	
	if($post->ID == fevr_get_luvoption('portfolio-page', -1)) {
		$slug = fevr_get_luvoption('custom-portfolio-slug', 'portfolio');
		wp_redirect(esc_url(site_url( (!empty($slug) ? $slug : 'portfolio') )),301);
	}
	else if($post->ID == fevr_get_luvoption('woocommerce-photo-reviews-page', -1)) {
		$slug = fevr_get_luvoption('woocommerce-photo-reviews-custom-slug', 'reviews');
		wp_redirect(esc_url(site_url( (!empty($slug) ? $slug : 'reviews') )),301);
	}
	else if($post->ID == fevr_get_luvoption('woocommerce-collections-page', -1)) {
		$slug = fevr_get_luvoption('woocommerce-collections-custom-slug', 'collections');
		wp_redirect(esc_url(site_url( (!empty($slug) ? $slug : 'collections') )),301);
	}
	else if($post->ID == fevr_get_luvoption('bbpress-page', -1)) {
		wp_redirect(esc_url(site_url(get_option('_bbp_forum_slug', 'forums'))),301);
	}
	else if(fevr_check_luvoption('custom-header-tag', $post->ID)) {
		$slug = get_tags();
		$slug = $slug[0]->slug;
		$tag_base = trailingslashit(get_option('tag_base', 'tag'));
		
		wp_redirect(esc_url(trailingslashit(site_url($tag_base . $slug))),301);
	}
	else if(fevr_check_luvoption('custom-header-category', $post->ID)) {
		$slug = get_categories();
		$slug = $slug[0]->slug;
		$category_base = trailingslashit(get_option('category_base', 'category'));
	
		wp_redirect(esc_url(trailingslashit(site_url($category_base . $slug))),301);
	}
	else if(fevr_check_luvoption('custom-header-author', $post->ID)) {
		$first_user_id = $wpdb->get_var('SELECT ID FROM ' . $wpdb->users . ' LIMIT 1');
		$link = get_author_posts_url($first_user_id);
	
		wp_redirect(esc_url(trailingslashit($link)),301);
	}
	else if(fevr_check_luvoption('custom-header-date', $post->ID)) {
		global $wpdb;
		$slug = $wpdb->get_var("SELECT YEAR(post_date) FROM $wpdb->posts WHERE post_status = 'publish' LIMIT 1");
	
		wp_redirect(esc_url(trailingslashit(site_url($slug))),301);
	}
}

// Flush rewrites after save redux options 
add_action('redux/options/fevr_options/saved', 'fevr_rewrites');

/**
 * Flush rewrites after modified theme settings in Redux
 */
function fevr_rewrites() {
	
	global $fevr_options;
	global $wp_rewrite;

	if (isset($_POST['fevr_options_ajax_save'])){
		$data = parse_str($_POST['data']);
	}
	
	if((isset($fevr_options['module-portfolio']) && $fevr_options['module-portfolio'] == 1) || (isset($data['fevr_options']['module-portfolio']) && $data['fevr_options']['module-portfolio'] == 1)) {
		$args = get_post_type_object("luv_portfolio");
		$args->rewrite["slug"] = (isset($fevr_options['custom-portfolio-slug']) && !empty($fevr_options['custom-portfolio-slug']) ? $fevr_options['custom-portfolio-slug'] : 'portfolio');
		@register_post_type($args->name, $args);
	}
	
	if((isset($fevr_options['woocommerce-photo-reviews']) && $fevr_options['woocommerce-photo-reviews'] == 1) || (isset($data['fevr_options']['woocommerce-photo-reviews']) && $data['fevr_options']['woocommerce-photo-reviews'] == 1)) {
		$args = get_post_type_object("luv_ext_reviews");
		$args->rewrite["slug"] = (isset($fevr_options['woocommerce-reviews-custom-slug']) && !empty($fevr_options['woocommerce-reviews-custom-slug']) ? $fevr_options['woocommerce-reviews-custom-slug'] : 'reviews');
		@register_post_type($args->name, $args);
	}
	
	if((isset($fevr_options['woocommerce-collections']) && $fevr_options['woocommerce-collections'] == 1) || (isset($data['fevr_options']['woocommerce-collections']) && $data['fevr_options']['woocommerce-collections'] == 1)) {
		$args = get_post_type_object("luv_collections");
		if (isset($args->name)){
			$args->rewrite["slug"] = (isset($fevr_options['woocommerce-collections-custom-slug']) && !empty($fevr_options['woocommerce-collections-custom-slug']) ? $fevr_options['woocommerce-collections-custom-slug'] : 'collections');
			@register_post_type($args->name, $args);
		}
	}
	
	if(fevr_check_luvoption('enable-mobile-app',1) && fevr_check_luvoption('mobile-app-home', '', '!=') || (isset($data['fevr_options']['enable-mobile-app']) && $data['fevr_options']['enable-mobile-app'] == 1 && isset($data['fevr_options']['mobile-app-home']) && !empty($data['fevr_options']['mobile-app-home']))) {
		$page = (isset($data['fevr_options']['mobile-app-home']) && !empty($data['fevr_options']['mobile-app-home']) ? $data['fevr_options']['mobile-app-home'] : fevr_get_luvoption('mobile-app-home', 0));
		add_rewrite_rule('^fevr-mobile-app-home/?', 'index.php?page_id=' . $page, 'top');
	}

	if(doing_action('redux/options/fevr_options/saved')) {
		flush_rewrite_rules(false);
	}
}

//======================================================================
// Updater
//======================================================================

// Create cron hook
add_action('fevr_update_checker', 'fevr_update_checker');

// Schedule event
if (!wp_next_scheduled('fevr_update_checker')){
	wp_schedule_event(time(), 'twicedaily', 'fevr_update_checker');
}

/**
 * Check for updates, and store result in fevr_update option
 */
function fevr_update_checker(){
	include_once trailingslashit(get_template_directory()) . 'luvthemes/includes/updater.inc.php';

	$updater = new Fevr_Updater(fevr_purchase_key());
	$updater->check_updates();
}

//======================================================================
// ENVATO Purchase key
//======================================================================

add_action('init', 'fevr_get_purchase_key');

/**
 * Get envato purchase key
 * for multisite subsites it uses the main site's purchase key
 */
function fevr_get_purchase_key(){
	if (is_admin()){
		global $fevr_purchase_key;
		$fevr_purchase_key = (defined('MULTISITE') && MULTISITE === true ? get_site_option('fevr_purchase_key','') : get_option('fevr_purchase_key',''));
	}
}


//======================================================================
// Luv CDN options
//======================================================================

add_action('init', 'fevr_get_cdn_options',8);

/**
 * Get MaxCDN options
 */
function fevr_get_cdn_options(){
	global $fevr_cdn_options;
	$fevr_cdn_options = get_option('fevr_cdn_options', array());
}

//======================================================================
// Tweaks
//======================================================================


// Custom 404
add_action('template_redirect', 'fevr_custom_404_page');

/**
 * Overwrite global $post object for all 404 page if it was set in Theme settings 
 */
function fevr_custom_404_page(){
	global $wp_query, $post;
	if (fevr_check_luvoption('custom-404','','!=') && $wp_query->is_404){
		$wp_query = new WP_Query('page_id='.fevr_get_luvoption('custom-404'));
		$post = $wp_query->post;
    	status_header(404);
	}
}

add_action('template_redirect', 'fevr_sounds_like_404', PHP_INT_MAX);

/**
 * Try guess the requested page for typos and redirect user 
 */
function fevr_sounds_like_404(){
	if (fevr_check_luvoption('sounds-like-404', 1) && is_404()){
		global $wpdb;
		$query = explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'));
		$query = end($query);
		$results = $wpdb->get_results($wpdb->prepare("SELECT ID, post_name FROM {$wpdb->posts} WHERE post_name SOUNDS LIKE %s", $query), ARRAY_A);

		if (count($results) > 0){
			$sounds_like = array();
			foreach ($results as $result){
				$sounds_like[levenshtein($query, $result['post_name'])] = $result['ID'];
			}
			ksort($sounds_like);
			$best_match = array_shift($sounds_like);

			wp_redirect(get_permalink($best_match));
			exit;
		}
	}
}

/**
 * Offer search tips for typos
 */
function fevr_search_tip(){
	global $wpdb;
	$query = get_query_var('s');
	$results = $wpdb->get_results($wpdb->prepare("SELECT ID, post_name FROM {$wpdb->posts} WHERE post_title SOUNDS LIKE %s", $query), ARRAY_A);
	
	if (count($results) > 0){
		$sounds_like = array();
		foreach ($results as $result){
			$sounds_like[levenshtein($query, $result['post_name'])] = $result['ID'];
		}
		ksort($sounds_like);
		$best_match = array_shift($sounds_like);
		$title	= strtolower(get_the_title($best_match));
		$search = trailingslashit(site_url()) . '?s=' . $title;
		$tip = sprintf(fevr_kses(__('Did you mean <strong><a href="%s">%s</a></strong>?', 'fevr')), esc_url($search), esc_html($title));
		
		echo '<p>'.fevr_kses($tip).'</p>';
	}
}

// Load mediaelement assets for infinite scroll appended media elements
add_action('wp_ajax_fevr_load_infinite_assets', 'fevr_load_infinite_assets');
add_action('wp_ajax_nopriv_fevr_load_infinite_assets', 'fevr_load_infinite_assets');

/**
 * Load media element assets via ajax for infinite scroll. We need this function because ajax loaded elements may contains media elements.
 */
function fevr_load_infinite_assets(){
	global $wp_scripts;
	do_action('fevr_ajax_scripts');
	
	wp_enqueue_style('wp-mediaelement');
	wp_enqueue_script('wp-mediaelement');
	
	wp_enqueue_script( 'fevr-owlcarousel');
	
	// Prevent loading jQuery again 
	foreach ($wp_scripts->registered as $key=>$value){
		foreach ($wp_scripts->registered[$key]->deps as $_key=>$_value){
			if ($_value == 'jquery'){
				unset ($wp_scripts->registered[$key]->deps[$_key]);
			}
		}
	}
		
	wp_print_styles();
	print_footer_scripts();
	
	die;
}

//======================================================================
// WooCommerce Headless
//======================================================================

add_action('init', 'fevr_headless', 0);

/**
 * Turn on headless mode if "headless" is set in query string
 */
function fevr_headless(){
	global $fevr_options, $luvthemes_core;
	if (isset($_GET['headless'])){
		fevr_is_headless(true);
		
		$fevr_options['woocommerce-sidebar-position'] = 'no-sidebar';
		$fevr_options['woocommerce-full-width'] = 1;
		$fevr_options['transparent-header'] = 0;
				
		do_shortcode('[fevr_enqueue_facebook_sdk]');
	}
}

// Add filters for headless
add_filter('post_type_link', 'fevr_headless_url');
add_filter('term_link', 'fevr_headless_url');
add_filter('woocommerce_get_endpoint_url', 'fevr_headless_url');
add_filter('woocommerce_get_shop_page_permalink', 'fevr_headless_url');
add_filter('woocommerce_get_cart_page_permalink', 'fevr_headless_url');
add_filter('woocommerce_get_checkout_page_permalink', 'fevr_headless_url');
add_filter('woocommerce_get_myaccount_page_permalink', 'fevr_headless_url');
add_filter('nav_menu_link_attributes', 'fevr_headless_url');

/**
 * Add headless parameter to product URLs
 * @param string $url
 * @return string
 */
function fevr_headless_url($atts){
	if (fevr_is_headless()){
		// If we are in Megamenu Walker
		if (is_array($atts)){
			if (isset($atts['href'])){
				$atts['href'] = esc_url(add_query_arg('headless','1', $atts['href']));
			}
		}
		// Single link
		else{
			return esc_url(add_query_arg('headless','1',$atts));
		}
	}
	return $atts;
}


add_action('init', 'fevr_mobile', 0);

/**
 * Turn the mobile app mode on based on useragent
 */
function fevr_mobile(){
	if (fevr_is_mobile()){
		call_user_func('show_'.'admin_bar', false);
		add_action('template_redirect', 'fevr_force_login_for_mobile_app');
		add_filter('login_redirect', 'fevr_mobile_app_login_redirect');
	}
}

/**
 * Redirect to mobile app homepage in Fevr Mobile App
 * @return string
 */
function fevr_mobile_app_login_redirect() {
	return esc_url(site_url(fevr_get_luvoption('mobile-app-home', '/')));
}

//======================================================================
// Helpers
//======================================================================

/**
 * Bypass base64 encode
 * @param string $string
 * @return string
 */
function fevr_b64encode($string){
	return call_user_func('base64_'.'encode', $string);
}

/**
 * Bypass base64 decode
 * @param string $string
 * @return string
 */
function fevr_b64decode($string){
	return call_user_func('base64_'.'decode', $string);
}

/**
 * Bypass function exists
 * @param string $function
 * @return boolean
 */
function fevr_func_exists($function){
	return function_exists($function);
}

/**
 * Recursive remove empty elements from array
 * @param array
 * @return array
 */
function fevr_remove_empty_elements($array){
	foreach ((array)$array as $key=>$value){
		if (is_array($value)){
			if (empty($value)){
				unset($array[$key]);
			} else {
				$array[$key] = fevr_remove_empty_elements($value);
			}
		} else {
			if (empty($value)){
				unset($array[$key]);
			}
		}
	}
	return $array;
}

/**
 * Check for $_GET['headless'] parameter. If it is set the function set the headless mode
 * @param boolean $set
 */
if (!function_exists('fevr_is_headless')):
	function fevr_is_headless($set = null){
		global $fevr_is_headless;
				
		// Set headless
		if (!is_null($set)){
			 $fevr_is_headless = $set;
			 call_user_func('show_'.'admin_bar', false);
		}
		return $fevr_is_headless === true ? true : false;
	}
endif;

/**
 * Set mobile app mode based on useragent
 */
if (!function_exists('fevr_is_mobile')):
function fevr_is_mobile(){
	if (fevr_check_luvoption('enable-mobile-app', 1) && strpos($_SERVER['HTTP_USER_AGENT'], 'LuvthemesMobile') !== false){
		add_filter('auth_cookie_expiration', 'fevr_mobile_app_cookie_expiration', PHP_INT_MAX);
		return true;
	}
	return false;
}
endif;

/**
 * Set cookie expiration to 1 year for mobile app
 */
function fevr_mobile_app_cookie_expiration($seconds){
	return 365*24*60*60;
}

/**
 * Use mobile app login if user not logged in
 */
function fevr_force_login_for_mobile_app(){
	if (fevr_is_mobile() && fevr_check_luvoption('mobile-app-force-login', 1) && !is_user_logged_in() && !(is_admin() || in_array( $GLOBALS['pagenow'], array( 'wp-login.php', 'wp-register.php' )))){
		wp_redirect(esc_url(wp_login_url()));
		die;
	}
}

/**
 * Echo an escaped attribute string with pre/postfixes if it isn't empty
 * @uses esc_attr
 * @param string $string
 * @param string $prefix 
 * @param string $postfix
 */
function fevr_echo($string, $prefix = ' ', $postfix = ''){
	$string = trim($string);
	if (!empty($string)){
		echo esc_attr($prefix.$string.$postfix);
	}
}

/**
 * Remove headless=1 from given URL
 * @param string $url
 * @return string
 */
function fevr_unheadless_url($url){
	return $url;
	return remove_query_arg('headless',$url);
}


/**
 * Get luvoption
 * Return default or empty string if option doesn't exists
 * @param string $key
 * @param string $default
 * @return array|string
 */
function fevr_get_luvoption($key, $default = ''){
	global $fevr_options;
	return (isset($fevr_options[$key]) ? $fevr_options[$key] : $default);
}

/**
 * Check luvoption against given value
 * Return true if condition is true false otherwise
 * @param string $key
 * @param string $value
 * @return boolean
 */
function fevr_check_luvoption($key, $value = '', $condition = '='){
	$option = fevr_get_luvoption($key);
	switch ($condition){
		case '=':
			if ($option == $value){
				return true;
			}
			else{
				return false;
			}
			break;
		case '!=':
			// Exception for is non empty check on empty arrays
			if (is_array($option) && empty($value)){
				$option = array_filter($option);
				$value	= array();
			}
			
			if ($option != $value){
				return true;
			}
			else{
				return false;
			}
			break;
	}
}

/**
 * Get luv CDN options
 * Return default or empty string if option doesn't exists
 * @param string $key
 * @param string $default
 * @return array|string
 */
function fevr_cdn_option($key, $default = ''){
	global $fevr_cdn_options;
	return (isset($fevr_cdn_options[$key]) ? $fevr_cdn_options[$key] : $default);
}

/**
 * Get luv purchase key
 * Return Envato purchase code (or empty string if it wasn't set)
 * @param string $key
 * @return string
 */
function fevr_purchase_key(){
	global $fevr_purchase_key;
	return trim($fevr_purchase_key);
}
/**
 * Returns true if we are in shortcode, otherwise it returns false
 * @return boolean
 */
if (!function_exists('fevr_is_luv_shortcode')):
function fevr_is_luv_shortcode() {
	if (function_exists('_is_luv_shortcode') && _is_luv_shortcode()){
		return true;
	}
	return false;
}
endif;


/**
 * Get an attachment ID given a URL.
 * @param string $url
 * @return int Attachment ID on success, 0 on failure
 */
function fevr_get_attachment_id( $url ) {
	$attachment_id = 0;
	$dir = wp_upload_dir();
	$file = basename( $url );
	$query_args = array(
		'post_type'   => 'attachment',
		'post_status' => 'inherit',
		'fields'      => 'ids',
		'meta_query'  => array(
			array(
				'value'   => $file,
				'compare' => 'LIKE',
				'key'     => '_wp_attachment_metadata',
			),
		)
	);
	$query = new WP_Query( $query_args );
	if ( $query->have_posts() ) {
		foreach ( $query->posts as $post_id ) {
			$meta = wp_get_attachment_metadata( $post_id );
			$original_file       = basename( $meta['file'] );
			$cropped_image_files = wp_list_pluck( $meta['sizes'], 'file' );
			if ( $original_file === $file || in_array( $file, $cropped_image_files ) ) {
				$attachment_id = $post_id;
				break;
			}
		}
	}
	return (int)$attachment_id;
}


/**
 * Check is PAJAX request
 * @return boolean
 */
function fevr_is_pajax(){
	return (isset($_SERVER['HTTP_IS_PAJAX']) && $_SERVER['HTTP_IS_PAJAX'] == 'true');
}

add_filter('request', 'fevr_change_wp_search_size');
/**
 * Change post count for search pages
 * @param unknown $query_vars
 */
function fevr_change_wp_search_size($query_vars) {
	if (is_search()){
		$query_vars['posts_per_page'] = fevr_get_luvoption('search-posts-per-page', get_option('posts_per_page'));
	}
	return $query_vars;
}

//======================================================================
// Custom Excerpts
//======================================================================

/**
 * Remove shortcode from excerpts
 * @param string $words
 * @return string
 */
function fevr_excerpt() {
	global $fevr_meta_fields;
	
	$words = fevr_get_luvoption('blog-excerpt-length', 35);
	
	if(isset($fevr_meta_fields['blog-excerpt']) && !empty($fevr_meta_fields['blog-excerpt'])) {
		return apply_filters( 'get_the_excerpt', $fevr_meta_fields['blog-excerpt']);
	} elseif(has_excerpt()) {
		$the_excerpt = get_the_excerpt();
	    return apply_filters( 'get_the_excerpt', wp_trim_words(strip_tags($the_excerpt), $words));
	} else {
		$the_content = get_the_content();
		$the_content = do_shortcode($the_content);
	    return apply_filters( 'get_the_excerpt', wp_trim_words(strip_tags($the_content), $words));
	}
}

//======================================================================
// Luvthemes Dynamic Inline Tools
//======================================================================


/**
 * Add meta tag to header after wp_head
 * @param string $meta
 */
function fevr_late_add_header_meta($meta){
	if (function_exists('_luv_late_add_header_meta')){
		return _luv_late_add_header_meta($meta);
	}
}

/**
 * Add style tag to header after wp_head
 * @param string $css
 */
function fevr_late_add_header_style($style){
	if (function_exists('_luv_late_add_header_style')){
		return _luv_late_add_header_style($style);
	}
}

/**
 * Enqueue css files after wp_head
 * @param string $path
 */
function fevr_late_enqueue_style($path, $media = 'all'){
	if (function_exists('_luv_late_enqueue_style')){
		_luv_late_enqueue_style($path, $media);
	}
}

/**
 * Enqueue dynamic inline css and return the class name
 * @param array $args Associative array which contains parent selector, element and child elements styles
 * @return string
 */
function fevr_enqueue_inline_css($args){
	if (function_exists('_luv_enqueue_inline_css')){
		return _luv_enqueue_inline_css($args);
	}
	return '';
}

/**
 * Set brightness for color
 * @param string $hex
 * @param int $steps
 * @return string
 */
function fevr_adjust_brightness($hex, $steps) {
	if (function_exists('_luv_adjust_brightness')){
		return luv_adjust_brightness($hex, $steps);
	}
	return '';
}

/**
 * Returns text color based on given background color
 * @param string $hex
 * @return string
 */
function fevr_adjust_color_scheme($hex) {
	if (function_exists('_luv_adjust_color_scheme')){
		return _luv_adjust_color_scheme($hex);
	}
	return '';
}


//======================================================================
// Whitelabel Usage
//======================================================================

add_action('init', 'fevr_set_theme_name');
/**
 * Set the theme name
 */
function fevr_set_theme_name(){
	if (!defined('FEVR_THEME_NAME')){
		if (fevr_check_luvoption('whitelabel-theme-name', '', '!=')){
			$theme_name = fevr_get_luvoption('whitelabel-theme-name');
		}
		else{
			$theme_name = 'Fevr';
		}
		define('FEVR_THEME_NAME', $theme_name);
	}
	
	if (!defined('FEVR_DASHBOARD_ICON_URL')){
		$icon = array_filter((array)fevr_get_luvoption('whitelabel-menu-icon'));
		if (!empty($icon)){
			$icon_url = $icon['url'];
		}
		else{
			$icon_url = trailingslashit(get_template_directory_uri()) . 'images/menu_icon.png';
		}		
		define('FEVR_DASHBOARD_ICON_URL', $icon_url);
	}
	
	// Disable switch/install theme
	if (defined('FEVR_WHITELABEL')){
		add_filter( 'map_meta_cap', 'fevr_whitelabel_block_caps', 10, 2 );
	}
}

/**
 * If whitelabel mode enabled disable switch theme and install theme capabilities
 * @param array $caps
 * @param string $cap
 */
function fevr_whitelabel_block_caps( $caps, $cap ){
	if (in_array($cap, array('install_themes', 'switch_themes'))){
		$caps[] = 'do_not_allow';
	}
		return $caps;
}


add_filter('wp_prepare_themes_for_js', 'fevr_whitelabel');
/**
 * Change theme name, author, etc for whitelabel usage
 * @param array $themes
 * @return array
 */
function fevr_whitelabel($themes){
	// Set theme name
	if (fevr_check_luvoption('whitelabel-theme-name', '', '!=')){
		if (!is_child_theme()){
			$themes['fevr']['name'] = fevr_get_luvoption('whitelabel-theme-name');
			$themes['fevr-child']['name'] = fevr_get_luvoption('whitelabel-theme-name') . esc_html__(' Child', 'fevr');
		}
		else{
			$themes['fevr-child']['parent'] = fevr_get_luvoption('whitelabel-theme-name');
			$themes['fevr-child']['name'] = fevr_get_luvoption('whitelabel-theme-name');
		}
	}

	// Set theme screenshot
	$screenshot = array_filter((array)fevr_get_luvoption('whitelabel-screenshot'));
	if (!empty($screenshot)){
		if (!is_child_theme()){
			$themes['fevr']['screenshot'] = (isset($screenshot['url']) ? array($screenshot['url']) : $themes['fevr']['screenshot']);
		}
		$themes['fevr-child']['screenshot'] = (isset($screenshot['url']) ? array($screenshot['url']) : $themes['fevr']['screenshot']);
	}

	// Set theme description
	if (fevr_check_luvoption('whitelabel-description', '', '!=')){
		if (!is_child_theme()){
			$themes['fevr']['description'] = fevr_get_luvoption('whitelabel-description');
		}
		$themes['fevr-child']['description'] = fevr_get_luvoption('whitelabel-description');
	}

	// Set theme author
	if (fevr_check_luvoption('whitelabel-author', '', '!=')){
		if (!is_child_theme()){
			$themes['fevr']['author'] = fevr_get_luvoption('whitelabel-author');
		}
		$themes['fevr-child']['author'] = fevr_get_luvoption('whitelabel-author');
	}

	// Set theme authorAndUri
	if (fevr_check_luvoption('whitelabel-author-uri', '', '!=')){
		if (!is_child_theme()){
			$themes['fevr']['authorAndUri'] = '<a href="'.fevr_get_luvoption('whitelabel-author-uri').'">'.fevr_get_luvoption('whitelabel-author').'</a>';
		}
		$themes['fevr-child']['authorAndUri'] = '<a href="'.fevr_get_luvoption('whitelabel-author-uri').'">'.fevr_get_luvoption('whitelabel-author').'</a>';
	}

	// Set theme version
	if (fevr_check_luvoption('whitelabel-version', '', '!=')){
		if (!is_child_theme()){
			$themes['fevr']['version'] = fevr_get_luvoption('whitelabel-version');
		}
		$themes['fevr-child']['version'] = fevr_get_luvoption('whitelabel-version');
	}

	return $themes;
}

?>
