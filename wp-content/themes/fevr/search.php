<?php
	
	get_header();
	
	// Classes for .posts-container and #content-wrapper
	$fevr_posts_container_classes = array();
	// Columns
	if(fevr_check_luvoption('search-columns', '', '!=')) {
		$fevr_posts_container_classes[] = $fevr_options['search-columns'];
	}
	
	// Equal height
	if(fevr_check_luvoption('search-equal-height', '1', '=')) {
		$fevr_posts_container_classes[] = 'masonry-equal-height';
	}
?>



<div id="content-wrapper" class="wrapper-padding">
	<div class="container">
		<div id="page-header-default">
			<h1 class="page-header-title"><?php esc_html_e('Search results for', 'fevr'); ?> "<?php echo esc_html(get_search_query()); ?>"</h1>
		</div>
	</div>
	
	<div class="container">
		<div class="l-grid-row">
			<main id="main-content">
				<?php
					get_search_form();
					
					$classes = array(
					'hentry',
					'post',
					);
					
					if ( have_posts() ) :
				?>
						<div class="search-results posts-container item-grid-container <?php echo esc_attr(implode(' ', $fevr_posts_container_classes)); ?> masonry masonry-standard" data-crop-images="true">
				<?php
						while ( have_posts() ) : the_post();
				?>
							<article id="post-<?php the_ID(); ?>" <?php post_class($classes); ?>>
								<?php get_template_part( 'luvthemes/post-templates/search'); ?>
							</article>
						<?php endwhile;?>
						</div>
				<?php else: ?>
						<?php if (fevr_get_luvoption('search-guess-typo',1)):?>
						<?php echo fevr_search_tip();?>
						<?php endif;?>
						<p><?php esc_html_e('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'fevr');?></p>
				<?php endif; ?>
				<div class="pagination-container">
					<?php
					$big = PHP_INT_MAX;
					echo paginate_links( array(
						'base' => str_replace( $big, '%#%',  get_pagenum_link( $big, false ) ) ,
						'format' => '?paged=%#%',
						'prev_text'          => '&larr;',
						'next_text'          => '&rarr;',
						'current' => max( 1, get_query_var('paged') ),
					) );
					?>
				</div>
			</main>
		</div>
	</div>
</div>


<?php get_footer(); ?>
