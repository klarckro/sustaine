<?php get_header(); ?>

<div id="content-wrapper">
	<div class="container">
		<div id="page-header-default">			
			<h1 class="page-header-title is-center"><?php esc_html_e('404 - Page Not Found', 'fevr'); ?></h1>
		</div>
	</div>
	
	<div class="container">
		<div class="l-grid-row">
			<main id="main-content" class="is-center">
				<h2><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'fevr' ); ?></h2>
				<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try a search?', 'fevr' ); ?></p>
				<?php get_search_form(); ?>
			</main>
		</div>
	</div>
</div>


<?php get_footer(); ?>