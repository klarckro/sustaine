<aside id="sidebar">
	<?php
		global $post;
		
		if(is_object($post)) {
			if($post->post_type == 'luv_portfolio') {
				$sidebar = fevr_get_luvoption('portfolio-sidebar');
				
			} elseif ($post->post_type == 'post') {
				$sidebar = fevr_get_luvoption('blog-sidebar');
				
			} elseif ($post->post_type == 'product') {
				$sidebar = fevr_get_luvoption('woocommerce-sidebar');
			
			} elseif ($post->post_type == 'fevr_ext_reviews') {
				$sidebar = fevr_get_luvoption('woocommerce-photo-reviews-sidebar');
			
			} elseif ($post->post_type == 'luv_collections') {
				$sidebar = fevr_get_luvoption('woocommerce-collections-sidebar');
				
			} elseif ($post->post_type == 'topic' || $post->post_type == 'forum' || $post->post_type == 'reply') {
				$sidebar = fevr_get_luvoption('bbpress-sidebar');
				
			} elseif ($post->post_type == 'page') {
				$fevr_meta_fields = get_post_meta( $post->ID, 'fevr_meta', true);
				$sidebar = isset($fevr_meta_fields['page-sidebar']) && !empty($fevr_meta_fields['page-sidebar']) ? $fevr_meta_fields['page-sidebar'] : 'page-sidebar';
			
			} else {
				$sidebar = 'blog-sidebar';
			}
		}
		else {
			$sidebar = 'blog-sidebar';
		}
		
		if (function_exists('dynamic_sidebar') && is_active_sidebar($sidebar)):
			dynamic_sidebar($sidebar);
		elseif (current_user_can('edit_theme_options')):
	?>	
	<div class="widget">			
		<h4 class="widget-title"><?php esc_html_e('Widget Area', 'fevr'); ?></h4>
		<?php if (is_user_logged_in() && current_user_can('manage_options')):?>
			<p class="no-widgets"><a href="<?php echo esc_url(admin_url('widgets.php')); ?>"><?php esc_html_e('Click here to assign widgets.', 'fevr'); ?></a></p>
		<?php endif;?>
	</div>
	<?php endif; ?>
</aside>