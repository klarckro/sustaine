<?php
/**
 * Customer new account email
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails/Plain
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

echo "= " . $email_heading . " =\n\n";

echo sprintf( esc_html__("You've unlocked a new discount on %s", 'fevr' ), $blogname) . "\n\n";
if (!empty($coupon_description)){
	echo esc_html($coupon_description) . "\n\n";
}
echo sprintf( esc_html__( "Your coupon code is %s.", 'fevr' ), $coupon_code ) . "\n\n";
if (!empty($coupon_expiry)){
	echo sprintf( esc_html__( "Your is valid until %s.", 'fevr' ), $coupon_expiry ) . "\n\n";
}

echo sprintf( esc_html__( 'Use your coupon now: %s.', 'fevr' ), wc_get_page_permalink( 'myaccount' ) ) . "\n\n";

echo apply_filters( 'woocommerce_email_footer_text', get_option( 'woocommerce_email_footer_text' ) );
