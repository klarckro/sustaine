<?php
/**
 * Customer new account email
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php do_action( 'woocommerce_email_header', $email_heading ); ?>

<p><?php printf( esc_html__( "You\'ve unlocked a new discount on %s:", 'fevr' ), esc_html( $blogname ) ); ?></p>
<?php if(!empty($coupon_description)):?>
<p><?php echo esc_html( $coupon_description ); ?></p>
<?php endif;?>
<p><?php printf( esc_html__( "Your coupon code is <strong>%s</strong>.", 'fevr' ), esc_html( $coupon_code ) ); ?></p>
<p><?php printf( esc_html__( "Your coupon code is valid until <strong>%s</strong>.", 'fevr' ), esc_html( $coupon_expiry ) ); ?></p>

<p><?php printf( esc_html__( 'Use your coupon now: %s.', 'fevr' ), wc_get_page_permalink( 'myaccount' ) ); ?></p>

<?php do_action( 'woocommerce_email_footer' ); ?>
