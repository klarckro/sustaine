<?php
/**
 * Order tracking form
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

?>

<form action="<?php echo esc_url( get_permalink( $post->ID ) ); ?>" method="post" class="track_order">

	<p><?php fevr_ew( 'To track your order please enter your Order ID in the box below and press the "Track" button. This was given to you on your receipt and in the confirmation email you should have received.', 'fevr', 'woocommerce' ); ?></p>

	<p class="form-row form-row-first"><label for="orderid"><?php fevr_ew( 'Order ID', 'fevr', 'woocommerce' ); ?></label> <input class="input-text" type="text" name="orderid" id="orderid" placeholder="<?php echo esc_attr(fevr__w('Found in your order confirmation email.', 'fevr', 'woocommerce')); ?>" /></p>
	<p class="form-row form-row-last"><label for="order_email"><?php fevr_ew( 'Billing Email', 'fevr', 'woocommerce' ); ?></label> <input class="input-text" type="text" name="order_email" id="order_email" placeholder="<?php echo esc_attr(fevr__w('Email you used during checkout.', 'fevr', 'woocommerce')); ?>" /></p>
	<div class="clear"></div>

	<p class="form-row"><input type="submit" class="button btn btn-full btn-global" name="track" value="<?php echo esc_attr(fevr__w('Track', 'fevr', 'woocommerce')); ?>" /></p>
	<?php wp_nonce_field( 'woocommerce-order_tracking' ); ?>

</form>
