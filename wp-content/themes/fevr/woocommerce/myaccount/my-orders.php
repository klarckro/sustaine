<?php
/**
 * My Orders
 *
 * Shows recent orders on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-orders.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Whishlist
if (isset($_COOKIE['luv-woocommerce-wishlist'])){
	$wishlist = array_filter(json_decode(stripslashes($_COOKIE['luv-woocommerce-wishlist']), true));
}
else{
	$wishlist = array();
}

$products = array();
foreach ((array)$wishlist as $item){
	$_product = new WC_Product($item);
	if ($_product->id){
		$products[] = $_product;
	}
}

?>

<?php if (count($wishlist) > 0):?>
<h2><?php echo apply_filters( 'woocommerce_my_account_whishlist_title', fevr__w( 'Whislist', 'fevr', 'woocommerce' ) ); ?></h2>
<table class="shop_table cart" cellspacing="0">
	<thead>
		<tr>
			<th class="product-remove">&nbsp;</th>
			<th class="product-thumbnail">&nbsp;</th>
			<th class="product-name"><?php fevr_ew( 'Product', 'fevr', 'woocommerce' ); ?></th>
			<th class="product-price"><?php fevr_ew( 'Price', 'fevr', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php do_action( 'woocommerce_before_cart_contents' ); ?>

		<?php
		foreach ($products as $_product) {
			$product_id		= $_product->id;
			$product_title	= $_product->post->post_title; 

			if ( $_product && $_product->exists() ) {
				?>
				<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $product_id) ); ?>">

					<td class="product-remove">
						<a href="#" class="luv-wc-wishlist" data-product-title="<?php echo esc_attr($product_title);?>" data-product-id="<?php echo esc_attr($product_id);?>" data-remove-closest="tr"><i class="ion-close"></i></a>
					</td>

					<td class="product-thumbnail">
						<?php
							$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $product_id);

							if ( ! $_product->is_visible() ) {
								echo fevr_kses($thumbnail);
							} else {
								printf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $product_id ) ), $thumbnail );
							}
						?>
					</td>

					<td class="product-name">
						<?php
							if ( ! $_product->is_visible() ) {
								echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $product_id ) . '&nbsp;';
							} else {
								echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s </a>', esc_url( $_product->get_permalink( $product_id ) ), $_product->get_title() ), $product_id);
							}

							// Meta data
							echo WC()->cart->get_item_data( $product_id );

						?>
					</td>

					<td class="product-price">
						<?php
							echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $product_id);
						?>
					</td>

				</tr>
				<?php
			}
		}

		do_action( 'woocommerce_cart_contents' );
		?>
		<?php do_action( 'woocommerce_after_cart_contents' ); ?>
	</tbody>
</table>
<?php endif;

$my_orders_columns = apply_filters( 'woocommerce_my_account_my_orders_columns', array(
	'order-number'  => esc_html(fevr__w('Order', 'fevr', 'woocommerce' )),
	'order-date'    => esc_html(fevr__w('Date', 'fevr', 'woocommerce' )),
	'order-status'  => esc_html(fevr__w('Status', 'fevr', 'woocommerce' )),
	'order-total'   => esc_html(fevr__w('Total', 'fevr', 'woocommerce' )),
	'order-actions' => '&nbsp;',
) );

$customer_orders = get_posts( apply_filters( 'woocommerce_my_account_my_orders_query', array(
	'numberposts' => $order_count,
	'meta_key'    => '_customer_user',
	'meta_value'  => get_current_user_id(),
	'post_type'   => wc_get_order_types( 'view-orders' ),
	'post_status' => array_keys( wc_get_order_statuses() )
) ) );

if ( $customer_orders ) : ?>

	<h2><?php echo apply_filters( 'woocommerce_my_account_my_orders_title', esc_html(fevr__w('Recent Orders', 'fevr', 'woocommerce' ))); ?></h2>

	<table class="shop_table shop_table_responsive my_account_orders">

		<thead>
			<tr>
				<?php foreach ( $my_orders_columns as $column_id => $column_name ) : ?>
					<th class="<?php echo esc_attr( $column_id ); ?>"><span class="nobr"><?php echo esc_html( $column_name ); ?></span></th>
				<?php endforeach; ?>
			</tr>
		</thead>

		<tbody>
			<?php foreach ( $customer_orders as $customer_order ) :
				$order      = wc_get_order( $customer_order );
				$item_count = $order->get_item_count();
				?>
				<tr class="order">
					<?php foreach ( $my_orders_columns as $column_id => $column_name ) : ?>
						<td class="<?php echo esc_attr( $column_id ); ?>" data-title="<?php echo esc_attr( $column_name ); ?>">
							<?php if ( has_action( 'woocommerce_my_account_my_orders_column_' . $column_id ) ) : ?>
								<?php do_action( 'woocommerce_my_account_my_orders_column_' . $column_id, $order ); ?>

							<?php elseif ( 'order-number' === $column_id ) : ?>
								<a href="<?php echo esc_url( $order->get_view_order_url() ); ?>">
									<?php echo esc_html(fevr_x('#', 'hash before order number', 'fevr', 'woocommerce')) . $order->get_order_number(); ?>
								</a>

							<?php elseif ( 'order-date' === $column_id ) : ?>
								<time datetime="<?php echo date( 'Y-m-d', strtotime( $order->order_date ) ); ?>" title="<?php echo esc_attr( strtotime( $order->order_date ) ); ?>"><?php echo date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ); ?></time>

							<?php elseif ( 'order-status' === $column_id ) : ?>
								<?php echo wc_get_order_status_name( $order->get_status() ); ?>

							<?php elseif ( 'order-total' === $column_id ) : ?>
								<?php echo sprintf( esc_html(fevr_n( '%s for %s item', '%s for %s items', $item_count, 'fevr', 'woocommerce'))), $order->get_formatted_order_total(), $item_count ); ?>

							<?php elseif ( 'order-actions' === $column_id ) : ?>
								<?php
									$actions = array(
										'pay'    => array(
											'url'  => $order->get_checkout_payment_url(),
											'name' => esc_html(fevr__w('Pay', 'fevr', 'woocommerce' ))
										),
										'view'   => array(
											'url'  => $order->get_view_order_url(),
											'name' => esc_html(fevr__w('View', 'fevr', 'woocommerce' ))
										),
										'cancel' => array(
											'url'  => $order->get_cancel_order_url( wc_get_page_permalink( 'myaccount' ) ),
											'name' => esc_html(fevr__w('Cancel', 'fevr', 'woocommerce' ))
										)
									);

									if ( ! $order->needs_payment() ) {
										unset( $actions['pay'] );
									}

									if ( ! in_array( $order->get_status(), apply_filters( 'woocommerce_valid_order_statuses_for_cancel', array( 'pending', 'failed' ), $order ) ) ) {
										unset( $actions['cancel'] );
									}

									if ( $actions = apply_filters( 'woocommerce_my_account_my_orders_actions', $actions, $order ) ) {
										foreach ( $actions as $key => $action ) {
											echo '<a href="' . esc_url( $action['url'] ) . '" class="button ' . sanitize_html_class( $key ) . '">' . esc_html( $action['name'] ) . '</a>';
										}
									}
								?>
							<?php endif; ?>
						</td>
					<?php endforeach; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
<?php endif; ?>
