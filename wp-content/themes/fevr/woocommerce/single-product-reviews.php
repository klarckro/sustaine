<?php
/**
 * Display single product reviews (comments)
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.2
 */
global $product;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! comments_open() ) {
	return;
}

?>
<div id="reviews">
	<div id="comments">
		<h2><?php
			if ( get_option( 'woocommerce_enable_review_rating' ) === 'yes' && ( $count = $product->get_review_count() ) )
				printf( esc_html(fevr_n('%s review for %s', '%s reviews for %s', $count, 'fevr', 'woocommerce')), $count, get_the_title() );
			else
				fevr_ew( 'Reviews', 'fevr', 'woocommerce' );
		?></h2>

		<?php if ( have_comments() ) : ?>

			<ol class="commentlist">
				<?php wp_list_comments( apply_filters( 'woocommerce_product_review_list_args', array( 'callback' => 'woocommerce_comments' ) ) ); ?>
			</ol>

			<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
				echo '<nav class="woocommerce-pagination">';
				paginate_comments_links( apply_filters( 'woocommerce_comment_pagination_args', array(
					'prev_text' => '&larr;',
					'next_text' => '&rarr;',
					'type'      => 'list',
				) ) );
				echo '</nav>';
			endif; ?>

		<?php else : ?>

			<p class="woocommerce-noreviews"><?php fevr_ew( 'There are no reviews yet.', 'fevr', 'woocommerce' ); ?></p>

		<?php endif; ?>
	</div>

	<?php if ( get_option( 'woocommerce_review_rating_verification_required' ) === 'no' || wc_customer_bought_product( '', get_current_user_id(), $product->id ) ) : ?>

		<div id="review_form_wrapper">
			<div id="review_form">
				<?php
					$commenter = wp_get_current_commenter();

					$comment_form = array(
						'title_reply'          => have_comments() ? fevr__w( 'Add a review', 'fevr', 'woocommerce' ) : fevr__w( 'Be the first to review', 'fevr', 'woocommerce' ) . ' &ldquo;' . get_the_title() . '&rdquo;',
						'title_reply_to'       => fevr__w( 'Leave a Reply to %s', 'fevr', 'woocommerce' ),
						'title_reply_after'	   => '</h3>'.fevr_get_woocommerce_photo_review_button(),
						'comment_notes_before' => '',
						'comment_notes_after'  => '',
						'class_submit'      => 'submit btn btn-global btn-full',
						'fields'               => array(
							'author' => '<p class="comment-form-author">' . '<label for="author">' . fevr__w( 'Name', 'fevr', 'woocommerce' ) . ' <span class="required">*</span></label> ' .
							            '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" aria-required="true" /></p>',
							'email'  => '<p class="comment-form-email"><label for="email">' . fevr__w( 'Email', 'fevr', 'woocommerce' ) . ' <span class="required">*</span></label> ' .
							            '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" aria-required="true" /></p>',
						),
						'label_submit'  => fevr__w( 'SUBMIT', 'fevr', 'woocommerce' ),
						'logged_in_as'  => '',
						'comment_field' => ''
					);

					if ( $account_page_url = wc_get_page_permalink( 'myaccount' ) ) {
						$comment_form['must_log_in'] = '<p class="must-log-in">' .  sprintf( fevr__w( 'You must be <a href="%s">logged in</a> to post a review.', 'fevr', 'woocommerce' ), esc_url( $account_page_url ) ) . '</p>';
					}

					if ( get_option( 'woocommerce_enable_review_rating' ) === 'yes' ) {
						$comment_form['comment_field'] = '<p class="comment-form-rating"><label for="rating">' . fevr__w( 'Your Rating', 'fevr', 'woocommerce' ) .'</label><select name="rating" id="rating">
							<option value="">' . fevr__w( 'Rate&hellip;', 'fevr', 'woocommerce' ) . '</option>
							<option value="5">' . fevr__w( 'Perfect', 'fevr', 'woocommerce' ) . '</option>
							<option value="4">' . fevr__w( 'Good', 'fevr', 'woocommerce' ) . '</option>
							<option value="3">' . fevr__w( 'Average', 'fevr', 'woocommerce' ) . '</option>
							<option value="2">' . fevr__w( 'Not that bad', 'fevr', 'woocommerce' ) . '</option>
							<option value="1">' . fevr__w( 'Very Poor', 'fevr', 'woocommerce' ) . '</option>
						</select></p>';
					}

					$comment_form['comment_field'] .= '<p class="comment-form-comment"><label for="comment">' . fevr__w( 'Your Review', 'fevr', 'woocommerce' ) . '</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>';

					comment_form( apply_filters( 'woocommerce_product_review_comment_form_args', $comment_form ) );
				?>
			</div>
		</div>

	<?php else : ?>

		<p class="woocommerce-verification-required"><?php fevr_ew( 'Only logged in customers who have purchased this product may leave a review.', 'fevr', 'woocommerce' ); ?></p>

	<?php endif; ?>

	<div class="clear"></div>
</div>
