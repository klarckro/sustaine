<?php
/**
 * Product Loop Start
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

global $fevr_wc_carousel, $fevr_is_wc_masonry, $fevr_wc_gutter;

$classes = array();

if (!empty($fevr_wc_carousel)){
	$classes[] = 'luv-carousel';
}

if ($fevr_is_wc_masonry === true){
	$classes[] = 'masonry';
}

if ($fevr_wc_gutter === true){
	$classes[] = 'masonry-no-gap';
}

?>
<ul class="products<?php fevr_echo(implode(' ', $classes));?>" <?php echo (!empty($fevr_wc_carousel) ? implode(' ', $fevr_wc_carousel) : '')?>>