<?php
/**
 * Cross-sells
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop, $fevr_options;

$crosssells = WC()->cart->get_cross_sells();

if ( sizeof( $crosssells ) == 0 ) return;

$meta_query = WC()->query->get_meta_query();

$args = array(
	'post_type'           => 'product',
	'ignore_sticky_posts' => 1,
	'no_found_rows'       => 1,
	'posts_per_page'      => apply_filters( 'woocommerce_cross_sells_total', $posts_per_page ),
	'orderby'             => $orderby,
	'post__in'            => $crosssells,
	'meta_query'          => $meta_query
);

$products = new WP_Query( $args );

$woocommerce_loop['columns'] = apply_filters( 'woocommerce_cross_sells_columns', $columns );

if ( $products->have_posts() ) : ?>
	<div class="cart-collaterals cart-collaterals-cross-sells">
		<div class="cross-sells">
			<div class="products-container <?php echo isset($fevr_options['woocommerce-columns']) && !empty($fevr_options['woocommerce-columns']) ? $fevr_options['woocommerce-columns'] : ''; ?> <?php echo isset($fevr_options['woocommerce-product-box-style']) && !empty($fevr_options['woocommerce-product-box-style']) ? $fevr_options['woocommerce-product-box-style'] : 'wc-style-1'; ?>">
				<h2><?php fevr_ew( 'You may be interested in&hellip;', 'fevr', 'woocommerce' ) ?></h2>
		
				<?php woocommerce_product_loop_start(); ?>
		
					<?php while ( $products->have_posts() ) : $products->the_post(); ?>
		
						<?php wc_get_template_part( 'content', 'product' ); ?>
		
					<?php endwhile; // end of the loop. ?>
		
				<?php woocommerce_product_loop_end(); ?>
			</div>
	
		</div>
	</div>

<?php endif;

wp_reset_query();
