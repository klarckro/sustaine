<?php
/**
 * Proceed to checkout button
 *
 * Contains the markup for the proceed to checkout button on the cart
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

echo '<a href="' . esc_url( WC()->cart->get_checkout_url() ) . '" class="checkout-button button alt wc-forward btn btn-global btn-full btn-icon btn-icon-slide-right btn-block btn-accent-1 btn-shortcode"><span>' . fevr__w( 'Proceed to Checkout', 'fevr', 'woocommerce' ) . '</span><i class="fa fa-long-arrow-right"></i></a>';
