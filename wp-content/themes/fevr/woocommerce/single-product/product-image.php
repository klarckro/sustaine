<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.6.3
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $post, $product;
?>
<div class="images">
	<?php
		if ( has_post_thumbnail() ) {
			$attachment_count = count( $product->get_gallery_attachment_ids() );
			$attachment_ids = $product->get_gallery_attachment_ids();
			$gallery          = $attachment_count > 0 ? '[product-gallery]' : '';
			$props            = wc_get_product_attachment_props( get_post_thumbnail_id(), $post );
			$image            = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
				'title'	 => $props['title'],
				'alt'    => $props['alt'],
			) );
			
			if ( $attachment_count > 0 ) {
				echo '<ul class="product-gallery-carousel" data-gallery-arrows="true">';
				echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<li class="slide-%s"><div class="product-zoom"><a href="%s" rel="woocommerce-attachment">%s</a></div></li>', get_post_thumbnail_id(), $props['url'], $image ), $post->ID );
				
				foreach ( $attachment_ids as $attachment_id ) {

					$image_link = wp_get_attachment_url( $attachment_id );
		
					if ( ! $image_link )
						continue;
		
					$image_title 	= esc_attr( get_the_title( $attachment_id ) );
					$image_caption 	= esc_attr( get_post_field( 'post_excerpt', $attachment_id ) );
		
					$image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_single' ), 0, $attr = array(
						'title'	=> $image_title,
						'alt'	=> $image_title
						) );
		
					echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<li class="slide-%s"><div class="product-zoom"><a href="%s" itemprop="image" rel="woocommerce-attachment" title="%s">%s</a></div></li>', $attachment_id, $image_link, esc_attr( $props['caption'] ), $image ), $attachment_id, $post->ID );
		
				}
				echo '</ul>';	
			} else {
				echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<div class="product-zoom"><a href="%s" rel="woocommerce-attachment">%s</a></div>', $props['url'], $image ), $post->ID );
			}
			
			
		} else {
			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), fevr__w( 'Placeholder', 'fevr', 'woocommerce' ) ), $post->ID );
		}

		do_action( 'woocommerce_product_thumbnails' );
	?>
</div>
