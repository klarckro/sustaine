<?php
/**
 * Thankyou page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( $order ) : ?>

	<?php if ( $order->has_status( 'failed' ) ) : ?>

		<p><?php fevr_ew( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction.', 'fevr', 'woocommerce' ); ?></p>

		<p><?php
			if ( is_user_logged_in() )
				fevr_ew( 'Please attempt your purchase again or go to your account page.', 'fevr', 'woocommerce' );
			else
				fevr_ew( 'Please attempt your purchase again.', 'fevr', 'woocommerce' );
		?></p>

		<p>
			<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php fevr_ew( 'Pay', 'fevr', 'woocommerce' ) ?></a>
			<?php if ( is_user_logged_in() ) : ?>
			<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php fevr_ew( 'My Account', 'fevr', 'woocommerce' ); ?></a>
			<?php endif; ?>
		</p>

	<?php else : ?>

		<h2><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', fevr__w( 'Thank you. Your order has been received.', 'fevr', 'woocommerce' ), $order ); ?></h2>

		<ul class="order_details">
			<li class="order">
				<?php fevr_ew( 'Order Number:', 'fevr', 'woocommerce' ); ?>
				<strong><?php echo fevr_kses($order->get_order_number()); ?></strong>
			</li>
			<li class="date">
				<?php fevr_ew( 'Date:', 'fevr', 'woocommerce' ); ?>
				<strong><?php echo date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ); ?></strong>
			</li>
			<li class="total">
				<?php fevr_ew( 'Total:', 'fevr', 'woocommerce' ); ?>
				<strong><?php echo fevr_kses($order->get_formatted_order_total()); ?></strong>
			</li>
			<?php if ( $order->payment_method_title ) : ?>
			<li class="method">
				<?php fevr_ew( 'Payment Method:', 'fevr', 'woocommerce' ); ?>
				<strong><?php echo fevr_kses($order->payment_method_title); ?></strong>
			</li>
			<?php endif; ?>
		</ul>
		<div class="clear"></div>

	<?php endif; ?>

	<?php do_action( 'woocommerce_thankyou_' . $order->payment_method, $order->id ); ?>
	<?php do_action( 'woocommerce_thankyou', $order->id ); ?>

<?php else : ?>

	<p><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', fevr__w( 'Thank you. Your order has been received.', 'fevr', 'woocommerce' ), null ); ?></p>

<?php endif; ?>
