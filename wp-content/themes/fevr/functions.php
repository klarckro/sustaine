<?php
/**
* WordPress related functions
*/

//======================================================================
// Include constants
//======================================================================

require_once trailingslashit(get_template_directory()) . 'luvthemes/constants.php';

//======================================================================
// Localization for 3rd party plugins
//======================================================================

if ( ! function_exists( 'fevr__w' ) ) :
	/**
	 * Wrapper for __() gettext function.
	 * @param  string $string     Translatable text string
	 * @param  string $textdomain Text domain, default: empty
	 * @return string
	 */
	function fevr__w( $string, $textdomain = '', $foreign = '' ) {
		if (!empty($foreign)){
			$textdomain = $foreign;
		}
		return call_user_func('__', $string, $textdomain );
	}
endif;
if ( ! function_exists( 'fevr_ew' ) ) :
	/**
	 * Wrapper for _e() gettext function.
	 * @param  string $string     Translatable text string
	 * @param  string $textdomain Text domain, default: empty
	 * @return void
	 */
	function fevr_ew( $string, $textdomain = '', $foreign = '') {
		if (!empty($foreign)){
			$textdomain = $foreign;
		}
		call_user_func('_e', $string, $textdomain );
	}
endif;

if ( ! function_exists( 'fevr_x' ) ) :
/**
 * Wrapper for _x() gettext function.
* @param  string $string     Translatable text string
* @param  string $context	Context
* @param  string $textdomain Text domain, default: empty
* @param  string $forign Forign textdomain
* @return void
*/
function fevr_x( $string, $context, $textdomain = '', $foreign ) {
	if (!empty($foreign)){
		$textdomain = $foreign;
	}
	return call_user_func('_x', $string, $context, $textdomain );
}
endif;

if ( ! function_exists( 'fevr_n' ) ) :
/**
 * Wrapper for _n() gettext function.
* @param  string $singular     Translatable text string
* @param  string $plural	Plural
* @param  string $textdomain Text domain, default: empty
* @param  string $forign Forign textdomain
* @return string
*/
function fevr_n( $singular, $plural, $number, $textdomain = '', $foreign ) {
	if (!empty($foreign)){
		$textdomain = $foreign;
	}
	return call_user_func('_n', $singular, $plural, $number, $textdomain );
}
endif;

if ( ! function_exists( 'fevr_nx' ) ) :
/**
 * Wrapper for _nx() gettext function.
* @param  string $singular     Translatable text string
* @param  string $plural	Plural
* @param  string $context	Context
* @param  string $textdomain Text domain, default: empty
* @param  string $forign Forign textdomain
* @return string
*/
function fevr_nx( $singular, $plural, $number, $context, $textdomain = '', $foreign ) {
	if (!empty($foreign)){
		$textdomain = $foreign;
	}
	return call_user_func('_nx', $singular, $plural, $number, $context, $textdomain );
}
endif;

if ( ! function_exists( 'fevr_n_noop' ) ) :
/**
 * Wrapper for _n_noop() gettext function.
* @param  string $string     Translatable text string
* @param  string $plural	Plural
* @param  string $textdomain Text domain, default: empty
* @param  string $forign Forign textdomain
* @return string
*/
function fevr_n_noop( $singular, $plural, $textdomain = '', $foreign ) {
	if (!empty($foreign)){
		$textdomain = $foreign;
	}
	return call_user_func('_n_noop', $singular, $plural, $textdomain );
}
endif;




//======================================================================
// Menu page handlers
//======================================================================

/**
 * Create menu elements
 */
function fevr_add_fevr_menu_page(){
	call_user_func_array('add_'.'menu_page', func_get_args());
}

/**
 * Create submenu elements
 */
function fevr_add_fevr_submenu_page(){
	call_user_func_array('add_'.'submenu_page', func_get_args());
}

//======================================================================
// Basic Helpers
//======================================================================


/**
 * Filter string using wp_kses
 * @param string $string
 * @return string
 */
function fevr_kses($string){

	// Allowed data attributes
	$data_attributes = array(
		'data-event-category' => array(),
		'data-event-action' => array(),
		'data-event-label' => array(),
		'data-event-value' => array(),
		'data-animation' => array(),
		'data-color' => array(),
		'data-tipso' => array(),
		'data-tooltip-color' => array(),
		'data-tooltip-background-color' => array(),
	);

	// Allowed tags
	$allowed_tags = array(
			'a' => array(
					'href' => array(),
					'title' => array(),
					'class' => array(),
					'style' => array(),
					'target' => array(),
					'data-page' => array(),
			),
			'br' => array(),
			'em' => array(),
			'strong' => array(),
			'div' => array(
					'id' => array(),
					'class' => array(),
					'style' => array(),
					'data-labels' => array(),
					'data-clockface' => array(),
					'data-countdown' => array(),
					'data-logout-url' => array(),
					'data-post_types' => array(),
					'data-box_style' => array(),
					'data-pagination' => array(),
					'data-posts_per_page' => array(),
					'data-columns' => array(),
					'data-hide_featured_image' => array(),
					'data-hide_title' => array(),
					'data-hide_excerpt' => array(),
					'data-hide_date' => array(),
					'data-hide_author' => array(),
					'data-hide_category' => array(),
					'data-hide_tags' => array(),
					'data-post_classes' => array(),
					'data-compare' => array(),
					'data-in' => array(),
					'data-meta_key' => array(),
					'data-meta_type' => array(),
					'data-href' => array(),
					'data-width' => array(),
					'data-order-by' => array(),
					'data-numposts' => array(),
			),
			'span' => array(
					'typeof' => array(),
					'id' => array(),
					'class' => array(),
					'style' => array(),
					'data-luv-share-count' => array(),
					'data-luv-share-url' => array(),
					'data-speed' => array(),
					'data-step' => array(),
			),
			'ul' => array(
					'id' => array(),
					'class' => array(),
					'style' => array(),
					'data-type' => array(),
					'data-zoom' => array(),
					'data-disable-ui' => array(),
					'data-scrollwheel' => array(),
					'data-map-style' => array(),
			),
			'li' => array(
					'id' => array(),
					'class' => array(),
					'style' => array(),
			),
			'i' => array(
					'id' => array(),
					'class' => array(),
					'style' => array(),
			),
			'p' => array(
					'id' => array(),
					'class' => array(),
					'style' => array(),
			),
			'pre' => array(
					'id' => array(),
					'class' => array(),
					'style' => array(),
			),
			'code' => array(
					'id' => array(),
					'class' => array(),
					'style' => array(),
			),
			'iframe' => array(
					'width' => array(),
					'height' => array(),
					'frameborder' => array(),
					'src' => array(),
					'marginwidth' => array(),
					'marginheight' => array(),
					'id' => array(),
					'class' => array(),
					'style' => array(),
			),
			'form' => array(
					'action' => array(),
					'method' => array(),
					'enctype' => array(),
					'id' => array(),
					'class' => array(),
					'style' => array(),
			),
			'img' => array(
					'width' => array(),
					'height' => array(),
					'src' => array(),
					'srcset' => array(),
					'data-src' => array(),
					'data-srcset' => array(),
					'data-luv-lazy-load' => array(),
					'marginwidth' => array(),
					'marginheight' => array(),
					'id' => array(),
					'class' => array(),
					'style' => array(),
					'alt' => array(),
			),
			'button' => array(
				'id'	=> array(),
				'class' => array(),	
				'style' => array(),
			),
			'input' => array(
					'type' => array(),
					'name' => array(),
					'value' => array(),
					'id' => array(),
					'placeholder' => array(),
					'class' => array(),
					'style' => array(),
					'autocomplete' => array(),
			),
			'textarea' => array(
					'name' => array(),
					'id' => array(),
					'placeholder' => array(),
					'class' => array(),
					'style' => array(),
					'autocomplete' => array(),
			),
			'label' => array(
					'for' => array(),
					'id' => array(),
					'class' => array(),
					'style' => array(),
			)
	);

	// Add extra attributes
	foreach ($allowed_tags as $key=>$value){
		$allowed_tags[$key] = array_merge($allowed_tags[$key], $data_attributes);
	}

	return wp_kses($string, $allowed_tags);
}


//======================================================================
// Load textdomain
//======================================================================
load_theme_textdomain( 'fevr', trailingslashit(get_template_directory()) . 'languages');

//======================================================================
// WooCommerce Image Sizes
//======================================================================

add_action( 'after_switch_theme', 'fevr_woocommerce_image_dimensions', 1 );

/**
 * Set image dimensions for WooCommerce
 * This function will run only after switch theme 
 */
function fevr_woocommerce_image_dimensions() {
	global $pagenow;
 
	if ( ! isset( $_GET['activated'] ) || $pagenow != 'themes.php' ) {
		return;
	}
	
  	$catalog = array(
		'width' 	=> '620',	// px
		'height'	=> '700',	// px
		'crop'		=> 1 		// true
	);
	
	$single = array(
		'width' 	=> '800',	// px
		'height'	=> '900',	// px
		'crop'		=> 1 		// true
	);
	
	$thumbnail = array(
		'width' 	=> '150',	// px
		'height'	=> '150',	// px
		'crop'		=> 1 		// false
	);
	
	// Image sizes
	update_option( 'shop_catalog_image_size', $catalog ); 		// Product category thumbs
	update_option( 'shop_single_image_size', $single ); 		// Single product image
	update_option( 'shop_thumbnail_image_size', $thumbnail ); 	// Image gallery thumbs
}

//======================================================================
// Include custom functions
//======================================================================

require_once trailingslashit(get_template_directory()) . 'luvthemes/functions.php';
require_once trailingslashit(get_template_directory()) . 'luvthemes/includes/megamenu.inc.php';
if (!defined('FEVR_WHITELABEL')){
	require_once trailingslashit(get_template_directory()) . 'luvthemes/includes/feature-request.inc.php';
}

//======================================================================
// Enqueue styles and scripts
//======================================================================

add_action( 'wp_enqueue_scripts', 'fevr_scripts' );
/**
 * Enqueue theme scripts and styles for frontend
 */
if (!function_exists('fevr_scripts')):
	function fevr_scripts() {
		global $wp_scripts, $wp_rewrite, $fevr_meta_fields;
		
		// Frontend ajax messages for localization
		$fevr_ajax_messages = array(
				'Add to wishlist'		=> esc_html__('Add to wishlist', 'fevr'),
				'Remove from wishlist'	=> esc_html__('Remove from wishlist', 'fevr'),
				'An error occurred!'	=> esc_html__('An error occurred!', 'fevr'),
				'added to wishlist'		=> esc_html__('added to wishlist', 'fevr'),
				'removed from wishlist'	=> esc_html__('removed from wishlist', 'fevr'),
				'added to cart'			=> esc_html__('added to cart', 'fevr'),
				'Back'					=> esc_html__('Back', 'fevr'),
		);
		
		// Lightbox settings
		$lightbox_settings = array(
				'portfolio'		=> (fevr_check_luvoption('lightbox-portfolio-enabled',1)),
				'woocommerce'	=> (fevr_check_luvoption('lightbox-woocommerce-enabled',1)),
				'photoreview'	=> (fevr_check_luvoption('lightbox-photoreview-enabled',1)),
				'attachment'	=> (fevr_check_luvoption('lightbox-attachment-enabled',1)),
				'gallery'		=> (fevr_check_luvoption('lightbox-gallery-enabled',1)),
				'skin'			=> (fevr_get_luvoption('lightbox-skin', 'dark')),
		);
		
		// Page header title for typewriter effect
		$page_header_title = (isset($fevr_meta_fields['page-header-title']) ? $fevr_meta_fields['page-header-title'] : '');
		
		//Styles
		$inline_dynamic_handler = 'fevr-main-styles';
		wp_enqueue_style( 'fevr-main-styles', trailingslashit(get_template_directory_uri()) . 'css/style.css' );
		wp_deregister_style( 'font-awesome' );
		
		if (fevr_check_luvoption('smart-iconset-enqueue', 1, '!=') || !defined('LUVTHEMES_CORE_VER')){
			wp_enqueue_style( 'font-awesome', trailingslashit(get_template_directory_uri()) . 'css/font-awesome.min.css' );
			wp_enqueue_style( 'ionicons', trailingslashit(get_template_directory_uri()) . 'css/ionicons.min.css' );
			wp_enqueue_style( 'lineaicons', trailingslashit(get_template_directory_uri()) . 'css/linea-icons.css' );
		}
		wp_enqueue_style( 'iLightBox', trailingslashit(get_template_directory_uri()) . 'css/ilightbox.css', array(), 'false', 'defer');
		wp_enqueue_style( 'iLightBox-skin', trailingslashit(get_template_directory_uri()) . 'css/iLightBox/' . $lightbox_settings['skin'] . '-skin/skin.css', array(), 'false', 'defer');
		
		// Promo effect assets
		if (is_page_template('page-templates/page-promo.php')){
			wp_enqueue_style( 'promo', trailingslashit(get_template_directory_uri()) . 'css/promo.css');
			wp_enqueue_script( 'promo', trailingslashit(get_template_directory_uri()) . 'js/min/promo-min.js', array('jquery'), FEVR_THEME_VER, false );
		}
		
		// Conditional Scripts for < Internet Explorer 9
		wp_enqueue_script( 'respond', trailingslashit(get_template_directory_uri()) . 'js/min/respond.min.js', array(), FEVR_THEME_VER, false );
		wp_enqueue_script( 'html5shiv', trailingslashit(get_template_directory_uri()) . 'js/min/html5shiv.min.js', array(), FEVR_THEME_VER, false );
		
		$wp_scripts->add_data("respond", 'conditional', 'lt IE 9');
		$wp_scripts->add_data("html5shiv", 'conditional', 'lt IE 9');
		
		// Common Scripts
		wp_enqueue_script( 'modernizr', trailingslashit(get_template_directory_uri()) . 'js/min/modernizr.min.js', array('jquery'), FEVR_THEME_VER, false );
		wp_enqueue_script( 'mousewheel', trailingslashit(get_template_directory_uri()) . 'js/min/jquery.mousewheel.min.js', array('jquery'), FEVR_THEME_VER, true );
		wp_enqueue_script( 'fitvid', trailingslashit(get_template_directory_uri()) . 'js/min/jquery.fitvids.min.js', array('jquery'), FEVR_THEME_VER, true );
		wp_enqueue_script( 'nice-select', trailingslashit(get_template_directory_uri()) . 'js/min/jquery.nice-select.min.js', array('jquery'), FEVR_THEME_VER, true );

		wp_enqueue_script( 'transit', trailingslashit(get_template_directory_uri()) . 'js/min/jquery.transit.min.js', array('jquery'), FEVR_THEME_VER, true );
		wp_enqueue_script( 'isotope-pkgd', trailingslashit(get_template_directory_uri()) . 'js/min/isotope.pkgd.min.js', array('jquery'), FEVR_THEME_VER, true );
		wp_enqueue_script( 'packer-pkgd', trailingslashit(get_template_directory_uri()) . 'js/min/packery-mode.pkgd.min.js', array('jquery'), FEVR_THEME_VER, true );
		wp_enqueue_script( 'imagesloaded-pkgd', trailingslashit(get_template_directory_uri()) . 'js/min/imagesloaded.pkgd.min.js', array('jquery'), FEVR_THEME_VER, true );
		wp_enqueue_script( 'background-check', trailingslashit(get_template_directory_uri()) . 'js/min/background-check.min.js', array('jquery'), FEVR_THEME_VER, true );
		wp_enqueue_script( 'ram', trailingslashit(get_template_directory_uri()) . 'js/min/jquery.requestAnimationFrame-min.js', array('jquery'), FEVR_THEME_VER, true );
		wp_enqueue_script( 'iLightBox', trailingslashit(get_template_directory_uri()) . 'js/ilightbox.packed.js', array('jquery'), FEVR_THEME_VER, true );
		
		// Enqueue headroom when the sticky header type is "scroll"
		if(fevr_check_luvoption('header-sticky', 1) && fevr_check_luvoption('header-sticky-type', 'header-sticky-on-scroll')) {
			wp_enqueue_script( 'headroom', trailingslashit(get_template_directory_uri()) . 'js/min/headroom.min.js', array('jquery'), FEVR_THEME_VER, true );
			wp_enqueue_script( 'jquery-headroom', trailingslashit(get_template_directory_uri()) . 'js/min/jQuery.headroom-min.js', array('jquery'), FEVR_THEME_VER, true );
		}
		
		// Enqueue Overlay Navigation js only if it is enabled
		if(fevr_check_luvoption('overlay-navigation', 1) && fevr_check_luvoption('overlay-navigation-only-nav', 1)) {
			wp_enqueue_script( 'omenu', trailingslashit(get_template_directory_uri()) . 'js/min/omenu-min.js', array('jquery'), FEVR_THEME_VER, true );
		}
		
		// Enqueue nice scroll js only if it is enabled
		if(fevr_check_luvoption('nice-scroll', 1)) {
			wp_enqueue_script( 'nice-scroll', trailingslashit(get_template_directory_uri()) . 'js/min/jquery.nicescroll.min.js', array('jquery'), FEVR_THEME_VER, true );
		}
				
		// WooCommerce Scripts
		// Magnifier for WooCommerce Single Images
		if(class_exists('woocommerce') && fevr_check_luvoption('woocommerce-magnifier-image', 1)) {
			wp_enqueue_script( 'easyzoom', trailingslashit(get_template_directory_uri()) . 'js/min/easyzoom.min.js', array('jquery'), FEVR_THEME_VER, true );
			
			if((is_woocommerce() || is_checkout() || is_cart() || is_account_page() || fevr_is_luv_shortcode()) ){
				wp_enqueue_script('wc-add-to-cart-variation');
			}
		}
	
		if(class_exists('woocommerce')) {
			$inline_dynamic_handler = 'fevr-woocommerce';
			wp_enqueue_script( 'woocommerce-scripts', trailingslashit(get_template_directory_uri()) . 'js/min/woocommerce-min.js', array('jquery'), FEVR_THEME_VER, true );
			wp_deregister_style( 'woocommerce-general' );
			wp_deregister_style( 'woocommerce-layout' );
			wp_deregister_style( 'woocommerce-smallscreen' );
			wp_enqueue_style( 'fevr-woocommerce', trailingslashit(get_template_directory_uri()) . 'css/partials/woocommerce.css' );
		}
		
		// Portfolio color palette
		if(is_single() && get_post_type() == 'luv_portfolio') {
			wp_enqueue_script( 'color-thief', trailingslashit(get_template_directory_uri()) . 'js/min/color-thief-min.js', array('jquery'), FEVR_THEME_VER, true );
		}
		
		// Midnight JS		
		if(fevr_check_luvoption('automatic-header-skin', 1)) {
			wp_enqueue_script( 'midnight', trailingslashit(get_template_directory_uri()) . 'js/min/midnight-min.js', array('jquery'), FEVR_THEME_VER, true );
		}
		
		// BBPress
		if(class_exists('bbpress')) {
			$inline_dynamic_handler = 'fevr-bbpress';
			wp_deregister_style( 'bbp-default' );
			wp_enqueue_style( 'fevr-bbpress', trailingslashit(get_template_directory_uri()) . 'css/partials/bbpress.css' );
		}
		
		// Theme main scripts
		wp_enqueue_script( 'fevr-init-scripts', trailingslashit(get_template_directory_uri()) . 'js/min/init-min.js', array('jquery'), FEVR_THEME_VER, true );
		if (class_exists('Vc_Manager')){
			// Enqueue Visual Composer extend script 
			wp_enqueue_script( 'fevr-vc-extend', trailingslashit(get_template_directory_uri()) . 'js/vc-extend.js', array('luvthemes-core'), FEVR_THEME_VER, true );
		}
		wp_localize_script('fevr-init-scripts', 'fevr', array('ajax_url' => admin_url('admin-ajax.php'), 'home_url' => home_url(), 'wp_nonce' => wp_create_nonce('_fevr'), 'messages' => $fevr_ajax_messages, 'lightbox' => $lightbox_settings, 'page_header_title' => $page_header_title, 'pagination_base' => $wp_rewrite->pagination_base));
	
		//Enqueue dynamic.css if the file exists
		if (is_writable(trailingslashit(get_template_directory()) . 'css/dynamic.css') && file_exists(trailingslashit(get_template_directory()) . 'css/dynamic.css') && fevr_check_luvoption('inline-css', 1, '!=')) {
			wp_enqueue_style( 'fevr-dynamic', trailingslashit(get_template_directory_uri()) . 'css/dynamic.css' );
		}
		// Or use inline CSS instead
		else{
			wp_add_inline_style($inline_dynamic_handler, get_option('fevr_dynamic_css',''));
		}
	
	}
endif;

add_action( 'admin_enqueue_scripts', 'fevr_admin_scripts');

if (!function_exists('fevr_admin_scripts')):
/**
 * Enqueue scripts and styles for backend 
 */
function fevr_admin_scripts() {
	$fevr_purchase_key = fevr_purchase_key();
	
	// Backend ajax messages for localization
	$luvthemes_admin_ajax_messages = array(
		'Published' =>esc_html__('Published', 'fevr'),
		'Modify' =>esc_html__('Modify', 'fevr'),
		'Upload' =>esc_html__('Upload', 'fevr'),
		'Search option'			=> esc_html__('Search option', 'fevr'),
		'Requires one element.'	=> esc_html__('Requires one element.', 'fevr'),
	);
	
	// Create Tips array
	if (!defined('FEVR_WHITELABEL')){
		if (empty($fevr_purchase_key)){
			$tips = array(
					'activate_copy' => array(
							'selector'	=> '#toplevel_page_fevr-dashboard',
							'content'	=> sprintf(
											'<h3>%s</h3><p>%s<br><br>%s<br>%s<br>%s<br>%s<br></p>',
											esc_html__('Activate your copy', 'fevr'),
											esc_html__('Activate your copy to be able to use some awesome features:', 'fevr'),
											esc_html__('- Layout Templates', 'fevr'),
											esc_html__('- LuvStock (free stock images)', 'fevr'),
											esc_html__('- Image Optimizer', 'fevr'),
											esc_html__('- One-Click Automated Updates', 'fevr')
										),
							'position'	=> 'top',
							'id'		=> 'activate_copy'
					)
			);
		}
		else{
			$tips = array(
				'layout_templates' => array(
					'selector'	=> '#menu-pages',
					'content'	=> sprintf('<h3>%s</h3><p>%s</p>', esc_html__('Premade Layout Templates', 'fevr'), esc_html__('Build a website within 10 minutes! With Layout Templates you can create WordPress pages from premade templates. You can change - or even keep -  the images, customize the content and it\'s done!', 'fevr')),
					'position'	=> 'top',
					'id'		=> 'layout_templates'
				),
				'media_features' => array(
					'selector'	=> '#menu-media',
					'content'	=> sprintf(
							'<h3>%s</h3><p>%s<br><br>%s</p>',
							esc_html__('Media Features', 'fevr'),
							esc_html__('- Use the free, unlimited image optimizer API, and compress loslessly your images to improve your site\'s performance!', 'fevr'),
							esc_html__('- Use LuvStock - free stock images for your projects', 'fevr')
							),
					'position'	=> 'top',
					'id'		=> 'media_features'
				)
			);
		}
	}
	else{
		$tips = array();
	}
	
	// Remove dismissed tips
	$dismissed_tips = (array)get_user_meta(get_current_user_id(), 'fevr_dismissed_tips', true);
	foreach ($tips as $key => $value){
		if (in_array($key, $dismissed_tips)){
			unset($tips[$key]);
		}
	}
	

	wp_enqueue_style( 'fevr-admin', trailingslashit(get_template_directory_uri()) . 'luvthemes/assets/css/admin.css' );
	wp_enqueue_style( 'fevr-filter-preview', trailingslashit(get_template_directory_uri()) . 'luvthemes/assets/css/filter-preview.css' );
	wp_deregister_style( 'font-awesome' );
	wp_enqueue_style( 'font-awesome', trailingslashit(get_template_directory_uri()) . 'css/font-awesome.min.css' );
	wp_enqueue_style( 'ionicons', trailingslashit(get_template_directory_uri()) . 'css/ionicons.min.css' );
	wp_enqueue_style( 'lineaicons', trailingslashit(get_template_directory_uri()) . 'css/linea-icons.css' );
	wp_enqueue_style( 'wp-color-picker' ); 
	wp_enqueue_script( 'isotope-pkgd', trailingslashit(get_template_directory_uri()) . 'js/min/isotope.pkgd.min.js', array('jquery'), FEVR_THEME_VER, true );
	wp_enqueue_script( 'packer-pkgd', trailingslashit(get_template_directory_uri()) . 'js/min/packery-mode.pkgd.min.js', array('jquery'), FEVR_THEME_VER, true );
	wp_enqueue_script( 'imagesloaded-pkgd', trailingslashit(get_template_directory_uri()) . 'js/min/imagesloaded.pkgd.min.js', array('jquery'), FEVR_THEME_VER, true );
	wp_enqueue_script( 'draggabilly-script', trailingslashit(get_template_directory_uri()) . 'luvthemes/assets/js/draggabilly.pkgd.min.js', array(), FEVR_THEME_VER, true );
	wp_enqueue_script( 'fevr-admin-script', trailingslashit(get_template_directory_uri()) . 'luvthemes/assets/js/admin.js', array('jquery', 'wp-color-picker','luvthemes-core'), FEVR_THEME_VER, true );
	wp_localize_script('fevr-admin-script', 'fevr_admin_ajax', array(
			'ajax_url' 		=> admin_url('admin-ajax.php'),
			'upload_page'	=> admin_url('upload.php'),
			'home_url' 		=> trailingslashit(home_url()),
			'wp_nonce' 		=> wp_create_nonce('fevr'),
			'messages' 		=> $luvthemes_admin_ajax_messages,
			'tips'			=> $tips,
			'colors'		=> array_slice(array_unique(array_merge(array_filter(
				array(
					fevr_get_luvoption('accent-color-1'),
					fevr_get_luvoption('accent-color-2'),
					fevr_get_luvoption('additional-color-1'),
					fevr_get_luvoption('additional-color-2'),
					fevr_get_luvoption('additional-color-3'),
				)),
				array(
						'#D89532','#E8E821','#81D742','#1A65A7','#8224E3','#FFFFFF'
				))
			),0,8)
	));
	wp_enqueue_media();
	
	// Redux search
	$current_screen = get_current_screen();
	if (is_object($current_screen) && $current_screen->id == strtolower(FEVR_THEME_NAME) . '_page_theme-options'){
		wp_enqueue_script( 'fevr-redux-extend', trailingslashit(get_template_directory_uri()) . 'luvthemes/assets/js/redux-extend.js', array('jquery'), FEVR_THEME_VER, true );
		wp_localize_script('fevr-redux-extend', 'fevr_search_object', Redux::$fields);
		
	}
	
	// GA dashboard widget
	if (fevr_check_luvoption('ga-dashboard-widget', 1) && fevr_check_luvoption('ga-cid', '', '!=') && fevr_check_luvoption('ga-vid', '', '!=')){
		wp_enqueue_script( 'fevr-ga-widget', trailingslashit(get_template_directory_uri()) . 'luvthemes/includes/ga-widget/js/ga-widget.js', array('jquery'), FEVR_THEME_VER, true );
		wp_localize_script('fevr-ga-widget', 'fevr_ga_widget', array(
				'clientid' => fevr_get_luvoption('ga-cid'),
				'vid' => fevr_get_luvoption('ga-vid'),
		));
	}
	
	// Tour tooltips
	wp_enqueue_style('wp-pointer');
	wp_enqueue_script('wp-pointer');
	
	if (defined('FEVR_WHITELABEL')){
		wp_enqueue_style( 'fevr-whitelabel', trailingslashit(get_template_directory_uri()) . 'luvthemes/assets/css/whitelabel.css' );
	}
}
endif;

// Enqueue comments-reply script
add_action( 'comment_form_before', 'fevr_enqueue_comments_reply' );

/**
 * Enqueue comments reply
 */
function fevr_enqueue_comments_reply() {
	if( get_option( 'thread_comments' ) )  {
		wp_enqueue_script( 'comment-reply' );
	}
}

//======================================================================
// Theme setup
//======================================================================


add_action( 'after_setup_theme', 'fevr_setup' );
/**
 * Theme setup
 * Add theme supports:
 * 		- post formats
 * 		- auto feed links
 * 		- html5
 * 		- woocommerce
 * 		- post thumbnails
 * 		- title-tag
 * 
 */
function fevr_setup(){
	// Post formats
	add_theme_support( 'post-formats', array('gallery', 'link', 'image', 'quote', 'video', 'audio', 'status') );
	
	// Theme supports
	add_theme_support( 'automatic-feed-links' );
	
	// HTML5
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form', 
		'comment-list',
		'gallery',
		'caption',
	) );
	
	// WooCommerce
	add_theme_support( 'woocommerce' );
	
	
	// Custom Image Sizes from Theme Options
	global $fevr_options;
	$custom_image_size = fevr_check_luvoption('custom-image-sizes', 1);
	// Image sizes
	add_theme_support( 'post-thumbnails' );

	// Lazy load placeholders
	add_image_size( 'fevr_lazyload', 10, 10, false );
	
	$general_w = ($custom_image_size && isset($fevr_options['custom-image-sizes-general']['width']) && !empty($fevr_options['custom-image-sizes-general']['width']) ? $fevr_options['custom-image-sizes-general']['width'] : 1000);
	$general_h = ($custom_image_size && isset($fevr_options['custom-image-sizes-general']['height']) && !empty($fevr_options['custom-image-sizes-general']['height']) ? $fevr_options['custom-image-sizes-general']['height'] : 620);
	add_image_size( 'fevr_post_thumb', $general_w, $general_h, true );
	
	$normal_w = ($custom_image_size && isset($fevr_options['custom-image-sizes-normal']['width']) && !empty($fevr_options['custom-image-sizes-normal']['width']) ? $fevr_options['custom-image-sizes-normal']['width'] : 500);
	$normal_h = ($custom_image_size && isset($fevr_options['custom-image-sizes-normal']['height']) && !empty($fevr_options['custom-image-sizes-normal']['height']) ? $fevr_options['custom-image-sizes-normal']['height'] : 500);
	add_image_size( 'fevr_normal', $normal_w, $normal_h, true );
	
	$wide_w = ($custom_image_size && isset($fevr_options['custom-image-sizes-wide']['width']) && !empty($fevr_options['custom-image-sizes-wide']['width']) ? $fevr_options['custom-image-sizes-wide']['width'] : 1000);
	$wide_h = ($custom_image_size && isset($fevr_options['custom-image-sizes-wide']['height']) && !empty($fevr_options['custom-image-sizes-wide']['height']) ? $fevr_options['custom-image-sizes-wide']['height'] : 500);
	add_image_size( 'fevr_wide', $wide_w, $wide_h, true );
	
	$tall_w = ($custom_image_size && isset($fevr_options['custom-image-sizes-tall']['width']) && !empty($fevr_options['custom-image-sizes-tall']['width']) ? $fevr_options['custom-image-sizes-tall']['width'] : 500);
	$tall_h = ($custom_image_size && isset($fevr_options['custom-image-sizes-tall']['height']) && !empty($fevr_options['custom-image-sizes-tall']['height']) ? $fevr_options['custom-image-sizes-tall']['height'] : 1000);
	add_image_size( 'fevr_tall', $tall_w, $tall_h, true );
	
	$wide_tall_w = ($custom_image_size && isset($fevr_options['custom-image-sizes-wide-tall']['width']) && !empty($fevr_options['custom-image-sizes-wide-tall']['width']) ? $fevr_options['custom-image-sizes-wide-tall']['width'] : 1000);
	$wide_tall_h = ($custom_image_size && isset($fevr_options['custom-image-sizes-wide-tall']['height']) && !empty($fevr_options['custom-image-sizes-wide-tall']['height']) ? $fevr_options['custom-image-sizes-wide-tall']['height'] : 1000);
	add_image_size( 'fevr_wide_tall', $wide_tall_w, $wide_tall_h, true );
	
	$full_w = ($custom_image_size && isset($fevr_options['custom-image-sizes-full']['width']) && !empty($fevr_options['custom-image-sizes-full']['width']) ? $fevr_options['custom-image-sizes-full']['width'] : 2000);
	add_image_size( 'fevr_full_size', $full_w );
	
	$featured_w = ($custom_image_size && isset($fevr_options['custom-image-sizes-featured']['width']) && !empty($fevr_options['custom-image-sizes-featured']['width']) ? $fevr_options['custom-image-sizes-featured']['width'] : 2000);
	$featured_h = ($custom_image_size && isset($fevr_options['custom-image-sizes-featured']['height']) && !empty($fevr_options['custom-image-sizes-featured']['height']) ? $fevr_options['custom-image-sizes-featured']['height'] : 800);
	add_image_size( 'fevr_featured_img', $featured_w, $featured_h, true );
	
	$reviews_w = 375;
	$reviews_h = 500;
	add_image_size( 'fevr_reviews_img', $reviews_w, $reviews_h, true );

	// Title tag
	add_theme_support( 'title-tag' ); 
	
}

add_filter('image_size_names_choose', 'fevr_image_sizes');
/**
 * Add theme's image sizes to media editor
 * @param array $sizes
 * @return $sizes
 */
function fevr_image_sizes($sizes) {
	$addsizes = array(
			"fevr_normal" => esc_html__( "Normal", 'fevr'),
			"fevr_wide" => esc_html__( "Wide", 'fevr'),
			"fevr_tall" => esc_html__( "Tall", 'fevr'),
	);
	return array_merge((array)$sizes, $addsizes);
}

//======================================================================
// Custom menu
//======================================================================

add_action('init', 'fevr_register_nav_menus');

/**
 * Regsister theme navigation menus
 */
function fevr_register_nav_menus(){
	$menus = array(
		'theme_main_navigation' => esc_html__('Primary Navigation','fevr'),
		'theme_top_navigation' => esc_html__('Top Navigation','fevr'),
		'theme_main_mobile_navigation' => esc_html__('Primary Navigation for Mobile (optional)','fevr'),
		'mobile_quick_navigation' => esc_html__('Mobile Quick Links (Search)','fevr'),
		'woocommerce_headless' => esc_html__('WooCommerce Facebook Tab','fevr'),
	);
	
	if (fevr_check_luvoption('header-layout', 'centered-logo')){
		$menus['theme_main_right_navigation'] = esc_html__('Primary Right Navigation', 'fevr');
	}
	
	if (fevr_check_luvoption('enable-mobile-app', 1)){
		$menus['mobile_app'] = sprintf(esc_html__('%s Mobile App','fevr'), FEVR_THEME_NAME);
	}
	
	register_nav_menus($menus);
}

//======================================================================
// Register Widget Areas
//======================================================================

add_action( 'widgets_init', 'fevr_widgets_init' );

if (!function_exists('fevr_widgets_init')):
/**
 * Initialize theme's widget areas 
 */
function fevr_widgets_init() {	
	//======================================================================
	// Slide Out Widget Area
	//======================================================================
	
	register_sidebar( array(
		'name'          => esc_html__('Off-Canvas Menu', 'fevr'),
		'id'            => 'off-canvas-menu-widgets',
		'before_widget' => '<div id="%1$s" class="off-canvas-menu-widget widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	
	//======================================================================
	// Overlay Navigation Widget Area
	//======================================================================
	$overlay_navigation_columns = fevr_get_luvoption('overlay-navigation-columns');
	if (!empty($overlay_navigation_columns)){
		if($overlay_navigation_columns > 1) {
			register_sidebars( $overlay_navigation_columns, array(
				'name'          => esc_html__('Overlay Navigation Widget Area %d', 'fevr'),
				'id'            => 'overlay-nav-widgets',
				'before_widget' => '<div id="%1$s" class="overlay-nav-widget widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4 class="widget-title">',
				'after_title'   => '</h4>',
			) );
		} else {
			register_sidebar( array(
				'name'          => esc_html__('Overlay Navigation Widget Area 1', 'fevr'),
				'id'            => 'overlay-nav-widgets',
				'before_widget' => '<div id="%1$s" class="overlay-nav-widget widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4 class="widget-title">',
				'after_title'   => '</h4>',
			) );
		}
	}
	
	//======================================================================
	// Blog Widget Area
	//======================================================================
	
	register_sidebar( array(
		'name'          => esc_html__('Blog Sidebar', 'fevr'),
		'id'            => 'blog-sidebar',
		'before_widget' => '<div id="%1$s" class="blog-widget widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	
	//======================================================================
	// Page Widget Area
	//======================================================================
	
	register_sidebar( array(
		'name'          => esc_html__('Page Sidebar', 'fevr'),
		'id'            => 'page-sidebar',
		'before_widget' => '<div id="%1$s" class="blog-widget widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	
	//======================================================================
	// Footer Widget Area
	//======================================================================
	
	$footer_widget_columns = fevr_get_luvoption('footer-widget-columns');
	if (!empty($footer_widget_columns)){
		if($footer_widget_columns > 1) {
			register_sidebars( $footer_widget_columns, array(
				'name'          => esc_html__('Footer Widget Area %d', 'fevr'),
				'id'            => 'footer-widgets',
				'before_widget' => '<div id="%1$s" class="footer-widget widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4 class="widget-title">',
				'after_title'   => '</h4>',
			) );
		} else {
			register_sidebar( array(
				'name'          => esc_html__('Footer Widget Area 1', 'fevr'),
				'id'            => 'footer-widgets',
				'before_widget' => '<div id="%1$s" class="footer-widget widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4 class="widget-title">',
				'after_title'   => '</h4>',
			) );
		}
	}
	
	//======================================================================
	// Custom Widget Area
	//======================================================================
	
	$custom_widgets = fevr_get_luvoption('custom-widgets');
	if (!empty($custom_widgets)){
		foreach ((array)$custom_widgets as $widget){
			if (!empty($widget)){
				register_sidebar( array(
					'name'          => $widget,
					'id'            => sanitize_title($widget),
					'before_widget' => '<div id="%1$s" class="custom-widget widget %2$s">',
					'after_widget'  => '</div>',
					'before_title'  => '<h4 class="widget-title">',
					'after_title'   => '</h4>',
				));
			}
		}
	}

}
endif;

//======================================================================
// WooCommerce
//======================================================================

// Change order of divs on the cart page
add_action( 'woocommerce_after_cart_table', 'fevr_reorder_cart_collaterals' );

function fevr_reorder_cart_collaterals() {
	remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
	add_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display', 11 );
}

// Set columns
add_filter('loop_shop_columns', 'fevr_wc_loop_columns');

if (!function_exists('fevr_wc_loop_columns')) {	
	$fevr_woocommerce_columns = fevr_get_luvoption('woocommerce-columns','three-columns');
	
	switch($fevr_woocommerce_columns) {
		case 'two-columns':
			$fevr_wc_columns = 2;
			
			break;
		
		case 'three-columns':
			$fevr_wc_columns = 3;
			
			break;
		
		case 'four-columns':
			$fevr_wc_columns = 4;
			
			break;
			
	}
	
	function fevr_wc_loop_columns() {
		global $fevr_wc_columns;
		
		return $fevr_wc_columns;
	}
}


// Related products

add_filter( 'woocommerce_output_related_products_args', 'fevr_wc_related_products_args' );

/**
 * Override related product columns and post per page arguments
 * @param array $args
 * @return array
 */
function fevr_wc_related_products_args( $args ) {
	global $fevr_wc_columns;
	
	$args['posts_per_page'] = $fevr_wc_columns*2;
	$args['columns'] = 1;
	return $args;
}

// Posts per page for woocommerce

add_filter( 'loop_shop_per_page', 'fevr_wc_posts_per_page', 20 );

/**
 * Override post per page for WooCommerce
 * @param int $cols
 * @return int
 */
function fevr_wc_posts_per_page($cols) {
	$woocommerce_posts_per_page = (int)fevr_get_luvoption('woocommerce-posts-per-page');
	if($woocommerce_posts_per_page > 0) {
		return $woocommerce_posts_per_page;
	}
	
	return $cols;
}

//======================================================================
// Custom Column Fields
//======================================================================

// Portfolio

// Add extra column for snippet post type
add_filter( 'manage_luv_portfolio_posts_columns' , 'fevr_add_portfolio_columns' );

/**
 * Add Shortcode Column for Portfolio custom post type
 * @param array $columns
 * @return array
 */
function fevr_add_portfolio_columns( $columns ) {
	return array(
			'cb' => '<input type="checkbox" />',
			'featured-image' => esc_html__('Featured image', 'fevr'),
			'title' => esc_html__('Title', 'fevr'),
			'taxonomy-luv_portfolio_categories' => esc_html__( 'Portfolio Categories', 'fevr' ),
			'taxonomy-luv_portfolio_tags' => esc_html__( 'Portfolio Tags', 'fevr' ),
			'date' => esc_html__('Date', 'fevr'),
	);
}

// Add shortcode field for snippets
add_action('manage_luv_portfolio_posts_custom_column', 'fevr_custom_portfolio_columns', 10, 2);

/**
 * Display Featured image Column for Portfolio custom post type
 * @param string $column
 * @param int $post_id
 */
function fevr_custom_portfolio_columns( $column, $post_id ) {
	if($column = 'featured-image') {
		global $post;
		echo get_the_post_thumbnail($post, 'thumbnail');
	}
}


// Snippets

// Add extra column for snippet post type
add_filter( 'manage_luv_snippets_posts_columns' , 'fevr_add_snippets_shortcode_columns' );

/**
 * Add Shortcode Column for Snippets custom post type
 * @param array $columns
 * @return array
 */
function fevr_add_snippets_shortcode_columns( $columns ) {
    return array(
    	'cb' => '<input type="checkbox" />',
		'title' => esc_html__('Title', 'fevr'),
		'luv_snippets_shortcode' => esc_html__( 'Shortcode', 'fevr' ),
		'date' => esc_html__('Date', 'fevr'),
	);
}

// Add shortcode field for snippets
add_action('manage_luv_snippets_posts_custom_column', 'fevr_custom_snippets_columns', 10, 2);

/**
 * Display Shortcode Column for Snippets custom post type
 * @param string $column
 * @param int $post_id
 */
function fevr_custom_snippets_columns( $column, $post_id ) {
	if($column = 'luv_snippets_shortcode') {
		global $post;
		echo '<span class="luv-custom-post-column">[luv_snippet id="'.$post->post_name.'"]</span>';
	}
}

// Sliders

// Add extra column for slider post type
add_filter( 'manage_luv_slider_posts_columns' , 'fevr_add_slider_shortcode_columns' );

/**
 * Add Shortcode Column for Slider custom post type
 * @param array $columns
 * @return array
 */
function fevr_add_slider_shortcode_columns( $columns ) {
    return array(
    	'cb' => '<input type="checkbox" />',
		'title' => esc_html__('Title', 'fevr'),
		'luv_slider_shortcode' => esc_html__( 'Shortcode', 'fevr' ),
		'date' => esc_html__('Date', 'fevr'),
	);
}

// Add shortcode field for slider
add_action('manage_luv_slider_posts_custom_column', 'fevr_custom_slider_columns', 10, 2);

/**
 * Display Shortcode Column for Slider custom post type
 * @param string $column
 * @param int $post_id
 */
function fevr_custom_slider_columns( $column, $post_id ) {
	if($column = 'luv_slider_shortcode') {
		global $post;
		echo '<span class="luv-custom-post-column">[luv_slider id="'.$post->post_name.'"]</span>';
	}
}

//======================================================================
// Photo Reviews
//======================================================================

// Ajax function for publish photo reviews
add_action('wp_ajax_fevr_accept_photo_review','fevr_ajax_accept_photo_review');
/**
 * Accept Photo Review
 */
function fevr_ajax_accept_photo_review() {
	// Check if our nonce is set.
	if (!wp_verify_nonce( $_POST['wp_nonce'], 'fevr' )) {
		return;
	}
	
	// Check permissions
	if ( ! current_user_can( 'edit_post', $_POST['photo_review_id'] ) ) {
		return;
	}
	
	$review = array(
		'ID'			=> (int)$_POST['photo_review_id'],
		'post_status'	=> 'publish'
	);
	
	// Finalize review
	wp_update_post($review);	
}

// Add extra column for quick publish button for photo reviews
add_filter( 'manage_luv_ext_reviews_posts_columns' , 'fevr_add_ext_reviews_columns' );

/**
 * Add Action Column for Photo Reviews custom post type
 * @param array $columns
 */
function fevr_add_ext_reviews_columns( $columns ) {
	$text = (fevr_check_luvoption('woocommerce-photo-reviews-coupon', '', '!=') ? esc_html__( 'Publish & Send Coupon', 'fevr' ) : esc_html__( 'Publish', 'fevr' ));
	return array(
			'cb' => '<input type="checkbox" />',
			'title' => esc_html__('Title', 'fevr'),
			'luv_ext_reviews_button' => $text,
			'date' => esc_html__('Date', 'fevr'),	
	);
}

// Add quick publish button for photo reviews
add_action('manage_luv_ext_reviews_posts_custom_column', 'fevr_custom_ext_reviews_columns', 10, 2);

/**
 * Display Action Column for Photo Reviews custom post type
 * @param string $column
 * @param int $post_id
 */
function fevr_custom_ext_reviews_columns( $column, $post_id ) {	
	$btn_text = (fevr_check_luvoption('woocommerce-photo-reviews-coupon', '', '!=') ? esc_html__( 'Approve & Send Coupon', 'fevr' ) : esc_html__( 'Approve', 'fevr' ));
	
	if($column = 'luv_ext_reviews_button') {
		global $post;
		if (!in_array($post->post_status, array('publish','draft','trash'))){
			echo '<a href="#" class="luv-btn luv-btn-blue ext-reviews-quick-publish" data-id="'.esc_attr($post_id).'"><span>' . esc_html($btn_text) . '</span><i class="dashicons dashicons-yes"></i></a>';
		}
		else if ($post->post_status == 'publish'){
			echo '<div class="luv-btn luv-btn-green on-hover"><span>' . esc_html__( 'Approved', 'fevr' ) . '</span><i class="dashicons dashicons-yes"></i></a>';
		}
	}
}

//======================================================================
// Dashboard
//======================================================================

// Add dashboard or admin menu
add_action('admin_menu', 'fevr_dashboard_menu');
add_action('network_admin_menu', 'fevr_dashboard_menu');

/**
 * Add theme's menu to WordPress admin menu
 */
function fevr_dashboard_menu(){
	fevr_add_fevr_menu_page( FEVR_THEME_NAME, FEVR_THEME_NAME, 'manage_options', 'fevr-dashboard', 'fevr_dashboard',  FEVR_DASHBOARD_ICON_URL, '4.1');
	fevr_add_fevr_submenu_page('fevr-dashboard', 'Dashboard', 'Dashboard', 'manage_options', 'fevr-dashboard');
}

/**
 * Display theme's dashboard
 */
function fevr_dashboard(){
	//Save purchase code
	if (isset($_REQUEST['luv-save-settings']) && $_POST['luv-save-settings'] == 'purchase-key'){
		//Verify nonce
		if (!isset($_REQUEST['luv-nonce']) || !wp_verify_nonce($_REQUEST['luv-nonce'], 'fevr-save-settings') || !current_user_can('manage_options')){
			return;
		}
		
		//Verify purchase key via Luv API
		$response = wp_remote_post ( FEVR_API_URL . 'validate/', array (
				'timeout' => 60,
				'sslverify' => false,
				'user-agent' => 'luv',
				'headers' => array (
						'X-ENVATO-PURCHASE-KEY' => trim ( $_POST['envato-purchase-key'] )
				),
				'body' => array (
						'site' => home_url ()
				)
		) );
		
		//Handle HTTP errors
		if (is_wp_error($response)) {
			fevr_print_admin_notice(array('class' => 'error', 'message' => esc_html__('Couldn\'t connect to server, please try again.', 'fevr')));
		}
		else{
			$response = json_decode($response['body'],true);
			//API error
			if ($response['error'] === true){
				fevr_print_admin_notice(array('class' => 'error', 'message' => esc_html__('API error: ', 'fevr') . $response['response']));
			}
			else if ($response === null){
				fevr_print_admin_notice(array('class' => 'error', 'message' => esc_html__('Unknown error, please try again.', 'fevr')));
			}
			//Success
			else{
				global $fevr_purchase_key;
				$fevr_purchase_key = $_POST['envato-purchase-key'];
				if (defined('MULTISITE') && MULTISITE === true){
					if (is_super_admin()){
						update_site_option('fevr_purchase_key', trim($fevr_purchase_key));
					}
				}
				else{
					update_option('fevr_purchase_key', trim($fevr_purchase_key));
				}
				fevr_print_admin_notice(array('class' => 'success', 'message' => esc_html__('You have successfully activated your copy!', 'fevr')));
			}
		}
		
	}
	else if (isset($_REQUEST['generate-fevr-mobile']) && $_POST['generate-fevr-mobile'] == 'generate'){
		//Verify nonce
		if (!isset($_REQUEST['luv-nonce']) || !wp_verify_nonce($_REQUEST['luv-nonce'], 'fevr-generate-mobile-app') || !current_user_can('manage_options')){
			return;
		}
		
		$data = array(
				'icon' => $_POST['icon'],
				'author_email' => $_POST['author_email'],
				'author_web' => $_POST['author_web'],
				'app_version' => $_POST['app_version'],
				'app_name' => $_POST['app_name'],
				'app_description' => $_POST['app_description'],
				'accent_color'	=> fevr_get_luvoption('accent-color-1')
		);
		
		$response = wp_remote_post ( FEVR_API_URL . 'build_mobile_app/', array (
				'timeout' => 60,
				'sslverify' => false,
				'user-agent' => 'luv',
				'headers' => array (
						'X-ENVATO-PURCHASE-KEY' => fevr_purchase_key()
				),
				'body' => array (
						'site' => home_url (),
						'args' => $data
				)
		) );
		
		//Handle HTTP errors
		if (is_wp_error($response)) {
			fevr_print_admin_notice(array('class' => 'error', 'message' => esc_html__('Couldn\'t connect to server, please try again.', 'fevr')));
		}
		
	}

	include_once trailingslashit(get_template_directory()) . 'luvthemes/includes/dashboard.inc.php';
}

//======================================================================
// Admin notices
//======================================================================

// Add theme's admin notices
add_action('admin_notices', 'fevr_admin_notices');

/**
 * Iterate throug the admin notices and display them
 */
function fevr_admin_notices(){
	// Run time notices
	global $fevr_admin_notices;
	foreach ((array)$fevr_admin_notices as $notice){
		fevr_print_admin_notice($notice);
	}
	
	// Updates
	$update_info = get_option('fevr_update', array());
	if (!isset($_GET['luv-updater']) && !empty($update_info) && !isset($update_info['dismiss']) && current_user_can('update_themes')){
		fevr_print_admin_notice(array(
			'class'		=> 'info',
			'message'	=> FEVR_THEME_NAME . ' ' . $update_info['version'] . esc_html__(' is available! Please update now.', 'fevr'),
			'dismiss'	=> 'updater'
		));
	}
	
	// Photo reviews
	if (fevr_check_luvoption('woocommerce-photo-reviews', 1) && current_user_can('manage_woocommerce')){
		$pending_reviews = get_option('_fevr_pending_photo_reviews', 0);
		if ((int)$pending_reviews > 0){
			fevr_print_admin_notice(array(
				'class'		=> 'info',
				'message'	=> ($pending_reviews == 1 ? esc_html__('One photo review is awaiting approval.', 'fevr') : $pending_reviews. esc_html__(' photo reviews are awaiting approval.', 'fevr')) . ' <a href="'.esc_url(admin_url('edit.php?post_type=luv_ext_reviews')).'">'.esc_html__('Go to photo reviews', 'fevr').'</a>',
				'dismiss'	=> 'photo_reviews'
			));
		}
	}
}

/**
 * Print the admin notice
 * @param array $notice
 */
function fevr_print_admin_notice($notice){ 
	$dismiss = isset($notice['dismiss']) ? '<a href="#" class="luv-admin-notice-dismiss" data-dismiss="'.esc_attr($notice['dismiss']).'"></a>' : ''; 
	echo '<div class="luv-admin-notice luv-admin-notice-'.esc_attr($notice['class']).'">' . fevr_kses($notice['message']) . $dismiss .'</div><br>';
}

// Create ajax hook
add_action('wp_ajax_fevr_dismiss_notice', 'fevr_dismiss_notice');

/**
 * Dismiss dismissable admin notices
 */
function fevr_dismiss_notice(){
	// Check if our nonce is set.
	if ( ! wp_verify_nonce( $_POST['wp_nonce'], 'fevr' ) || !current_user_can('manage_options')) {
		return;
	}
	
	// Dissmiss updates
	if ($_POST['dismiss'] == 'updater'){
		$update_info = get_option('fevr_update', array());
		$update_info['dismiss'] = 1;
		update_option('fevr_update', $update_info);
	}
	// Photo reviews
	else if ($_POST['dismiss'] == 'photo_reviews'){
		update_option('_fevr_pending_photo_reviews', 0);
	}
	
	wp_die();
}

// Create ajax hook
add_action('wp_ajax_fevr_dismiss_tip', 'fevr_dismiss_tip');

/**
 * Dismiss tip
 */
function fevr_dismiss_tip(){
	// Check if our nonce is set.
	if ( ! wp_verify_nonce( $_POST['wp_nonce'], 'fevr' )) {
		return;
	}

	// Get dismissed tips
	$dismissed_tips = array_filter((array)get_user_meta(get_current_user_id(), 'fevr_dismissed_tips', true));
	
	// Dissmiss tip
	$dismissed_tips[$_POST['id']] = $_POST['id'];
	
	// Update dismissed tips
	update_user_meta(get_current_user_id(), 'fevr_dismissed_tips', $dismissed_tips);
	
	wp_die();
}

// Default content width
if (!isset($content_width)){
	global $content_width;
	$content_width = apply_filters('content_width',1080);
}

// Add bloginfo to title
add_filter('wp_title', 'fevr_wp_title', 10, 2);

/**
 * Generate WP Title 
 * @param string $title
 * @param string $sep
 */
 
function fevr_wp_title( $title, $sep ) {
	global $paged, $page;
	
	if ( is_feed() ) {
		return $title;
	}
	
	$title .= get_bloginfo( 'name' );
	
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}
	
	if ( $paged >= 2 || $page >= 2 ) {
		$title = sprintf( esc_html__( 'Page %s', 'fevr' ), max( $paged, $page ) ) . " $sep $title";
	} 
	return $title;
} 

add_action( 'after_switch_theme', 'fevr_redirect_dashboard', 1 );
/**
 * Redirect to theme setup after switch theme
 */
function fevr_redirect_dashboard(){
	if (!defined('FEVR_WHITELABEL')){
		wp_redirect(admin_url('admin.php?page=fevr_setup'),302);
	}
}

/**
 *  Backward Compatibility for title_tag
 */
function fevr_theme_slug_render_title() {
	ob_start();
	wp_title( '|', true, 'right' );
	$wp_title = ob_get_clean();
	
	echo sprintf('<%s>'.esc_html($wp_title).'</%s>','title','title');
}

if ( ! function_exists( '_wp_render_title_tag' ) ){
	add_action( 'wp_head', 'fevr_theme_slug_render_title' );
}

/**
 *  Backward Compatibility for inline styles
 */
if (!function_exists('wp_add_inline_style')):
	function wp_add_inline_style($handler, $style){
		echo '<style>' . esc_html($style) . '</style>';
	}
endif;
//======================================================================
// Custom Password Protected Form
//======================================================================

function fevr_password_form() {
    global $post;
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
    $o = '<form class="pw-protected" action="' . esc_url( add_query_arg('action', 'postpass', wp_login_url()) ) . '" method="post"><p>
    ' . esc_html__( 'To view this protected post, enter the password below:', 'fevr' ) . '</p>
    <label for="' . esc_attr($label) . '">' . esc_html__( 'Password:', 'fevr') . ' </label><input name="post_password" id="' . esc_attr($label) . '" type="password" size="20" maxlength="20"><input type="submit" class="btn btn-global btn-full btn-accent-2" name="Submit" value="' . esc_html__( 'Submit', 'fevr' ) . '">
    </form>
    ';
    return $o;
}
add_filter( 'the_password_form', 'fevr_password_form' );

?>